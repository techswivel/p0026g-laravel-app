    activeArtistTable = $("#example").DataTable({
        dom: "<'row  pt-1 pb-3'<'col-lg-3 col-md-12 pl-2 pr-2'<'value'>><'col-lg-9 col-md-12 pl-2 pr-2'<'card border-radius8'<'card-header topBorderRadius pb-0'<'artistText'>il><'card-body border-radius8 table-responsive p-0'<rt>>>p>",
        initComplete: function (settings, json) {},
    });

    $("div.value").html(
        `<div class="card border-radius8">
                <div class="card-header topBorderRadius">
                    <p class="mb-0" style="font-weight:200" >Add New Artist</p>
                </div>
                <div class="card-body border-radius8 " style="padding:12px">
                    <form id="createArtistForm" >
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px "><b>First Name</b></span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter First Name" style="font-size: 13.5px" id="artistFirstName" name="artistFirstName" >
                            <div class="invalid-feedback" id="artistFirstNameError"></div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px ">Last Name</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Last Name" style="font-size: 13.5px" id="artistLastName" name="artistLastName" >
                            <div class="invalid-feedback" id="artistLastNameError"></div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px ">Email</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Email" style="font-size: 13.5px" id="email" name="email" >
                            <div class="invalid-feedback" id="emailError"></div>
                        </div>
                        <label class="m-0 mt-1"><span class="text-dark add-song-label-text" style="font-size: 12px ">Password</span></label>
                        <div class="input-group mt-1 mb-3 input-group-sm round-7" id="show_hide_password">
                            <input type="password" class="form-control input-sm round-7 password-radius" style="font-size: 13px" placeholder="Set Password" id="artistPassword" name="artistPassword">
                            <div class="input-group-append rounded" style="height: 29.5px">
                                <div class="input-group-text eye-icon pt-2" id="artistPassword-eye" style=""><a href="#">
                                    <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
                                </div>
                            </div>
                            <div class="invalid-feedback" id="artistPasswordError"></div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"><span class="text-dark add-song-label-text" style="font-size: 12px ">Image</span></label>
                            <input type="file" class="form-control-file input-sm "   placeholder="Choose Tumbnail (Max 1MB)" style="font-size: 12.5px" id="artistAvatar" name="artistAvatar" >
                            <small class="add-song-span-text mt-1" style="font-size: 9px">Max. Size 2MB ( Only Jpg, Png, Jpeg )</small>
                            <div class="invalid-feedback" id="artistAvatarError"></div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0  mb-1"><span class="text-dark add-song-label-text" style="font-size: 12px ">About Artist</span></label>
                            <textarea rows="3" cols="50" name="artistBio"  id="artistBio"   class="form-control add-song-input-text input-xs rounded border-radius8"  placeholder="Enter Bio.."></textarea>
                            <div class="invalid-feedback" id="artistBioError"></div>
                        </div>
                        <div class="pt-2 mb-2 mt-1 form-group input-group-sm input_field " >
                            <input type="submit" id="fd" class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-4 pr-4 border-radius8 account-Create-button" style="font-size: 12px" value="Add Artist">
                            <span class="clearText">Clear all</span>
                        </div>
                    </form>
                </div>
            </div>`
    );

    $("div.artistText").html(
        '<span class="pr-2 all-noti-text"style="font-weight:400;float:left">Artist List</span>'
    );

    $("#activeArtistSearch").keyup(function () {
        activeArtistTable.search(this.value).draw();
    });
    $("#show_hide_password a").on("click", function (event) {
        event.preventDefault();
        if ($("#show_hide_password input").attr("type") == "text") {
            $("#show_hide_password input").attr("type", "password");
            $("#show_hide_password span").addClass("fa-eye-slash");
            $("#show_hide_password span").removeClass("fa-eye");
        } else if ($("#show_hide_password input").attr("type") == "password") {
            $("#show_hide_password input").attr("type", "text");
            $("#show_hide_password span").removeClass("fa-eye-slash");
            $("#show_hide_password span").addClass("fa-eye");
        }
    });

    $("#show_hide_editPassword a").on("click", function (event) {
        event.preventDefault();
        if ($("#show_hide_editPassword input").attr("type") == "text") {
            $("#show_hide_editPassword input").attr("type", "password");
            $("#show_hide_editPassword span").addClass("fa-eye-slash");
            $("#show_hide_editPassword span").removeClass("fa-eye");
        } else if ($("#show_hide_editPassword input").attr("type") == "password") {
            $("#show_hide_editPassword input").attr("type", "text");
            $("#show_hide_editPassword span").removeClass("fa-eye-slash");
            $("#show_hide_editPassword span").addClass("fa-eye");
        }
    });

function showErrorMessage(error, message) {
        if (error) {
            $("#" + message + "Error").show();
            $("#" + message + "Error").html(error);
        }
}

function hideErrorMessage(message) {
    $("#" + message + "Error").hide();
}

// Create artist
$("#createArtistForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#createArtistForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/admin/active/artists/create",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 400) {
                var errors = [
                    "artistFirstName",
                    "artistLastName",
                    "email",
                    "artistPassword",
                    "artistBio",
                    "artistAvatar",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            }else{
                location.reload();
            }
        },
    });
});



$(".clearText").click(function(){
    $("#createArtistForm")[0].reset();
    var errors = [
        "artistFirstName",
        "artistLastName",
        "email",
        "artistPassword",
        "artistBio",
        "artistAvatar",
    ];
    for (var i = 0; i < errors.length; i++) {
        hideErrorMessage(errors[i]);
    }
  });


function  artistDetail(id){
    $("#editArtistForm")[0].reset();
    var errors = [
        "editArtistFirstName",
        "editArtistLastName",
        "editArtistEmail",
        "editArtistPassword",
        "editArtistBio",
        "editArtistAvatar",
    ];
    for (var i = 0; i < errors.length; i++) {
        hideErrorMessage(errors[i]);
    }
    $.LoadingOverlay('show');
    $.get("/admin/active/artists/show/" + id, function (response) {
        if (response.status == 500) {
            toastr.error(response.message);
        } else {
            $("#artistId").val(id);
            $("#editArtistFirstName").val(response.artist.firstName);
            $("#editArtistLastName").val(response.artist.lastName);
            $("#editArtistBio").val(response.artist.aboutArtist);
            $("#editArtistEmail").val(response.artist.email);
            if(response.artist.avatar){
                $("#editArtistAvatarDisplay").css(
                    "background-image",
                    "url(" + thumbnailUrl(response.artist.avatar) + ")"
                );
            }else{
                $("#editArtistAvatarDisplay").css(
                    "background-image",
                    "url(../image/avatar/default-avatar.png)"
                );
            }
            $("#editArtistModal").modal("toggle");
            $.LoadingOverlay('hide');
        }
    });
}


$("#editArtistForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editArtistForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/admin/active/artists/update",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 400) {
                var errors = [
                    "editArtistFirstName",
                    "editArtistLastName",
                    "editArtistEmail",
                    "editArtistPassword",
                    "editArtistBio",
                    "editArtistAvatar",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    if(key == 'email'){
                        $("#editArtistEmailError").show();
                        $("#editArtistEmailError").html(value[0]);
                    }else{
                    showErrorMessage(value[0], key);
                    }
                }
            }else{
                $("#editArtistModal").modal("toggle");
                location.reload();
            }
        },
    });
});

$(function () {
    $("#editArtistAvatar").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById("editArtistAvatarDisplay").style.backgroundImage =
            "url(" + tmppath + ")";
    });
});

let deletedArtistId;
function artistDelete(id) {
    $("#deleteArtistModal").modal("toggle");
    deletedArtistId = id;
}
$("#deleteArtistForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/admin/active/artists/delete/" + deletedArtistId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 200) {
                toastr.success(response.message);
                $("#deleteArtistModal").modal("toggle");
                window.location.href = '/admin/active/artists';
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
});

let blockArtistId;
function artistBlock(id) {
    $("#blockArtistModal").modal("toggle");
    blockArtistId = id;
}
$("#blockArtistForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/admin/active/artists/block/" + blockArtistId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 200) {
                toastr.success(response.message);
                $("#blockArtistModal").modal("toggle");
                window.location.href = '/admin/block/artists';
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
});
