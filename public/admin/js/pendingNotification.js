pendingTable = $("#pendingNotificationTable").DataTable({
    dom: "<'row'<'col-lg-12 pt-3'l><'col-lg-12 pl-0 pr-0 table-responsive'rt><'col-lg-12 pr-2 pb-2'p>>",
    initComplete: function (settings, json) {},
});

function notificationDetail(id){
    $.LoadingOverlay('show');
    $.get("/admin/artist/notifications/detail/" + id, function (response) {
        $.LoadingOverlay('hide');
        if (response.status == 500) {
            toastr.error(response.message);
        } else if (response.status == 200) {
            const time = new Date(response.notification.createdAt);
            var month;
            if (time.getMonth() < 9) {
                month = "0" + (time.getMonth() + 1);
            } else {
                month = time.getMonth() + 1;
            }
            const date =
                time.getDate() + "/" + month + "/" + time.getFullYear();
            avatar = 'artist/image/avatar/default-avatar.png';
            if(response.notification.get_artist.avatar){
                avatar = thumbnailUrl(response.notification.get_artist.avatar);
            }    
            $("#notificationDetailModalDetail").html(
                `<div class="row">
                <div class="col-12 mb-2">
                    <p class="notification-title-text"><b>
                    `+response.notification.title+`
                    </b></p>
                </div>
                <div class="col-12 mb-3 notification-de-text" >
                    <small>
                    `+response.notification.message+`
                    </small>
                </div>
            </div>
            <hr>
            <div class="row m-0">
                <div class="col-lg-6 pl-0">
                    <small >Artist</small>
                </div>
                <div class="col-lg-6 pr-0">
                    <small style="float: right">
                        <img src="`+ avatar +`" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  Abdul raheem
                    </small>
                </div>
            </div>
            <hr>
            <div class="row m-0">
                <div class="col-lg-6 pl-0">
                    <small >Date</small>
                </div>
                <div class="col-lg-6 pr-0">
                    <small style="float: right"> `+ date +`</small>
                </div>
            </div>`
            );
            if(response.notification.status == 'Pending'){
                $("#notificationDetailModalDetailFooter").html(
                    `    <a class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" onclick="rejectNotification(`+response.notification.id+`)" style="margin-right: 4px;background-color: #ff6250;color: white;" >Reject</a>
                    <a class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" onclick="approveNotification(`+response.notification.id+`)"  style="margin-right: 4px;background-color: #27dc2d;color: white;">Approve</a>`
                );
            }else{
                $("#notificationDetailModalDetailFooter").html(
                    `    <a class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" onclick="rejectNotification(`+response.notification.id+`)" style="margin-right: 4px;background-color: #ff6250;color: white;" >Delete</a>
                   `
                );
            }
            $('#notificationDetailModal').modal('toggle')
        }
    });
}

let rejectNotificationId;
function rejectNotification(id) {
    $('#rejectNotificationModal').modal('toggle')
    rejectNotificationId = id;  
}
$("#rejectNotificationForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/admin/artist/notifications/reject/" + rejectNotificationId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 200) {
                toastr.success(response.message);
                $("#rejectNotificationModal").modal("toggle");
                location.reload();
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
});

let approveNotificationId;
function approveNotification(id) {
    $('#approveNotificationModal').modal('toggle');
    approveNotificationId = id;
}
$("#approveNotificationForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/admin/artist/notifications/approve/" + approveNotificationId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 200) {
                toastr.success(response.message);
                $("#approveNotificationModal").modal("toggle");
                location.reload();
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
});
