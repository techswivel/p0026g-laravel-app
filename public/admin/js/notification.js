    function getNotificationStatusValue() {
        var conceptName = $("#notificationstatus").find(":selected").text();
        if (conceptName == "All") {
            notificationTable.search("").draw();
        } else {
            notificationTable.search(conceptName).draw();
        }
    }

    function showErrorMessage(error, message) {
        if (error) {
            $("#" + message + "Error").show();
            $("#" + message + "Error").html(error);
        }
    }

    function hideErrorMessage(message) {
        $("#" + message + "Error").hide();
    }

    $("#addNotificationForm").submit(function (e) {
        e.preventDefault();
        let formData = new FormData($("#addNotificationForm")[0]);
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $.ajax({
            url: "/admin/notification",
            type: "POST",
            data: formData,
            beforeSend: function () {
                $.LoadingOverlay('show');
            },
            contentType: false,
            processData: false,
            success: function (response) {
                $.LoadingOverlay('hide');
                if (response.status == 400) {
                    var errors = ["notificationTitle", "notificationMessage"];
                    for (var i = 0; i < errors.length; i++) {
                        hideErrorMessage(errors[i]);
                    }
                    for (var [key, value] of Object.entries(response.errors)) {
                        showErrorMessage(value[0], key);
                    }
                } else if (response.status == 500) {
                    toastr.error(response.message);
                } else {
                    $("#addNotificationForm")[0].reset();
                    const truncate = (str, max, suffix) =>
                        str.length < max
                            ? str
                            : `${str.substr(
                                  0,
                                  str
                                      .substr(0, max - suffix.length)
                                      .lastIndexOf(" ")
                              )}${suffix}`;

                    // Example
                    $("#addNotificationModal").modal("toggle");
                    location.reload(true);
                }
            },
        });
    });

    function notificationDetail(id) {
        $.LoadingOverlay('show');
        $.get("/admin/notification/" + id, function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 500) {
                toastr.error(response.message);
            } else if (response.status == 200) {
                const time = new Date(response.notification.createdAt);
                var month;
                if (time.getMonth() < 9) {
                    month = "0" + (time.getMonth() + 1);
                } else {
                    month = time.getMonth() + 1;
                }
                const date =
                    time.getDate() + "/" + month + "/" + time.getFullYear();
                $("#notificationDetailModalDetail").html(
                    `<div class="row">
                        <div class="col-12 mb-2">
                            <p class="notification-title-text"><b>` +
                        response.notification.title +
                        `</b></p>
                        </div>
                        <div class="col-12 mb-3 notification-de-text" >
                            <small>` +
                        response.notification.message +
                        `</small>
                        </div>
                    </div>
                    <hr>
                    <div class="row m-0">

                        <div class="col-lg-6 pl-0">
                            <small >Date</small>
                        </div>
                        <div class="col-lg-6 pr-0">
                            <small style="float: right">` +
                        date +
                        `</small>
                        </div>
                    </div>`
                );
                $("#notificationDetailModal").modal("toggle");

            }
        });
    }

function notificationShowDetail(id) {
        $.LoadingOverlay('show');
        $.get("/admin/notificationViewDetail/" + id, function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 500) {
                toastr.error(response.message);
            } else if (response.status == 200) {
            var topics=response.topics;
               const time = new Date(response.notification.createdAt);
                            var month;
                            if (time.getMonth() < 9) {
                                month = "0" + (time.getMonth() + 1);
                            } else {
                                month = time.getMonth() + 1;
                            }
                            const date =
                                time.getDate() + "/" + month + "/" + time.getFullYear();
                                var userDetailsHtml = '';
                                 userDetailsHtml += '<div class="row">';
                                 userDetailsHtml += '<div class="col-12 mb-2">';
                                 userDetailsHtml += '<p class="notification-title-text"><b>'+response.notification.title +'</b></p>';
                                 userDetailsHtml += '</div>';
                                 userDetailsHtml += '<div class="col-12 mb-3 notification-de-text" >';
                                 userDetailsHtml +=' <small>' + response.notification.message + '</small>';
                                  userDetailsHtml += '</div>';
                                  userDetailsHtml += '</div>';
                                  userDetailsHtml += '<hr>';
                                  userDetailsHtml += '<div class="row m-0">';
                                  userDetailsHtml += '<div class="col-lg-6 pl-0">';
                                  userDetailsHtml += '<small >Date</small>';
                                  userDetailsHtml += '</div>';
                                  userDetailsHtml += '<div class="col-lg-6 pr-0">';
                                  userDetailsHtml += '<small style="float: right">' +date +'</small>';
                                  userDetailsHtml += '</div>';
                                  userDetailsHtml += '</div>';
                                  userDetailsHtml += '<hr>';
                                  userDetailsHtml +='<div class="row m-2">';
                                  userDetailsHtml +='<div class="small-space col-12 pl-0 pr-0">';
                                $.each(topics ,function(i,item){

                                userDetailsHtml +='<small class=" light-grey-text">';
                                userDetailsHtml +='<span class="btn btn-outline-dark m-1 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12" style="background-color:#212529">'+item.topicName+'</span>';
                                userDetailsHtml +='</small>';

                                });
                                userDetailsHtml +='</div>';
                                userDetailsHtml +='</div>';
                           $("#notificationShowDetailValue").html(userDetailsHtml);

                $("#notificationShowDetail").modal("show");



            }
        });
    }
    function notificationDelete(id) {
        $("#deleteNotificationModal").modal("toggle");
        deletedNotificationId = id;
    }
        $("#deleteNotificationForm").submit(function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
            });
            $.ajax({
                url: "/admin/notification/" + deletedNotificationId,
                type: "DELETE",
                beforeSend: function () {
                    $.LoadingOverlay('show');
                },
                success: function (response) {
                    if (response.status == 500) {
                        toastr.error(response.message);
                    } else if (response.status == 200) {
                        $(`#notificationId` + deletedNotificationId).remove();
                        $("#deleteNotificationModal").modal("toggle");
                        $.LoadingOverlay('hide');
                        location.reload(true);
                    }
                },
            });
        });

     function notificationTopicDelete(id) {
            $("#deleteNotificationTopicModal").modal("toggle");
            deletedNotificationId = id;
        }

    $("#deleteNotificationFormTopic").submit(function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $.ajax({
            url: "/admin/notificationTopicDelete/" + deletedNotificationId,
            type: "GET",
            beforeSend: function () {
                $.LoadingOverlay('show');
            },
            success: function (response) {
                if (response.status == 500) {
                    toastr.error(response.message);
                } else if (response.status == 200) {
                    toastr.success(response.message);
                    $("#deleteNotificationTopicModal").modal("show");
                    $.LoadingOverlay('hide');
                    location.reload(true);
                }
            },
        });
    });
var notificationUrl= baseUrl + '/admin/notificationsSend';
function showErrorMessage(error, message) {
    if (error) {
        $("#" + message + "Error").show();
        $("#" + message + "Error").html(error);
    }
}

function hideErrorMessage(message) {
    $("#" + message + "Error").hide();
}
$("#addNotificationFormAdminTopic").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#addNotificationFormAdmin")[0]);
    var title=$('input[name="title"]').val();
    var description=$('#description').val();
    var android=$('input[name="Android_Users"]:checked').val();
    var ios=$('input[name="iOS_Users"]:checked').val();
    var freePlan=$('input[name="free_plan_user"]:checked').val();
    var plan=[];
    $('input[name="packageName"]:checked').each(function() {
       plan.push(this.value);
    });
     var checked = $('input[type="checkbox"]:checked').length;
     if (checked >0){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: notificationUrl,
        type:"POST",
        data:{
             title:title,
             description:description,
             free_plan_user:freePlan,
             Android_Users:android,
             IOS_Users:ios,
             packageName:plan

        },
          beforeSend: function () {
                    $.LoadingOverlay('show');
                },
        success: function (response) {
        console.log(response);
            $.LoadingOverlay('hide');
            if (response.status == 200) {
            $('#addNotificationFormAdmin').modal('hide');
                          toastr.success(response.message);
                    window.location.reload();
            } else{
                toastr.error('Notification not sent');
                $("#addNotificationFormAdmin")[0].reset();
                $("#myNotification").modal("toggle");
            }
        },
    });
    }else{
                $.LoadingOverlay('hide');
               $("#myNotification").modal("show");
               toastr.error('Please select any check box');

         }
});

