approvedTable = $("#approvedNotificationTable").DataTable({
    dom: "<'row'<'col-lg-12 pt-3'l><'col-lg-12 pl-0 pr-0 table-responsive'rt><'col-lg-12 pr-2 pb-2'p>>",
    initComplete: function (settings, json) {},
});

let deletedNotificationId;
function deleteNotification(id) {
    $('#deleteNotificationModal').modal('toggle');
    deletedNotificationId = id;
}
$("#deleteNotificationForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/admin/artist/notifications/delete/" + deletedNotificationId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 200) {
                toastr.success(response.message);
                $("#deleteNotificationModal").modal("toggle");
                location.reload();
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
});
