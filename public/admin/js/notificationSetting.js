var notificationLimitUrl= baseUrl + '/admin/notificationLimit';

$("#perdayNotification").submit(function (e) {
    e.preventDefault();
    var number=$('input[name="numberPerDay"]').val();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: notificationLimitUrl,
        type:"POST",
        data:{
             numberPerDay:number,

        },
          beforeSend: function () {
                    $.LoadingOverlay('show');
                },
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 200) {
                          toastr.success(response.message);
                    window.location.reload();
            } else{
                toastr.error('Notification limit not correct');

            }
        },
    });

});
 var statusUrl=baseUrl +'/admin/notificationArtistStatus';
 $(document).ready(function() {
    $('.artistStatus').on('change',function() {
      $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        var id=$(this).val();
        var isNotificationEnabled=$(this).parent("div").parent("label").prev("input").val();
      $.ajax({
                 type: "GET",
                 dataType: "json",
                 url: statusUrl,
                 data: {'id': id,'isNotificationEnabled':isNotificationEnabled},
                    beforeSend: function () {
                                   $.LoadingOverlay('show');
                               },
                       success: function (response) {
                           $.LoadingOverlay('hide');
                           if (response.status == 200) {
                                         toastr.success(response.message);
                                   window.location.reload();
                           } else{
                               toastr.error('Notification limit not correct');

                           }
                       },
             });
    });
    $('.noActiveStatus').on('change',function() {
      $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
         var user_id = $(this).attr('data-id');
            var id=$(this).val();
           var isNotificationEnabled=$(this).parent("div").parent("label").prev("input").val();

          $.ajax({
                     type: "GET",
                     dataType: "json",
                     url: statusUrl,
                     data: {'id': id,'isNotificationEnabled':isNotificationEnabled},
                        beforeSend: function () {
                                       $.LoadingOverlay('show');
                                   },
                           success: function (response) {
                               $.LoadingOverlay('hide');
                               if (response.status == 200) {
                                             toastr.success(response.message);
                                       window.location.reload();
                               } else{
                                   toastr.error('Notification limit not correct');

                               }
                           },
                 });
        });
    });
