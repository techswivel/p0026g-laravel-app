$(document).ready(function () {
    table = $("#blockArtistTable").DataTable({
        dom: `<'row'<'col pl-1 pr-1'<'card m-2 border-radius8lr'<'.card-header p-2 pl-3 '<'allnoti'>il><'card-body p-0 table-responsive'<rt>>>p>>`,
        initComplete: function (settings, json) {},
    });
    $("div.allnoti").html(
        '<span class="pt-2 pr-2 all-noti-text"style="font-weight:400;float:left">Artist Lists</span>'
    );
    $("#filterSearch").keyup(function () {
        table.search(this.value).draw();
    });
});

let unblockArtistId;
function artistUnblock(id) {
    $("#unblockArtistModal").modal("toggle");
    unblockArtistId = id;

}
$("#unblockArtistForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/admin/block/artists/unblock/" + unblockArtistId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 200) {
                toastr.success(response.message);
                $("#unblockArtistModal").modal("toggle");
                window.location.href = '/admin/active/artists';
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
});