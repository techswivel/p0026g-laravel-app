function showErrorMessage(error, message) {
    if (error) {
        $("#" + message + "Error").show();
        $("#" + message + "Error").html(error);
    }
}

function hideErrorMessage(message) {
$("#" + message + "Error").hide();
}

$(".clearText").click(function(){
    $("#createPackageForm")[0].reset();
    var errors = [
        "planTitle",
        "planPrice",
        "planDuration",
    ];
    for (var i = 0; i < errors.length; i++) {
        hideErrorMessage(errors[i]);
    }
  });

// Create package
$("#createPackageForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#createPackageForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/admin/package",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 400 || response.status == 500) {
                toastr.error(response.message);
                var errors = [
                    "planTitle",
                    "planPrice",
                    "planDuration",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            }else{
                $("#createPackageForm")[0].reset();
                location.reload();
            }
        },
    });
});

let deletedPackageId;
function packageDelete(id) {
    $("#deletePackageModal").modal("toggle");
    deletedPackageId = id;
}
$("#deletePackageForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/admin/package/" + deletedPackageId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 200) {
                toastr.success(response.message);
                $("#deletePackageModal").modal("toggle");
                location.reload();
            } else if (response.status == 500 || response.status == 400) {
                toastr.error(response.message);
            }
        },
    });
});

function editPackage(id) {
    $("#editPackageForm")[0].reset();
    var errors = [
        "editPlanTitle",
        "editPlanPrice",
        "editPlanDuration",
    ];
    for (var i = 0; i < errors.length; i++) {
        hideErrorMessage(errors[i]);
    }
    $.LoadingOverlay('show');
    $.get("/admin/package/show/" + id, function (response) {
        if (response.status == 500) {
            toastr.error(response.message);
        } else {
            $("#editPackageId").val(response.package.id);
            $("#editPlanTitle").val(response.package.name);
            $("#editPlanDuration").val(response.package.duration);
            $("#editPlanPrice").val(response.package.price)

            var html = ``;
            $.each(response.package.prices , function(index,price){
                html += `<option value="${price.priceTier}" ${parseFloat(response.package.price).toFixed(1) == price.customerPrice ? 'selected' : ''}>${price.customerPrice}</option>`
            });

            $("#editPlanPriceTier").html(html);

            $("#editPackageModal").modal("toggle");
            $.LoadingOverlay('hide');
        }
    });
}

$("#editPackageForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editPackageForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/admin/package/update",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 400) {
                var errors = [
                    "editPlanTitle",
                    "editPlanPrice",
                    "editPlanDuration",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
                $("#editArtistModal").modal("toggle");
            }else{
                location.reload();
            }

        },
    });
});

$('.package-next-btn').on('click', function(){
    $("#stepNo").val(1)

    if($("#packageId").val().length == 0){
        let formData = new FormData($("#createPackageForm")[0]);
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $.ajax({
            url: "/admin/package",
            type: "POST",
            data: formData,
            beforeSend: function () {
                $.LoadingOverlay('show');
            },
            contentType: false,
            processData: false,
            success: function (response) {
                $.LoadingOverlay('hide');
                if (response.status == 400) {
                    var errors = [
                        "planTitle",
                        "planPrice",
                        "planDuration",
                    ];
                    for (var i = 0; i < errors.length; i++) {
                        hideErrorMessage(errors[i]);
                    }
                    for (var [key, value] of Object.entries(response.errors)) {
                        showErrorMessage(value[0], key);
                    }
                }else{
                    $.LoadingOverlay('hide');
                    $('.progress-step-1').removeClass(' active')
                    $('.progress-step-2').addClass(' active')

                    $('#pills-home').removeClass('show active')
                    $('#pills-profile').addClass('show active')
                    $("#stepNo").val(2)

                    $("#packageId").val(response[1].packageId)

                    var html = ``;
                    $.each(response , function(index,price){
                        parseInt(index) == 1 ? $("#subscriptionPrice").val(price.customerPrice) : '';
                        html += `<option value="${price.priceTier}">${price.customerPrice}</option>`
                    });

                    $("#priceTier").html(html);
                }
            },
        });

    }else{
        $('.progress-step-1').removeClass(' active')
        $('.progress-step-2').addClass(' active')

        $('#pills-home').removeClass('show active')
        $('#pills-profile').addClass('show active')
        $("#stepNo").val(2)
    }
});

$('.package-previous-btn').on('click', function(){
    $('.progress-step-2').removeClass(' active')
    $('.progress-step-1').addClass(' active')

    $('#pills-profile').removeClass('show active')
    $('#pills-home').addClass('show active')

    $("#stepNo").val(1)
});


$("#priceTier").on('change', function(){
    $("#subscriptionPrice").val($('option:selected',this).text());
});


$("#editPlanPriceTier").on('change', function(){
    $("#editPlanPrice").val($('option:selected',this).text());
});
