var sellerURL = baseUrl + '/admin/users/detail/';
var urlDetail = baseUrl + '/admin/usersDetail';
var defaultImage = baseUrl + '/defaultImages/default-avatar.png'
var imageUrl=baseUrl  + '/svg'

$('table').on('click', 'tbody .viewSeller', function() {
    $.LoadingOverlay('show')
    var sellerId = $(this).attr('data-id')
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: sellerURL + sellerId + '/details',
        success: function(data) {
            if (data.status) {
                var userDetail = data.data;
                var getSong=userDetail.user_buying_histories;
                var userPlan=userDetail?userDetail.user_active_packages:null;
                if(userPlan==null){
                    var planName='No Plan Selected';
                    var planDuration='';
                    var planPrice='';
                    var customTag='';
                    var customTagLin='';
                }else{
                    var planName=userPlan.package.name;
                    var planDuration=userPlan.package.duration;
                    var planPrice=userPlan.package.price;
                    var customTag='$';
                    var customTagLin='/';
                }
                var date = userDetail.formattedDate;
                var images = userDetail.images ? userDetail.images : null
                var img = userDetail.avatar? userDetail.avatarFullUrl : defaultImage
                var name = userDetail.firstName ? userDetail.firstName +' '+userDetail.lastName :'null';
                var userDetailsHtml = '';
                userDetailsHtml += '<div class="card-body box-profile p-0"> ';
                userDetailsHtml += '<div class = "text-center" >';
                userDetailsHtml += '</div>';
                userDetailsHtml += ' <ul id="purchaseList" class="products-list product-list-in-card pl-3 pr-1">';
                userDetailsHtml += ' <li class="item pt-1 pb-1" style="border-bottom: none">';
                userDetailsHtml += ' <div class="product-img pt-1">';
                userDetailsHtml += ' <img src="' + img + '" class="border-radius8" alt="Product Image"  style="width: 45.5px;height:45.5px">';
                userDetailsHtml += ' </div>';
                userDetailsHtml += '<div class="product-info" style="margin-top: 4px">';
                userDetailsHtml +='<small  id="buyerName" class="product-title font-szie10">' + userDetail.name +'</small>';
                userDetailsHtml +='<small id="buyerEmail" class="product-description font-szie10 light-grey-text">' + userDetail.email +'</small>';
                userDetailsHtml += '<div>';
               userDetailsHtml +='</li>';
               userDetailsHtml +='</ul>';
               userDetailsHtml +='<div class="card-header p-0 pt-3 " style="background-color: white; border-top-left-radius:8px !important;border-top-right-radius:8px !important;">';
               userDetailsHtml +='<div class="row ml-0">';
                userDetailsHtml += '<ul class="nav nav-pills card-header-pills m-0 nav-justified" id="bologna-list"  role="tablist" style="width:100%">';
                userDetailsHtml += '<li style="" class="nav-item"><span style="" class="nav-link active rounded-0" href="#menu1" data-toggle="tab">General info</span></li>&nbsp;&nbsp';
                userDetailsHtml += '<li class="nav-item" style=""><span class="nav-link rounded-0" href="#menu2" data-toggle="tab">Buying History</span></li>&nbsp;&nbsp';
                userDetailsHtml += '</ul>';
                userDetailsHtml += '</div>';
                userDetailsHtml +='</div>';
                userDetailsHtml += '<div id="menu1" class="tab-pane fade active show pt-2" style="margin-top:12px">';
                userDetailsHtml +='<div class="row m-0 ml-3"><img style="" src="'+imageUrl+'/awesome-calendar-day.svg">&nbsp;&nbsp</img><label class="mb-0">Date of Birth</label> <span style="font-weight:600" class="pl-5">'+date+'</span></div>'
                userDetailsHtml +='<div class="row m-0 mt-2 ml-3"><img style="" src="'+imageUrl+'/awesome-phone.svg"></img>&nbsp;&nbsp<label class="mb-0">Phone</label> <span style="font-weight:600" class="pl-5">'+userDetail.phoneNumber+'</span></div>'
                userDetailsHtml +='<div class="row m-0 mt-2 ml-3"><img style="" src="'+imageUrl+'/ionic-md-male.svg"></img>&nbsp;&nbsp<label class="mb-0"> Gender</label> <span style="font-weight:600" class="pl-5">'+userDetail.gender+'</span></div>'
               userDetailsHtml += '<div class="row m-0 mt-2 ml-3"><div style="">';
               userDetailsHtml += '<label> <strong class="address" style="font-weight: 600;">Address<hr/></strong> </label><br>';
               userDetailsHtml += '<span>'+userDetail.address+'  </span>';
               userDetailsHtml += '</div></div>';
               userDetailsHtml += '<div class="row m-0 mt-2 ml-3"><div style="">';
               userDetailsHtml += '<label><strong class="product-title" style="font-weight: 600;">User Type</strong><hr/> </label><br>';
               userDetailsHtml +='<div  class="card p-0 m-0 border-radius8 off-white " style="width:248px;background-color: #E8E8E8">';
               userDetailsHtml +='<div class="card-body m-2 m-0 p-0 border-radius8">';
               userDetailsHtml += '<span>Current Plan</span><br>';
               userDetailsHtml += '<label><strong class="product-title" style="font-weight: 800;">'+ planName+'</strong></label><br>';
               userDetailsHtml += '<span>'+customTag+ planPrice+customTagLin+planDuration+'</span>';
               userDetailsHtml +='</div>';
               userDetailsHtml +='</div></br>';
               userDetailsHtml += '</div></div>';
               userDetailsHtml +='<div class="row m-0 mt-2 ml-3 mr-3"><a  href="'+urlDetail+'/'+userDetail.id+'" style="width: 100%;border-radius:8px" class="btn btn-danger">View Full Details</a></div>';
               userDetailsHtml +='</div>';
               userDetailsHtml += '<div id="menu2" class="tab-pane fade ">';
               userDetailsHtml += '<div style="margin-top:-338px">';
               userDetailsHtml += '<div class="row m-0 mt-2 ml-4"><span style="margin-left:-5px"> Last 30 Days</span> <a href="'+urlDetail+'/'+userDetail.id+'"><span style="margin-left:66px"> View All</a> </span></div>';
              $.each(getSong, function(i, item1) {
                var song=item1.get_songs;
                $.each(song ,function(i,item){
                  var imgSong = item.thumbnail? item.avatarFullUrl : defaultImage;
                    userDetailsHtml +='<div class="row m-0 mt-2 ml-3 mr-3"><ul  class="products-list product-list-in-card pl-0 pr-1" style="width:100%">';
                    userDetailsHtml +='<li class="item pt-1 pb-1" style="border-bottom: none">';
                    userDetailsHtml +='<div class="product-img pt-1">';
                    userDetailsHtml +='<img src="'+ imgSong +'" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">';
                    userDetailsHtml +='</div>';
                    userDetailsHtml +='<div class="product-info ml-5">';
                    userDetailsHtml +='<small  class="product-title font-szie10 ">'+item.name+'<span class="float-right ">$'+item.price+'</span></small>';
                    userDetailsHtml +='<small class="product-description font-szie10 light-grey-text">'+item.songLength+'</small>';
                    userDetailsHtml +='</div>';
                    userDetailsHtml +='</li>';
                    userDetailsHtml +='</ul></div>';
                });
                 });
                userDetailsHtml +='</div>';
                userDetailsHtml += '</div>';
                userDetailsHtml += '</div>';
                userDetailsHtml += '';
                userDetailsHtml += '</div>';
                $('.seller-details').html(userDetailsHtml);
                            $.LoadingOverlay('hide')

                window.location = "#seller-details"
                $('body').addClass('sidebar-mini control-sidebar-slide-open')
                document.getElementById("mySideClose").style.width = "37px";
            } else {
                toastr.error('Something went wrong!');
            }


        },
        error: function(data) {
            $.LoadingOverlay('hide');
            $('body').removeClass('sidebar-mini control-sidebar-slide-open')
            toastr.error('Something went wrong!')
        }

    });


});

function closeNav() {
    document.getElementById("mySideClose").style.width = "0px";
    $('body').removeClass('sidebar-mini control-sidebar-slide-open')
}



