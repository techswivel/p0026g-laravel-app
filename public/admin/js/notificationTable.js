$(document).ready(function () {
    notificationTable = $("#notificationTable").DataTable({
        dom: "<'row  p-2 pt-2 padding-top0'<'col-lg-4 pt-1'<'notificationDiv'>><'col-lg-6 pr-3'l<'notificationstatusDiv'>><'col-lg-2 pr-0 pl-0'<'notificationSearch'>>><'bottom'rtp>",
        initComplete: function (settings, json) {
        },
    });

    if(isNotificationLimit){
        button = `<small class="float-left ml-2 pt-1"  style="font-weight:600">Notifications</small><button type="button" style="width:100px;font-size:12px" data-toggle="modal" data-target="#addNotificationModal" class="btn ml-2 red-button btn-xs border-radius8 add-song-button"><i class="fas fa-plus"></i> Add New</button>`
    }else{
        button = `<small class="float-left ml-2 pt-1"  style="font-weight:600">Notifications</small>`
    }
    $("div.notificationDiv").html(
        button
    );
    $("div.notificationstatusDiv").html(
        '<select class="float-right p-1  month-select font-size12" style="margin-right: 8px;background-color:white;border:0.5px solid #E1E4E6" id="notificationstatus" name="song-status" onchange="getNotificationStatusValue();"><option>All</option><option>Pending</option><option>Rejected</option><option>Approved</option><option>Deleted By Admin</option></select>'
    );

    $("div.notificationSearch").html(
        '<input type= "search" class="form-control  float-right font-size12" id="followfilterSearch"  placeholder="Search">'
    );
    $("#followfilterSearch").keyup(function () {
        notificationTable.search(this.value).draw();
    });
});
