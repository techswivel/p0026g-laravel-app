var currentYear = new Date().getFullYear();
(function () {
    let months = [
        currentYear,
        "Last month",
        "Last Week",
        currentYear - 1,
        currentYear - 2,
    ];
    var month_selected = "All"; //(new Date).getMonth(); // current month
    var option = "";

    for (let i = 0; i < months.length; i++) {
        let month_number = i + 1;

        // value month number with 0. [01 02 03 04..]
        let month = month_number <= 9 ? "0" + month_number : month_number;

        let selected = i === month_selected ? " selected" : "";
        option +=
            '<option value="' +
            month +
            '"' +
            selected +
            ">" +
            months[i] +
            "</option>";
    }
    document.getElementById("saleReportChart").innerHTML = option;
})();

function sum(obj) {
    var sum = 0;
    for (var el in obj) {
        if (obj.hasOwnProperty(el)) {
            sum += parseFloat(obj[el]);
        }
    }
    return sum;
}

function revenue(current, previous) {
    if (previous == 0) {
        value = 100;
        return value;
    } else {
        value = current - previous;
        value = value / previous;
        value = value * 100;
        return value;
    }
}

function changeRevenueStatus(revenueValue) {
    if (revenueValue < 0) {
        $("#revenueColor").removeClass("text-success");
        $("#revenueColor").addClass("text-danger");

        $("#revenueArrow").removeClass("fa-arrow-up");
        $("#revenueArrow").addClass("fa-arrow-down");
        document.getElementById("revenueValue").innerText =
            Math.abs(revenueValue).toString() + "%";
    } else {
        $("#revenueColor").removeClass("text-danger");
        $("#revenueColor").addClass("text-success");

        $("#revenueArrow").removeClass("fa-arrow-down");
        $("#revenueArrow").addClass("fa-arrow-up");
        document.getElementById("revenueValue").innerText =
            Math.abs(revenueValue).toString() + "%";
    }
}

// eslint-disable-next-line no-unused-vars
var salesChart = new Chart($("#sales-chart"), {
    type: "bar",
    data: {
        labels: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
        ],
        datasets: [
            {
                backgroundColor: "#ba011a",
                borderColor: "#ba011a",
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            },
        ],
    },
    options: {
        maintainAspectRatio: false,
        tooltips: {
            mode: "index",
            intersect: true,
        },
        hover: {
            mode: "index",
            intersect: true,
        },
        legend: {
            display: false,
        },
        scales: {
            yAxes: [
                {
                    // display: false,
                    gridLines: {
                        display: true,
                        lineWidth: "6px",
                        color: "rgba(0, 0, 0, .2)",
                        zeroLineColor: "transparent",
                    },
                    ticks: {
                        display: false,
                    },
                },
            ],
            xAxes: [
                {
                    barPercentage: 0.6,
                    display: true,
                    gridLines: {
                        display: false,
                    },
                    ticks: {
                        fontColor: "#242424",
                        fontStyle: "lighter",
                        fontSize: "9",
                    },
                },
            ],
        },
    },
});

$(function () {
    "use strict";

    var ticksStyle = {
        fontColor: "#242424",
        fontStyle: "lighter",
        fontSize: "9",
    };

    var mode = "index";
    var intersect = true;
    $.get("/admin/dashboard/salesReport/" + currentYear, function (response) {
        if (response.status == 200) {
            var revenueValue = revenue(
                sum(response.monthly).toFixed(2),
                response.previousYearSales
            ).toFixed(2);
            changeRevenueStatus(revenueValue);
            var $salesChart = $("#sales-chart");
            // eslint-disable-next-line no-unused-vars
            salesChart = new Chart($salesChart, {
                type: "bar",
                data: {
                    labels: [
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "May",
                        "Jun",
                        "Jul",
                        "Aug",
                        "Sep",
                        "Oct",
                        "Nov",
                        "Dec",
                    ],
                    datasets: [
                        {
                            backgroundColor: "#ba011a",
                            borderColor: "#ba011a",
                            data: [
                                response.monthly[0],
                                response.monthly[1],
                                response.monthly[2],
                                response.monthly[3],
                                response.monthly[4],
                                response.monthly[5],
                                response.monthly[6],
                                response.monthly[7],
                                response.monthly[8],
                                response.monthly[9],
                                response.monthly[10],
                                response.monthly[11],
                            ],
                        },
                    ],
                },
                options: {
                    maintainAspectRatio: false,
                    tooltips: {
                        mode: mode,
                        intersect: intersect,
                    },
                    hover: {
                        mode: mode,
                        intersect: intersect,
                    },
                    legend: {
                        display: false,
                    },
                    scales: {
                        yAxes: [
                            {
                                // display: false,
                                gridLines: {
                                    display: true,
                                    lineWidth: "6px",
                                    color: "rgba(0, 0, 0, .2)",
                                    zeroLineColor: "transparent",
                                },
                                ticks: {
                                    display: false,
                                },
                            },
                        ],
                        xAxes: [
                            {
                                barPercentage: 0.6,
                                display: true,
                                gridLines: {
                                    display: false,
                                },
                                ticks: ticksStyle,
                            },
                        ],
                    },
                },
            });
        } else if (response.status == 500) {
            toastr.error(response.message);
        }
    });
});


function changeStatusOfChart() {
    var chartData = $("#saleReportChart").find(":selected").text();
    if (chartData == "Last month") {
        salesChart.destroy();
        $(function () {
            "use strict";

            var ticksStyle = {
                fontColor: "#242424",
                fontStyle: "lighter",
                fontSize: "9",
            };

            var mode = "index";
            var intersect = true;
            $.LoadingOverlay('show');
            $.get("/admin/dashboard/salesReport/"+chartData, function (response) {
                $.LoadingOverlay('hide');
                if (response.status == 200) {
                    document.getElementById("aboutChart").innerText =
                        "This Month";
                    var revenueValue = revenue(
                        sum(response.day).toFixed(2),
                        response.previousMonthValue
                    ).toFixed(2);
                    changeRevenueStatus(revenueValue);

                    var $salesChart = $("#sales-chart");
                    // eslint-disable-next-line no-unused-vars
                    salesChart = new Chart($salesChart, {
                        type: "bar",
                        data: {
                            labels: [
                                "1-5",
                                "6-10",
                                "11-15",
                                "16-20",
                                "21-25",
                                "26-30",
                            ],
                            datasets: [
                                {
                                    backgroundColor: "#ba011a",
                                    borderColor: "#ba011a",
                                    data: [
                                        response.day[0],
                                        response.day[1],
                                        response.day[2],
                                        response.day[3],
                                        response.day[4],
                                        response.day[5],
                                    ],
                                },
                            ],
                        },
                        options: {
                            maintainAspectRatio: false,
                            tooltips: {
                                mode: mode,
                                intersect: intersect,
                            },
                            hover: {
                                mode: mode,
                                intersect: intersect,
                            },
                            legend: {
                                display: false,
                            },
                            scales: {
                                yAxes: [
                                    {
                                        // display: false,
                                        gridLines: {
                                            display: true,
                                            lineWidth: "6px",
                                            color: "rgba(0, 0, 0, .2)",
                                            zeroLineColor: "transparent",
                                        },
                                        ticks: {
                                            display: false,
                                        },
                                    },
                                ],
                                xAxes: [
                                    {
                                        barPercentage: 0.6,
                                        display: true,
                                        gridLines: {
                                            display: false,
                                        },
                                        ticks: ticksStyle,
                                    },
                                ],
                            },
                        },
                    });
                } else if (response.status == 500) {
                    toastr.error(response.message);
                }
            });
        });
    } else if (chartData == "Last Week") {
        salesChart.destroy();
        $(function () {
            "use strict";

            var ticksStyle = {
                fontColor: "#242424",
                fontStyle: "lighter",
                fontSize: "9",
            };

            var mode = "index";
            var intersect = true;
            $.LoadingOverlay('show');
            $.get("/admin/dashboard/salesReport/"+chartData, function (response) {
                $.LoadingOverlay('hide');
                if (response.status == 200) {
                    document.getElementById("aboutChart").innerText =
                        "This Week";
                    var revenueValue = revenue(
                        sum(response.weekDays).toFixed(2),
                        response.previousWeekValue
                    ).toFixed(2);
                    changeRevenueStatus(revenueValue);
                    var $salesChart = $("#sales-chart");
                    // eslint-disable-next-line no-unused-vars
                    salesChart = new Chart($salesChart, {
                        type: "bar",
                        data: {
                            labels: [
                                "Monday",
                                "Tuesday",
                                "Wednesday",
                                "Thursday",
                                "Friday",
                                "Saturday",
                                "Sunday",
                            ],
                            datasets: [
                                {
                                    backgroundColor: "#ba011a",
                                    borderColor: "#ba011a",
                                    data: [
                                        response.weekDays[0],
                                        response.weekDays[1],
                                        response.weekDays[2],
                                        response.weekDays[3],
                                        response.weekDays[4],
                                        response.weekDays[5],
                                        response.weekDays[6],
                                    ],
                                },
                            ],
                        },
                        options: {
                            maintainAspectRatio: false,
                            tooltips: {
                                mode: mode,
                                intersect: intersect,
                            },
                            hover: {
                                mode: mode,
                                intersect: intersect,
                            },
                            legend: {
                                display: false,
                            },
                            scales: {
                                yAxes: [
                                    {
                                        // display: false,
                                        gridLines: {
                                            display: true,
                                            lineWidth: "6px",
                                            color: "rgba(0, 0, 0, .2)",
                                            zeroLineColor: "transparent",
                                        },
                                        ticks: {
                                            display: false,
                                        },
                                    },
                                ],
                                xAxes: [
                                    {
                                        barPercentage: 0.6,
                                        display: true,
                                        gridLines: {
                                            display: false,
                                        },
                                        ticks: ticksStyle,
                                    },
                                ],
                            },
                        },
                    });
                } else if (response.status == 500) {
                    toastr.error(response.message);
                }
            });
        });
    } else {
        salesChart.destroy();
        $(function () {
            "use strict";

            var ticksStyle = {
                fontColor: "#242424",
                fontStyle: "lighter",
                fontSize: "9",
            };

            var mode = "index";
            var intersect = true;
            $.LoadingOverlay('show');
            $.get(
                "/admin/dashboard/salesReport/" + chartData,
                function (response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 200) {
                        document.getElementById("aboutChart").innerText =
                            "This Year";
                        var revenueValue = revenue(
                            sum(response.monthly).toFixed(2),
                            response.previousYearSales
                        ).toFixed(2);
                        changeRevenueStatus(revenueValue);
                        var $salesChart = $("#sales-chart");
                        // eslint-disable-next-line no-unused-vars
                        salesChart = new Chart($salesChart, {
                            type: "bar",
                            data: {
                                labels: [
                                    "Jan",
                                    "Feb",
                                    "Mar",
                                    "Apr",
                                    "May",
                                    "Jun",
                                    "Jul",
                                    "Aug",
                                    "Sep",
                                    "Oct",
                                    "Nov",
                                    "Dec",
                                ],
                                datasets: [
                                    {
                                        backgroundColor: "#ba011a",
                                        borderColor: "#ba011a",
                                        data: [
                                            response.monthly[0],
                                            response.monthly[1],
                                            response.monthly[2],
                                            response.monthly[3],
                                            response.monthly[4],
                                            response.monthly[5],
                                            response.monthly[6],
                                            response.monthly[7],
                                            response.monthly[8],
                                            response.monthly[9],
                                            response.monthly[10],
                                            response.monthly[11],
                                        ],
                                    },
                                ],
                            },
                            options: {
                                maintainAspectRatio: false,
                                tooltips: {
                                    mode: mode,
                                    intersect: intersect,
                                },
                                hover: {
                                    mode: mode,
                                    intersect: intersect,
                                },
                                legend: {
                                    display: false,
                                },
                                scales: {
                                    yAxes: [
                                        {
                                            // display: false,
                                            gridLines: {
                                                display: true,
                                                lineWidth: "6px",
                                                color: "rgba(0, 0, 0, .2)",
                                                zeroLineColor: "transparent",
                                            },
                                            ticks: {
                                                display: false,
                                            },
                                        },
                                    ],
                                    xAxes: [
                                        {
                                            barPercentage: 0.6,
                                            display: true,
                                            gridLines: {
                                                display: false,
                                            },
                                            ticks: ticksStyle,
                                        },
                                    ],
                                },
                            },
                        });
                    } else if (response.status == 500) {
                        toastr.error(response.message);
                    }
                }
            );
        });
    }
}

function userDetail(id) {
    
}

