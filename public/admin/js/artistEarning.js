$(document).ready(function () {
    table = $("#artistEarningTable").DataTable({
        dom: "<'row'<'col'<'card m-2 border-radius8lr'<'.card-header p-2 pl-3 pt-3'<'row'<'col-lg-6'<'earningDiv'>><'col-lg-4'l><'col-lg-2'<'earningSearch'>>>><'card-body p-0 table-responsive'<rt>>>p>>",
        initComplete: function (settings, json) {},
    });
    $("div.earningDiv").html(
        '<span class="pr-2 all-noti-text"style="font-weight:600">All Artist</span>'
    );
    $("div.earningSearch").html(
        '<input type= "search" class="form-control  float-right font-size12 pt-0 pb-0" id="earningSearch"  placeholder="Search">'
    );

    $("#filterSearch").keyup(function () {
        table.search(this.value).draw();
    });
});

function amount(id) {
    $("#artistId").val(id);
    $.get("/admin/artist/earning/card/" + id, function (response) {
        if(response.status == 500){
            toastr.error(response.message);
        }else{
            if(response.card){
                $("#userAccountName").text(response.card.cardHolderName);
                $("#userAccountNumber").text(response.card.cardId);
            }else{
                $("#userAccountName").text('xxxx');
                $("#userAccountNumber").text('xxxx');
            }
            $("#transferConfirmationModal").modal("toggle");
        }
    });
}

function showErrorMessage(error, message) {
    if (error) {
        $("#" + message + "Error").show();
        $("#" + message + "Error").html(error);
    }
}

function hideErrorMessage(message) {
$("#" + message + "Error").hide();
}

// Create Artist Earning
$("#transferConfirmForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#transferConfirmForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/admin/artist/earning/amount",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 400) {
                var errors = [
                    "amount",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            }else if(response.status == 500){
                toastr.error(response.message);
            }else{
                toastr.success(response.message);
                $("#transferConfirmForm")[0].reset();
                location.reload();
            }
        },
    });
});

 setTimeout(function(){
     $("#earningSearch").on('keyup',function () {
        table.search(this.value).draw();
     });
 }, 1000)

