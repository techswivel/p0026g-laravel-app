$.LoadingOverlay('show');

options = null;
planOption = null;
$.get("/admin/package/name", function (response) {
    if(response.status == 200){
        let months = ["All","Songs earnings", "Album earnings", "Preorder earnings"];
        response.package.forEach(function log(name) {
            planOption += `<option value="`+name.id+`"
            >`+name.name+`</option>`
        });
    
        var month_selected = "None"; //(new Date).getMonth(); // current month
        var option = "";
        option = '<option value="none">None</option>'; // first option
    
        for (let i = 0; i < months.length; i++) {
            let month_number = i + 1;
    
            // value month number with 0. [01 02 03 04..]
            let month = month_number <= 9 ? "0" + month_number : month_number;
    
            let selected = i === month_selected ? " selected" : "";
            option +=
                '<option value="' +
                months[i].replaceAll(" ", "")
                 +
                '"' +
                selected +
                ">" +
                months[i] +
                "</option>";
        }
        options = option + planOption;

    }else{
        toastr.error(response.message);
    }
    $(document).ready(function () {
        followerTable = $("#purchasedHistoryTable").DataTable({
            dom: "<'row m-0 p-0 pt-2 padding-top0'<'col-lg-4'<'HistoryDiv'>><'col-lg-4'<'purchaseStatusDiv'>><'col-lg-2'l><'col-lg-2'<'purchaedHistorySearch'>>><'row m-0 p-0 pt-2 padding-top0 table-responsive'rt><'row m-0 float-right p-2'p>",
            initComplete: function (settings, json) {
            },
        });
        $("div.HistoryDiv").html(
            '<small class="float-left ml-2"  style="font-weight:600">History List</small>'
        );
        $("div.purchaseStatusDiv").html(
            '<select class="p-1 float-right  month-select font-size12" style="margin-right: 8px;background-color:white;border:0.5px solid #E1E4E6" id="purchasedHistoryStatus" name="song-status" onchange="getPurchasedHistoryStatusValue();">'+options+'</select>'
        );
    
        $("div.purchaedHistorySearch").html(
            '<input type= "search" class="form-control  float-right font-size12 pt-0 pb-0" id="purchasedHistorySearch"  placeholder="Search">'
        );
        $("#purchasedHistorySearch").keyup(function () {
            followerTable.search(this.value).draw();
        });
    });
    $.LoadingOverlay('hide');
});
        


function buyerDetail(id) {
    $.LoadingOverlay('show');
    $.get("/admin/purchased/history/detail/" + id, function (response) {
        $.LoadingOverlay('hide');
        if (response.status == 200) {
            document.getElementById("plan").innerHTML ='';
            $("#buyerName").text(
                response.purchase.user.firstName +
                    " " +
                    response.purchase.user.lastName
            );
            $("#buyerEmail").text(response.purchase.user.email);
            if (response.purchase.user.avatar) {
                $("#buyerImage").attr(
                    "src",
                    thumbnailUrl(response.purchase.user.avatar)
                );
            } else {
                $("#buyerImage").attr("src", "/artist/image/avatar/default-avatar.png");
            }
            document.getElementById("purchaseList").innerHTML = "";
            if (response.purchase.itemType == 'SONG') {
                document.getElementById("purchaseList").innerHTML =
                    `<li class="item pt-1 pb-1" style="border-bottom: none">
                            <div class="product-img pt-1">
                    <img src="` +
                    thumbnailUrl(response.purchase.song.thumbnail) +
                    `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                        </div>
                    <div class="product-info ml-5">
                        <small  class="product-title font-szie10 ">` +
                    response.purchase.song.name +
                    `<span class="float-right ">$` +
                    response.purchase.song.price +
                    `</span></small>
                    <small class="product-description font-szie10 light-grey-text">` +
                    response.purchase.song.songLength +
                    `</small>
                </div>
            </li>`;
            } else if (response.purchase.itemType == 'ALBUM') {
                document.getElementById("purchaseList").innerHTML =
                    `<li class="item pt-1 pb-1" style="border-bottom: none">
                            <div class="product-img pt-1">
                            <img src="` +
                    thumbnailUrl(response.purchase.album.thumbnail) +
                    `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                        </div>
                        <div class="product-info ml-5">
                        <small  class="product-title font-szie10 ">` +
                    response.purchase.album.name +
                    `<span class="float-right ">$` +
                    response.price +
                    `</span></small>
                            <small class="product-description font-szie10 light-grey-text">` +
                    response.albumSongs.length +
                    ` songs</small>
                        </div>
                        </li><hr>`;
                response.albumSongs.forEach(function log(data) {
                    document.getElementById("purchaseList").innerHTML +=
                        `<li class="item pt-1 pb-1" style="border-bottom: none">
                            <div class="product-img pt-1">
                            <img src="` +
                        thumbnailUrl(data.thumbnail) +
                        `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                            </div>
                            <div class="product-info ml-5">
                            <small  class="product-title font-szie10 ">` +
                        data.name +
                        `<span class="float-right ">$` +
                        data.price +
                        `</span></small>
                            <small class="product-description font-szie10 light-grey-text">` +
                        data.songLength +
                        `</small>
                            </div>
                            </li>`;
                });
            }else{
                document.getElementById("plan").innerHTML =
                `  <div  class="card p-0 m-0 mt-3 border-radius8 off-white " style="width: 100%">
                <div class="card-body m-2 m-0 p-0 border-radius8">
                    <div class="row">
                        <div class="col-lg-12">
                              <small class="float-left ml-0 pt-1"  style="font-weight:500;color: gray;">Current</small><br>
                              <h6 class="mt-1 mb-0">`+response.purchase.package.name+`</h6>
                              <small id="buyerAddress" class="pb-2  " style="font-weight:500;color: gray;">$`+response.purchase.package.price+` / `+response.purchase.package.duration+` Month</small>
                       </div>
                    </div>
                </div>
              </div>
            `
            }
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("mySideClose").style.width = "37px";
        } else if (response.status == 500) {
            toastr.error(response.message);
        }
    });
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("mySideClose").style.width = "0";
}

function getPurchasedHistoryStatusValue(){
    var conceptName = $("#purchasedHistoryStatus").find(":selected").val();
    window.location.href = '/admin/purchased/history/' + conceptName;    
}