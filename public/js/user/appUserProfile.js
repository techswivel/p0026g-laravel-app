// set, edit and get the Song Status is Song Paid or Free



$(function () {
    var myDate = new Date();
    $("#birthDate").datepicker({
       defaultDate: myDate,
        format: "mm/dd/yy", // <-- format, not dateFormat
        showOtherMonths: true,
        selectOtherMonths: true,
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        //gotoCurrent: true,
       orientation: "top" // <-- and add this
    });
});


// Disable form submissions if there are invalid fields
(function () {
    "use strict";
    window.addEventListener(
        "load",
        function () {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName("needs-validation");
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(
                forms,
                function (form) {
                    form.addEventListener(
                        "submit",
                        function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add("was-validated");
                        },
                        false
                    );
                }
            );
        },
        false
    );
})();

// show image in circle div

$(function () {
    $("#avatar").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById("image-show-detail").style.backgroundImage =
            "url(" + tmppath + ")";
    });
});

//danger border around field (red)

$(document).ready(function () {
    $("#name").on("click", function (event) {
        event.preventDefault();
        $("#name").addClass("border-danger");
    });
    $("#name").mouseleave(function () {
        $("#name").removeClass("border-danger");
    });
    $("#address").on("click", function (event) {
        event.preventDefault();
        $("#address").addClass("border-danger");
    });
    $("#address").mouseleave(function () {
        $("#address").removeClass("border-danger");
    });
    $("#gender").on("click", function (event) {
        event.preventDefault();
        $("#gender").addClass("border-danger");
    });
    $("#gender").mouseleave(function () {
        $("#gender").removeClass("border-danger");
    });
});

// Edit Profile
var sellerURL = baseUrl + '/admin/users/update';
var user_delete_url = baseUrl + '/admin/user/delete';
$("#editProfileFormAppUser").submit(function (e) {

    e.preventDefault();
    let formData = new FormData($("#editProfileFormAppUser")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    var id=$('#id').val();
    var url=sellerURL+'/'+id;
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
            toastr.success('User profile updated successfully!');
            $("#edit-Profile").hide();
        },
        contentType: false,
        processData: false,
        success: function (response) {

            if (response.status == 400) {
                $.LoadingOverlay('hide');

                $("#edit-Profile").show();
                if (response.errors.name) {
                    $("#nameError").show();
                    $("#nameError").html(response.errors.name);
                } else if (!response.errors.name) {
                    $("#nameError").hide();
                }
                if (response.errors.gender) {
                    $("#genderError").show();
                    $("#genderError").html(response.errors.gender);
                } else if (!response.errors.email) {
                    $("#emailError").hide();
                }
                if (response.errors.address) {
                    $("#addressError").show();
                    $("#addressError").html(response.errors.address);
                } else if (!response.errors.address) {
                    $("#addressError").hide();
                }
                 if (response.errors.city) {
                    $("#cityError").show();
                    $("#cityError").html(response.errors.city);
                } else if (!response.errors.city) {
                    $("#cityError").hide();
                }
                 if (response.errors.state) {
                    $("#stateError").show();
                    $("#stateError").html(response.errors.state);
                } else if (!response.errors.state) {
                    $("#stateError").hide();
                }
               if (response.errors.country) {
                    $("#stateError").show();
                    $("#stateError").html(response.errors.country);
                } else if (!response.errors.country) {
                    $("#stateError").hide();
                }
                 if (response.errors.zipCode) {
                        $("#zipCodeError").show();
                        $("#zipCodeError").html(response.errors.zipCode);
                    } else if (!response.errors.zipCode) {
                        $("#zipCodeError").hide();
                    }
                if (response.errors.avatar) {
                    $("#avatarError").show();
                    $("#avatarError").html(response.errors.avatar);
                } else if (!response.errors.avatar) {
                    $("#avatarError").hide();
                }
            } else {
               $.LoadingOverlay('hide');
                location.reload(true);
             if (response.status == 200) {
               toastr.error('User profile updated successfully!');
             }
            }
        },
    });
});

  $(document).ready(function () {
          $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
// show the modal when delete button clicked
$('#deleteAppUser').on('click' ,function(e){
   e.preventDefault();
   $('#myModalDeleteUser').modal('show');
   var id=$('#id').val();
   $('#userDelete').on('click',function(){
        var user_Url= user_delete_url+'/'+id;
        $.ajax({
            type:'GET',
            url: user_Url,
              beforeSend: function ()
              {
                     $.LoadingOverlay('show');
              },
            success:function(response){
             $.LoadingOverlay('hide');
            $('#myModalDeleteUser').modal('hide');
              toastr.success(response.message);
               window.location.href = baseUrl + '/admin/users';
        },
         error: function(data) {
              $.LoadingOverlay('hide');
            $('#myModalDeleteUser').modal('show');

            toastr.error(data.message);
    }
    })
    });
});
});
$(document).ready(function () {
    var $search = $("#albumFilterSearch").on("input", function () {
        var matcher = new RegExp($(this).val(), "gi");
        $(".box")
            .show()
            .not(function () {
                return matcher.test($(this).find(".name").text());
            })
            .hide();
    });
});


