var sellerURL = baseUrl + '/admin/users/detail/';
var urlDetail = baseUrl + '/admin/usersDetail';
var defaultImage = baseUrl + '/defaultImages/default-avatar.png'
var imageUrl=baseUrl  + '/svg'

$('table').on('click', 'tbody .viewSeller', function() {
    var sellerId = $(this).attr('data-id')
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: sellerURL + sellerId + '/details',
        success: function(data) {
            if (data.status) {
                var userDetail = data.data;
                var plan = data.package;
                var getSong=userDetail.user_buying_histories;
                var userPlan=userDetail?userDetail.user_active_packages:null;
                if(userPlan==null){
                var planName='No Plan Selected';
                var planDuration='';
                var planPrice='';
                var customTag='';
                var customTagLin='';
                }else{
                    var planName=userPlan.package.name;
                    var planDuration=userPlan.package.duration;
                    var planPrice=userPlan.package.price;
                    var customTag='$';
                    var customTagLin='/';
                }
                var date = userDetail.formattedDate;
                var images = userDetail.images ? userDetail.images : null
                var img = userDetail.avatar? userDetail.avatarFullUrl : defaultImage
                var name = userDetail.firstName ? userDetail.firstName +' '+userDetail.lastName :'null';
                var userDetailsHtml = '';
                userDetailsHtml += '<div class="card-body box-profile"> ';
                userDetailsHtml += '<div class = "text-center" >';
                userDetailsHtml += '</div>';
                userDetailsHtml += ' <ul id="purchaseList" class="products-list product-list-in-card pl-0 pr-1">';
                userDetailsHtml += ' <li class="item pt-1 pb-1" style="border-bottom: none">';
                userDetailsHtml += ' <div class="product-img pt-1">';
                userDetailsHtml += ' <img src="' + img + '" class="border-radius8" alt="Product Image"  style="width: 45.5px;height:45.5px">';
                userDetailsHtml += ' </div>';
                userDetailsHtml += '<div class="product-info" style="margin-top: 4px">';
                userDetailsHtml +='<small  id="buyerName" class="product-title font-szie10">' + userDetail.name +'</small>';
                userDetailsHtml +='<small id="buyerEmail" class="product-description font-szie10 light-grey-text">' + userDetail.email +'</small>';
                userDetailsHtml += '<div>';
               userDetailsHtml +='</li>';
               userDetailsHtml +='</ul>';
               userDetailsHtml +='<div class="card-header pb-0" style="background-color: white; border-top-left-radius:8px !important;border-top-right-radius:8px !important;">';
               userDetailsHtml +='<div class="row ml-0">';
                userDetailsHtml += '<ul class="nav nav-pills card-header-pills nav-justified" id="bologna-list" style="margin-left: -35px" role="tablist">';
                userDetailsHtml += '<li style="margin-left:-44px"class="nav-item"><span style="margin-left: 17px" class="nav-link active" href="#menu1" data-toggle="tab">General info</span></li>&nbsp;&nbsp';
                userDetailsHtml += '<li class="nav-item" style="width:200px "><span class="nav-link" href="#menu2" data-toggle="tab">Buying History</span></li>&nbsp;&nbsp';
                userDetailsHtml += '</ul>';
                userDetailsHtml += '</div>';
                userDetailsHtml +='</div>';
                userDetailsHtml += '<div id="menu1" class="tab-pane fade active show" style="margin-top:12px">';
                userDetailsHtml +='<img style="margin-left: -26px" src="'+imageUrl+'/awesome-calendar-day.svg">&nbsp;&nbsp</img><label>Date of Birth</label> <span style="margin-left:38px;font-weight:600">'+date+'</span><br>'
                userDetailsHtml +='<img style="margin-left: -26px" src="'+imageUrl+'/awesome-phone.svg"></img>&nbsp;&nbsp<label>Phone</label> <span style="margin-left:65px;font-weight:600">'+userDetail.phoneNumber+'</span><br>'
                userDetailsHtml +='<img style="margin-left: -26px" src="'+imageUrl+'/ionic-md-male.svg"></img>&nbsp;&nbsp<label> Gender</label> <span style="margin-left:116px;font-weight:600">'+userDetail.gender+'</span>'
               userDetailsHtml += '<div style="margin-left: -20px">';
               userDetailsHtml += '<label> <strong  style="font-weight: 600;">Address</strong><hr/> </label><br>';
               userDetailsHtml += '<span>'+userDetail.address+'  </span>';
               userDetailsHtml += '</div>';
               userDetailsHtml += '<div style="margin-left: -20px">';
               userDetailsHtml += '<label><strong class="product-title" style="font-weight: 600;">User Type</strong><hr/> </label><br>';
               userDetailsHtml +='<div  class="card p-0 m-0 border-radius8 off-white " style="width:248px;background-color: #E8E8E8">';
               userDetailsHtml +='<div class="card-body m-2 m-0 p-0 border-radius8">';
               userDetailsHtml += '<span>Current Plan</span><br>';
               userDetailsHtml += '<label><strong class="product-title" style="font-weight: 800;">'+ planName+'</strong></label><br>';
               userDetailsHtml += '<span>'+customTag+ planPrice+customTagLin+planDuration+'</span>';
               userDetailsHtml +='</div>';
               userDetailsHtml +='</div></br>';
               userDetailsHtml += '</div>';
               userDetailsHtml +='<a  href="'+urlDetail+'/'+userDetail.id+'" style="width: 100%;border-radius:8px" class="btn btn-danger">View Full Details</a>';
               userDetailsHtml +='</div>';
               userDetailsHtml += '<div id="menu2" class="tab-pane fade ">';
               userDetailsHtml += '<div style="margin-top:-338px">';
               userDetailsHtml += '<span style="margin-left:-5px"> Last 30 Days</span> <a href="'+urlDetail+'/'+userDetail.id+'"><span style="margin-left:66px"> View All</a> </span><br><br>';
              $.each(getSong, function(i, item1) {
                var song=item1.get_songs;
                $.each(song ,function(i,item){
                  var imgSong = item.thumbnail? item.avatarFullUrl : defaultImage;
                    userDetailsHtml +='<ul  class="products-list product-list-in-card pl-0 pr-1">';
                    userDetailsHtml +='<li class="item pt-1 pb-1" style="border-bottom: none">';
                    userDetailsHtml +='<div class="product-img pt-1">';
                    userDetailsHtml +='<img src="'+ imgSong +'" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">';
                    userDetailsHtml +='</div>';
                    userDetailsHtml +='<div class="product-info ml-5">';
                    userDetailsHtml +='<small  class="product-title font-szie10 ">'+item.name+'<span class="float-right ">$'+item.price+'</span></small>';
                    userDetailsHtml +='<small class="product-description font-szie10 light-grey-text">'+item.songLength+'</small>';
                    userDetailsHtml +='</div>';
                    userDetailsHtml +='</li>';
                    userDetailsHtml +='</ul>';
                });
                 });
                userDetailsHtml +='</div>';
                userDetailsHtml += '</div>';
                userDetailsHtml += '</div>';
                userDetailsHtml += '';
                userDetailsHtml += '</div>';
                $('.seller-details').html(userDetailsHtml);
                            $.LoadingOverlay('hide')

                window.location = "#seller-details"
                $('body').addClass('sidebar-mini control-sidebar-slide-open')

            } else {
                toastr.error('Something went wrong!');
            }


        },
        error: function(data) {
            $.LoadingOverlay('hide')
            $('body').removeClass('sidebar-mini control-sidebar-slide-open')
            toastr.error('Something went wrong!')
        }

    });


});
$('#cancelBtn').click(function() {
    setTimeout(function(){
        location.reload();
    }, 200);
});


