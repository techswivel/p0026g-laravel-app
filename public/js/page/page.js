     $(document).ready(function() {
            var summernoteForm = $('.needs-validation');
            var summernoteElement = $('.summernote');
            var summernoteValidator = summernoteForm.validate({
                rules: {
                    title: {
                        required: true,
                        maxlength: 50
                    },
                    description: {
                        required: true,
                    },
                },
                messages: {

                    title: {
                        required: "Please enter title",
                        maxlength: "Your last name maxlength should be 50 characters long."
                    },
                    description: {
                        required: "The description is required",
                        // maxlength: "Your last name maxlength should be 50 characters long."
                    },
                },
                errorElement: "div",
                errorClass: 'is-invalid',
                validClass: 'is-valid',
                ignore: ':hidden:not(.summernote),.note-editable.card-block',
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("invalid-feedback");
                    // console.log(element);
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.siblings("label"));
                    } else if (element.hasClass("summernote")) {
                        error.insertAfter(element.siblings(".note-editor"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            $('.summernote').summernote({
                height: 200,
                // code
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height','codeview']],
                ],
                fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48'],
                placeholder: 'Write the description',
                required:true,
                callbacks: {
                    message: 'The content is required and cannot be empty',
                    onChange: function (description, $editable) {
                        // Note that at this point, the value of the `textarea` is not the same as the one
                        // you entered into the summernote editor, so you have to set it yourself to make
                        // the validation consistent and in sync with the value.
                        summernoteElement.val(summernoteElement.summernote('isEmpty') ? "" : description);

                        // You should re-validate your element after change, because the plugin will have
                        // no way to know that the value of your `textarea` has been changed if the change
                        // was done programmatically.
                        summernoteValidator.element(summernoteElement);
                    }
                }
            });
        });
