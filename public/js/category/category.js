$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
(function () {
    "use strict";
    window.addEventListener(
        "load",
        function () {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName("needs-validation");
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(
                forms,
                function (form) {
                    form.addEventListener(
                        "submit",
                        function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add("was-validated");
                        },
                        false
                    );
                }
            );
        },
        false
    );
})();
$("#editProfileForm").on('submit', (function(e) {
    e.preventDefault()
    $.LoadingOverlay("show");
    $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/categories',
        data: new FormData(this),
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success: function(data) {
            resetForm('editProfileForm')
            $.LoadingOverlay("hide");
            if (data.status == true) {
                toastr.success(data.data)
                $('#addMusicCategory').modal('hide');
                $(this).trigger("reset");
                    window.location.reload();
            } else {
                $.LoadingOverlay("hide");
                $('#addMusicCategory').modal('show');
                toastr.error(data.data)
            }

        },
        error: function(data) {
            $.LoadingOverlay("hide");
            var message = data.responseJSON ? data.responseJSON.response : null
            if (message != null) {
                toastr.error(data.responseJSON.response.message)
            }


        }

    });


}));
function resetForm(id) {
    $('.dropify-clear').trigger('click')
    document.getElementById(id).reset();
}
