$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});

$(document).ready(function () {
    var isd = $("#pageLoadId").val();
    $("#listingCat").click(listingCat(isd));
});

var defaultImage = baseUrl + "/defaultImages/default-avatar.png";

function listingCat(id) {
    $.get("/admin/categories/detail/" + id, function (response) {
        var getSong = response.data.category_songs;
        var getAlbums = response.albums;

        songs = getSong;
        albums = getAlbums;

        if (response.status == true) {
            var htmlDiv = "";
            var albumInPlaylist = "";
            var playlistSong = "";

            htmlDiv += ' <div class="card-header music-card-header p-0">';
            htmlDiv +=
                '<div class="row m-0 p-0" style="background-color: #E9B3C2;height: 193px;">';
            htmlDiv += '<div class="col-lg-6 col-md-6 col-sm-6 pl-4">';
            htmlDiv += '<div class="row music-category-header">';
            htmlDiv +=
                '<small class="text-white h4 border-radius8 p-2 pl-3 "  style="background-color:#5A1C52; width:180px">' +
                response.data.name +
                "</small>";
            htmlDiv +=
                '<span class="dot-edit-delete categoryEdit" onClick="categoryEdit(' +
                response.data.id +
                ')" ><i class="bi bi-pencil-fill"></i></span>';
            htmlDiv +=
                '<span class="dot-edit-delete" onClick="deleteMusicCategory(' +
                response.data.id +
                ')"><i class="bi bi-trash-fill"></i></span>';
            htmlDiv += "</div>";
            htmlDiv += ' <div class="row">';
            htmlDiv +=
                '<small class="text-white border-radius8 pr-1 pl-3 "  style="background-color:#5A1C52; width:135px;padding-top:1px;padding-bottom:1px"> ' +
                getSong.length +
                " Songs| " +
                response.albumCount +
                " Albums</small>";
            htmlDiv += "</div>";
            htmlDiv += "</div>";
            htmlDiv += '<div class="col-lg-6 col-md-6 col-sm-6 p-3">';
            htmlDiv +=
                '<div class="back" style="float:right;background-image: url(' +
                response.data.avatarFullUrl +
                ');  ">';
            htmlDiv += '<div class="text-center trans">';
            htmlDiv += "</div>";
            htmlDiv += "</div>";
            htmlDiv += "</div>";
            htmlDiv += "</div>";

            // tap_link button
            htmlDiv += '<div class="row m-0">';
            htmlDiv += '<div class="col-lg-9 col-md-9">';
            htmlDiv += '<ul class="nav nav-pills ml-auto " style="margin-left: -8px !important;margin-top: 4px !important;font-size: 11px;">';
            htmlDiv += '<li class="nav-item text-center" id="songtableSearch" style="width: 120px;"><span class="nav-link songtableSearch active"  href="#tab_1" data-toggle="tab" style="">All Songs</span></li>';
            htmlDiv += '<li class="nav-item text-center" style="width: 120px;"><span class="nav-link albumDivSearch" href="#tab_2" data-toggle="tab">All Albums</span></li>';
            htmlDiv += "</ul>";
            htmlDiv += "</div>";
            htmlDiv += '<div class="col-lg-3 col-md-3">';
            htmlDiv += '<input onClick="categoryFilterSearch()" type= "search" class="form-control  m-1 ml-0  float-right font-size12" id="categoryFilterSearch"  placeholder="Search">';
            htmlDiv += "</div>";
            htmlDiv += "</div>";
            // tap open song show code
            htmlDiv += '<div class="card-body p-0">';
            htmlDiv += '<div class="tab-content">';
            htmlDiv += '<div class="tab-pane active table-responsive" id="tab_1">';
            htmlDiv += '<table id="songTable" class="table display">';
            htmlDiv += ' <thead id="song-thead">';
            htmlDiv += "<tr>";
            htmlDiv += '<th style="width: 240px">';
            htmlDiv += "</th>";
            htmlDiv += "<th>";
            htmlDiv += "</th>";
            htmlDiv += "</tr>";
            htmlDiv += "</thead>";
            if (getSong.length == 0) {
                htmlDiv += "<tbody>";
                htmlDiv += '<tr style="background-color: white !important;">';
                htmlDiv += '<td  class="p-0 pl-2" style="width: 240px">';
                htmlDiv += "</td>";
                htmlDiv += "<td>";
                htmlDiv += "</td>";
                htmlDiv += "<td> No Records Found";
                htmlDiv += "</td>";
                htmlDiv += "</tr>";
                htmlDiv += "</tbody>";

                playlistSong += `<p class="text-center w-100">No Song Found!</p>`;


            } else {
                htmlDiv += "<tbody>";
                playlistSong = ''
                $.each(getSong, function (i, item) {


                    if (item.isPaid == 1) {
                        var paid = `<span class="dot-prime" style="margin-left: -26px"><i class="fas fa-crown"></i></span>
                                    <small class="product-title font-szie10">${item.name}
                                        <span class="light-grey-text font-weight-lighter"> (Paid. $${item.price})</span>
                                        <i class="bi bi-play-circle-fill mediaPlayer playInTable" id="playInTable${item.id}" onclick="playMusic(${item.id})"></i>
                                    </small>
                                    <small class="isPaided product-description font-szie10 light-grey-text" style="margin-left: -5px !important">${item.songLength}</small>`;
                    } else {
                        var paid = `<small class="product-title font-szie10" style="margin-left: -5px !important">${item.name}
                                        <span class="light-grey-text font-weight-lighter" ></span>
                                        <i class="bi bi-play-circle-fill mediaPlayer playInTable" id="playInTable${item.id}" onclick="playMusic(${item.id})"></i>
                                    </small>
                                    <small class="isPaided product-description font-szie10 light-grey-text" style="margin-left: -5px !important">${item.songLength}</small>`;

                    }

                    if (item.videoFile) {
                        var video = '<span class="p-1 pl-2 pr-2 text-Audio text-video border-radius8 font-size12">Video</span>';
                    }else{
                        var video = ''
                    }

                    var imgSong = item.thumbnail ? item.avatarFullUrl : defaultImage;

                    htmlDiv += `<tr style="background-color: white !important;">
                        <td  class="p-0 pl-2" style="width: 240px">
                            <ul class="products-list product-list-in-card pl-2 pr-1">
                                <li class="item">
                                    <div class="product-img pt-1">
                                        <img src="${imgSong}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                    </div>
                                    <div class="product-info ml-5 w-100">
                                        ${paid}
                                    </div>
                                </li>
                            </ul>
                        </td>
                        <td style="padding-left:350px;">
                            <span class=" p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                        </td>
                        <td class="pl-0 pr-5">
                            ${video}
                        </td>
                    </tr>`




                    if (item.isPaid) {
                        var songText = `<span class="dot-prime"><i class="fas fa-crown"></i></span>
                                        <small class="font-szie10 position-absolute ml-2">${item.get_artist.name}</small>
                                        <p class="font-szie14 font-weight-bold m-0 position-absolute top-18px ml-4">${item.name}</p>`
                    } else {
                        var songText = `<small class="font-szie10 position-absolute ml-4">${item.get_artist.name}</small>
                                        <p class="font-szie14 font-weight-bold m-0 position-absolute top-18px ml-4 songNameInAlbumList">${item.name}</p>`
                    }

                    playlistSong += `<div class="row my-3">
                                <div class="col-md-10 col-10">
                            <div class="row no-gutters">
                                <div class="col-md-1 col-1">
                                    <img src="${item.avatarFullUrl}" class="border-radius8" alt="Product Image" style="width: 35.5px;height:35.5px">
                                </div>
                                <div class="col-md-10 col-10 h-0">
                                    ${songText}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-2 text-center">
                            <i onclick="playMusic(${item.id})" id="playInSongListModal${item.id}" class="bi bi-play-circle-fill mediaPlayer playPause playInSongListModal"></i>
                        </div>
                    </div>`;


                });

                htmlDiv += "</tbody>";
            }

            htmlDiv += "</table>";
            htmlDiv += "</div>";
            //second tab
            htmlDiv += '<div class="tab-pane" id="tab_2">';
            htmlDiv += ' <div class="row m-0 mt-4" >';

            var albumcheckCount = response.albumCount;
            if (albumcheckCount == 0) {
                    htmlDiv +=  ' <span  class="name" style="font-size: 12px;color:black;margin-left:200px">No Records found </span>';
            } else {
                $.each(getAlbums, function (i, item) {

                    htmlDiv += `<div class=" box ml-2" style="width: 88px" id="albumSearchCat">
                                    <center class=" float-left">
                                        <a href="#" onclick="albumDetailCat(${item.id})">
                                            <img src="${item.avatarFullUrl}"  class="border-radius5" width="80px" height="80px" alt="">
                                            <small  class="name" style="font-size: 10px;color:black">${item.name} </small>
                                        </a>
                                    </center>
                                </div>`

                    albumInPlaylist += `<div class="swiper-slide cursor-pointer" onclick="albumSongs(${item.id})">
                                            <img src="${item.avatarFullUrl}" class="border-radius8" width="120px"
                                                height="70px" alt="">
                                            <div class="overlay-wrap border-radius8">
                                                <div class="overlay-text">${item.name}</div>
                                            </div>
                                        </div>`

                });

            }

            htmlDiv += "</div>";
            htmlDiv += "</div>";
            htmlDiv += "</div>";
            htmlDiv += "</div>";
            //end div
            htmlDiv += "</div>";


            $("#categoryDetail").html(htmlDiv);
            $(".songListsModal").html(playlistSong);
            if (albumcheckCount != 0) {
                $(".swiper-wrapper").html(albumInPlaylist);

                var swiper = new Swiper(".mySwiper", {
                    slidesPerView: 3,
                    spaceBetween: 10,
                    loop: false,
                    loopFillGroupWithBlank: true,
                    pagination: {
                        el: ".swiper-pagination",
                        clickable: true,
                    },
                    navigation: {
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev",
                    },

                });

            }else{
                $(".swiper-wrapper").html(`<p class="text-center w-100">No Album Found!</p>`)
            }



        } else {
        }
    });

}

function albumDetailCat(id) {
    $.get("/admin/detail" + "/" + id, function (response) {
        if (response.count == 0) {
            image = `<div>
                        <img src="${response.album.avatarFullUrl}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                    </div> </div>
                    <div class="col-8 pl-3">
                        <div class="p-1 pl-0" id="song-title-text" >`;
            if (response.album.albumStatus == "Paid") {

            image =`<div>
                        <img src="${response.album.avatarFullUrl}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                    </div>
                    </div>
                    <div class="col-1 pl-3">
                    <span class="dot-prime"><i class="fas fa-crown"></i></span>
                    </div>
                    <div class="col-7 pl-3">
                        <div class="p-1 pl-0" id="song-title-text" style="margin-left:-42px">`;
            }
            $("#albumThumbnail").html(
                `<div class="col-1">
                   ` +
                    image +
                    `
                            <small class="d-flex align-items-start" style="font-weight: 500">` +
                    response.album.name +
                    `
                            </small>
                            <small class="d-flex align-items-end " style="font-size:9px;">` +
                    response.count +
                    ` Songs</small>
                        </div>
                    </div>
                    <div class="col-3  ">
                        <div class="float-right">
                            <button type="button" class="close pt-0" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`
            );
            document.getElementById(
                "albumSongListTable"
            ).innerHTML = `<tbody id="albumSongListTableBody">
            </tbody>
            `;
            var userDetailsHtml = "";
            userDetailsHtml +=
                '<div class="card-body box-profile" style="display:block"> ';
            userDetailsHtml += '<div class = "text-center">';
            userDetailsHtml += "<strong>No records found</strong>";
            userDetailsHtml += "</div>";
            userDetailsHtml += "</div>";
            $(".no-record").html(userDetailsHtml);
            $(".no-record").addClass("d-block");
            $("#albumDetail").modal("toggle");
        } else {
            if (response.status == 200) {
                image =
                    ` <div >
                        <img src="` +
                    response.album.avatarFullUrl +
                    `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                </div>

                </div>
                <div class="col-8 pl-3">
                    <div class="p-1 pl-0" id="song-title-text" >`;
                if (response.album.albumStatus == "Paid") {
                    image =
                        ` <div >
                        <img src="` +
                        response.album.avatarFullUrl +
                        `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                </div>

                </div>
                <div class="col-1 pl-3">
                <span class="dot-prime"><i class="fas fa-crown"></i></span>
                </div>
                <div class="col-7 pl-3">
                    <div class="p-1 pl-0" id="song-title-text" style="margin-left:-42px">`;
                }
                $("#albumThumbnail").html(
                    `<div class="col-1">
               ` +
                        image +
                        `
                        <small class="d-flex align-items-start" style="font-weight: 500">` +
                        response.album.name +
                        `
                        </small>
                        <small class="d-flex align-items-end " style="font-size:9px;">` +
                        response.count +
                        ` Songs</small>
                    </div>
                </div>
                <div class="col-3  ">
                    <div class="float-right">
                        <button type="button" class="close pt-0" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>`
                );
                document.getElementById(
                    "albumSongListTable"
                ).innerHTML = `<tbody id="albumSongListTableBody">
        </tbody>
        `;
                response.songs.forEach(function log(song) {
                    paid =
                        '<small class="product-description font-szie10 light-grey-text">Free song</small>';
                    length =
                        '<small class="product-description font-szie10 light-grey-text">' +
                        song.songLength +
                        "</small>";
                    image =
                        `<img src="` +
                        song.avatarFullUrl +
                        `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">`;
                    if (song.isPaid) {
                        paid =
                            '<small class="product-description font-szie10 light-grey-text">Paid. $' +
                            song.price +
                            "</small>";
                        image =
                            `<img src="` +
                            song.avatarFullUrl +
                            `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px"><span class="dot-prime" style="margin-left: -10px"><i class="fas fa-crown"></i></span>`;
                    }
                    isVideo = `    <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                    `;
                    if (song.videoFile) {
                        isVideo = `    <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                    `;
                    }
                    document.getElementById("albumSongListTable").innerHTML +=
                        `
                <tr id="albumSongList` +
                        song.id +
                        `">
                    <td  class="p-0 pl-2">
                        <ul class="products-list product-list-in-card pl-2 pr-1">
                            <li class="item">
                                <div class="product-img pt-1">
                                    ` +
                        image +
                        `
                                </div>
                                <div class="product-info ml-5">
                                    <small  class="product-title font-szie10">` +
                        song.name +
                        `

                                    </small>
                                    ` +
                        length +
                        `
                                </div>
                            </li>
                        </ul>
                    </td>
                    <td class="pl-0 pr-0">
                        ` +
                        isVideo +
                        `
                    </td>
                    <td></td>
                    <td class="pb-2" >

                          ` +
                        paid +
                        `
                    </td>
                </tr>
            `;
                });
                $("#albumDetail").modal("toggle");
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        }
    });
}

function categoryEdit(id) {
    $("#editMusicCategory").modal("show");
    $("#updateCategoriesForm").attr(
        "action",
        baseUrl + "/admin/categories/" + id + "/update"
    );
    $.ajax({
        type: "get",
        dataType: "json",
        url: baseUrl + "/admin/categories/" + id + "/edit",
        success: function (data) {
            var subcategories = data.data;
            $('input[name="name"]').val(subcategories.name);
            $(".test img").attr("src", function () {
                return (
                    subcategories.avatarFullUrl +
                    "?Action=thumbnail&Width=80&Height=80"
                );
            });
        },
        error: function (data) {
            $("#editMusicCategory").modal("show");
            $.LoadingOverlay("hide");
            toastr.error("Something went wrong!");
        },
    });
}

function deleteMusicCategory(id) {
    $("#deleteMusicCategory").modal("show");
    var id = id;
    $("#playlistTrash12").on("click", function (e) {
        e.preventDefault();
        var url = baseUrl + "/admin/categories-delete/" + id;
        $.LoadingOverlay("show");
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                if (response.status == true) {
                    $.LoadingOverlay("hide");
                    $("#deleteMusicCategory").modal("hide");
                    toastr.success(response.data);
                    window.location.reload();
                } else {
                    console.log("song hire");
                    $.LoadingOverlay("hide");
                    $("#deleteMusicCategory").modal("hide");
                    $("#NotdeleteMusicCategory").modal("show");
                }
            },
            error: function (data) {
                $.LoadingOverlay("hide");
                $("#deleteMusicCategory").modal("show");
                toastr.error(data.message);
            },
        });
    });
}
$("#NotplaylistTrash").on("submit", function () {
    $("#NotdeleteMusicCategory").modal("hide");
});
$("#updateCategoriesForm").on("submit", function () {
    $.LoadingOverlay("show");
});
function preview() {
    frame.src = URL.createObjectURL(event.target.files[0]);
}

function categoryFilterSearch() {
    $("#categoryFilterSearch").val("");
    var $search = $("#categoryFilterSearch").on("input", function () {
        var value = $(this).val().toLowerCase();
        $("#songTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });

        try {
            var matcher = new RegExp($(this).val(), "gi");
            $(".box")
                .show()
                .not(function () {
                    return matcher.test($(this).find(".name").text());
                })
                .hide();
        } catch (error) {}
    });
}
