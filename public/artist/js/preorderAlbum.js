$("#addAlbumPreorderForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#addAlbumPreorderForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/preorder/album",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 400) {
                var errors = [
                    "preorderAlbumName",
                    "preorderAlbumThumbnail",
                    "preorderAlbumDate",
                    "preorderAlbumTime",
                    "preorderSelectedSongs",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else if (response.status == 500) {
                toastr.error(response.message);
            } else {
                $("#addAlbumPreorderForm")[0].reset();
                $("#addAlbumPreorderModal").modal("toggle");
                location.reload(true);
            }
        },
    });
});


$('.add-preorder-album-next-btn').on('click', function(){
    $("#addPreorderAlbumSongStepNo").val(1)
    let formData = new FormData($("#addAlbumPreorderForm")[0]);

    if($("#addPreorderAlbumId").val().length == 0){
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $.ajax({
            url: "/" + url + "/preorder/album",
            type: "POST",
            data: formData,
            beforeSend: function () {
                $.LoadingOverlay("show");
            },
            contentType: false,
            processData: false,
            success: function (response) {
                $.LoadingOverlay("hide");
                if (response.status == 400) {
                    var errors = [
                        "preorderAlbumName",
                        "preorderAlbumThumbnail",
                        "preorderAlbumDate",
                        "preorderAlbumTime",
                        "preorderSelectedSongs",
                    ];
                    for (var i = 0; i < errors.length; i++) {
                        hideErrorMessage(errors[i]);
                    }
                    for (var [key, value] of Object.entries(response.errors)) {
                        showErrorMessage(value[0], key);
                    }
                } else if (response.status == 500) {
                    toastr.error(response.message);
                } else {

                    $('.add-preorder-album-progress-step-1').removeClass(' active')
                    $('.add-preorder-album-progress-step-2').addClass(' active')

                    $('#add-album-pills-home').removeClass('show active')
                    $('#add-album-pills-profile').addClass('show active')
                    $("#addPreorderAlbumSongStepNo").val(2)


                    $("#addPreorderAlbumId").val(response.album[1].albumId)

                    var html = ``;
                    $.each(response.album, function(index,price){
                        parseInt(index) == 1 ? $("#albumIAppPurchasePrice").val(price.customerPrice) : '';
                        html += `<option value="${price.priceTier}">${price.customerPrice}</option>`
                    });

                    $("#addAlbumPreorderPrice").html(html);
                    $("#albumPreorderSongsCost").html('$'+response.album[1].songsCost);

                }
            },
        });
    }else{
        $('.add-preorder-album-progress-step-1').removeClass(' active')
        $('.add-preorder-album-progress-step-2').addClass(' active')

        $('#add-album-pills-home').removeClass('show active')
        $('#add-album-pills-profile').addClass('show active')
        $("#addPreorderAlbumSongStepNo").val(2)
    }

});

$('.add-preorder-album-previous-btn').on('click', function(){
    $('.add-preorder-album-progress-step-2').removeClass(' active')
    $('.add-preorder-album-progress-step-1').addClass(' active')

    $('#add-album-pills-profile').removeClass('show active')
    $('#add-album-pills-home').addClass('show active')

    $("#preorderAlbumSongStepNo").val(1)
});


$("#addAlbumPreorderPrice").on('change', function(){
    $("#albumIAppPurchasePrice").val($('option:selected',this).text());
});


$("#editAlbumPreorderPrice").on('change', function(){
    $("#editAlbumInAppPurchasePrice").val($('option:selected',this).text());
});



function storeAlbumSong() {
    $("#modalStatus").val("AddModal");
    $("#addAlbumPreorderModal").modal("toggle");
    $("#addPreorderSongModal").modal("toggle");
}

function editStoreAlbumSong() {
    $("#modalStatus").val("editModal");
    $("#editAlbumPreorderModal").modal("toggle");
    $("#addPreorderSongModal").modal("toggle");
}

$(".preorderAlbumSongCanelbutton").click(function () {
    $("#addPreorderSongModal").modal("toggle");
});

$(".editPreorderAlbumSongCanelbutton").click(function () {
    $("#editPreorderSongModal").modal("toggle");
});

$("#albumSongPreorderStatus").val("Paid Song");

function setPreorderAlbumSongStatusValue() {
    var conceptName = $("#editAlbumSongPreorderStatus")
        .find(":selected")
        .text();
    if (conceptName == "Free Song") {
        $("#editPreorderprice-div").hide(300);
    } else if (conceptName == "Paid Song") {
        $("#editPreorderprice-div").show(300);
    }
}

function removePreorderAlbumSong(id) {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/preorder/album/song/" + id,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 200) {
                toastr.success(response.message);
                $(`.albumSongRemove` + id + ``).remove();
                $("#preorderSelectedSongs option[value=" + id + "]").remove();
                $("#preorderAlbumSongReomve" + id).remove();
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
}

function editPreorderAlbumSong(id) {
    $.LoadingOverlay("show");
    $.get("/" + url + "/song/" + id, function (response) {
        $("#editPreorderAlbumSongid").val(id);
        $("#editModalStatus").val("AddModal");
        $("#edtiAlbumSongPreorderName").val(response.name);
        if (response.isPaid) {
            $("#editAlbumSongPreorderStatus").val("Paid Song");
            $("#editAlbumSongPreorderPrice").val(response.price);
            $("#editPreorderprice-div").show();
        } else {
            $("#editAlbumSongPreorderStatus").val("Free Song");
            $("#editPreorderprice-div").hide();
            $("#editAlbumSongPreorderPrice").val("");
        }
        $("#editAlbumSongPreorderCategory").val(response.categoryId);
        $("#editAlbumSongPreorderLanguage").val(response.languageId);
        $("#editAlbumSongPreorderLyrics").val(response.lyrics);
        $("#editAlbumSongPreorderThumbnailDisplay").css(
            "background-image",
            "url(" + thumbnailUrl(response.thumbnail) + ")"
        );
        if (response.videoFile) {
            $(".videoFormRound").show();
            $(".videoForm").hide();
        } else {
            $(".videoFormRound").hide();
            $(".videoForm").show();
        }

        var html = ``;
        $.each(response.prices, function(index,price){
            html += `<option value="${price.priceTier}" ${price.customerPrice == parseFloat(response.price) ? 'selected' : ''}>${price.customerPrice}</option>`
        });

        $("#editAlbumSongPreorderPriceTier").html(html);


        $.LoadingOverlay("hide");
        $("#addAlbumPreorderModal").modal("toggle");
        $("#editPreorderSongModal").modal("toggle");
    });
}

function updateEditPreorderAlbumSong(id) {
    $.LoadingOverlay("show");
    $.get("/" + url + "/song/" + id, function (response) {
        $("#editSongToAlbumPreorderForm")[0].reset();
        var errors = [
            "edtiAlbumSongPreorderName",
            "editAlbumSongPreorderStatus",
            "editAlbumSongPreorderPrice",
            "editAlbumSongPreorderCategory",
            "editAlbumSongPreorderLanguage",
            "editAlbumSongPreorderThumbnail",
            "editAlbumSongPreorderAudio",
            "editAlbumSongPreorderVideo",
            "editAlbumSongPreorderVideo1",
            "editAlbumSongPreorderLyrics",
        ];
        for (var i = 0; i < errors.length; i++) {
            hideErrorMessage(errors[i]);
        }
        $("#editPreorderAlbumSongid").val(id);
        $("#editModalStatus").val("EditModal");
        $("#edtiAlbumSongPreorderName").val(response.name);
        if (response.isPaid) {
            $("#editAlbumSongPreorderStatus").val("Paid Song");
            $("#editAlbumSongPreorderPrice").val(response.price);
            $("#editPreorderprice-div").show();
        } else {
            $("#editAlbumSongPreorderStatus").val("Free Song");
            $("#editAlbumSongPreorderPrice").val("");
            $("#editPreorderprice-div").hide();
        }
        $("#editAlbumSongPreorderCategory").val(response.categoryId);
        $("#editAlbumSongPreorderLanguage").val(response.languageId);
        $("#editAlbumSongPreorderLyrics").val(response.lyrics);
        $("#editAlbumSongPreorderThumbnailDisplay").css(
            "background-image",
            "url(" + thumbnailUrl(response.thumbnail) + ")"
        );
        if (response.videoFile) {
            $(".videoFormRound").show();
            $(".videoForm").hide();
        } else {
            $(".videoFormRound").hide();
            $(".videoForm").show();
        }

        var html = ``;
        $.each(response.prices , function(index,price){
            html += `<option value="${price.priceTier}" ${price.customerPrice == parseFloat(response.price) ? 'selected' : ''}>${price.customerPrice}</option>`
        });

        $("#editAlbumSongPreorderPriceTier").html(html);


        $.LoadingOverlay("hide");
        $("#editAlbumPreorderModal").modal("toggle");
        $("#editPreorderSongModal").modal("toggle");
    });
}

function removeEditPreorderAlbumSong(id) {
    preorderSelectSongName = document.getElementById(
        "deleteAlbumPreorderSelectedSongs"
    );
    const option = new Option(id, id, false, true);
    // add it to the list
    preorderSelectSongName.add(option, undefined);
    $(`.editAlbumSongRemove` + id + ``).remove();
    $("#editPreorderSelectedSongs option[value=" + id + "]").remove();
}

$("#addSongToAlbumPreorderForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#addSongToAlbumPreorderForm")[0]);

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/preorder/album/song",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 400) {
                var errors = [
                    "albumSongPreorderName",
                    "albumSongPreorderStatus",
                    "albumSongPreorderPrice",
                    "albumSongPreorderCategory",
                    "albumSongPreorderLanguage",
                    "albumSongPreorderThumbnail",
                    "albumSongPreorderAudio",
                    "albumSongPreorderVideo",
                    "albumSongPreorderLyrics",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else if (response.status == 500) {
                toastr.error(response.message);
            } else if (response.status == 300) {
                song = response.song;
                preorderSelectSongName = document.getElementById(
                    "editPreorderSelectedSongs"
                );
                const option = new Option(song.id, song.id, false, true);
                // add it to the list
                preorderSelectSongName.add(option, undefined);
                document.getElementById(
                    "editPreorderalbumSongList"
                ).innerHTML +=
                    `
                <div class="col-2 pb-1 editAlbumSongRemove` +
                    song.id +
                    ` " id="albumSongImageEditModal` +
                    song.id +
                    `">
                                <img src="` +
                    thumbnailUrl(song.thumbnail) +
                    `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                            </div>
                            <div class="col-7 p-2 editAlbumSongRemove` +
                    song.id +
                    `" id="albumSongNameEditModal` +
                    song.id +
                    `">
                                <small>` +
                    song.name +
                    `</small>
                            </div>
                            <div class="col 2 p-2 pr-3 editAlbumSongRemove` +
                    song.id +
                    `">
                                <i class="float-right red-text bi bi-pencil-fill" onclick="editPreorderAlbumSong(` +
                    song.id +
                    `)"></i>
                            </div>
                            <div class="col-1 p-2 editAlbumSongRemove` +
                    song.id +
                    `">
                                <i class="float-right bi bi-x-lg" onclick="removeEditPreorderAlbumSong(` +
                    song.id +
                    `)"></i>
                            </div>
                `;
                $("#addSongToAlbumPreorderForm")[0].reset();
                $("#addPreorderSongModal").modal("toggle");
                $("#editAlbumPreorderModal").modal("toggle");
            } else {
                song = response.song;
                preorderSelectSongName = document.getElementById(
                    "preorderSelectedSongs"
                );
                const option = new Option(song.id, song.id, false, true);
                // add it to the list
                preorderSelectSongName.add(option, undefined);
                document.getElementById("preorderalbumSongList").innerHTML +=
                    `
                    <div class="col-2 pb-1 albumSongRemove` +
                    song.id +
                    ` ">
                                    <img src="` +
                    thumbnailUrl(song.thumbnail) +
                    `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                </div>
                                <div class="col-7 p-2 albumSongRemove` +
                    song.id +
                    `">
                                    <small>` +
                    song.name +
                    `</small>
                                </div>
                                <div class="col 2 p-2 pr-3 albumSongRemove` +
                    song.id +
                    `">
                                    <i class="float-right red-text bi bi-pencil-fill" onclick="editPreorderAlbumSong(` +
                    song.id +
                    `)"></i>
                                </div>
                                <div class="col-1 p-2 albumSongRemove` +
                    song.id +
                    `">
                                    <i class="float-right bi bi-x-lg" onclick="removePreorderAlbumSong(` +
                    song.id +
                    `)"></i>
                                </div>
                    `;
                $("#addSongToAlbumPreorderForm")[0].reset();
                $('.add-preorder-album-song-progress-step-2').removeClass(' active')
                $('.add-preorder-album-song-progress-step-1').addClass(' active')

                $('#preorder-album-song-pills-profile').removeClass('show active')
                $('#preorder-album-song-pills-home').addClass('show active')

                $("#preorderAlbumSongStepNo").val(1)
                $("#preorderAlbumSongId").val('')
                $("#preorderAlbumSongPrice").val('')

                $("#addPreorderSongModal").modal("toggle");
                $("#addAlbumPreorderModal").modal("toggle");
            }
        },
    });
});

function deletePreorderAlbum(id) {
    $("#deletePreorderAlbumModal").modal("toggle");
    deletedPreorderAlbumId = id;
}

$("#deletePreorderAlbumForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/preorder/album/" + deletedPreorderAlbumId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 200) {
                toastr.success(response.message);
                $("#deletePreorderAlbumModal").modal("toggle");
                location.reload();
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
});

function editPreorderAlbum(id) {
    $.LoadingOverlay("show");
    $.get("/" + url + "/preorder/album/" + id, function (response) {
        $.LoadingOverlay("hide");
        if (response.status == 500) {
            toastr.error(response.message);
        } else {
            $("#editAlbumPreorderForm")[0].reset();
            var errors = [
                "editPreorderAlbumName",
                "editPreorderAlbumThumbnail",
                "editPreorderAlbumDate",
                "editPreorderAlbumTime",
                "editPreorderSelectedSongs",
            ];
            for (var i = 0; i < errors.length; i++) {
                hideErrorMessage(errors[i]);
            }
            $("#editPreorderAlbumId").val(id);
            $("#editPreorderAlbumName").val(response.album.name);
            $("#editPreorderAlbumThumbnailDisplay").css(
                "background-image",
                "url(" + thumbnailUrl(response.album.thumbnail) + ")"
            );
            $("#deleteAlbumPreorderSelectedSongs").empty();
            $("#editPreorderAlbumDate").val(response.album.date);
            $("#editPreorderAlbumTime").val(response.album.time);
            $("#editPreorderSelectedSongs").empty();
            editPreorderSelectSongName = document.getElementById(
                "editPreorderSelectedSongs"
            );
            document.getElementById("editPreorderalbumSongList").innerHTML = ``;
            response.songs.forEach(function log(song) {
                albumSong = song.song;
                const option = new Option(
                    albumSong.id,
                    albumSong.id,
                    false,
                    true
                );
                // add it to the list
                editPreorderSelectSongName.add(option, undefined);
                document.getElementById(
                    "editPreorderalbumSongList"
                ).innerHTML +=
                    `
                <div id="albumSongImageEditModal` +
                    albumSong.id +
                    `" class="col-2 pb-1 editAlbumSongRemove` +
                    albumSong.id +
                    ` ">
                        <img src="` +
                    thumbnailUrl(albumSong.thumbnail) +
                    `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                    </div>
                        <div id="albumSongNameEditModal` +
                    albumSong.id +
                    `" class="col-7 p-2 editAlbumSongRemove` +
                    albumSong.id +
                    `">
                    <small>` +
                    stringTrim(albumSong.name, 25) +
                    `</small>
                    </div>
                <div class="col 2 p-2 pr-3 editAlbumSongRemove` +
                    albumSong.id +
                    `">
                <i class="float-right red-text bi bi-pencil-fill" onclick="updateEditPreorderAlbumSong(` +
                    albumSong.id +
                    `)"></i>
                </div>
                <div class="col-1 p-2 editAlbumSongRemove` +
                    albumSong.id +
                    `">
                    <i class="float-right bi bi-x-lg" onclick="removeEditPreorderAlbumSong(` +
                    albumSong.id +
                    `)"></i>
                    </div>
                    `;
            });

            $("#editAlbumInAppPurchasePrice").val(response.album.iAppPurchasePrice)

            var html = ``;
            $.each(response.album.prices , function(index,price){
                html += `<option value="${price.priceTier}" ${price.customerPrice == parseFloat(response.album.iAppPurchasePrice) ? 'selected' : ''}>${price.customerPrice}</option>`
            });

            $("#editAlbumPreorderPrice").html(html);
            $("#editAlbumSongsCost").html('$'+response.album.prices[1].songsCost);


            $("#editAlbumPreorderModal").modal("toggle");
        }
    });
}

$(function () {
    $("#editAlbumSongPreorderThumbnail").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById(
            "editAlbumSongPreorderThumbnailDisplay"
        ).style.backgroundImage = "url(" + tmppath + ")";
    });
});

$(function () {
    $("#editPreorderAlbumThumbnail").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById(
            "editPreorderAlbumThumbnailDisplay"
        ).style.backgroundImage = "url(" + tmppath + ")";
    });
});

function addPreorderAlbum(id) {
    $.LoadingOverlay("show");
    $.get("/" + url + "/preorder/albumSong/" + id, function (response) {
        $("#preorderSelectedSongs").empty();
        preorderSelectSongName = document.getElementById(
            "preorderSelectedSongs"
        );
        document.getElementById("preorderalbumSongList").innerHTML = ``;
        response.preorderAlbumSongs.forEach(function log(song) {
            const option = new Option(song.id, song.id, false, true);
            // add it to the list
            preorderSelectSongName.add(option, undefined);
            document.getElementById("preorderalbumSongList").innerHTML +=
                `
                    <div class="col-2 pb-1 albumSongRemove` +
                song.id +
                ` " id="albumSongImageAddModal` +
                song.id +
                `">
                    <img src="` +
                thumbnailUrl(song.thumbnail) +
                `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                </div>
                <div class="col-7 p-2 albumSongRemove` +
                song.id +
                `" id="albumSongNameAddModal` +
                song.id +
                `">
                    <small>` +
                stringTrim(song.name, 25) +
                `</small>
                </div>
                <div class="col 2 p-2 pr-3 albumSongRemove` +
                song.id +
                `">
                    <i class="float-right red-text bi bi-pencil-fill" onclick="editPreorderAlbumSong(` +
                song.id +
                `)"></i>
                </div>
                <div class="col-1 p-2 albumSongRemove` +
                song.id +
                `">
                        <i class="float-right bi bi-x-lg" onclick="removePreorderAlbumSong(` +
                song.id +
                `)"></i>
                </div>
                    `;
        });
        $.LoadingOverlay("hide");
        $("#addAlbumPreorderModal").modal("toggle");
    });
}

function setPreorderAlbumStatusValue() {
    var preorderType = $("#preorderTypeAlbum").find(":selected").text();
    if (preorderType == "Song") {
        $("#addAlbumPreorderModal").modal("toggle");
        $("#addSongPreorderModal").modal("toggle");
    }
}

function resetPreorderAlbumStatusValue() {
    var preorderType = $("#preorderTypeAlbum").find(":selected").text();
    if (preorderType == "Song") {
        $("#editAlbumPreorderModal").modal("toggle");
        $("#addSongPreorderModal").modal("toggle");
    }
}

$("#editSongToAlbumPreorderForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editSongToAlbumPreorderForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/preorder/album/song/update",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 400) {
                var errors = [
                    "edtiAlbumSongPreorderName",
                    "editAlbumSongPreorderStatus",
                    "editAlbumSongPreorderPrice",
                    "editAlbumSongPreorderCategory",
                    "editAlbumSongPreorderLanguage",
                    "editAlbumSongPreorderThumbnail",
                    "editAlbumSongPreorderAudio",
                    "editAlbumSongPreorderVideo",
                    "editAlbumSongPreorderVideo1",
                    "editAlbumSongPreorderLyrics",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else if (response.status == 500) {
                toastr.error(response.message);
            } else if (response.status == 200) {
                $(`#albumSongImageAddModal` + response.song.id + ``).html(
                    `<img src=" ` +
                        thumbnailUrl(response.song.thumbnail) +
                        `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                `
                );
                $(`#albumSongNameAddModal` + response.song.id + ``).html(
                    `    <small>` +
                        stringTrim(response.song.name, 25) +
                        `</small>
                `
                );
                $("#editSongToAlbumPreorderForm")[0].reset();
                $("#editPreorderSongModal").modal("toggle");
                $("#addAlbumPreorderModal").modal("toggle");
            } else {
                $(`#albumSongImageEditModal` + response.song.id + ``).html(
                    `<img src=" ` +
                        thumbnailUrl(response.song.thumbnail) +
                        `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                `
                );
                $(`#albumSongNameEditModal` + response.song.id + ``).html(
                    `    <small>` +
                        stringTrim(response.song.name, 25) +
                        `</small>
                `
                );
                $("#editSongToAlbumPreorderForm")[0].reset();
                $("#editPreorderSongModal").modal("toggle");
                $("#editAlbumPreorderModal").modal("toggle");
            }
        },
    });
});

$("#editAlbumPreorderForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editAlbumPreorderForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/preorder/album/update",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 400) {
                var errors = [
                    "editPreorderAlbumName",
                    "editPreorderAlbumThumbnail",
                    "editPreorderAlbumDate",
                    "editPreorderAlbumTime",
                    "editPreorderSelectedSongs",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else if (response.status == 500) {
                toastr.error(response.message);
            } else {
                $("#editAlbumPreorderForm")[0].reset();
                $("#editAlbumPreorderModal").modal("toggle");
                location.reload(true);
            }
        },
    });
});

function preorderAlbumPurchaseList(id) {
    $.LoadingOverlay("show");
    $.get("/" + url + "/preorder/album/buyerList/" + id, function (response) {
        if (response.status == 200) {
            $("#preorderAlbumInDetail").html(
                `
        <div class="col-1">
            <div>
                <img src="` +
                    thumbnailUrl(response.album.thumbnail) +
                    `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
            </div>
        </div>
        <div class="col-8 pl-3">
            <div class="p-1 pl-0" id="song-title-text">
                <small class="d-flex align-items-start" style="font-weight: 500">` +
                    response.album.name +
                    `<small class="ml-2 mt-1" style="font-size:9px"> (Price $` +
                    response.sum.toFixed(2) +
                    `)</small></small>
                <small class="d-flex align-items-end " style="font-size:9px;">` +
                    response.album.preOrderReleaseDate +
                    `</small>
            </div>
        </div>
        <div class="col-3">
            <div class="float-right">
                <button type="button" class="close" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>`
            );
            document.getElementById("preorderAlbumListOfSong").innerHTML = ``;
            if (response.albumSongs.length) {
                response.albumSongs.forEach(function log(song) {
                    var pending = ''
                    if (song.song.status == 'PENDING'){
                        pending = '<div class="light-grey-text font-weight-lighter" style="margin-left: -5px !important"> <i class="fa fa-clock text-orange"></i> Pending</div>'
                    }else{
                        pending = '';
                    }

                    if (song.song.videoFile) {
                        video = `    <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                    <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>`;
                    } else {
                        video = `    <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>`;
                    }
                    if (song.song.isPaid) {
                        paid =
                            `<small class="product-description font-szie10 light-grey-text">$` +
                            song.song.price +pending+
                            `</small>`;
                    } else {
                        paid = `<small class="product-description font-szie10 light-grey-text">Free song</small>`;
                    }
                    document.getElementById(
                        "preorderAlbumListOfSong"
                    ).innerHTML +=
                        `
                <tr id="preorderAlbumListOfSong` +
                        song.song.id +
                        `">
                <td class="p-0 pl-2">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item">
                            <div class="product-img pt-1">
                                   <img src="` +
                        thumbnailUrl(song.song.thumbnail) +
                        `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                            </div>
                            <div class="product-info ml-5">
                                <small  class="product-title font-szie10">` +
                        stringTrim(song.song.name, 12) +
                        ` <span class="light-grey-text"> (` +
                        song.song.songLength +
                        `)</span></small>
                                ` +
                        paid +
                        `
                            </div>
                       </li>
                    </ul>
                </td>
                <td>
                    ` +
                        video +
                        `
             </td>
                <td></td>
              <td class="pb-2" >
                    <a class="btn text-left btn-xs orange-button border-radius8 small-button"  href="javascript:void(0)"  onclick="deletePreorderAlbumSongFrom(` +
                        song.song.id +
                        `)" ><span class="bi bi-trash3"  id="trash-iconq"></span></a>
                </td>
            </tr>
            `;
                });
            } else {
                document.getElementById(
                    "preorderAlbumListOfSong"
                ).innerHTML += `<tr style="text-align: center;">
        <td class="border-none p-2" colspan="3">
        No data is available!
        </td>
        </tr>`;
            }
            document.getElementById("preorderAlbumBuyerList").innerHTML = ``;
            if (response.purchase.length) {
                response.purchase.forEach(function log(buyer) {
                    document.getElementById(
                        "preorderAlbumBuyerList"
                    ).innerHTML +=
                        `
            <tr>
                <td>
                    <div class="d-flex flex-row">
                        <img src="` +
                        thumbnailUrl(buyer.user.avatar) +
                        `" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                        <div class="ml-2 padding-top5"><small>` +
                        buyer.user.firstName +
                        ` ` +
                        buyer.user.lastName +
                        `</small></div>
                    </div>
                </td>
                <td><small>` +
                        buyer.user.email +
                        `</small></td>
                <td><small>` +
                        timeCarbon(buyer.user.createdAt) +
                        `</small></td>
            </tr>
            `;
                });
            } else {
                document.getElementById(
                    "preorderAlbumBuyerList"
                ).innerHTML += `<tr style="text-align: center;">
                <td class="border-none p-2" colspan="3">
                No data is available!
                </td>
                </tr>`;
            }
            $.LoadingOverlay("hide");
            $("#preorderAlbumDetailModal").modal("toggle");
        } else {
            $.LoadingOverlay("hide");
            toastr.error(response.message);
        }
    });
}

function deletePreorderAlbumSongFrom(id) {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/preorder/album/song/" + id,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 200) {
                toastr.success(response.message);
                $("#preorderAlbumListOfSong" + id).remove();
                location.reload();
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
}



$('.preorder-album-song-next-btn').on('click', function(){
    let formData = new FormData($("#addSongToAlbumPreorderForm")[0]);
    $("#preorderAlbumSongStepNo").val(1)

    if($("#preorderAlbumSongId").val().length == 0){

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $.ajax({
            url: "/" + url + "/preorder/album/song",
            type: "POST",
            data: formData,
            beforeSend: function () {
                $.LoadingOverlay("show");
            },
            contentType: false,
            processData: false,
            success: function (response) {
                $.LoadingOverlay("hide");
                if (response.status == 400) {
                    var errors = [
                        "albumSongPreorderName",
                        "albumSongPreorderStatus",
                        "albumSongPreorderPrice",
                        "albumSongPreorderCategory",
                        "albumSongPreorderLanguage",
                        "albumSongPreorderThumbnail",
                        "albumSongPreorderAudio",
                        "albumSongPreorderVideo",
                        "albumSongPreorderLyrics",
                    ];
                    for (var i = 0; i < errors.length; i++) {
                        hideErrorMessage(errors[i]);
                    }
                    for (var [key, value] of Object.entries(response.errors)) {
                        showErrorMessage(value[0], key);
                    }
                } else if (response.status == 500) {
                    toastr.error(response.message);
                } else if (response.status == 300) {
                    song = response.song;
                    preorderSelectSongName = document.getElementById(
                        "editPreorderSelectedSongs"
                    );
                    const option = new Option(song.id, song.id, false, true);
                    // add it to the list
                    preorderSelectSongName.add(option, undefined);
                    document.getElementById(
                        "editPreorderalbumSongList"
                    ).innerHTML +=
                        `
                    <div class="col-2 pb-1 editAlbumSongRemove` +
                        song.id +
                        ` " id="albumSongImageEditModal` +
                        song.id +
                        `">
                                    <img src="` +
                        thumbnailUrl(song.thumbnail) +
                        `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                </div>
                                <div class="col-7 p-2 editAlbumSongRemove` +
                        song.id +
                        `" id="albumSongNameEditModal` +
                        song.id +
                        `">
                                    <small>` +
                        song.name +
                        `</small>
                                </div>
                                <div class="col 2 p-2 pr-3 editAlbumSongRemove` +
                        song.id +
                        `">
                                    <i class="float-right red-text bi bi-pencil-fill" onclick="editPreorderAlbumSong(` +
                        song.id +
                        `)"></i>
                                </div>
                                <div class="col-1 p-2 editAlbumSongRemove` +
                        song.id +
                        `">
                                    <i class="float-right bi bi-x-lg" onclick="removeEditPreorderAlbumSong(` +
                        song.id +
                        `)"></i>
                                </div>
                    `;
                    $("#addSongToAlbumPreorderForm")[0].reset();
                    $("#addPreorderSongModal").modal("toggle");
                    $("#editAlbumPreorderModal").modal("toggle");
                } else {
                    song = response.song;

                    $('.add-preorder-album-song-progress-step-1').removeClass(' active')
                    $('.add-preorder-album-song-progress-step-2').addClass(' active')

                    $('#preorder-album-song-pills-home').removeClass('show active')
                    $('#preorder-album-song-pills-profile').addClass('show active')

                    $("#preorderAlbumSongStepNo").val(2)
                    $("#preorderAlbumSongId").val(response.song[1].songId)

                    var html = ``;
                    $.each(response.song, function(index,price){
                        parseInt(index) == 1 ? $("#preorderAlbumSongPrice").val(price.customerPrice) : '';
                        html += `<option value="${price.priceTier}">${price.customerPrice}</option>`
                    });

                    $("#albumSongPreorderPrice").html(html);

                }
            },
        });

    }else{
        $('.add-preorder-album-song-progress-step-1').removeClass(' active')
        $('.add-preorder-album-song-progress-step-2').addClass(' active')

        $('#preorder-album-song-pills-home').removeClass('show active')
        $('#preorder-album-song-pills-profile').addClass('show active')

        $("#preorderAlbumSongStepNo").val(2)
    }
});

$('.preorder-album-song-previous-btn').on('click', function(){
    $('.add-preorder-album-song-progress-step-2').removeClass(' active')
    $('.add-preorder-album-song-progress-step-1').addClass(' active')

    $('#preorder-album-song-pills-profile').removeClass('show active')
    $('#preorder-album-song-pills-home').addClass('show active')

    $("#preorderAlbumSongStepNo").val(1)
});


$("#albumSongPreorderPrice").on('change', function(){
    $("#preorderAlbumSongPrice").val($('option:selected',this).text());
});


$("#editAlbumSongPreorderPriceTier").on('change', function(){
    $("#editAlbumSongPreorderPrice").val($('option:selected',this).text());
});
