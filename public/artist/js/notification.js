$(document).ready(function () {
    table = $("#example").DataTable({
        dom: "<'row'<'col'<'card m-2 border-radius8lr'<'.card-header p-2 pl-3 notificationModalHeader'<'allnoti'>il><'card-body p-0 table-responsive'<rt>>>p>>",
        initComplete: function (settings, json) {},
    });
    $("div.allnoti").html(
        '<span class="pr-2 all-noti-text"style="font-weight:600">All Notifications</span>'
    );
    $("#filterSearch").keyup(function () {
        table.search(this.value).draw();
    });
});

(function () {
    let months = ["Pending", "Rejected", "Approved", "Deleted By Admin"];
    var month_selected = "All"; //(new Date).getMonth(); // current month
    var option = "";
    option = '<option value="' + 00 + '">All</option>'; // first option

    for (let i = 0; i < months.length; i++) {
        let month_number = i + 1;

        // value month number with 0. [01 02 03 04..]
        let month = month_number <= 9 ? "0" + month_number : month_number;

        let selected = i === month_selected ? " selected" : "";
        option +=
            '<option value="' +
            month +
            '"' +
            selected +
            ">" +
            months[i] +
            "</option>";
    }
    document.getElementById("status").innerHTML = option;
})();

function getStatusValue() {
    var conceptName = $("#status").find(":selected").text();
    if (conceptName == "All") {
        table.search("").draw();
    } else {
        table.search(conceptName).draw();
    }
}

// Disable form submissions if there are invalid fields
(function () {
    "use strict";
    window.addEventListener(
        "load",
        function () {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName("needs-validation");
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(
                forms,
                function (form) {
                    form.addEventListener(
                        "submit",
                        function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add("was-validated");
                        },
                        false
                    );
                }
            );
        },
        false
    );
})();

function showErrorMessage(error, message) {
    if (error) {
        $("#" + message + "Error").show();
        $("#" + message + "Error").html(error);
    }
}

function hideErrorMessage(message) {
    $("#" + message + "Error").hide();
}

$("#addNotificationForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#addNotificationForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/artist/notification",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 400) {
                var errors = ["notificationTitle", "notificationMessage"];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else if (response.status == 500) {
                toastr.error(response.message);
            } else {
                $("#addNotificationForm")[0].reset();
                const truncate = (str, max, suffix) =>
                    str.length < max
                        ? str
                        : `${str.substr(
                              0,
                              str
                                  .substr(0, max - suffix.length)
                                  .lastIndexOf(" ")
                          )}${suffix}`;

                // Example
                $("#addNotificationModal").modal("toggle");
                location.reload(true);
            }
        },
    });
});

function notificationDetail(id) {
    $.LoadingOverlay('show');
    $.get("/artist/notification/" + id, function (response) {
        $.LoadingOverlay('hide');
        if (response.status == 500) {
            toastr.error(response.message);
        } else if (response.status == 200) {
            const time = new Date(response.notification.createdAt);
            var month;
            if (time.getMonth() < 9) {
                month = "0" + (time.getMonth() + 1);
            } else {
                month = time.getMonth() + 1;
            }
            const date =
                time.getDate() + "/" + month + "/" + time.getFullYear();
            $("#notificationDetailModalDetail").html(
                `<div class="row">
                    <div class="col-12 mb-2">
                        <p class="notification-title-text"><b>` +
                    response.notification.title +
                    `</b></p>
                    </div>
                    <div class="col-12 mb-3 notification-de-text" >
                        <small>` +
                    response.notification.message +
                    `</small>
                    </div>
                </div>
                <hr>
                <div class="row m-0">
                    
                    <div class="col-lg-6 pl-0">
                        <small >Date</small>
                    </div>
                    <div class="col-lg-6 pr-0">
                        <small style="float: right">` +
                    date +
                    `</small>
                    </div>
                </div>`
            );
            $("#notificationDetailModal").modal("toggle");
        
        }
    });
}

function notificationDelete(id) {
    $("#deleteNotificationModal").modal("toggle");
    deletedNotificationId = id;
}
$("#deleteNotificationForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/artist/notification/" + deletedNotificationId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        success: function (response) {
            if (response.status == 500) {
                toastr.error(response.message);
            } else if (response.status == 200) {
                $(`#notificationId` + deletedNotificationId).remove();
                $("#deleteNotificationModal").modal("toggle");
                $.LoadingOverlay('hide');
                location.reload(true);
            }
        },
    });
});
