
// Datatable of Song Table
$(document).ready(function () {
    songTable = $("#songTable").DataTable({
        ordering: false,
        scrollY: true,
        dom: "<'row  p-2 pt-2 padding-top0'<'col-lg-4 '<'song-div'>><'col-lg-6 pl-1 pr-3'l<'song-status-div mr-2'>><'col-lg-2 pl-1 pr-3'<'song-search'>>><'bottom'rtp>",
        initComplete: function (settings, json) {},
    });
    $("div.song-status-div").html(
        '<select class="float-right p-1  month-select font-size12" style="margin-right: 8px;background-color:white;border:0.5px solid #E1E4E6" id="song-status" name="song-status" onchange="getStatusValue();"><option>All Song</option><option>Free Song</option><option>Paid Song</option></select>'
    );
    $("div.song-div").html(
        '<small class="float-left ml-2 pt-1"  style="font-weight:600">Songs List</small> <button type="button" style="width:100px;font-size:12px" data-toggle="modal" data-target="#addSong" class="btn ml-2 red-button btn-xs border-radius8 add-song-button"><i class="fas fa-plus"></i> Add Song</button>'
    );
    $("div.song-search").html(
        '<input type= "search" class="form-control ml-3 mr-0 float-right font-size12" id="songfilterSearch"  placeholder="Search">'
    );
    $("#songfilterSearch").keyup(function () {
        songTable.search(this.value).draw();
    });


});

function getStatusValue() {
    var conceptName = $("#song-status").find(":selected").text();
    if (conceptName == "All Song") {
        myFunction("All");
    } else if (conceptName == "Paid Song") {
        myFunction("Paid");
    } else {
        myFunction("Free");
    }
}

function myFunction(filter) {
    var table, tr, td, i, txtValue;
    table = document.getElementById("songTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("small")[1];
        if (filter == "All") {
            tr[i].style.display = "";
        } else if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}


$("#songStatus").val("Paid Song");

function showErrorMessage(error, message) {
    if (error) {
        $("#" + message + "Error").show();
        $("#" + message + "Error").html(error);
    }
}

function showErrorMessageClass(error, message) {
    if (error) {
        $("." + message + "Error").show();
        $("." + message + "Error").html(error);
    }
}

function hideErrorMessageClass(message) {
    $("." + message + "Error").hide();
}

function hideErrorMessage(message) {
    $("#" + message + "Error").hide();
}
// Add New Song
$("#addSongForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#addSongForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/"+url+"/song",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.status == 500) {
                $.LoadingOverlay('hide');
                toastr.error(response.message);
            } else if (response.status == 400) {
                $.LoadingOverlay('hide');
                //Show Errors on the Modal
                var errors = [
                    "songTitle",
                    "songStatus",
                    "songPrice",
                    "songCategory",
                    "songLanguage",
                    "songThumbnail",
                    "songAudio",
                    "songVideo",
                    "songLyrics",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else {
                $(".dataTables_empty").hide();
                var isPaid;
                var isAudio = `<span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>`;
                var isVideo = `<span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>`;
                var image;
                if (response.isPaid) {
                    isPaid = "Paid. $" + response.price;
                    image =
                        `<img src="` +
                        thumbnailUrl(response.thumbnail) +
                        `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px"><span class="dot-prime"><i class="fas fa-crown"></i></span>`;
                } else {
                    isPaid = "Free Song";
                    image =
                        `<img src="` +
                        thumbnailUrl(response.thumbnail) +
                        `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">`;
                }
                if (response.videoFile) {
                    isAudio = isAudio + ` ` + isVideo;
                }
                $("#songTable").append(
                    ` <tr id="id` +
                        response.id +
                        `" style="background-color: white !important;">
                <td  class="p-0 pl-2">
                    <ul class="products-list product-list-in-card pl-2 pr-1">
                        <li class="item">
                            <div id="imageDiv` +
                        response.id +
                        `" class="product-img pt-1">
                                ` +
                        image +
                        `
                            </div>
                            <div id="titleDiv` +
                        response.id +
                        `" class="product-info ml-5">
                                <small  class="product-title font-szie10">` +
                        response.name +
                        `<span class="light-grey-text font-weight-lighter"> (` +
                        response.songLength +
                        `)</span>
                                    <span class=" dot-play"><i class="bi bi-pause-fill" style="color: white;margin-left:3.5px !important"></i></span>
                                </small>
                            <small class="product-description font-szie10 light-grey-text">` +
                        isPaid +
                        `</small>
                            </div>
                        </li>
                    </ul>
                </td>
                <td id="isVideo` +
                        response.id +
                        `" class="pl-0 pr-0">
                    ` +
                        isAudio +
                        `
                </td>
                <td class="pb-2" >
                    <div class="float-right">
                    <a class="btn text-left btn-xs orange-button border-radius8 small-button"  href="javascript:void(0)"  onclick="deleteSong(` +
                        response.id +
                        `)" ><span class="bi bi-trash3"  id="trash-iconq"></span></a>
                    <a class="btn text-left btn-xs red-button border-radius8 small-button" style="margin-right: 7px" href="javascript:void(0)"  onclick="editSong(` +
                        response.id +
                        `)"><span class="bi bi-pencil-square fa-md" id="pencil-iconq"></span></a>
                    </div>
                </td>
            </tr>`
                );
                $('#addSong').modal('toggle')
                $.LoadingOverlay('hide');
                $("#addSongForm")[0].reset();
                location.reload();
            }
        },
    });
});

// Delete Song
let deletedSongId;
function deleteSong(id) {
    $("#deleteSong").modal("toggle");
    deletedSongId = id;
}
$("#deleteSongForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/"+url+"/song/" + deletedSongId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 200) {
                toastr.success(response.message);
                $("#deleteSong").modal("toggle");
                $("#id" + deletedSongId).remove();
                location.reload();
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
});

// Get the data for edit
function editSong(id) {
    $.LoadingOverlay('show');
    $("#editSongForm")[0].reset();
    var editErrors = [
        "editSongName",
        "editSongStatus",
        "editPrice",
        "editSongCategory",
        "editSongLanguage",
        "editThumbnail",
        "editAudioFile",
        "editVideoFile",
        "editVideoFile1",
        "editSongLyrics",
    ];
    for (var i = 0; i < editErrors.length; i++) {
        hideErrorMessage(editErrors[i]);
    }

    $.get("/"+url+"/song" + "/" + id, function (response) {
        if (response.status == 500) {
            $.LoadingOverlay('hide');
            toastr.error(response.message);
        } else {
            $("#id").val(id);
            $("#editSongName").val(response.name);
            if (response.isPaid) {
                $("#editSongStatus").val("Paid Song");
                $("#edit-price-div").show();
                $("#editPrice").val(response.price);

                var html = ``;
                $.each(response.prices , function(index,price){
                    parseFloat(response.price)  != 0.00 ? '' : (parseInt(index) == 1 ? $("#editPrice").val(price.customerPrice) : '')
                     html += `<option value="${price.priceTier}" ${price.customerPrice == parseFloat(response.price) ? 'selected' : ''}>${price.customerPrice}</option>`
                });

                $("#editPriceTier").html(html);


            } else {
                $("#editSongStatus").val("Free Song");
                $("#edit-price-div").hide();
                $("#editPrice").val("");
            }
            $("#editSongCategory").val(response.categoryId);
            $("#editSongLanguage").val(response.languageId);
            $("#editSongLyrics").val(response.lyrics);
            $("#editSongThumbnail").css(
                "background-image",
                "url(" + thumbnailUrl(response.thumbnail) + ")"
            );
            if (response.videoFile) {
                $(".videoFormRound").show();
                $(".videoForm").hide();
            } else {
                $(".videoFormRound").hide();
                $(".videoForm").show();
            }
            $.LoadingOverlay('hide');
            $("#editSong").modal("toggle");
        }

    });
}

$(function () {
    $("#editThumbnail").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById("editSongThumbnail").style.backgroundImage =
            "url(" + tmppath + ")";
    });
});

// Edit Song
$("#editSongForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editSongForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/"+url+"/song/update",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 500) {
                toastr.error(response.message);
            } else if (response.status == 400) {
                var editErrors = [
                    "editSongName",
                    "editSongStatus",
                    "editPrice",
                    "editSongCategory",
                    "editSongLanguage",
                    "editThumbnail",
                    "editAudioFile",
                    "editVideoFile",
                    "editVideoFile1",
                    "editSongLyrics",
                ];
                for (var i = 0; i < editErrors.length; i++) {
                    hideErrorMessage(editErrors[i]);
                }
                // show Error on the Modal
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            }else {
                $("#editSong").modal("toggle");
                location.reload();


            }
        },
    });
});


$('.song-next-btn').on('click', function(){

    let formData = new FormData($("#addSongForm")[0]);
    $("#stepNo").val(1)

    if($("#songId").val().length == 0 && $("#songStatus").val() == 'Paid Song'){
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $.ajax({
            url: "/"+url+"/song",
            type: "POST",
            data: formData,
            beforeSend: function () {
                $.LoadingOverlay('show');
            },
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.status == 500) {
                    $.LoadingOverlay('hide');
                    toastr.error(response.message);
                } else if (response.status == 400) {
                    $.LoadingOverlay('hide');
                    //Show Errors on the Modal
                    var errors = [
                        "songTitle",
                        "songStatus",
                        "songCategory",
                        "songLanguage",
                    ];

                    for (var i = 0; i < errors.length; i++) {
                        hideErrorMessage(errors[i]);
                    }

                    for (var [key, value] of Object.entries(response.errors)) {
                        showErrorMessage(value[0], key);
                    }
                } else {
                    $.LoadingOverlay('hide');
                    $('.progress-step-1').removeClass(' active')
                    $('.progress-step-2').addClass(' active')

                    $('#pills-home').removeClass('show active')
                    $('#pills-profile').addClass('show active')
                    $("#stepNo").val(2)

                    if(response.hasOwnProperty('isPaid')){
                        $("#songId").val(response.id)
                    }else{
                        $("#songId").val(response[1].songId)

                        var html = ``;
                        $.each(response , function(index,price){
                            parseInt(index) == 1 ? $("#songPrice").val(price.customerPrice) : '';
                            html += `<option value="${price.priceTier}">${price.customerPrice}</option>`
                        });

                        $("#priceTier").html(html);

                    }
                }
            },
        });
    }else{
        $('.progress-step-1').removeClass(' active')
        $('.progress-step-2').addClass(' active')

        $('#pills-home').removeClass('show active')
        $('#pills-profile').addClass('show active')
        $("#stepNo").val(2)
    }
});

$('.song-previous-btn').on('click', function(){
    $('.progress-step-2').removeClass(' active')
    $('.progress-step-1').addClass(' active')

    $('#pills-profile').removeClass('show active')
    $('#pills-home').addClass('show active')

    $("#stepNo").val(1)
});

$("#priceTier").on('change', function(){
    $("#songPrice").val($('option:selected',this).text());
});

$("#editPriceTier").on('change', function(){
    $("#editPrice").val($('option:selected',this).text());
});
