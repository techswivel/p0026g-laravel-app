$(document).ready(function () {
    document.getElementById("selectedSongsName").value = "";
    document.getElementById("editSelectedSongsName").value = "";
});

function stringTrim(str, length, ending) {
    if (length == null) {
        length = 100;
    }
    if (ending == null) {
        ending = "...";
    }
    if (str.length > length) {
        return str.substring(0, length - ending.length) + ending;
    } else {
        return str;
    }
}

$('.album-song-next-btn').on('click', function(){
    let formData = new FormData($("#addAlbumForm")[0]);

    $("#albumStepNo").val(1)

    if($("#albumId").val().length == 0){
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $.ajax({
            url: "/"+url+"/album",
            type: "POST",
            data: formData,
            beforeSend: function () {
                $.LoadingOverlay('show');
            },
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.status == 500) {
                    $.LoadingOverlay('hide');
                    toastr.error(response.message);
                } else if (response.status == 400) {
                    $.LoadingOverlay('hide');
                    //Show Errors on the Modal
                    var errors = [
                        "albumName",
                        "albumThumbnail",
                        "selectedSongsName",
                    ];

                    for (var i = 0; i < errors.length; i++) {
                        hideErrorMessage(errors[i]);
                    }

                    for (var [key, value] of Object.entries(response.errors)) {
                        showErrorMessage(value[0], key);
                    }
                } else {


                    if(response.album.hasOwnProperty('createdType')){
                        $("#albumId").val(response.album.id)
                        location.reload();
                    }else{

                        $('.add-album-progress-step-1').removeClass(' active')
                        $('.add-album-progress-step-2').addClass(' active')

                        $('#album-pills-home').removeClass('show active')
                        $('#album-pills-profile').addClass('show active')
                        $("#albumStepNo").val(2)

                        $("#albumId").val(response.album[1].albumId)

                        var html = ``;
                        $.each(response.album , function(index,price){
                            parseInt(index) == 1 ? $("#addAlbumiAppPurchasePrice").val(price.customerPrice) : '';
                            html += `<option value="${price.priceTier}">${price.customerPrice}</option>`
                        });

                        $("#albumPriceTier").html(html);

                        if(response.album.albumStatus != 'Free'){
                            $("#songsCost").html('$'+response.album[1].songsCost);
                        }
                    }

                    $.LoadingOverlay('hide');

                }
            },
        });
    }else{
        $('.add-album-progress-step-1').removeClass(' active')
        $('.add-album-progress-step-2').addClass(' active')

        $('#album-pills-home').removeClass('show active')
        $('#album-pills-profile').addClass('show active')
        $("#albumStepNo").val(2)
    }
});

$('.album-song-previous-btn').on('click', function(){
    $('.add-album-progress-step-2').removeClass(' active')
    $('.add-album-progress-step-1').addClass(' active')

    $('#album-pills-profile').removeClass('show active')
    $('#album-pills-home').addClass('show active')

    $("#albumStepNo").val(1)
});

$("#albumPriceTier").on('change', function(){
    $("#addAlbumiAppPurchasePrice").val($('option:selected',this).text());
});


$("#albumStatus").on('change', function(){
   if($('option:selected',this).val() == 'Free'){
    $('.album-song-next-btn').val('Create')
   }else{
    $('.album-song-next-btn').val('Next')
   }
});

$("#albumEditPriceTier").on('change', function(){
    $("#editIAppPurchasePrice").val($('option:selected',this).text());
});

// Add New Album
$("#addAlbumForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#addAlbumForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/" + url + "/album",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.status == 400) {
                $.LoadingOverlay("hide");
                var errors = [
                    "albumName",
                    "albumThumbnail",
                    "selectedSongsName",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else if (response.status == 500) {
                $.LoadingOverlay("hide");
                toastr.error(response.message);
            } else if (response.status == 200) {
                image =
                    `<img src="` +
                    thumbnailUrl(response.album.thumbnail) +
                    `"  class="border-radius5" width="80px" height="80px" alt="">`;
                if (response.album.albumStatus == "Paid") {
                    image =
                        `<img src="` +
                        thumbnailUrl(response.album.thumbnail) +
                        `"  class="border-radius5" width="80px" height="80px" alt=""><span class="dot-prime"><i class="fas fa-crown"></i></span>`;
                }
                $("#listAddSong").empty();
                $("#selectSongName").empty();
                $("#addAlbumForm")[0].reset();
                $("#addAlbumModal").modal("toggle");
                $.LoadingOverlay("hide");
                location.reload();
            }
        },
    });
});

// Select Song Modal
function listTheSelectSong(id) {
    $.LoadingOverlay("show");
    $.get("/" + url + "/album/listTheSong/" + id, function (response) {
        if (response.selectsongs.length) {
            document.getElementById("listSong").innerHTML = "";
            if($("#albumStatus option:selected").val() == 'Paid'){
                var selectsongs = response.selectsongs.filter(item => item.isPaid == 1)
            }else{
                var selectsongs = response.selectsongs.filter(item => item.isPaid == 0)
            }

            selectsongs.forEach(function log(song) {
                var pending = ''
                if (song.status == 'PENDING'){
                    pending = '<div class="light-grey-text font-weight-lighter" style="margin-left: -5px !important"> <i class="fa fa-clock text-orange"></i> Pending</div>'
                }else{
                    pending = '';
                }

                paid = '<small class="d-flex align-items-end light-grey-text font-size10" >Free Song</small>';
                if (song.isPaid) {
                    paid = `<small class="d-flex align-items-end light-grey-text font-size10" >Paid. $` +
                        song.price +
                        `</small>`+pending;
                    image =
                        `<div class="col-1 mt-1 ml-0">
                            <img src="` +
                        thumbnailUrl(song.thumbnail) +
                        `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                        </div>
                        <div class="col-2">
                            <span class="dot-prime" style="margin-left: -12px"><i class="fas fa-crown"></i></span>
                        </div>`;
                } else {
                    image =
                        `<div class="col-1 mt-1 ml-0">
                            <img src="` +
                        thumbnailUrl(song.thumbnail) +
                        `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                        </div>
                        <div class="col-2">
                        </div>`;
                }
                document.getElementById("listSong").innerHTML +=
                    `<div id="id` +
                    song.id +
                    `" class="row mt-1 mb-3" style="margin: 0px; margin-left:24px">
                        <div class="col-2 p-1 pr-0 mt-1 mr-0 float-right">
                            <div class="icheck-danger d-inline">
                                <input  type="checkbox" name="checkbox[]" class="checkSingle" value="` +
                    song.id +
                    `" id="checkboxPrimary` +
                    song.id +
                    `" />
                                <label for="checkboxPrimary` +
                    song.id +
                    `"> </label>
                            </div>
                        </div>
                        ` +
                    image +
                    `
                        <div class="col-6 " style="padding-left: 0px">
                            <small class="d-flex align-items-start">` +
                    stringTrim(song.name, 12) +
                    ` <span class="ml-1 light-grey-text font-size10 pt-1">(` +
                    song.songLength +
                    `)</span></small>
                            ` +
                    paid +
                    `
                        </div>
                    </div>  `;
            });
            $.LoadingOverlay("hide");
            $("#addSelectingSongForm")[0].reset();
            $("#addAlbumModal").modal("toggle");
            $("#selectingSongModal").modal("toggle");
        } else {
            $.LoadingOverlay("hide");
            $("#AllcheckDisplay").html(`<p>No Song left to select!</p>`);
            $("#addSelectingSongForm")[0].reset();
            $("#addAlbumModal").modal("toggle");
            $("#selectingSongModal").modal("toggle");
        }
    });
}

// Edit All Checkbox
$("#editCheckboxPrimaryAll").change(function () {
    if (this.checked) {
        $(".checkSingle").each(function () {
            this.checked = true;
        });
    } else {
        $(".checkSingle").each(function () {
            this.checked = false;
        });
    }
});

// All Checkbox
$("#checkboxPrimaryAll").change(function () {
    if (this.checked) {
        $(".checkSingle").each(function () {
            this.checked = true;
        });
    } else {
        $(".checkSingle").each(function () {
            this.checked = false;
        });
    }
});

// Cancel Button of select Song
$(".canelButton").click(function () {
    $("#selectingSongModal").modal("hide");
    $("#addAlbumModal").modal("toggle");
});

// Add Selecting Song Form
$("#addSelectingSongForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#addSelectingSongForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.LoadingOverlay("show");
    $.ajax({
        url: "/" + url + "/album/select/song",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.status == 400) {
                $.LoadingOverlay("hide");
                showErrorMessage(response.errors.checkbox, "selectSong");
            } else {
                $("#selectedSongsName").empty();
                hideErrorMessage("selectSong");
                // create a new option
                selectSongName = document.getElementById("selectedSongsName");
                document.getElementById("listAddSong").innerHTML = "";
                response.songs.forEach(function log(song) {
                    const option = new Option(song.id, song.id, false, true);
                    // add it to the list
                    selectSongName.add(option, undefined);
                    let paid, image;
                    if (song.isPaid) {
                        image =
                            `                <img src="` +
                            thumbnailUrl(song.thumbnail) +
                            `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">

                        </div>
                        <div class="col-2 song` +
                            song.id +
                            `">
                        <span class="dot-prime" style="margin-left: -37px"><i class="fas fa-crown"></i></span>
                        </div>`;
                        paid =
                            `<small class="d-flex align-items-end light-grey-text font-size10">Paid. $` +
                            song.price +
                            `</small>`;
                    } else {
                        image =
                            `                <img src="` +
                            thumbnailUrl(song.thumbnail) +
                            `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">

                        </div>
                        <div class="col-2 song` +
                            song.id +
                            `">
                        </div>`;
                        paid = `<small class="d-flex align-items-end light-grey-text font-size10">Free Song</small>`;
                    }
                    document.getElementById("listAddSong").innerHTML +=
                        `<div class="col-2 mt-1 song` +
                        song.id +
                        `">
                        ` +
                        image +
                        `

                                <div class="col-6 song` +
                        song.id +
                        `">
                                    <small class="d-flex align-items-start song` +
                        song.id +
                        `">` +
                        stringTrim(song.name, 12) +
                        ` <span class="ml-1 light-grey-text font-size10 pt-1 song` +
                        song.id +
                        `" >(` +
                        song.songLength +
                        `)</span></small>
                                    ` +
                        paid +
                        `
                                </div>
                                <div class="col-2 p-2 song` +
                        song.id +
                        `">
                                    <i class="float-right bi bi-x-lg " onclick="removeSongFromList(` +
                        song.id +
                        `)"></i>
                                </div>`;
                });
                $.LoadingOverlay("hide");
                $("#selectingSongModal").modal("hide");
                $("#addAlbumModal").modal("toggle");
            }
        },
    });
});

// Remove Song From Album
function removeSongFromList(id) {
    $(".song" + id).remove();
    $("#selectedSongsName option[value=" + id + "]").remove();
}
// Show Album Detail
function albumDetail(id) {
    $.LoadingOverlay("show");
    $.get("/" + url + "/album/detail" + "/" + id, function (response) {
        if (response.status == 200) {

            if (response.album.status == 'PENDING'){
                var pendingAlbum = '<div class="light-grey-text text-white font-weight-lighter" style="margin-left: -5px !important"> <i class="fa fa-clock text-orange"></i> Pending</div>'
            }else{
                var pendingAlbum = '';
            }


            image =
                ` <div >
                        <img src="` +
                thumbnailUrl(response.album.thumbnail) +
                `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                </div>

                </div>
                <div class="col-8 pl-3">
                    <div class="p-1 pl-0" id="song-title-text" >`;
            if (response.album.albumStatus == "Paid") {
                image =
                    ` <div >
                        <img src="` +
                    thumbnailUrl(response.album.thumbnail) +
                    `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                </div>

                </div>
                <div class="col-1 pl-3">
                <span class="dot-prime"><i class="fas fa-crown"></i></span>
                </div>
                <div class="col-7 pl-3">
                    <div class="p-1 pl-0" id="song-title-text" style="margin-left:-42px">`;
            }
            $("#albumThumbnail").html(
                `<div class="col-1">
               ` +
                    image +
                    `
                        <small class="d-flex align-items-start" style="font-weight: 500">` +
                    response.album.name +
                    `
                        <span class="dot-edit-delete" onclick="albumUpdate(` +
                    response.album.id +
                    `)"><i class="ml-1 mt-1 bi bi-pencil-fill pencil"></i></span>
                        <span class="dot-edit-delete" onclick="albumDelete(` +
                    response.album.id +
                    `)"><i class="ml-1 mt-1 bi bi-trash3-fill pencil"></i></span>
                        </small>
                        <small class="d-flex align-items-end " style="font-size:9px;">` +
                    response.count +
                    ` Songs</small>`+pendingAlbum+`
                    </div>
                </div>
                <div class="col-3  ">
                    <div class="float-right">
                        <button type="button" class="close pt-0" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>`
            );
            document.getElementById(
                "albumSongListTable"
            ).innerHTML = `<tbody id="albumSongListTableBody">
        </tbody>
        `;
            if (response.songs.length) {
                response.songs.forEach(function log(song) {
                    var pending = ''
                    paid =
                        '<small class="product-description font-szie10 light-grey-text">Free song</small>';
                    image =
                        `<img src="` +
                        thumbnailUrl(song.thumbnail) +
                        `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">`;
                    if (song.status == 'PENDING'){
                        pending = '<div class="light-grey-text font-weight-lighter" style="margin-left: -5px !important"> <i class="fa fa-clock text-orange"></i> Pending</div>'
                    }else{
                        pending = '';
                    }

                    if (song.isPaid) {
                        paid = '<small class="product-description font-szie10 light-grey-text">Paid. $' +
                            song.price + "</small>"+pending;
                        image =
                            `<img src="` +
                            thumbnailUrl(song.thumbnail) +
                            `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px"><span class="dot-prime"><i class="fas fa-crown"></i></span>`;
                    }
                    isVideo = `    <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                    `;
                    if (song.videoFile) {
                        isVideo = `    <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                    `;
                    }
                    document.getElementById("albumSongListTable").innerHTML +=
                        `
                <tr id="albumSongList` +
                        song.id +
                        `">
                    <td  class="p-0 pl-2">
                        <ul class="products-list product-list-in-card pl-2 pr-1">
                            <li class="item">
                                <div class="product-img pt-1">
                                    ` +
                        image +
                        `
                                </div>
                                <div class="product-info ml-5">
                                    <small  class="product-title font-szie10">` +
                        stringTrim(song.name, 12) +
                        ` <span class="light-grey-text"> (` +
                        song.songLength +
                        `)</span>
                                        <span class=" dot-play"><i class="bi bi-pause-fill" style="color: white;margin-left:3.5px !important"></i></span>
                                    </small>
                                    ` +
                        paid +
                        `
                                </div>
                            </li>
                        </ul>
                    </td>
                    <td class="pl-0 pr-0">
                        ` +
                        isVideo +
                        `
                    </td>
                    <td></td>
                    <td class="pb-2" >
                        <button class="btn text-left btn-xs orange-button border-radius8 small-button" onclick="albumSongDelete(` +
                        song.id +
                        `)"><span class="bi bi-trash3" style="padding-left:0px" id="trash-icon"></span></button>
                        <button class="btn text-left btn-xs red-button border-radius8 small-button"  onclick="albumSongUpdate(` +
                        song.id +
                        `)"><span class="bi bi-pencil-square fa-md" style="padding-left:0px" id="pencil-icon"></span></button>
                    </td>
                </tr>
            `;
                });
            } else {
                document.getElementById(
                    "albumSongListTable"
                ).innerHTML += `<tr style="text-align: center;">
                <td class="border-none p-2" colspan="3">
                No data is available!
                </td>
                </tr>`;
            }
            $.LoadingOverlay("hide");
            $("#albumDetail").modal("toggle");
        } else if (response.status == 500) {
            $.LoadingOverlay("hide");
            toastr.error(response.message);
        }
    });
}
//  Delete Album modal
function albumDelete(id) {
    $("#deleteAlbum").modal("toggle");
    deletedAlbumId = id;
}

$("#deleteAlbumForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/album/" + deletedAlbumId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        success: function (response) {
            if (response.status == 200) {
                $.LoadingOverlay("hide");
                toastr.success(response.message);
                $("#deleteAlbum").modal("toggle");
                $("#albumDetail").modal("toggle");
                $(`#album` + deletedAlbumId + ``).remove();
                $("#loader").addClass("hidden");
                location.reload();
            } else if (response.status == 500) {
                $.LoadingOverlay("hide");
                toastr.error(response.message);
            }
        },
    });
});

// Remove Song From Album
function albumSongDelete(id) {
    $("#deleteSonginAlbum").modal("toggle");
    deletedAlbumSongId = id;
}

$("#deleteSongFromAlbumForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/album/song/" + deletedAlbumSongId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        success: function (response) {
            if (response.status == 200) {
                if (response.album.albumStatus == "Paid") {
                    $("#album" + response.album.id).html(
                        `
                    <center class=" float-left">
                    <a href="javascript:void(0)" onclick="albumDetail(` +
                            response.album.id +
                            `)"><img src="` +
                            thumbnailUrl(response.album.thumbnail) +
                            `"  class="border-radius5" width="80px" height="80px" alt=""><span class="dot-prime"><i class="fas fa-crown"></i></span>
                    <small  class="name" style="font-size: 10px;color:black">` +
                            response.album.name +
                            ` </small></a>
                </center>
                    `
                    );
                } else {
                    $("#album" + response.album.id).html(
                        `
                    <center class=" float-left">
                    <a href="javascript:void(0)" onclick="albumDetail(` +
                            response.album.id +
                            `)"><img src="` +
                            thumbnailUrl(response.album.thumbnail) +
                            `"  class="border-radius5" width="80px" height="80px" alt="">
                    <small  class="name" style="font-size: 10px;color:black">` +
                            response.album.name +
                            ` </small></a>
                </center>
                    `
                    );
                }
                $.LoadingOverlay("hide");
                toastr.success(response.message);
                $(`#albumSongList` + deletedAlbumSongId + ``).remove();
                $("#deleteSonginAlbum").modal("toggle");
                $("#albumDetail").modal("toggle");
                location.reload();
            } else if (response.status == 500) {
                $.LoadingOverlay("hide");
                toastr.error(response.message);
            }
        },
    });
});

// Update Album
function albumUpdate(id) {
    $.LoadingOverlay("show");
    $("#editSelectSongName").empty();
    document.getElementById("editListAddSong").innerHTML = "";

    $("#editAlbumForm")[0].reset();
    $("#editAlbumNameError").hide();
    $("#editAlbumThumbnailError").hide();
    $("#editSelectedSongsNameError").hide();

    $.get("/" + url + "/album/" + id, function (response) {
        if (response.status == 200) {
            $("#editAlbumId").val(id);
            $("#editAlbumName").val(response.album.name);
            $("#editAlbumModal").modal("toggle");
            $("#editThumbnailSong").css(
                "background-image",
                "url(" + thumbnailUrl(response.album.thumbnail) + ")"
            );
            $("#editIAppPurchasePrice").val(response.album.iAppPurchasePrice)
            $("#selectSongName").empty();
            hideErrorMessage("selectSong");
            // create a new option
            selectSongName = document.getElementById("editSelectedSongsName");
            document.getElementById("editListSaveSong").innerHTML = "";
            // create a new option
            response.songs.forEach(function log(song) {
                const option = new Option(song.id, song.id, false, true);
                // add it to the list
                selectSongName.add(option, undefined);
                let paid, image;
                if (song.isPaid) {
                    image =
                        `                <img src="` +
                        thumbnailUrl(song.thumbnail) +
                        `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">

                        </div>
                        <div class="col-2 songEdit` +
                        song.id +
                        `">
                        <span class="dot-prime" style="margin-left: -37px"><i class="fas fa-crown"></i></span>
                        </div>`;
                    paid =
                        `<small class="d-flex align-items-end light-grey-text font-size10">Paid. $` +
                        song.price +
                        `</small>`;
                } else {
                    image =
                        `                <img src="` +
                        thumbnailUrl(song.thumbnail) +
                        `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">

                        </div>
                        <div class="col-2 songEdit` +
                        song.id +
                        `">
                        </div>`;
                    paid = `<small class="d-flex align-items-end light-grey-text font-size10">Free Song</small>`;
                }
                document.getElementById("editListSaveSong").innerHTML +=
                    `<div class="col-2 mt-1 songEdit` +
                    song.id +
                    `">
                        ` +
                    image +
                    `

                                <div class="col-6 songEdit` +
                    song.id +
                    `">
                                    <small class="d-flex align-items-start songEdit` +
                    song.id +
                    `">` +
                    stringTrim(song.name, 12) +
                    ` <span class="ml-1 light-grey-text font-size10 pt-1 songEdit` +
                    song.id +
                    `" >(` +
                    song.songLength +
                    `)</span></small>
                                    ` +
                    paid +
                    `
                                </div>
                                <div class="col-2 p-2 songEdit` +
                    song.id +
                    `">
                                    <i class="float-right bi bi-x-lg " onclick="removeSongFromEditList(` +
                    song.id +
                    `)"></i>
                                </div>`;
            });



            var html = ``;
            $.each(response.album.prices , function(index,price){
                parseFloat(response.album.iAppPurchasePrice)  != 0.00 ? '' : (parseInt(index) == 1 ? $("#editIAppPurchasePrice").val(price.customerPrice) : '')
                html += `<option value="${price.priceTier}" ${price.customerPrice == parseFloat(response.album.iAppPurchasePrice) ? 'selected' : ''}>${price.customerPrice}</option>`
            });

            $("#albumEditPriceTier").html(html);

            if(response.album.albumStatus != 'Free'){
                $("#editAlbumForm #price-div").removeClass('d-none');
                $("#editSongsCost").html('$'+response.album.prices[1].songsCost);
            }else if(response.album.albumStatus != 'Partially'){
                $("#editSongsCost").html('Free Album');
                $("#editAlbumForm #price-div").addClass('d-none');
            }else{
                $("#editSongsCost").html('Free Album');
                $("#editAlbumForm #price-div").addClass('d-none');
            }

            $.LoadingOverlay("hide");

        } else if (response.status == 500) {
            toastr.error(response.message);
        }
    });

}

$(function () {
    $("#editAlbumThumbnail").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById("editThumbnailSong").style.backgroundImage =
            "url(" + tmppath + ")";
    });

    $("#editSongAlbumForThumbnail").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById(
            "editSongAlbumThumbnail"
        ).style.backgroundImage = "url(" + tmppath + ")";
    });
});

function removeSongFromEditList(id) {
    $(".songEdit" + id).remove();
    $("#editSelectedSongsName option[value=" + id + "]").remove();
}

function editListTheSelectSong(id) {
    $.LoadingOverlay("show");
    $.get("/" + url + "/album/listTheSong/" + id, function (response) {
        if (response.selectsongs.length) {
            document.getElementById("editListSong").innerHTML = "";

            if($("#albumStatus option:selected").val() == 'Paid'){
                var selectsongs = response.selectsongs.filter(item => item.isPaid == 1)
            }else{
                var selectsongs = response.selectsongs.filter(item => item.isPaid == 0)
            }


            selectsongs.forEach(function log(song) {
                paid =
                    '    <small class="d-flex align-items-end light-grey-text font-size10" >Free Song</small>';
                if (song.isPaid) {
                    paid =
                        `    <small class="d-flex align-items-end light-grey-text font-size10" >Paid. $` +
                        song.price +
                        `</small>`;
                    image =
                        `<div class="col-1 mt-1 ml-0">
                            <img src="` +
                        thumbnailUrl(song.thumbnail) +
                        `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                        </div>
                        <div class="col-2">
                            <span class="dot-prime" style="margin-left: -12px"><i class="fas fa-crown"></i></span>
                        </div>`;
                } else {
                    image =
                        `<div class="col-1 mt-1 ml-0">
                            <img src="` +
                        thumbnailUrl(song.thumbnail) +
                        `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                        </div>
                        <div class="col-2">
                        </div>`;
                }
                document.getElementById("editListSong").innerHTML +=
                    `<div id="id` +
                    song.id +
                    `" class="row mt-1 mb-3" style="margin: 0px; margin-left:24px">
                        <div class="col-2 p-1 pr-0 mt-1 mr-0 float-right">
                            <div class="icheck-danger d-inline">
                                <input  type="checkbox" name="checkbox[]" class="checkSingle" value="` +
                    song.id +
                    `" id="editcheckboxPrimary` +
                    song.id +
                    `" />
                                <label for="editcheckboxPrimary` +
                    song.id +
                    `"> </label>
                            </div>
                        </div>
                        ` +
                    image +
                    `
                        <div class="col-6 " style="padding-left: 0px">
                            <small class="d-flex align-items-start">` +
                    stringTrim(song.name, 12) +
                    ` <span class="ml-1 light-grey-text font-size10 pt-1">(` +
                    song.songLength +
                    `)</span></small>
                            ` +
                    paid +
                    `
                        </div>
                    </div>  `;
            });
            $("#editAlbumModal").modal("toggle");
            $("#editSelectingSongModal").modal("toggle");
            $.LoadingOverlay("hide");
        } else {
            $("#editSelectSongDisplay").html(`<p>No Song left to select!</p>`);
            $("#editAlbumModal").modal("toggle");
            $("#editSelectingSongModal").modal("toggle");
            $.LoadingOverlay("hide");
        }
    });
}

$("#editSelectingSongForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editSelectingSongForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.LoadingOverlay("show");
    $.ajax({
        url: "/" + url + "/album/select/song",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.status == 400) {
                $.LoadingOverlay("hide");
                showErrorMessage(response.errors.checkbox, "editSelectSong");
            } else {
                hideErrorMessage("editSelectSong");
                // create a new option
                selectSongName = document.getElementById(
                    "editSelectedSongsName"
                );
                document.getElementById("editListAddSong").innerHTML = "";
                response.songs.forEach(function log(song) {
                    const option = new Option(song.id, song.id, false, true);
                    // add it to the list
                    selectSongName.add(option, undefined);
                    let paid, image;
                    if (song.isPaid) {
                        image =
                            `                <img src="` +
                            thumbnailUrl(song.thumbnail) +
                            `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">

                        </div>
                        <div class="col-2 songEdit` +
                            song.id +
                            `">
                        <span class="dot-prime" style="margin-left: -37px"><i class="fas fa-crown"></i></span>
                        </div>`;
                        paid =
                            `<small class="d-flex align-items-end light-grey-text font-size10">Paid. $` +
                            song.price +
                            `</small>`;
                    } else {
                        image =
                            `                <img src="` +
                            thumbnailUrl(song.thumbnail) +
                            `" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">

                        </div>
                        <div class="col-2 songEdit` +
                            song.id +
                            `">
                        </div>`;
                        paid = `<small class="d-flex align-items-end light-grey-text font-size10">Free Song</small>`;
                    }
                    document.getElementById("editListAddSong").innerHTML +=
                        `<div class="col-2 mt-1 songEdit` +
                        song.id +
                        `">
                        ` +
                        image +
                        `

                                <div class="col-6 songEdit` +
                        song.id +
                        `">
                                    <small class="d-flex align-items-start songEdit` +
                        song.id +
                        `">` +
                        stringTrim(song.name, 12) +
                        ` <span class="ml-1 light-grey-text font-size10 pt-1 songEdit` +
                        song.id +
                        `" >(` +
                        song.songLength +
                        `)</span></small>
                                    ` +
                        paid +
                        `
                                </div>
                                <div class="col-2 p-2 songEdit` +
                        song.id +
                        `">
                                    <i class="float-right bi bi-x-lg " onclick="removeSongFromEditList(` +
                        song.id +
                        `)"></i>
                                </div>`;
                });
                $.LoadingOverlay("hide");
                $("#editSelectingSongModal").modal("hide");
                $("#editAlbumModal").modal("toggle");
            }
        },
    });
});
// Edit Album Form
$("#editAlbumForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editAlbumForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/album/update",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.status == 400) {
                $.LoadingOverlay("hide");
                var errors = [
                    "editAlbumName",
                    "editAlbumThumbnail",
                    "editSelectedSongsName",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else if (response.status == 500) {
                $.LoadingOverlay("hide");
                toastr.error(response.message);
            } else {
                $.LoadingOverlay("hide");
                $("#editAlbumModal").modal("toggle");
                $("#albumDetail").modal("toggle");
                if (response.updateAlbum.albumStatus == "Paid") {
                    $("#album" + response.updateAlbum.id).html(
                        `
                <center class=" float-left">
                <a href="javascript:void(0)" onclick="albumDetail(` +
                            response.updateAlbum.id +
                            `)"><img src="` +
                            thumbnailUrl(response.updateAlbum.thumbnail) +
                            `"  class="border-radius5" width="80px" height="80px" alt=""><span class="dot-prime"><i class="fas fa-crown"></i></span>
                <small  class="name" style="font-size: 10px;color:black">` +
                            response.updateAlbum.name +
                            ` </small></a>
            </center>
                `
                    );
                } else {
                    $("#album" + response.updateAlbum.id).html(
                        `
                <center class=" float-left">
                <a href="javascript:void(0)" onclick="albumDetail(` +
                            response.updateAlbum.id +
                            `)"><img src="` +
                            thumbnailUrl(response.updateAlbum.thumbnail) +
                            `"  class="border-radius5" width="80px" height="80px" alt="">
                <small  class="name" style="font-size: 10px;color:black">` +
                            response.updateAlbum.name +
                            ` </small></a>
            </center>
                `
                    );
                }
                location.reload();
            }
        },
    });
});

$(".canelEditButton").click(function () {
    $("#editAlbumModal").modal("toggle");
    $("#editSelectingSongModal").modal("hide");
});

function albumSongUpdate(id) {
    $.LoadingOverlay("show");
    $.get("/" + url + "/song" + "/" + id, function (response) {
        $("#editSongAlbumForm")[0].reset();
        var editErrors = [
            "editSongName",
            "editSongStatus",
            "editPrice",
            "editSongCategory",
            "editSongLanguage",
            "editThumbnail",
            "editAudioFile",
            "editVideoFile",
            "editVideoFile1",
            "editSongLyrics",
        ];
        for (var i = 0; i < editErrors.length; i++) {
            hideErrorMessageClass(editErrors[i]);
        }

        if (response.status == 500) {
            $.LoadingOverlay("hide");
            toastr.error(response.message);
        } else {
            $("#songAlbumId").val(id);
            $("#editSongAlbumName").val(response.name);
            if (response.isPaid) {
                $("#editSongAlbumStatus").val("Paid Song");
                $("#editSongAlbumPrice").val(response.price);
                $("#editPriceSongField").show();
            } else {
                $("#editSongAlbumStatus").val("Free Song");
                $("#editSongAlbumPrice").val("");
                $("#editPriceSongField").hide();
            }
            $("#editSongAlbumLyrics").val(response.lyrics);
            $("#editSongAlbumCategory").val(response.categoryId);
            $("#editSongAlbumLanguage").val(response.languageId);
            $("#editSongAlbumThumbnail").css(
                "background-image",
                "url(" + thumbnailUrl(response.thumbnail) + ")"
            );
            if (response.videoFile) {
                $(".videoFormRound").show();
                $(".videoForm").hide();
            } else {
                $(".videoFormRound").hide();
                $(".videoForm").show();
            }

            $.LoadingOverlay("hide");
            $("#editSongAlbumModal").modal("toggle");
        }
    });
}

// Edit Song
$("#editSongAlbumForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editSongAlbumForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/" + url + "/song/update",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        contentType: false,
        processData: false,
        success: function (response) {
            var editErrors = [
                "editSongName",
                "editSongStatus",
                "editPrice",
                "editSongCategory",
                "editSongLanguage",
                "editThumbnail",
                "editAudioFile",
                "editVideoFile",
                "editVideoFile1",
                "editSongLyrics",
            ];
            if (response.status == 500) {
                $.LoadingOverlay("hide");
                toastr.error(response.message);
            } else if (response.status == 400) {
                $.LoadingOverlay("hide");
                for (var i = 0; i < editErrors.length; i++) {
                    hideErrorMessageClass(editErrors[i]);
                }
                // show Error on the Modal
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessageClass(value[0], key);
                }
            } else {
                for (var i = 0; i < editErrors.length; i++) {
                    hideErrorMessageClass(editErrors[i]);
                }
                $.get(
                    "/" + url + "/album/albumStatus/" + response.id,
                    function (response) {
                        response = response.album;
                        if (response.albumStatus == "Paid") {
                            $("#album" + response.id).html(
                                `
                    <center class=" float-left">
                    <a href="javascript:void(0)" onclick="albumDetail(` +
                                    response.id +
                                    `)"><img src="` +
                                    thumbnailUrl(response.thumbnail) +
                                    `"  class="border-radius5" width="80px" height="80px" alt=""><span class="dot-prime"><i class="fas fa-crown"></i></span>
                    <small  class="name" style="font-size: 10px;color:black">` +
                                    response.name +
                                    ` </small></a>
                </center>
                    `
                            );
                        } else {
                            $("#album" + response.id).html(
                                `
                    <center class=" float-left">
                    <a href="javascript:void(0)" onclick="albumDetail(` +
                                    response.id +
                                    `)"><img src="` +
                                    thumbnailUrl(response.thumbnail) +
                                    `"  class="border-radius5" width="80px" height="80px" alt="">
                    <small  class="name" style="font-size: 10px;color:black">` +
                                    response.name +
                                    ` </small></a>
                </center>
                    `
                            );
                        }
                    }
                );
                if (response.isPaid) {
                    $("#imageDiv" + response.id).html(
                        `    <img src="` +
                            thumbnailUrl(response.thumbnail) +
                            `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px"><span class="dot-prime"><i class="fas fa-crown"></i></span>
                                `
                    );
                    $("#titleDiv" + response.id).html(
                        `<small class="product-title font-szie10">` +
                            response.name +
                            `<span class="light-grey-text font-weight-lighter" > (` +
                            response.songLength +
                            `)</span>
                                    <span class=" dot-play"><i class="bi bi-pause-fill" style="color: white;margin-left:3.5px !important"></i></span>
                                </small>
                            <small class="product-description font-szie10 light-grey-text">Paid. $` +
                            response.price +
                            `</small>

                                `
                    );
                } else {
                    $("#imageDiv" + response.id).html(
                        `       <img src="` +
                            thumbnailUrl(response.thumbnail) +
                            `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                    `
                    );
                    $("#titleDiv" + response.id).html(
                        `<small class="product-title font-szie10">` +
                            response.name +
                            `<span class="light-grey-text font-weight-lighter" > (` +
                            response.songLength +
                            `)</span>
                                    <span class=" dot-play"><i class="bi bi-pause-fill" style="color: white;margin-left:3.5px !important"></i></span>
                                </small>
                               <small  class="product-description font-szie10 light-grey-text">Free song</small>

                                `
                    );
                }
                if (response.videoFile) {
                    $("#isVideo" + response.id)
                        .html(`<span class=" p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>  `);
                } else {
                    $("#isVideo" + response.id).html(
                        `<span class=" p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>`
                    );
                }
                $("#editSongAlbumModal").modal("toggle");
                $("#albumDetail").modal("toggle");
                $.LoadingOverlay("hide");
                location.reload();
            }
        },
    });
});


