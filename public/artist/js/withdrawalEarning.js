$(document).ready(function() {
    $("#month").on('click', function(event) {
      event.preventDefault();
      $('#month').addClass( "border-danger" );        
    });
    $("#month" ).mouseleave(function() {
      $('#month').removeClass( "border-danger" );
    });
    $("#accountName").on('click', function(event) {
      event.preventDefault();
      $('#accountName').addClass( "border-danger" );        
    });
    $("#accountName" ).mouseleave(function() {
      $('#accountName').removeClass( "border-danger" );
    });
    $("#accountNumber").on('click', function(event) {
      event.preventDefault();
      $('#accountNumber').addClass( "border-danger" );        
    });
    $("#accountNumber" ).mouseleave(function() {
      $('#accountNumber').removeClass( "border-danger" );
    });
    $("#editAccountName").on('click', function(event) {
      event.preventDefault();
      $('#editAccountName').addClass( "border-danger" );        
    });
    $("#editAccountName" ).mouseleave(function() {
      $('#editAccountName').removeClass( "border-danger" );
    });
    $("#editAccountNumber").on('click', function(event) {
      event.preventDefault();
      $('#editAccountNumber').addClass( "border-danger" );        
    });
    $("#editAccountNumber" ).mouseleave(function() {
      $('#editAccountNumber').removeClass( "border-danger" );
    });      
    $("#year").on('click', function(event) {
      event.preventDefault();
      $('#year').addClass( "border-danger" );
    });
    $("#year" ).mouseleave(function() {
      $('#year').removeClass( "border-danger" );
    });   
});

(function () {
  currentYear = (new Date).getFullYear();
  let months = [
    "Year",
    currentYear,
    currentYear - 1,
    currentYear - 2,
];
var month_selected = "All"; //(new Date).getMonth(); // current month
var option = "";
for (let i = 0; i < months.length; i++) {
    let month_number = i + 1;
    // value month number with 0. [01 02 03 04..]
    let month = month_number <= 9 ? "0" + month_number : month_number;
    let selected = i === month_selected ? " selected" : "";
    option +=
        '<option value="' +
        month +
        '"' +
        selected +
        ">" +
        months[i] +
        "</option>";
}
document.getElementById("year").innerHTML = option;
})();

(function () {
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month_selected = 'Month';     //(new Date).getMonth(); // current month
    var option = '';
    option = '<option value="'+00+'">Month</option>'; // first option

    for (let i = 0; i < months.length; i++) {
        let month_number = (i + 1);

        // value month number with 0. [01 02 03 04..]
        let month = (month_number <= 9) ? '0' + month_number : month_number;


        let selected = (i === month_selected ? ' selected' : '');
        option += '<option value="' + month + '"' + selected + '>' + months[i] + '</option>';
    }
    document.getElementById("month").innerHTML = option;
})();

  // Disable form submissions if there are invalid fields
  (function() {'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();

     
  function getMonthValue() {
    var month = $('#month').find(":selected").text();
    var year = $('#year').find(":selected").text();
    if(month == 'Month'){
      if(year == 'Year'){
        $('.all').show();
      }else{
        $('.all').hide();
        $('.month'+month).show();
      }
    }else if(year == 'Year'){
      $('.all').hide();
      $('.month'+month).show();
    }else{
      $('.all').hide();
      $('.m'+month+year+'y').show();
    }
 }
 function getYearValue() {
  var month = $('#month').find(":selected").text();
  var year = $('#year').find(":selected").text();
  if(year == 'Year'){
    if(month == 'Month'){
      $('.all').show();
    }else{
      $('.all').hide();
      $('.month'+month).show();
    }
  }else if(month == 'Month'){
    $('.all').hide();
    $('.year'+year).show();
  }else{
    $('.all').hide();
    $('.m'+month+year+'y').show();
  }
}
 
 function showErrorMessage(error, message) {
  if (error) {
      $("#" + message + "Error").show();
      $("#" + message + "Error").html(error);
  }
}

function hideErrorMessage(message) {
  $("#" + message + "Error").hide();
}


 $("#addAccountForm").submit(function (e) {
  e.preventDefault();
  let formData = new FormData($("#addAccountForm")[0]);
  $.ajaxSetup({
      headers: {
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
  });

  $.ajax({
      url: "/artist/withdrawal/earning/store/account",
      type: "POST",
      data: formData,
      beforeSend: function () {
        $.LoadingOverlay('show');
      },
      contentType: false,
      processData: false,
      success: function (response) {
          $.LoadingOverlay('hide');
          if (response.status == 400) {
              var errors = [
                  "accountName",
                  "accountNumber",
              ];
              for (var i = 0; i < errors.length; i++) {
                  hideErrorMessage(errors[i]);
              }
              for (var [key, value] of Object.entries(response.errors)) {
                  showErrorMessage(value[0], key);
              }
          } else if (response.status == 500) {
              toastr.error(response.message);
          }else if (response.status == 200){
            toastr.success('Your account detail is save!');
            $("#addAccountForm")[0].reset();
            $("#addAccountModal").modal('toggle');  
            location.reload();
          } 
      },
  });
});

function editAccountDetail(id) {
  $.LoadingOverlay('show');
  $("#editAccountForm")[0].reset();
  $("#editAccountNameError").hide();
  $("#editAccountNumberError").hide();
  $.get("/artist/withdrawal/earning/account/detail/" + id, function (response) {
    $.LoadingOverlay('hide');
      if (response.status == 500) {
          toastr.error(response.message);
      } else {
          $("#id").val(id);
          $("#editAccountName").val(response.card.cardHolderName);
          $("#editAccountNumber").val(response.card.cardId);
          $("#editAccountModal").modal("toggle");
      }
  });
}


$("#editAccountForm").submit(function (e) {
  e.preventDefault();
  let formData = new FormData($("#editAccountForm")[0]);
  $.ajaxSetup({
      headers: {
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
  });

  $.ajax({
      url: "/artist/withdrawal/earning/update/account",
      type: "POST",
      data: formData,
      beforeSend: function () {
        $.LoadingOverlay('show');
      },
      contentType: false,
      processData: false,
      success: function (response) {
        $.LoadingOverlay('hide');
        if (response.status == 400) {
              var errors = [
                  "editAccountName",
                  "editAccountNumber",
              ];
              for (var i = 0; i < errors.length; i++) {
                  hideErrorMessage(errors[i]);
              }
              for (var [key, value] of Object.entries(response.errors)) {
                  showErrorMessage(value[0], key);
              }
          } else if (response.status == 500) {
              toastr.error(response.message);
          }else if (response.status == 200){
            toastr.success('Your account detail is updated!');
            $("#editAccountForm")[0].reset();
            $("#editAccountModal").modal('toggle');  
            location.reload();
          } 
      },
  });
});


$("#withdrawalForm").submit(function (e) {
  e.preventDefault();
  let formData = new FormData($("#withdrawalForm")[0]);
  $.ajaxSetup({
      headers: {
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
  });

  $.ajax({
      url: "/artist/withdrawal/earning",
      type: "POST",
      data: formData,
      beforeSend: function () {
        $.LoadingOverlay('show');
      },
      contentType: false,
      processData: false,
      success: function (response) {
        $.LoadingOverlay('hide');
          $("#withdrawalModal").show();
          if (response.status == 500) {
              toastr.error(response.message);
          }else if (response.status == 200){
            toastr.success('Your request has been saved!');
            $("#withdrawalForm")[0].reset();
            $("#withdrawalModal").modal('toggle');  
            location.reload();
          }else if(response.status == 100){
            $("#withdrawalForm")[0].reset();
            $("#withdrawalModal").modal('toggle');
            $("#withdrawalResponseModal").modal('toggle');
            toastr.success(response.message);
          }else if(response.status == 300) {
            $("#withdrawalForm")[0].reset();
            $("#withdrawalModal").modal('toggle');
            console.log(response.currentWithdrawal);
            $("#againWithdrawalMoney").addClass("rwithdrawal-text");
            document.getElementById('againWithdrawalMoney').innerText = 'You are going to initiate withdrawal request of   $'+response.currentWithdrawal;
            $("#againWithdrawalModal").modal('toggle');
          }
      },
  });
});

$("#againWithdrawalForm").submit(function (e) {
  e.preventDefault();
  let formData = new FormData($("#againWithdrawalForm")[0]);
  $.ajaxSetup({
      headers: {
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
  });

  $.ajax({
      url: "/artist/withdrawal/earning/again",
      type: "POST",
      data: formData,
      beforeSend: function () {
        $.LoadingOverlay('show');
      },
      contentType: false,
      processData: false,
      success: function (response) {
        $.LoadingOverlay('hide');
          if (response.status == 200){
            toastr.success('Your request has been saved!');
            $("#againWithdrawalForm")[0].reset();
            $("#againWithdrawalModal").modal('toggle');  
            location.reload();
          }
      },
  });
});
