$(document).ready(function () {
    statTable = $("#earningStatTable").DataTable({
        dom: "<'row  p-2 pt-2 padding-top0'<'col-lg-4 pt-1'<'earningstat-div'>><'col-lg-6 pr-3'l><'col-lg-2 pr-3'<'earningstat-search'>>><'bottom'rtp>",
        initComplete: function (settings, json) {
            $(".dataTables_paginate").addClass("dataTables_paginate");
            // $('.dataTables_length').addClass('dataTables_length');
        },
    });
    $("div.earningstat-div").html(
        '<small class="float-left ml-2"  style="font-weight:600">Earning History</small>'
    );
    $("div.earningstat-search").html(
        '<input type= "search" class="form-control  float-right font-size12" id="statfilterSearch"  placeholder="Search">'
    );
    $("#statfilterSearch").keyup(function () {
        statTable.search(this.value).draw();
    });
});

function purchaseDetail(id) {
    $.LoadingOverlay('show');
    $.get("/"+url+"/earningStats/item/" + id, function (response) {
        $.LoadingOverlay('hide');
        if (response.status == 200) {
            $("#buyerName").text(
                response.purchase.user.firstName +
                    " " +
                    response.purchase.user.lastName
            );
            $("#buyerEmail").text(response.purchase.user.email);
            if (
                response.purchase.user.address &&
                response.purchase.user.city &&
                response.purchase.user.country
            ) {
                $("#buyerAddress").text(
                    response.purchase.user.address +
                        " " +
                        response.purchase.user.city +
                        ", " +
                        response.purchase.user.country
                );
            } else {
                $("#buyerAddress").text("");
            }
            if (response.purchase.user.avatar) {
                $("#buyerImage").attr(
                    "src",
                    thumbnailUrl(response.purchase.user.avatar)
                );
            } else {
                $("#buyerImage").attr("src", "image/avatar/default-avatar.png");
            }
            document.getElementById("purchaseList").innerHTML = "";
            if (response.purchase.isSong) {
                document.getElementById("purchaseList").innerHTML =
                    `<li class="item pt-1 pb-1" style="border-bottom: none">
                            <div class="product-img pt-1">
                    <img src="` +
                    thumbnailUrl(response.purchase.song.thumbnail) +
                    `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                        </div>
                    <div class="product-info ml-5">
                        <small  class="product-title font-szie10 ">` +
                    response.purchase.song.name +
                    `<span class="float-right ">$` +
                    response.purchase.song.price +
                    `</span></small>
                    <small class="product-description font-szie10 light-grey-text">` +
                    response.purchase.song.songLength +
                    `</small>
                </div>
            </li>`;
            } else {
                document.getElementById("purchaseList").innerHTML =
                    `<li class="item pt-1 pb-1" style="border-bottom: none">
                            <div class="product-img pt-1">
                            <img src="` +
                    thumbnailUrl(response.purchase.album.thumbnail) +
                    `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                        </div>
                        <div class="product-info ml-5">
                        <small  class="product-title font-szie10 ">` +
                    response.purchase.album.name +
                    `<span class="float-right ">$` +
                    response.price +
                    `</span></small>
                            <small class="product-description font-szie10 light-grey-text">` +
                    response.albumSongs.length +
                    ` songs</small>
                        </div>
                        </li><hr>`;
                response.albumSongs.forEach(function log(data) {
                    document.getElementById("purchaseList").innerHTML +=
                        `<li class="item pt-1 pb-1" style="border-bottom: none">
                            <div class="product-img pt-1">
                            <img src="` +
                        thumbnailUrl(data.thumbnail) +
                        `" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                            </div>
                            <div class="product-info ml-5">
                            <small  class="product-title font-szie10 ">` +
                        data.name +
                        `<span class="float-right ">$` +
                        data.price +
                        `</span></small>
                            <small class="product-description font-szie10 light-grey-text">` +
                        data.songLength +
                        `</small>
                            </div>
                            </li>`;
                });
            }
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("mySideClose").style.width = "37px";
        } else if (response.status == 500) {
            toastr.error(response.message);
        }
    });
}


function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("mySideClose").style.width = "0";
}
