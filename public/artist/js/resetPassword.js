$(document).ready(function () {
    $("#password").on("click", function (event) {
        event.preventDefault();
        $("#password").addClass("border-danger");
        $("#password-eye").addClass("border-danger");
    });
    $("#password").mouseleave(function () {
        $("#password").removeClass("border-danger");
        $("#password-eye").removeClass("border-danger");
    });
    $("#confirmPassword").on("click", function (event) {
        event.preventDefault();
        $("#confirmPassword").addClass("border-danger");
        $("#confirmPassword-eye").addClass("border-danger");
    });
    $("#confirmPassword").mouseleave(function () {
        $("#confirmPassword").removeClass("border-danger");
        $("#confirmPassword-eye").removeClass("border-danger");
    });
});
$(document).ready(function () {
    $("#show_hide_password a").on("click", function (event) {
        event.preventDefault();
        if ($("#show_hide_password input").attr("type") == "text") {
            $("#show_hide_password input").attr("type", "password");
            $("#show_hide_password span").addClass("fa-eye-slash");
            $("#show_hide_password span").removeClass("fa-eye");
        } else if ($("#show_hide_password input").attr("type") == "password") {
            $("#show_hide_password input").attr("type", "text");
            $("#show_hide_password span").removeClass("fa-eye-slash");
            $("#show_hide_password span").addClass("fa-eye");
        }
    });

    $("#show_hide_confirmPassword a").on("click", function (event) {
        event.preventDefault();
        if ($("#show_hide_confirmPassword input").attr("type") == "text") {
            $("#show_hide_confirmPassword input").attr("type", "password");
            $("#show_hide_confirmPassword span").addClass("fa-eye-slash");
            $("#show_hide_confirmPassword span").removeClass("fa-eye");
        } else if (
            $("#show_hide_confirmPassword input").attr("type") == "password"
        ) {
            $("#show_hide_confirmPassword input").attr("type", "text");
            $("#show_hide_confirmPassword span").removeClass("fa-eye-slash");
            $("#show_hide_confirmPassword span").addClass("fa-eye");
        }
    });
});
// Disable form submissions if there are invalid fields
(function () {
    "use strict";
    window.addEventListener(
        "load",
        function () {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName("needs-validation");
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(
                forms,
                function (form) {
                    form.addEventListener(
                        "submit",
                        function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            } else {
                            }
                            form.classList.add("was-validated");
                        },
                        false
                    );
                }
            );
        },
        false
    );
})();
