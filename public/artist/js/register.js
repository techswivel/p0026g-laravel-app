$(function () {
    $("#avatar").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById("image-show").style.backgroundImage =
            "url(" + tmppath + ")";
    });
});
$(document).ready(function () {
    $("#show_hide_password a").on("click", function (event) {
        event.preventDefault();
        if ($("#show_hide_password input").attr("type") == "text") {
            $("#show_hide_password input").attr("type", "password");
            $("#show_hide_password span").addClass("fa-eye-slash");
            $("#show_hide_password span").removeClass("fa-eye");
        } else if ($("#show_hide_password input").attr("type") == "password") {
            $("#show_hide_password input").attr("type", "text");
            $("#show_hide_password span").removeClass("fa-eye-slash");
            $("#show_hide_password span").addClass("fa-eye");
        }
    });
});

$(document).ready(function () {
    $("#show_hide_password_confirm a").on("click", function (event) {
        event.preventDefault();
        if ($("#show_hide_password_confirm input").attr("type") == "text") {
            $("#show_hide_password_confirm input").attr("type", "password");
            $("#show_hide_password_confirm span").addClass("fa-eye-slash");
            $("#show_hide_password_confirm span").removeClass("fa-eye");
        } else if (
            $("#show_hide_password_confirm input").attr("type") == "password"
        ) {
            $("#show_hide_password_confirm input").attr("type", "text");
            $("#show_hide_password_confirm span").removeClass("fa-eye-slash");
            $("#show_hide_password_confirm span").addClass("fa-eye");
        }
    });
});
$(document).ready(function () {
    $("#firstName").on("click", function (event) {
        event.preventDefault();
        $("#firstName").addClass("border-danger");
    });
    $("#firstName").mouseleave(function () {
        $("#firstName").removeClass("border-danger");
    });
    $("#lastName").on("click", function (event) {
        event.preventDefault();
        $("#lastName").addClass("border-danger");
    });
    $("#lastName").mouseleave(function () {
        $("#lastName").removeClass("border-danger");
    });
    $("#email").on("click", function (event) {
        event.preventDefault();
        $("#email").addClass("border-danger");
    });
    $("#email").mouseleave(function () {
        $("#email").removeClass("border-danger");
    });
    $("#password").on("click", function (event) {
        event.preventDefault();
        $("#password").addClass("border-danger");
        $("#password-eye").addClass("border-danger");
    });
    $("#password").mouseleave(function () {
        $("#password").removeClass("border-danger");
        $("#password-eye").removeClass("border-danger");
    });
    $("#confirmPassword").on("click", function (event) {
        event.preventDefault();
        $("#confirmPassword").addClass("border-danger");
        $("#confirmPassword-eye").addClass("border-danger");
    });
    $("#confirmPassword").mouseleave(function () {
        $("#confirmPassword").removeClass("border-danger");
        $("#confirmPassword-eye").removeClass("border-danger");
    });
    $("#aboutArtist").on("click", function (event) {
        event.preventDefault();
        $("#aboutArtist").addClass("border-danger");
    });
    $("#aboutArtist").mouseleave(function () {
        $("#aboutArtist").removeClass("border-danger");
    });
});
// Disable form submissions if there are invalid fields
(function () {
    "use strict";
    window.addEventListener(
        "load",
        function () {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName("needs-validation");
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(
                forms,
                function (form) {
                    form.addEventListener(
                        "submit",
                        function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add("was-validated");
                        },
                        false
                    );
                }
            );
        },
        false
    );
})();
