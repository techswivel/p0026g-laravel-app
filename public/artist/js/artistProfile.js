// set, edit and get the Song Status is Song Paid or Free


function setStatusValue() {
    var conceptName = $("#songStatus").find(":selected").text();
    if (conceptName == "Free Song") {
        $("#price-div").hide(300);
    } else if (conceptName == "Paid Song") {
        $("#price-div").show(300);
    }
}

function editStatusValue() {
    var conceptName = $("#editSongStatus").find(":selected").text();
    if (conceptName == "Free Song") {
        $("#edit-price-div").hide(300);
} else if (conceptName == "Paid Song") {
        $("#edit-price-div").show(300);
}

    var conceptName = $("#editSongAlbumStatus").find(":selected").text();
    if (conceptName == "Free Song") {
        $("#editPriceSongField").hide(300);
    } else if (conceptName == "Paid Song") {
        $("#editPriceSongField").show(300);
    }
}

// Disable form submissions if there are invalid fields
(function () {
    "use strict";
    window.addEventListener(
        "load",
        function () {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName("needs-validation");
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(
                forms,
                function (form) {
                    form.addEventListener(
                        "submit",
                        function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add("was-validated");
                        },
                        false
                    );
                }
            );
        },
        false
    );
})();

// show image in circle div

$(function () {
    $("#avatar").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById("image-show-detail").style.backgroundImage =
            "url(" + tmppath + ")";
    });
});

//danger border around field (red)

$(document).ready(function () {
    $("#name").on("click", function (event) {
        event.preventDefault();
        $("#name").addClass("border-danger");
    });
    $("#name").mouseleave(function () {
        $("#name").removeClass("border-danger");
    });
    $("#email").on("click", function (event) {
        event.preventDefault();
        $("#email").addClass("border-danger");
    });
    $("#email").mouseleave(function () {
        $("#email").removeClass("border-danger");
    });
    $("#aboutArtist").on("click", function (event) {
        event.preventDefault();
        $("#aboutArtist").addClass("border-danger");
    });
    $("#aboutArtist").mouseleave(function () {
        $("#aboutArtist").removeClass("border-danger");
    });
});

// Edit Profile
$("#editProfileForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editProfileForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/artist/profile/update",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay('show');
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay('hide');
            if (response.status == 400) {
                if (response.errors.name) {
                    $("#nameError").show();
                    $("#nameError").html(response.errors.name);
                } else if (!response.errors.name) {
                    $("#nameError").hide();
                }
                if (response.errors.email) {
                    $("#emailError").show();
                    $("#emailError").html(response.errors.email);
                } else if (!response.errors.email) {
                    $("#emailError").hide();
                }
                if (response.errors.aboutArtist) {
                    $("#aboutArtistError").show();
                    $("#aboutArtistError").html(response.errors.aboutArtist);
                } else if (!response.errors.aboutArtist) {
                    $("#aboutArtistError").hide();
                }
                if (response.errors.avatar) {
                    $("#avatarError").show();
                    $("#avatarError").html(response.errors.avatar);
                } else if (!response.errors.avatar) {
                    $("#avatarError").hide();
                }
            } else {
                $("#edit-Profile").modal('toggle');
                location.reload(true);
            }
        },
    });
});

$(document).ready(function () {
    var $search = $("#albumFilterSearch").on("input", function () {
        try {
            var matcher = new RegExp($(this).val(), "gi");
        $(".box")
            .show()
            .not(function () {
                return matcher.test($(this).find(".name").text());
            })
            .hide();
        } catch (error) {

        }
    });
});


function editArtistProfile(user){
    $("#editProfileForm")[0].reset();
    var errors = [
        "name",
        "email",
        "avatar",
        "aboutArtist",
    ];
    for (var i = 0; i < errors.length; i++) {
        hideErrorMessage(errors[i]);
    }
    if(user.avatar){
        $("#image-show-detail").css(
            "background-image",
            "url(" + thumbnailUrl(user.avatar) + ")"
        );
    }else{
        $("#image-show-detail").css(
            "background-image",
            "url(../artist/image/avatar/default-avatar.png)"
        );
    }
    $("#name").val(user.firstName+' '+user.lastName);
    $("#email").val(user.email);
    $("#aboutArtist").val(user.aboutArtist);
    $("#edit-Profile").modal("toggle");
}
