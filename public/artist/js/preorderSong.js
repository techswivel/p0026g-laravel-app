var todaysDate = moment().tz(unescape(getCookie("timezone")));

$(function () {
    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", { placeholder: "dd/mm/yyyy" });
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", { placeholder: "mm/dd/yyyy" });
    //Money Euro
    $("[data-mask]").inputmask();

    //Date picker
    $("#reservationdate").datetimepicker({
        format: "L",
    });

    $("#editReservationdate").datetimepicker({
        format: "L",
    });

    //Date picker
    $("#albumReservationdate").datetimepicker({
        format: "L",
    });

    $("#editAlbumReservationdate").datetimepicker({
        format: "L",
    });
    //Timepicker
    $("#timepicker").datetimepicker({
        format: "LT",
    });
    $("#editTimepicker").datetimepicker({
        format: "LT",
    });
    $("#albumTimepicker").datetimepicker({
        format: "LT",
    });
    $("#editAlbumTimepicker").datetimepicker({
        format: "LT",
    });

});

//set time Acording to Timezone
$("#preorderSongTime,#preorderAlbumTime").val(todaysDate.format('LT'));

$("#addSongPreorderForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#addSongPreorderForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        url: "/" + url + "/preorder/song",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 400) {
                var errors = [
                    "preorderTypeSong",
                    "preorderSongName",
                    "preorderSongPrice",
                    "preorderSongCategory",
                    "preorderSongLanguage",
                    "preorderSongThumbnail",
                    "preorderAudioFile",
                    "preorderVideoFile",
                    "preorderSongLyrics",
                    "preorderSongDate",
                    "preorderSongTime",
                ];
                for (var i = 0; i < errors.length; i++) {
                    hideErrorMessage(errors[i]);
                }
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else if (response.status == 500) {
                toastr.error(response.message);
            } else {
                $("#addSongPreorderForm")[0].reset();
                $("#addSongPreorderModal").modal("toggle");
                location.reload(true);
            }
        },
    });
});

let deletedPreorderSongId;
function deletePreorderSong(id) {
    $("#deletePreorderSongModal").modal("toggle");
    deletedPreorderSongId = id;
}

$("#deletePreorderSongForm").submit(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/" + url + "/preorder/song/" + deletedPreorderSongId,
        type: "DELETE",
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 200) {
                toastr.success(response.message);
                $("#deletePreorderSongModal").modal("toggle");
                location.reload();
            } else if (response.status == 500) {
                toastr.error(response.message);
            }
        },
    });
});

function editPreorderSong(id) {
    $("#editSongPreorderForm")[0].reset();
    $("#editPreorderSongPriceError").hide();
    $("#editPreorderSongNameError").hide();
    $("#editPreorderSongCategoryError").hide();
    $("#editPreorderSongLanguageError").hide();
    $("#editPreorderSongThumbnailError").hide();
    $("#editPreorderVideoFileError").hide();
    $("#editPreorderAudioFileError").hide();
    $("#editPreorderVideoFile1Error").hide();
    $("#editPreorderSongLyricsError").hide();
    $("#editPreorderSongDateError").hide();
    $("#editPreorderSongTimeError").hide();
    $.LoadingOverlay("show");
    $.get("/" + url + "/preorder/song" + "/" + id, function (response) {
        $.LoadingOverlay("hide");
        if (response.status == 500) {
            toastr.error(response.message);
        } else {

            $("#preorderEditSongId").val(response.id);
            $("#editPreorderSongName").val(response.name);
            $("#editPreorderSongPrice").val(response.price);
            $("#editPreorderSongCategory").val(response.categoryId);
            $("#editPreorderSongLanguage").val(response.languageId);
            $("#editPreorderSongThumbnailDisplay").css(
                "background-image",
                "url(" + thumbnailUrl(response.thumbnail) + ")"
            );
            if (response.videoFile) {
                $(".videoFormRound").show();
                $(".videoForm").hide();
            } else {
                $(".videoFormRound").hide();
                $(".videoForm").show();
            }
            $("#editPreorderSongLyrics").val(response.lyrics);
            $("#preorderEditPrice").val(parseFloat(response.price))
            $("#editPreorderSongDate").val(response.date);
            $("#editPreorderSongTime").val(response.time);



            var html = ``;
            $.each(response.prices , function(index,price){
                parseFloat(response.price)  != 0.00 ? '' : (parseInt(index) == 1 ? $("#preorderEditPrice").val(price.customerPrice) : '')
                html += `<option value="${price.priceTier}" ${price.customerPrice == parseFloat(response.price) ? 'selected' : ''}>${price.customerPrice}</option>`
            });

            $("#editPreorderPriceTier").html(html);
            $("#editSongPreorderModal").modal("toggle");
        }
    });
}

$(function () {
    $("#editPreorderSongThumbnail").on("change", function () {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        document.getElementById(
            "editPreorderSongThumbnailDisplay"
        ).style.backgroundImage = "url(" + tmppath + ")";
    });
});

// Edit Song
$("#editSongPreorderForm").submit(function (e) {
    e.preventDefault();
    let formData = new FormData($("#editSongPreorderForm")[0]);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "/" + url + "/preorder/song/update",
        type: "POST",
        data: formData,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        contentType: false,
        processData: false,
        success: function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 500) {
                toastr.error(response.message);
            } else if (response.status == 400) {
                var editErrors = [
                    "editPreorderTypeSong",
                    "editPreorderSongName",
                    "editPreorderSongPrice",
                    "editPreorderSongCategory",
                    "editPreorderSongLanguage",
                    "editPreorderSongThumbnail",
                    "editPreorderAudioFile",
                    "editPreorderVideoFile",
                    "editPreorderVideoFile1",
                    "editPreorderSongLyrics",
                    "editPreorderSongDate",
                    "editPreorderSongTime",
                ];
                for (var i = 0; i < editErrors.length; i++) {
                    hideErrorMessage(editErrors[i]);
                }
                // show Error on the Modal
                for (var [key, value] of Object.entries(response.errors)) {
                    showErrorMessage(value[0], key);
                }
            } else {
                $("#editSongPreorderForm")[0].reset();
                $("#editSongPreorderModal").modal("toggle");
                $("#loader").addClass("hidden");

                location.reload();
            }
        },
    });
});

function preorderSongPurchaseList(id) {
    $.LoadingOverlay("show");
    $.get(
        "/" + url + "/preorder/song/buyerList" + "/" + id,
        function (response) {
            $.LoadingOverlay("hide");
            if (response.status == 500) {
                toastr.error(response.message);
            } else if (response.status == 200) {
                var pending = ''
                if (response.song.status == 'PENDING'){
                    pending = '<div class="light-grey-text font-weight-lighter" style="margin-left: -5px !important"> <i class="fa fa-clock text-orange"></i> Pending</div>'
                }else{
                    pending = '';
                }

                $("#preorderSongBuyerDetail").modal("toggle");
                $("#preorderSongDetail").html(
                    `<div class="col-2 p-0">
            <div>
                <img src="` +
                        thumbnailUrl(response.song.thumbnail) +
                        `" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
            </div>
        </div>
        <div class="col-7 p-0">
            <div class="p-1 pl-0" id="song-title-text">
                <small class="d-flex align-items-start" style="font-weight: 500">` +
                        response.song.name +
                        ` <small class="ml-2 mt-1" style="font-size:9px"> (Price $` +
                        response.song.price +
                        `)</small></small>`+pending+`
                <small class="d-flex align-items-end " style="font-size:9px;">` +
                        response.song.preOrderReleaseDate +
                        `</small>
            </div>
        </div>
        <div class="col-3">
            <div class="float-right">
                <button type="button" class="close" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>`
                );
                document.getElementById(
                    "preorderSongbuyerDetail"
                ).innerHTML = ``;
                if (response.purchase.length) {
                    response.purchase.forEach(function log(buyer) {
                        if (buyer.user.avatar) {
                            image =
                                `<img src="` +
                                thumbnailUrl(buyer.user.avatar) +
                                `" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">`;
                        } else {
                            image = `<img src="/artist/image/avatar/default-avatar.png" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  `;
                        }
                        document.getElementById(
                            "preorderSongbuyerDetail"
                        ).innerHTML +=
                            `<tr>
                <td class="border-none p-2">
                    <div class="d-flex flex-row">
                    ` +
                            image +
                            `
                    <div class="ml-2"><small>` +
                            buyer.user.firstName +
                            " " +
                            buyer.user.lastName +
                            `</small></div>
                </div>

            </td>
            <td class="border-none p-2"><small>` +
                            buyer.user.email +
                            `</small></td>
            <td class="border-none p-2"><small>` +
                            timeCarbon(buyer.user.createdAt) +
                            `</small></td>
            </tr>`;
                    });
                } else {
                    document.getElementById(
                        "preorderSongbuyerDetail"
                    ).innerHTML += `<tr style="text-align: center;">
                <td class="border-none p-2" colspan="3">
                No data is available!
                </td>
                </tr>`;
                }
            }
        }
    );
}

artistId = $("#artistId").val();
var url = window.location.href.split('/')

$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});

$.ajax({
    url: `/${url[3]}/carbon/date/${artistId}`,
    type: "POST",
    contentType: false,
    processData: false,
    success: function (response) {
        if (response.status == 200) {
            response.preorderSongsCountDown.forEach(function log(CountDown) {
                setPreorderTimeCounter("song", CountDown);
            });
            response.preorderAlbumsCountDown.forEach(function log(CountDown) {
                setPreorderTimeCounter("album", CountDown);
            });
        } else {
            toastr.error("Fail to show preorder counter down");
        }
    },
});


function setPreorderTimeCounter(status, dateTime) {
    // Set the date we're counting down to
    var countDownDate = new Date(dateTime.preOrderReleaseDate).getTime();
    // Update the count down every 1 second
    var x = setInterval(function () {
        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor(
            (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        var value = null;
        if (status == "song") {
            value = "preorderSongtimeCounter" + dateTime.id;
        } else if (status == "album") {
            value = "preorderAlbumtimeCounter" + dateTime.id;
        }
        // Output the result in an element with id="demo"
        document.getElementById(value).innerHTML =
            days + ": " + hours + ": " + minutes + ": " + seconds + " ";

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById(value).innerHTML = "EXPIRED";
        }
    }, 1000);
}

function setPreorderSongStatusValue() {
    var preorderType = $("#preorderTypeSong").find(":selected").text();
    if (preorderType == "Album") {
        $("#addSongPreorderModal").modal("toggle");
        $("#addAlbumPreorderModal").modal("toggle");
    }
}

function resetPreorderSongStatusValue() {
    var preorderType = $("#preorderTypeSong").find(":selected").text();
    if (preorderType == "Album") {
        $("#editSongPreorderModal").modal("toggle");
        $("#addAlbumPreorderModal").modal("toggle");
    }
}



$('#preorder-song-next-btn').on('click', function(){

    let formData = new FormData($("#addSongPreorderForm")[0]);

    $("#preorderStepNo").val(1)

    if($("#preorderSongId").val().length == 0){
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $.ajax({
            url: "/"+url+"/preorder/song",
            type: "POST",
            data: formData,
            beforeSend: function () {
                $.LoadingOverlay('show');
            },
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.status == 500) {
                    $.LoadingOverlay('hide');
                    toastr.error(response.message);
                } else if (response.status == 400) {
                    $.LoadingOverlay('hide');
                    //Show Errors on the Modal
                    var errors = [
                        "songTitle",
                        "songStatus",
                        "songCategory",
                        "songLanguage",
                    ];

                    for (var i = 0; i < errors.length; i++) {
                        hideErrorMessage(errors[i]);
                    }

                    for (var [key, value] of Object.entries(response.errors)) {
                        showErrorMessage(value[0], key);
                    }
                } else {
                    $.LoadingOverlay('hide');

                    $('.add-preorder-song-progress-step-1').removeClass(' active')
                    $('.add-preorder-song-progress-step-2').addClass(' active')

                    $('#preorder-pills-home').removeClass('show active')
                    $('#preorder-pills-profile').addClass('show active')
                    $("#preorderStepNo").val(2)

                    $("#preorderSongId").val(response[1].songId)

                    var html = ``;
                    $.each(response , function(index,price){
                        parseInt(index) == 1 ? $("#preorderSongPrice").val(price.customerPrice) : '';
                        html += `<option value="${price.priceTier}">${price.customerPrice}</option>`
                    });

                    $("#preorderPriceTier").html(html);
                }
            },
        });
    }else{
        $('.add-preorder-song-progress-step-1').removeClass(' active')
        $('.add-preorder-song-progress-step-2').addClass(' active')

        $('#preorder-pills-home').removeClass('show active')
        $('#preorder-pills-profile').addClass('show active')
        $("#preorderStepNo").val(2)
    }

});

$('.preorder-song-previous-btn').on('click', function(){

    $('.add-preorder-song-progress-step-2').removeClass(' active')
    $('.add-preorder-song-progress-step-1').addClass(' active')

    $('#preorder-pills-profile').removeClass('show active')
    $('#preorder-pills-home').addClass('show active')

    $("#preorderStepNo").val(1)
});


$("#preorderPriceTier").on('change', function(){
    $("#preorderSongPrice").val($('option:selected',this).text());
});


$("#editPreorderPriceTier").on('change', function(){
    $("#preorderEditPrice").val($('option:selected',this).text());
});
