$(document).ready(function () {
    $("#password").on("click", function (event) {
        event.preventDefault();
        $("#password").addClass("border-danger");
        $("#password_eye").addClass("border-danger");
    });
    $("#password").mouseleave(function () {
        $("#password").removeClass("border-danger");
        $("#password_eye").removeClass("border-danger");
    });
    $("#current_password").on("click", function (event) {
        event.preventDefault();
        $("#current_password").addClass("border-danger");
        $("#current_password_eye").addClass("border-danger");
    });
    $("#current_password").mouseleave(function () {
        $("#current_password").removeClass("border-danger");
        $("#current_password_eye").removeClass("border-danger");
    });
    $("#password_confirmation").on("click", function (event) {
        event.preventDefault();
        $("#password_confirmation").addClass("border-danger");
        $("#password_confirmation_eye").addClass("border-danger");
    });
    $("#password_confirmation").mouseleave(function () {
        $("#password_confirmation").removeClass("border-danger");
        $("#password_confirmation_eye").removeClass("border-danger");
    });
});
$(document).ready(function () {
    $("#show_hide_password a").on("click", function (event) {
        event.preventDefault();
        if ($("#show_hide_password input").attr("type") == "text") {
            $("#show_hide_password input").attr("type", "password");
            $("#show_hide_password span").addClass("fa-eye-slash");
            $("#show_hide_password span").removeClass("fa-eye");
        } else if ($("#show_hide_password input").attr("type") == "password") {
            $("#show_hide_password input").attr("type", "text");
            $("#show_hide_password span").removeClass("fa-eye-slash");
            $("#show_hide_password span").addClass("fa-eye");
        }
    });

    $("#show_hide_current_password a").on("click", function (event) {
        event.preventDefault();
        if ($("#show_hide_current_password input").attr("type") == "text") {
            $("#show_hide_current_password input").attr("type", "password");
            $("#show_hide_current_password span").addClass("fa-eye-slash");
            $("#show_hide_current_password span").removeClass("fa-eye");
        } else if (
            $("#show_hide_current_password input").attr("type") == "password"
        ) {
            $("#show_hide_current_password input").attr("type", "text");
            $("#show_hide_current_password span").removeClass("fa-eye-slash");
            $("#show_hide_current_password span").addClass("fa-eye");
        }
    });

    $("#show_hide_confirm_password a").on("click", function (event) {
        event.preventDefault();
        if ($("#show_hide_confirm_password input").attr("type") == "text") {
            $("#show_hide_confirm_password input").attr("type", "password");
            $("#show_hide_confirm_password span").addClass("fa-eye-slash");
            $("#show_hide_confirm_password span").removeClass("fa-eye");
        } else if (
            $("#show_hide_confirm_password input").attr("type") == "password"
        ) {
            $("#show_hide_confirm_password input").attr("type", "text");
            $("#show_hide_confirm_password span").removeClass("fa-eye-slash");
            $("#show_hide_confirm_password span").addClass("fa-eye");
        }
    });
});
// Disable form submissions if there are invalid fields
(function () {
    "use strict";
    window.addEventListener(
        "load",
        function () {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName("needs-validation");
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(
                forms,
                function (form) {
                    form.addEventListener(
                        "submit",
                        function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            } else {
                            }
                            form.classList.add("was-validated");
                        },
                        false
                    );
                }
            );
        },
        false
    );
})();
