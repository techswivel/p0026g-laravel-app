$(document).ready(function () {
    followerTable = $("#followerTable").DataTable({
        dom: "<'row  p-2 pt-2 padding-top0'<'col-lg-4 pt-1'<'follower-div'>><'col-lg-6 pr-3'l><'col-lg-2 pr-0 pl-0'<'follower-search'>>><'bottom'rtp>",
        initComplete: function (settings, json) {
            $(".dataTables_paginate").addClass("dataTables_paginate");
            $(".dataTables_length").addClass("dataTables_length");
        },
    });
    $("div.follower-div").html(
        '<small class="float-left ml-2"  style="font-weight:600">All Followers</small>'
    );
    $("div.follower-search").html(
        '<input type= "search" class="form-control  float-right font-size12" id="followfilterSearch"  placeholder="Search">'
    );
    $("#followfilterSearch").keyup(function () {
        followerTable.search(this.value).draw();
    });
});
