<?php
return [
    'serviceAccountJsonFile' => [
        'file' => base_path().'/'.env('SERVICE_ACCOUNT_CREDENTIALS')
    ],
    'packageName' => [
        'name' => env('PACKAGE_NAME')
    ],
];
