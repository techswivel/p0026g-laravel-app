{{--@extends('layouts.master')--}}
@extends('artist.layouts.master')

@section('title','Change Password')
@section('style')
<link href="{{asset('css/common.css')}}" rel="stylesheet">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper bg-white">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Change password</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Change </a></li>
                            <li class="breadcrumb-item active">Password</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

        <div class="container-fluid content-centered">

            <form  class="products" method="post" action="{{route('change.password')}}"  enctype="multipart/form-data">
                        <div class="col-12">
                        @csrf
                        <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Old Password</label>
                                    <input type="password" required name="oldPassword" class="form-control" id="old-password"
                                           placeholder="Old password">
                                    <span toggle="#old-password" class="fa fa-fw fa-eye field-icon toggle-password"
                                          style="position: relative;float: right !important;   top: -23px;  left: -11px;"></span>
                                </div>
                            </div>
                         <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>New Password</label>
                                    <input type="password" name="password" required class="form-control" id="password-field"
                                           placeholder="New password">
                                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                          style="position: relative;float: right !important;   top: -23px;  left: -11px;"></span>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputPassword4">Confirm New Password</label>
                                    <input type="password" class="form-control" required id="password-field2"
                                           name="confirmPassword" placeholder="Confirm Password">
                                    <span toggle="#password-field2" class="fa fa-fw fa-eye field-icon toggle-password"
                                          style="position: relative;float: right !important;   top: -23px;  left: -11px;"></span>

                                </div>

                            </div>
                         <div class="text-right row">
                               <div class="col-md-6 col-lg-6 col-sm-6">
                                <button class="btn btn-block btn-danger red-button btn-xs rounded">Save</button>
                            </div>
                         </div>
                        </div>
                    </form>
                    </div>


        </section>
         <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')
<script>
     $(".toggle-password").click(function () {
             $(this).removeClass("fa-eye fa-eye-slash");
           $(this).addClass("fa-eye");
            if ($(this).attr("type") == "password") {
                $(this).attr("type", "text");
            } else {
                $(this).removeClass("fa-eye");
                $(this).addClass("fa-eye fa-eye-slash");
                $(this).attr("type", "password");
            }})

           $(".toggle-password").click(function () {
           $(this).removeClass("fa-eye fa-eye-slash");
           $(this).addClass("fa-eye");
            if ($(this).attr("type") == "password") {
                $(this).attr("type", "text");
            } else {
                $(this).removeClass("fa-eye");
                $(this).addClass("fa-eye fa-eye-slash");
                $(this).attr("type", "password");
            }
        });
        $(".toggle-password").click(function () {
            $(this).removeClass("fa-eye fa-eye-slash");
             $(this).addClass("fa-eye");
            if ($(this).attr("type") == "password") {
                $(this).attr("type", "text");
            } else {
                $(this).removeClass("fa-eye");
                $(this).addClass("fa-eye fa-eye-slash");
                $(this).attr("type", "password");
            }
        });
</script>
@endsection



