<script src="{{ asset('artist/js/song.js') }}"></script>
<script src="{{ asset('artist/js/artistProfile.js') }}"></script>
<script src="{{ asset('admin/js/activeArtist.js') }}"></script>
<script src="{{ asset('admin/js/blockArtist.js') }}"></script>
<script src="{{ asset('admin/js/notificationTable.js') }}"></script>
<script src="{{ asset('admin/js/notification.js') }}"></script>
<script src="{{ asset('artist/js/album.js') }}"></script>
<script src="{{ asset('artist/js/follower.js') }}"></script>
<script src="{{ asset('artist/js/earningStats.js') }}"></script>
<script src="{{ asset('artist/js/preorderSong.js') }}"></script>
<script src="{{ asset('artist/js/preorderAlbum.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/inputmask/jquery.inputmask.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://getbootstrap.com/2.3.2/assets/js/bootstrap-tooltip.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<script>

    var songs = @json($songs)

    var songIds = @json($songs->pluck('id'));

    var albums = @json($albums)

    // This url variable has the role of auth user
    var url = "{{ $role }}";
    var isNotificationLimit = "{{ $isNotificationLimit }}";

    function thumbnailUrl(path) {
        return `{{Helper::urlPath(`+path +`)}}`+path;
    }

    function timeCarbon(date) {
        <?php $va = `<script>date</script>`; ?>;
        return <?php echo json_encode(Helper::carbonDate($va)); ?>;
    }
</script>

@include('scripts.artist.playerJs')
