<script src="https://cdnjs.cloudflare.com/ajax/libs/wavesurfer.js/1.3.7/wavesurfer.min.js"></script>

<script>
    // These variables has the song id's
    let activeSongId = 0,
        activeAlbumId = 0,
        prevSongId = 0,
        nextSongId = 0,
        albumAllSongs = 0,
        musicBarOpenTimeCount = 0;




    //Wavesurfer create object
    var wavesurfer = WaveSurfer.create({
        container: '#waveform',
        backend: 'MediaElement',
        waveColor: "lightgrey",
        progressColor: "black",
        autoCenter: true,
        height: 30,
        cursorWidth: 1,
        cursorColor: "lightgray",
        barWidth: 2,
        barGap: null,
        fillParent: true,
        normalize: true,
        removeMediaElementOnDestroy: false,
        xhr: {
            cache: 'default',
            mode: 'no-cors',
            method: 'GET',
        },
    });

    //Open MediaPlayer
    function playMusic(id, albumId = null) {
        var loaded = 0;
        $.LoadingOverlay('show');

        musicBarOpenTimeCount += 1;

        showAlbums();

        if (albumId == null) {
            var song = songs.find(item => item.id == id);
            activeAlbumId = 0
            findNextAndPrevSong(song.id)
            showAllSongs()
        } else {
            var song = albumAllSongs.find(item => item.id == id);
            activeAlbumId = albumId
            findNextAndPrevSongAlbum(song.id)
        }

        activeSongId = song.id

        $(".songImageModal").attr('src', song.avatarFullUrl)
        $(".songArtistName").html(song.get_artist.name)
        $(".songName").html((song.name.length > 17 ? song.name.substr(0, 17) + '...' : song.name))
        $(".songName").attr('title', song.name);

        wavesurfer.empty();
        wavesurfer.load(song.audioFileUrl);

        wavesurfer.drawBuffer();

        wavesurfer.on('loading', (percent) => {
            if (parseInt(percent) == 100) {
                $('.totalTime').html(song.songLength)
                document.getElementById("waveform").firstChild.style.overflow = 'hidden'

                $(`#playPause`).attr("class", 'bi-play-circle-fill mediaPlayer playPause')
                $(`.playInTable`).attr("class", 'bi-play-circle-fill mediaPlayer')
                $(`.playInSongListModal`).attr("class", 'bi-play-circle-fill mediaPlayer playInSongListModal')


                if (musicBarOpenTimeCount == 1) {
                    wavesurfer.play();
                    wavesurfer.drawBuffer();
                    $.LoadingOverlay('hide');
                    $("#musicBar").modal({
                        backdrop: false,
                        keyboard: false
                    });

                } else {

                    setTimeout(() => {
                        wavesurfer.play();
                        $.LoadingOverlay('hide');
                        $("#musicBar").modal({
                            backdrop: false,
                            keyboard: false
                        });
                    }, 3000);

                }

                $("#playPause").attr('class', 'bi-pause-circle-fill mediaPlayer playPause');
                $(`#playInTable${activeSongId}`).attr("class", 'bi-pause-circle-fill mediaPlayer playInTable')
                $(`#playInSongListModal${activeSongId}`).attr("class",
                    'bi-pause-circle-fill mediaPlayer playInSongListModal')

                wavesurfer.on('ready', updateTimer)
                wavesurfer.on('audioprocess', updateTimer)
                wavesurfer.on('seek', updateTimer)

            }
        });

        wavesurfer.setMute(false);

        $('.volume').attr("class", 'bi-volume-up-fill mediaPlayer volume');
    }

    // Show all albums
    function showAlbums(){
        var albumInPlaylist = '';

        if(albums.length > 0){
            $.each(albums, function (i, item) {
                albumInPlaylist += `<div class="swiper-slide cursor-pointer" onclick="albumSongs(${item.id})">
                                <img src="${item.avatarFullUrl}" class="border-radius8" width="120px"  height="70px" alt="">
                                <div class="overlay-wrap border-radius8">
                                    <div class="overlay-text">${item.name}</div>
                                </div>
                            </div>`

            });

            $(".swiper-wrapper").html(albumInPlaylist);

            var swiper = new Swiper(".mySwiper", {
                slidesPerView: 3,
                spaceBetween: 10,
                loop: false,
                loopFillGroupWithBlank: true,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },

            });

        }else{
            $(".swiper-wrapper").html(`<p class="text-center w-100">No Album Found!</p>`)
        }

    }

    //show all Songs
    function showAllSongs() {
        var html = '';

        if(songs.length > 0){
            $.each(songs, function(index, song) {

                if (song.isPaid) {
                    var songText = `<span class="dot-prime"><i class="fas fa-crown"></i></span>
                                    <small class="font-szie10 position-absolute ml-2">${song.get_artist.name}</small>
                                    <p class="font-szie14 font-weight-bold m-0 position-absolute top-18px ml-4">${song.name}</p>`
                } else {
                    var songText = `<small class="font-szie10 position-absolute ml-4">${song.get_artist.name}</small>
                                    <p class="font-szie14 font-weight-bold m-0 position-absolute top-18px ml-4 songNameInAlbumList">${song.name}</p>`
                }

                html += `<div class="row my-3">
                    <div class="col-md-10 col-10">
                        <div class="row no-gutters">
                            <div class="col-md-1 col-1">
                                <img src="${song.avatarFullUrl}" class="border-radius8" alt="Product Image" style="width: 35.5px;height:35.5px">
                            </div>
                            <div class="col-md-10 col-10 h-0">
                                ${songText}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-2 text-center">
                        <i onclick="playMusic(${song.id})" id="playInSongListModal${song.id}" class="bi bi-play-circle-fill mediaPlayer playPause playInSongListModal"></i>
                    </div>
                </div>`;


            });
        }else{
            html += '<p class="text-center w-100">No Song Found!</p>';
        }

        $('.songListsModal').html(html)

    }

    //show album Songs
    function albumSongs(id) {

        var album = albums.find(item => item.id == id);
        albumAllSongs = album.get_song_relation;
        var html = '';

        if(albumAllSongs.length > 0){
            $.each(albumAllSongs, function(index, song) {

                if (song.isPaid) {
                    var songText =
                        `<span class="dot-prime"><i class="fas fa-crown"></i></span>
                    <small class="font-szie10 position-absolute ml-2">${song.get_artist.name}</small>
                    <p class="font-szie14 font-weight-bold m-0 position-absolute top-18px ml-4 songNameInAlbumList">${song.name}</p>`
                    } else {
                        var songText =
                            `<small class="font-szie10 position-absolute ml-4">${song.get_artist.name}</small>
                    <p class="font-szie14 font-weight-bold m-0 position-absolute top-18px ml-4 songNameInAlbumList">${song.name}</p>`
                    }

                    html += `<div class="row my-3">
                        <div class="col-md-10 col-10">
                            <div class="row no-gutters">
                                <div class="col-md-1 col-1">
                                    <img src="${song.avatarFullUrl}" class="border-radius8" alt="Product Image" style="width: 35.5px;height:35.5px">
                                </div>
                                <div class="col-md-10 col-10 h-0">
                                    ${songText}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-2 text-center">
                            <i onclick="playMusic(${song.id},${id})" id="playInSongListModal${song.id}" class="bi bi-play-circle-fill mediaPlayer playPause playInSongListModal"></i>
                        </div>
                    </div>`;


            });

            $('.songListsModal').html(html)
        }else{
            $(".mySwiper").html(`<div class="row my-3">
                                <div class="col-md-12 col-12 text-center">
                                    <p>No Album Found!</p>
                                </div>
                            </div>`)
        }


    }

    //show playing song time progress
    function updateTimer() {
        var formattedTime = secondsToTimestamp(Math.round(wavesurfer.getCurrentTime()));
        $('.time').text(formattedTime);

        wavesurfer.on('finish', function() {
            $('#playPause').attr("class", 'bi-play-circle-fill mediaPlayer playPause');
            $(`#playInTable${activeSongId}`).attr("class", 'bi-play-circle-fill mediaPlayer playInTable')
            $(`#playInSongListModal${activeSongId}`).attr("class",
                'bi-play-circle-fill mediaPlayer playInSongListModal')
        });
    }

    //convert seonds to timestamp
    function secondsToTimestamp(seconds) {

        seconds = Math.floor(seconds);
        var h = Math.floor(seconds / 3600);
        var m = Math.floor((seconds - (h * 3600)) / 60);
        var s = seconds - (h * 3600) - (m * 60);

        h = h < 10 ? '0' + h : h;
        m = m < 10 ? '0' + m : m;
        s = s < 10 ? '0' + s : s;
        if (h != 0) {
            return h + ':' + m + ':' + s;
        } else {
            return m + ':' + s;
        }

    }


    function playPauseIcons() {
        var activeSongCalssName = $(`#playInTable${activeSongId}`).attr("class");
        var activeSongCalssNameListModal = $(`#playInSongListModal${activeSongId}`).attr("class");
        var className = $("#playPause").attr("class");

        if (className != undefined) {
            if (className.includes("bi-play-circle-fill")) {
                $("#playPause").removeAttr("class", 'bi-play-circle-fill mediaPlayer playPause');
                $("#playPause").attr("class", 'bi-pause-circle-fill mediaPlayer playPause');
            } else if (className.includes("bi-pause-circle-fill")) {
                $("#playPause").removeAttr("class", 'bi-pause-circle-fill mediaPlayer playPause');
                $("#playPause").attr("class", 'bi-play-circle-fill mediaPlayer playPause');
            }
        }

        if (activeSongCalssName != undefined) {
            if (activeSongCalssName.includes("bi-play-circle-fill")) {
                $(`#playInTable${activeSongId}`).removeAttr("class", `bi-play-circle-fill mediaPlayer`);
                $(`#playInTable${activeSongId}`).attr("class", `bi-pause-circle-fill mediaPlayer playInTable`);
            } else if (activeSongCalssName.includes("bi-pause-circle-fill")) {
                $(`#playInTable${activeSongId}`).removeAttr("class", `bi-pause-circle-fill mediaPlayer`);
                $(`#playInTable${activeSongId}`).attr("class", `bi-play-circle-fill mediaPlayer playInTable`);
            }
        }

        if (activeSongCalssNameListModal != undefined) {
            if (activeSongCalssNameListModal.includes("bi-play-circle-fill")) {
                $(`#playInSongListModal${activeSongId}`).removeAttr("class", `bi-play-circle-fill mediaPlayer`);
                $(`#playInSongListModal${activeSongId}`).attr("class",
                    `bi-pause-circle-fill mediaPlayer playInSongListModal`);
            } else if (activeSongCalssNameListModal.includes("bi-pause-circle-fill")) {
                $(`#playInSongListModal${activeSongId}`).removeAttr("class", `bi-pause-circle-fill mediaPlayer`);
                $(`#playInSongListModal${activeSongId}`).attr("class",
                    `bi-play-circle-fill mediaPlayer playInSongListModal`);
            }
        }
    }

    // Find index of next and previous song id
    function findNextAndPrevSong(songId) {
        const currentSongIndex = songs.map(object => object.id).indexOf(songId);
        //to find previous song
        if (currentSongIndex != 0) {
            let prevIndex = currentSongIndex - 1;
            if (songs[prevIndex] != undefined) {
                prevSongId = songs[prevIndex].id;
            }

        } else {
            prevSongId = 0;
        }

        //to find next song
        if ((songs.length - 1) != currentSongIndex) {
            let nextIndex = currentSongIndex + 1;
            if (songs[nextIndex] != undefined) {
                nextSongId = songs[nextIndex].id;
            }
        } else {
            nextSongId = 0;
        }

    }

    // Find index of next and previous song id in Album Playlist
    function findNextAndPrevSongAlbum(songId) {
        const currentSongIndex = albumAllSongs.map(object => object.id).indexOf(songId);
        //to find previous song
        if (currentSongIndex != 0) {
            let prevIndex = currentSongIndex - 1;
            if (albumAllSongs[prevIndex] != undefined) {
                prevSongId = albumAllSongs[prevIndex].id;
            }

        } else {
            prevSongId = 0;
        }


        //to find next song
        if ((albumAllSongs.length - 1) != currentSongIndex) {
            let nextIndex = currentSongIndex + 1;
            if (albumAllSongs[nextIndex] != undefined) {
                nextSongId = albumAllSongs[nextIndex].id;
            }
        } else {
            nextSongId = 0;
        }

    }

    //Play and Pause music on Click and also change icons
    $("#playPause").click(function() {
        playPauseIcons()
        wavesurfer.playPause();
    });

    //Play previous music , if available in previous index
    $(".prevPlay").click(function() {
        if (activeAlbumId == 0) {
            var song = songs.find(item => item.id == prevSongId);
            if (song != undefined) {
                playMusic(song.id)
                findNextAndPrevSong(song.id)
            }
        } else {
            var song = albumAllSongs.find(item => item.id == prevSongId);
            if (song != undefined) {
                playMusic(song.id, activeAlbumId)
                findNextAndPrevSongAlbum(song.id)
            }
        }

        $('.volume').attr("class", 'bi-volume-up-fill mediaPlayer volume');
    });

    //Play next music , if available in next index
    $(".nextPlay").click(function() {
        if (activeAlbumId == 0) {
            var song = songs.find(item => item.id == nextSongId);
            if (song != undefined) {
                playMusic(song.id)
                findNextAndPrevSong(song.id)
            }
        } else {
            var song = albumAllSongs.find(item => item.id == nextSongId);
            if (song != undefined) {
                playMusic(song.id, activeAlbumId)
                findNextAndPrevSongAlbum(song.id)
            }
        }

        $('.volume').attr("class", 'bi-volume-up-fill mediaPlayer volume');
    });

    //mute and unmute volume of music
    $(".volume").click(function() {
        var className = $(this).attr("class");
        if (className.includes("bi-volume-up-fill")) {
            $(this).removeAttr("class", 'bi-volume-up-fill mediaPlayer volume');
            $(this).attr("class", 'bi-volume-mute-fill mediaPlayer volume');
        } else {
            $(this).removeAttr("class", 'bi-volume-mute-fill mediaPlayer volume');
            $(this).attr("class", 'bi-volume-up-fill mediaPlayer volume');
        }

        wavesurfer.toggleMute();
    });

    //Stop Playing Music on Close Music Bar
    $(".closeMusicBar").click(function() {
        wavesurfer.stop();

        $(`#playInTable${activeSongId}`).removeAttr("class", `bi-pause-circle-fill mediaPlayer`);
        $(`#playInTable${activeSongId}`).attr("class", `bi-play-circle-fill mediaPlayer playInTable`);

        $('#songListModal').modal('hide');
        musicBarOpenTimeCount = 0;
    });


    $('#musicBar,#songListModal').on('shown.bs.modal', function() {
        $(document).off('focusin.modal');
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '0');
        $('#musicBar,#songListModal').css('padding-right', '0');
    });

    $('#songTable').on('page.dt', function(e) {
        if (wavesurfer.isPlaying()) {
            var songAllIds = $.grep(songIds, function(value) {
                return value != activeSongId;
            });
        } else {
            var songAllIds = songIds
        }

        $('#songTable').on('draw.dt', function() {
            songAllIds.forEach(function(songId, index) {
                if (songId != activeSongId) {
                    if ($(`#playInTable${songId}`).attr("id") != undefined) {
                        $(`#playInTable${songId}`).removeAttr("class",
                            `bi-pause-circle-fill mediaPlayer playInTable`);
                        $(`#playInTable${songId}`).attr("class",
                            `bi-play-circle-fill mediaPlayer playInTable`);

                    }
                }


                if (songId == activeSongId) {
                    if (wavesurfer.isPlaying()) {
                        $(`#playInTable${activeSongId}`).removeAttr("class",
                            `bi-play-circle-fill mediaPlayer playInTable`);
                        $(`#playInTable${activeSongId}`).attr("class",
                            `bi-pause-circle-fill mediaPlayer playInTable`);
                    } else {
                        $(`#playInTable${activeSongId}`).removeAttr("class",
                            `bi-pause-circle-fill mediaPlayer playInTable`);
                        $(`#playInTable${activeSongId}`).attr("class",
                            `bi-play-circle-fill mediaPlayer playInTable`);
                    }
                }


            })

            //This function is calling from artist/js/song.js
            getStatusValue();
        });


    });


</script>
