@extends('artist.layouts.master')

@section('title')
Withdrawal and Earnings
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('artist/css/withdrawalEarning.css')}}">
<style>

</style>
@endsection

@section('content')
    <div class="row pt-0 pl-1">
        <p class="withdrawal-text ">Withdrawal Earnings</p>
    </div>
    <div class="row pt-1" >
        <div class="col-lg-3 col-md-12 in-one-line">
            <div class="balance-card card ml-2  border-radius8 red-background">
                <div class="card-body  border-radius8">
                    <div class="row pl-0">
                        <div class="col">
                            <P class="font-weight-lighter text-white pt-1 balance-text">YOUR BALANCE</P>
                        </div>
                    </div>
                    <div class="row pl-0 value-balance">
                        <div class="col">
                            <p class="text-white pt-1">${{$withdrawalPrice}}</p>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col pt-5">
                        </div>
                    </div>
                    <div class="row mt-5 pl-0">
                        <div class="col pt-3">
                            <P id="payoutClick" class="font-weight-lighter text-white font-weight-bold payout-text" >PAYOUT ACCOUNT</P>
                            @if(!$card)
                            <small class="font-weight-lighter text-white">No bank account added yet.</small>
                            @endif
                        </div>
                    </div>

                    <!-- if user account exists  -->
                    @if($card)
                    <div id="userAccount" class="card mt-2 mb-0 border-radius8 off-white" >
                        <div class="card-body p-0 border-radius8">
                            <div class="row" style="margin: 0px">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="d-flex align-items-start">
                                        <p  class="pt-2 pl-2 userAccountName" >{{$card->cardHolderName}}</p>
                                    </div>
                                    <div class="d-flex align-items-end">
                                        <p class="pb-2 pl-2 userAccountNumber">{{$card->cardId}}</p>
                                    </div>
                                </div> 
                                <div class="col-4 pr-2" >
                                    <div class="d-flex align-items-center">
                                        <a class="red-text pt-3 pl-2 changeLink"  onclick="editAccountDetail({{$card->id}})"><b>Change</b></a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="row pt-2">
                        <div class="col">
                            <a href="#" id="withdrawalButton"  class="btn btn-block rounded-lg red-button font-weight-lighter text-white border-radius8 add-card" data-toggle="modal" data-target="#withdrawalModal" >Request for Withdrawal</a>
                        </div>
                    </div>
                    @else
                    <div class="row pt-2">
                        <div class="col">
                            <a href="#" id="addAccountButton"  class="btn btn-block rounded-lg red-button font-weight-lighter text-white border-radius8 add-card" data-toggle="modal" data-target="#addAccountModal" >Add Card</a>
                        </div>
                    </div>
                    @endif
                    
                    
                </div>
            </div>
        </div>
            <div class="col-lg-9 col-md-12" >
                <div class="card">
                    <div class="card-header pb-0" style="background-color:white">
                        <div class="row">
                            <div class="earn-col  p-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title">Total Earnings</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >${{ Helper::totalEarning(auth()->user()->id) }}</span>
                                </div>
                             </div>
                            <div class="earn-col p-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title">Songs Earnings</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >${{ Helper::totalSongEarning() }}</span>
                                </div>
                            </div>
                            <div class="earn-col p-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title" >Album Earnings</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >${{ Helper::totalAlbumEarning() }}</span>
                                </div>
                            </div>
                            <div class="earn-col p-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title" >Available Balance</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >${{ Helper::number_format_short($availableBalance) }}</span>
                                </div>
                            </div>
                            <div class="earn-col p-2">

                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-total-title" >Total Withdrawal Amount</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >${{ Helper::number_format_short($totalWithdrawalAmount) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pl-0 pt-2 pr-2">
                        <div class="row" style="margin: 0px !important;">
                            <div class`="col-md-12 payout-history-text" style="width: 100% !important;">
                                <small class="payout-text ml-2" style="font-weight:600">Payouts History</small>
                                <select class="float-right month-select" style="background-color: white" id="month" name="month" onchange="getMonthValue();"></select>
                                <select class="float-right year-select" style="background-color: white" id="year" name="year" onchange="getYearValue();"></select>
                            </div>
                            <div class`="col-md-12 pt-5  pl-0 pr-5" style="width: 100% !important;">
                                @if(!$withwithdrawalLists->isEmpty())
                                    <div class="timeline" style="padding-left: 20px;">
                                        @foreach ($withwithdrawalLists as $withwithdrawalList)
                                            <div class="dot-left all m{{Carbon\Carbon::parse($withwithdrawalList->createdAt)->format('F')}}{{Carbon\Carbon::parse($withwithdrawalList->createdAt)->format('Y')}}y  month{{Carbon\Carbon::parse($withwithdrawalList->createdAt)->format('F')}}  year{{Carbon\Carbon::parse($withwithdrawalList->createdAt)->format('Y')}}" id="{{$withwithdrawalList->id}}" style="margin-bottom: 9px">
                                                <span class="dot-earn"></span>
                                                <small class="payout-time pr-5">{{Carbon\Carbon::parse($withwithdrawalList->createdAt)->format('d/m/Y .g:iA')}}</small>
                                                @if ($withwithdrawalList->withdrawalStatus == 'Requested')
                                                    <small class="payout-detail pl-lg-5"> Withdrawal Request Initiated</small>
                                                    <small class="payout-price text-warning timeline-right" style="color: orange !important"> ${{$withwithdrawalList->amount}}</small>
                                                @else
                                                    <small class="payout-detail pl-lg-5"> Administration Cleared Your Funds</small>
                                                    <small class="payout-price text-danger timeline-right" > ${{$withwithdrawalList->amount}}</small>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="pt-4">
                                        <p class="text-center" style="font-size: 12px">No data available in table.</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- Add Account Modal -->
@include('artist.withdrawalEarning.modals.addAccount')

<!-- Edit Account Modal -->
@include('artist.withdrawalEarning.modals.editAccount')

<!-- Withdrawal Modal  -->
@include('artist.withdrawalEarning.modals.withdrawal')

<!-- Withdrawal Response Modal  -->
@include('artist.withdrawalEarning.modals.withdrawalResponse')

<!--Again Withdrawal Modal  -->
@include('artist.withdrawalEarning.modals.againWithdrawal')


<div id="loader" class="lds-dual-ring hidden overlay"></div>

@endsection


@section('script')
<script src="{{ asset('artist/js/withdrawalEarning.js')}}" ></script>
@endsection


