{{-- Edit Account Modal --}}
<div class="modal fade" id="editAccountModal">
    <div class="modal-dialog modal-dialog-centered modal-sm ">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pb-2  pt-3" >
                <small class="modal-title edit-bank-text"><b>Edit Bank Details</b></small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editAccountForm">
            <div class="modal-body">
                <input id="id" name="id" type="hidden">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8"  placeholder="Account Name" id="editAccountName" name="editAccountName" value="Muzamli Afridi" >
                            <div class="invalid-feedback" id="editAccountNameError"></div>
                        </div>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8"  placeholder="Account Number" id="editAccountNumber" name="editAccountNumber" value="1234567891234567" >
                            <div class="invalid-feedback" id="editAccountNumberError"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                  <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>
