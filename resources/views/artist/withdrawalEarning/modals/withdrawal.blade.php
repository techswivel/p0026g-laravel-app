{{-- Withdrawal Modal --}}
<div class="modal fade" id="withdrawalModal">
    <div class="modal-dialog modal-dialog-centered modal-sm ">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <small class="modal-title"><b>Confirmation</b></small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <p class="rwithdrawal-text">You are going to initiate withdrawal request of ${{$withdrawalPrice}}</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form id="withdrawalForm">
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Confirm">
                </form>
            </div>
        </div>
    </div>
</div>
