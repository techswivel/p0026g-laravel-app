{{-- Add Account Modal --}}
<div class="modal fade" id="addAccountModal">
    <div class="modal-dialog modal-dialog-centered modal-sm ">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b>Add Bank Details </b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addAccountForm" >
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8 add-bank-feild"  placeholder="Account Name" style="font-size: 12.5px" id="accountName" name="accountName">
                            <div class="invalid-feedback" id="accountNameError"></div>
                        </div>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8 add-bank-feild"  placeholder="Account Number" style="font-size: 12.5px" id="accountNumber" name="accountNumber">
                            <div class="invalid-feedback" id="accountNumberError"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>
