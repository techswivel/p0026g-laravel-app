@extends('artist.layouts.master')

@section('title')
Privacy Policy
@endsection


@section('style')

@endsection
@section('content')

<div class="row">
    <div class="col-lg-6 offset-lg-3" style="margin-top: 35px">
        @if ($policy)
                <h5 class="mb-3"><b>{!! $policy->title !!}</b></h5>
                <p class=" grey-text">{!! $policy->description !!}</p>
        @else

        @endif
    </div>
</div>
@endsection


@section('script')

@endsection