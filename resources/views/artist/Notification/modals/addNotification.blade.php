{{-- Add Notification Modal --}}
<div class="modal fade" id="addNotificationModal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content border-radius12">
            <div class="modal-header p-3 pb-2 " >
                <small class="modal-title"><b>New Notification ({{Helper::totalNotification($artist->id)[0]}}/{{Helper::totalNotification($artist->id)[1]}} left)</b></small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addNotificationForm">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" id="artistId" name="artistId" value="{{$artist->id}}">
                        <label class="m-0"><small class="text-dark mb-0 font-wegiht600"><b>Title</b></small></label>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8"  placeholder="Enter title" id="notificationTitle" name="notificationTitle">
                            <div class="invalid-feedback" id="notificationTitleError"></div>
                        </div>
                        <label class="m-0 mt-1"><small class="text-dark mb-0 font-wegiht600"><b>Message</b></small></label>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <textarea rows="7" cols="50" name="notificationMessage"  id="notificationMessage"  class="form-control input-xs rounded border-radius8"  placeholder="Enter Notification Message" form="addNotificationForm" ></textarea>
                            <div class="invalid-feedback" id="notificationMessageError"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Send For Approval">
            </div>
            </form>
        </div>
    </div>
</div>
