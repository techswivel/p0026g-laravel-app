@extends('artist.layouts.master')

@section('title')
Notification
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('css/customnotification.css')}}">
<link rel="stylesheet" href="{{ asset('artist/css/notification.css')}}">
@endsection

@section('content')
<div class="row pl-4 pr-4  pt-1 pb-3">
    <div class="col-lg-8 pl-0">
        <span class="withdrawal-text pl-0">Send Notification</span>
        @if (auth()->user()->isNotificationEnabled == 1)
            @if ($isNotificationLimit)
                <button type="button" data-toggle="modal" data-target="#addNotificationModal" class="btn ml-2 red-button btn-xs border-radius8 addNotificationButton "><i class="fas fa-plus"></i> Add new</button>
            @endif
        @endif
    </div>
    <div class="col-lg-2 pl-0 pr-1">
        <select class="float-right p-1 month-select" id="status" name="status" onchange="getStatusValue();"></select>
    </div>
    <div class="col-lg-2 pl-0 pr-0">
        <input type= "search" class="form-control float-right" id="filterSearch"  placeholder="Search">
    </div>
</div>

{{-- Notification Table --}}
<table id="example" class=" display table-striped" >
    <thead>
        <tr style="text-align: center;height:30px">
            <th>Date </th>
            <th>Status</th>
            <th>Notification title</th>
            <th>Notification</th>
            <th></th>
        </tr>
    </thead>
    <tbody id="notificationTable">
        @foreach ($notifications as $notification)
            <tr id="notificationId{{$notification->id}}">
                <td class="p-3">{{Carbon\Carbon::parse($notification->createdAt)->format('d/m/Y')}}</td>
                @if ($notification->status == 'Pending')
                    <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Pending border-radius8">Pending</span></td>
                @elseif ($notification->status == 'Rejected')
                    <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
                @elseif ($notification->status == 'Approved')
                    <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Approved border-radius8">Approved</span></td>
                @elseif ($notification->status == 'DeletedByAdmin')
                    <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-deleted border-radius8">Deleted By Admin</span></td>
                @elseif ($notification->status == 'DeletedByArtist')
                    <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-deleted border-radius8">Deleted By Artist</span></td>
                @endif
                <td class="p-3">{{Str::limit($notification->title, 36)}}</td>
                <td class="p-3">{{ Str::limit($notification->message, 87) }}</td>
                <td class="p-3 float-right d-flex justify-content-start" >
                    <a class="btn text-left btn-xs red-button border-radius8 small-button" style="margin-right: 4px" onclick="notificationDetail({{$notification->id}})"><i class="bi bi-eye-fill"></i></a>
                    <a class="btn text-left btn-xs orange-button border-radius8 small-button"   onclick="notificationDelete({{$notification->id}})"><span class="bi bi-trash3"  id="trash-iconq"></span></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{{-- Add Notification Modal --}}
@include('artist.Notification.modals.addNotification')

{{-- Notification detail Modal --}}
@include('artist.Notification.modals.notificationDetail')

{{-- Delete Notification Modal --}}
@include('artist.Notification.modals.deleteNotification')

<div id="loader" class="lds-dual-ring hidden overlay"></div>

@endsection

@section('script')
<script src="{{ asset('artist/js/notification.js')}}" ></script>
@endsection
