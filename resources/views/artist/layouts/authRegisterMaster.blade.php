<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Sign Up</title>
  <!-- Website Icon -->  
  <link rel="icon" type="image/x-icon" href="{{ asset('artist/image/logo/logo.svg')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/app.css')}}">
  <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
  <!--style sheet -->
  @yield('style')
</head>
<body>
<div class="container-fluid m-0">
    <div class="row">
        <div class="col-lg-6" id="back">
            <div class="p-4 logo">
                <img src="{{ asset('artist/image/logo/logo.svg')}}" alt="logo" width="95" height="64">
            </div>
        </div>
        <div class="col-lg-6 pl-5 pr-5 pb-5 main-wrapper">
              @yield('content')
        </div>
    </div>
</div>

<script src="{{ asset('js/app.js')}}"></script>

@yield('script')

</body>
</html> 

