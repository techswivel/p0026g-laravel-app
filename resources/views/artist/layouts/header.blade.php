<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" id="header-nav">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <div class="user-panel  pb-3  d-flex">
                @if (auth()->user()->avatar)
                    <img src="{!! Helper::urlPath(auth()->user()->avatar) !!}"  class="img-size-32 img-circle img-22"  alt="User Image" >
                @else
                      <img src="{{ asset('artist/image/avatar/default-avatar.png')}}"  class="img-size-32 img-circle img-22"  alt="User Image" >
                @endif
            </div>
        </a>
        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
          <div class="dropdown-item disabled pl-2">
            <!-- Message Start -->
            <div class="media">
              @if (auth()->user()->avatar)
                    <img src="{!! Helper::urlPath(auth()->user()->avatar) !!}"  class="img-size-32 img-circle img-22"  alt="User Image" >
              @else
                    <img src="{{ asset('artist/image/avatar/default-avatar.png')}}"  class="img-size-32 img-circle img-22"  alt="User Image" >
              @endif
                <div class="media-body pl-2">
                <p class="dropdown-item-title">
                  {{auth()->user()->firstName.' '.auth()->user()->lastName}}
                </p>
                <small class="text-sm">{{auth()->user()->email}}</small>

              </div>
            </div></div>
          <div class="dropdown-divider"></div>
          <div class="dropdown-item p-0" >
            <div class="container">
                @role('admin')
                <div class="p-0 float-right d-flex justify-content-start " >
                  <a class="btn text-left btn-xs red-button border-radius8 small-button px-2" href="{{url('admin/changePassword')}}" style="margin-right: 4px" >Change Passwords</a>
                  <a href="{{ route('logout') }}" type="button" class="btn text-left btn-xs red-button border-radius8 small-button px-2" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">Log out</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="">
                    @csrf

                </form>
                </div>
                {{-- <a class="btn btn-danger rounded" style="margin-left: -35px" href="{{url('admin/changePassword')}}">Change Passwords</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="">
                    @csrf
                    <button class="btn btn-danger rounded" style="margin-right: -21px">Logout</button>

                </form> --}}
                @endrole
                @role('artist')
                 <a href="{{ route('logout') }}" type="button" class="btn btn-block btn-danger red-button btn-xs rounded" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">Log out</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="">
                    @csrf

                </form>
                @endrole
            </div>
            </div>
        </div>
      </li>
       </ul>
  </nav>
  <!-- /.navbar -->

