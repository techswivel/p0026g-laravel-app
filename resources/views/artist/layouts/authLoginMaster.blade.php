<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <title>@yield('title')</title>
  <!-- Website Icon -->  
  <link rel="icon" type="image/x-icon" href="{{ asset('artist/image/logo/logo.svg')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/app.css')}}">
 <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
 
  <!--style sheet -->
  @yield('style')
</head>
<body>
  <div class="login-box">
    <div class="login-logo"> 
      <img src="{{ asset('artist/image/logo/logo.svg')}}" alt="logo" width="95" height="64">
    </div>
    <div class="contrainer ">
      @yield('content')
    </div>
  </div>

<!-- AdminLTE App -->
<script src="{{ asset('js/app.js')}}"></script>

@yield('script')

</body>
</html>
