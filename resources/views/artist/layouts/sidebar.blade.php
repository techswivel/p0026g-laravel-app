<aside class="main-sidebar sidebar-dark-primary elevation-4 text-xs" style="background-color: #242424;">
    <!-- Brand Logo -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-1  pb-0 mb-0 d-flex" style="margin-top: 4px !important;">
      <div class="text-center">
        <div class="d-flex align-items-center">
          <p class="" style="color: white;font-size: 15px;margin-top: 1px !important;">Q the Music </p>
        </div>
        </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item width180">
          <a href="{{ route('artist.dashboard')}}" class="nav-link white-color width180">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item width180">
          <a href="{{ route('artist.profile')}}" class="nav-link white-color width180">
            <i class="nav-icon fas fa-guitar"></i>
            <p>
                Artists
            </p>
          </a>
        </li>
        <li class="nav-item width180">
          <a href="{{ route('artist.notification')}}" class="nav-link white-color width180">
            <i class="nav-icon far fa-bell"></i>
            <p>
                Notifications
            </p>
          </a>
        </li>
        <li class="nav-item width180">
          <a href="{{ route('artist.WithdrawalEarnnig')}}" class="nav-link white-color width180">
            <i class="nav-icon fas fa-money-check-alt"></i>
            <p>
                Withdrawal Earnings
            </p>
          </a>
        </li>
        <li class="nav-item width180">
            <a href="{{ route('artist.terms')}}" class="nav-link white-color width180">
                <i class="nav-icon far fa-handshake"></i>
              <p>
                Terms and conditions
              </p>
            </a>
          </li>
          <li class="nav-item width180">
            <a href="{{ route('artist.policy')}}" class="nav-link white-color width180">
                <i class="nav-icon fas fa-business-time"></i>
              <p>
                Privacy policy
              </p>
            </a>
          </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>


