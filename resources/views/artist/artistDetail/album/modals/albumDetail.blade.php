{{-- Album detail Modal --}}
<div class="modal fade" id="albumDetail">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div id="albumThumbnail" class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" style="background-color: #242424;color:white">
               
            </div>
            <div class="modal-body p-0 ">
                <div class="row">
                    <div class="col-12 table-wrapper-scroll-y my-custom-scrollbar"">
                        <table id="albumSongListTable" class="table responsive ">
                            <tbody id="albumSongListTableBody">
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
