{{-- Select Song  Modal --}}
<div class="modal fade" id="editSelectingSongModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Select Songs</b></p>
                <button type="button" class="close canelEditButton"  >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editSelectingSongForm" >
            <div class="modal-body pl-1">
                <!-- checkbox -->
                <div id="editSelectSongDisplay" class="row mt-1 mb-3" style="margin: 0px; margin-left:24px" >
                    <div class="col-2 p-1 pr-0 mt-1 mr-0 float-right">
                        <div class="icheck-danger d-inline">
                            <input  type="checkbox"  id="editCheckboxPrimaryAll" />
                            <label for="editCheckboxPrimaryAll"> </label>
                        </div> 
                    </div>
                    <div class="col-10 p-2">
                        <p class="modal-title add-bank-text" ><b class="font-size12">Select All</b></p>
                    </div>
                </div>
                <div id="editListSong">
                </div>
                <div class="row mt-1 mb-3" style="margin: 0px; margin-left:24px">
                    <div class="invalid-feedback" id="editSelectSongError"></div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey canelEditButton" >Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>
