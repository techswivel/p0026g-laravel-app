{{-- Add Album  Modal --}}
<div class="modal fade" id="addAlbumModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pr-2 p-0 border-0">
                <p class="modal-title add-bank-text"><b class="font-size12">Create Album</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <ul class="nav nav-pills border-bottom" role="tablist">
                <li class="nav-link add-album-progress-step-1 w-50 active"></li>
                <li class="nav-link add-album-progress-step-2 w-50"></li>
            </ul>
            <form id="addAlbumForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" id="albumStepNo" name="stepNo" value="1">
                            <input type="hidden" id="albumId" name="albumId">
                            <input type="hidden" id="addAlbumiAppPurchasePrice" name="iAppPurchasePrice">
                            <input type="hidden" name="artistId" value="{{ $artist->id }}">

                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="album-pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                                        <div class="mb-2 mt-0 form-group input-group-sm input_field ">
                                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"  placeholder="Enter Album title" maxlength="30" style="font-size: 12.5px" id="albumName" name="albumName" required>
                                            <div class="invalid-feedback" id="albumNameError"></div>
                                        </div>

                                        <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                            <label class="m-0 mb-0"> <span class="text-dark add-song-label-text">Thumbnail </span></label><br>
                                            <input type="file" style="font-size: 12.5px;margin: .4rem 0;color:gray" id="albumThumbnail" name="albumThumbnail" accept="image/*" required><br>
                                            <span class="add-song-span-text ">Max. Size 2MB ( Only Jpg, Png, Jpeg | Dimensions 1:1 )</span>
                                            <div class="invalid-feedback" id="albumThumbnailError"></div>
                                        </div>

                                        <div id="album-status-div" class="mb-2 mt-1 input-group-sm input_field ">
                                            <label class="m-0 mb-1"><span class="text-dark add-song-label-text">Is Album?</span></label>
                                            <select name="albumStatus" id="albumStatus" class="form-control">
                                                <option value="Paid">Paid Album</option>
                                                <option value="Free">Free Album</option>
                                            </select>
                                        </div>

                                        <div id="edit-price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Add Song</span></label>
                                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text" placeholder="Select Songs" id="selectSong" onclick="listTheSelectSong({{ $artist->id }})" name="selectSong" readonly>
                                            <div class="invalid-feedback" id="selectedSongsNameError"></div>
                                        </div>

                                        <select style="display: none" class="select form-control input-sm border-radius8 add-song-input-text" multiple data-mdb-placeholder="Example placeholder" id="selectedSongsName" name="selectedSongsName[]" multiple></select>

                                        <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                            <div class="invalid-feedback">Please fill out this field.</div>
                                        </div>

                                        <div class="row my-1" id="listAddSong"></div>

                                        <div class="modal-footer justify-content-between p-0 pt-2 " style="margin-left: 4px">
                                            <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey"  data-dismiss="modal">Canel</button>
                                            <input type="button" class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button album-song-next-btn" value="Next">
                                        </div>

                                </div>
                                <div class="tab-pane fade" id="album-pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <div class="mb-2 mt-0 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"><span class="text-dark add-song-label-text">Album Price:</span></label>
                                        <span class="text-dark add-song-label-text" id="songsCost"></span>
                                    </div>

                                    <div id="price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                        <label class="m-0 mb-1"><span class="text-dark add-song-label-text">Price</span></label>
                                        <select name="priceTier" id="albumPriceTier" class="form-control"></select>
                                        <div class="invalid-feedback" id="albumSongPriceError"></div>
                                    </div>

                                    <div class="modal-footer justify-content-between p-0 pt-2 " style="margin-left: 4px">
                                        <button type="button" class="btn btn-xs py-1 px-3 border-radius8 light-grey album-song-previous-btn" >Previous</button>
                                        <input type="submit" class="btn red-button p3 btn-xs mr-2 py-1 px-3 border-radius8 account-Create-button" value="Create">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
