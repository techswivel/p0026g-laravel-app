{{-- Edit Album  Modal --}}
<div class="modal fade" id="editAlbumModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pb-2 pr-2">
                <p class="modal-title add-bank-text"><b class="font-size12">Edit Album</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editAlbumForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" id="editAlbumId" name="albumId">
                            <input type="hidden" id="artistId" name="artistId" value="{{ $artist->id }}">
                            <input type="hidden" id="editIAppPurchasePrice" name="iAppPurchasePrice">

                            <div class="mb-2 mt-0 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span
                                        class="text-dark add-song-label-text">Title</span></label>
                                <input type="text" class="form-control input-sm border-radius8 add-song-input-text"
                                    maxlength="30" placeholder="Enter Album title" style="font-size: 12.5px"
                                    id="editAlbumName" name="editAlbumName" required>
                                <div class="invalid-feedback" id="editAlbumNameError"></div>
                            </div>

                            <div class="mb-2 mt-1 form-group input-group-sm input_field">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Thumbnail
                                    </span></label>
                                <div class="d-flex flex-row">
                                    <div id="editThumbnailSong" class="roundedImage rounded-circle  mt-0 editSongModal">
                                    </div>
                                    <div class="d-flex align-items-end image-div">
                                        <label>
                                            <span class="dot d-flex justify-content-center">
                                                <span class="bi bi-pencil-square fa-md" id="pencil-iconq"
                                                    style="color: white;font-size:11px"></span>
                                            </span>
                                            <input type="file" id="editAlbumThumbnail" name="editAlbumThumbnail"
                                                style="display: none;" accept="image/*">
                                        </label>
                                    </div>
                                </div>
                                <div class="add-song-span-text mt-1">Max. Size 2MB ( Only Jpg, Png, Jpeg | Dimensions 1:1 )</div>
                            </div>
                            <div class="invalid-feedback" id="editAlbumThumbnailError"></div>

                            <div id="edit-price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Add
                                        Song</span></label>
                                <input type="text" class="form-control input-sm border-radius8 add-song-input-text"
                                    placeholder="Select Songs" id="editSelectSong"
                                    onclick="editListTheSelectSong({{ $artist->id }})" name="editSelectSong" readonly>
                                <div class="invalid-feedback" id="editSelectedSongsNameError"></div>
                            </div>

                            <select style="display: none" class="select form-control input-sm border-radius8 add-song-input-text" multiple
                                data-mdb-placeholder="Example placeholder" id="editSelectedSongsName"
                                name="editSelectedSongsName[]" multiple>
                            </select>


                            <div class="row mt-1" id="editListSaveSong">
                            </div>

                            <div class="row mt-1" id="editListAddSong">
                            </div>

                            <div class="mb-2 mt-3 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Album Price:</span></label>
                                <p class="text-dark add-song-label-text" id="editSongsCost"></p>

                            </div>

                            <div id="price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                <label class="m-0 mb-1"><span class="text-dark add-song-label-text">Price</span></label>
                                <select name="priceTier" id="albumEditPriceTier" class="form-control"></select>
                                <div class="invalid-feedback" id="albumEditSongPriceError"></div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                    <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey"
                        data-dismiss="modal">Canel</button>
                    <input type="submit"
                        class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button"
                        value="Update">
                </div>
            </form>
        </div>
    </div>
</div>
