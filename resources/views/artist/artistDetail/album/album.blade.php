<div class="row m-0 mb-1 mt-2">
    <div class="col-md-6">
        <small class="float-left ml-2 pt-1"  style="font-weight:600">Albums List</small>
        <button type="button" data-toggle="modal" data-target="#addAlbumModal" style="font-size:12px"class="btn ml-2 red-button btn-xs border-radius8 add-album-button"><i class="fas fa-plus"></i> Create Album</button>
    </div>
    <div class="col-md-6">
        <input type= "search" style="width: 137.5px; margin:0px !important; margin-top:1px !important;margin-right:9px !important" class="form-control ml-3 mr-3  float-right font-size12" id="albumFilterSearch"  placeholder="Search">
    </div>
</div>

<div class="row m-0 mt-4" id="albumList">
    @if (count($albums) != 0)
        @foreach ($albums as $album)
        <div class=" box ml-2" style="width: 88px" id="album{{$album->id}}">
            <center class=" float-left">
            @if ($album->albumStatus == 'Paid')
                <a href="javascript:void(0)" onclick="albumDetail({{$album->id}})"><img src="{!! Helper::urlPath($album->thumbnail) !!}"  class="border-radius5" width="80px" height="80px" alt="">
                <small  class="name" style="font-size: 10px;color:black">{{Str::limit($album->name, 12)}}</small></a>
            </center>
            <span class="dot-prime" style="margin-left:-22px"><i class="fas fa-crown"></i></span>
            @else
                <a href="javascript:void(0)" onclick="albumDetail({{$album->id}})"><img src="{!! Helper::urlPath($album->thumbnail) !!}"  class="border-radius5" width="80px" height="80px" alt="">
                <small  class="name" style="font-size: 10px;color:black">{{Str::limit($album->name, 12)}} </small></a>
            </center>
            @endif
        </div>
        @endforeach
    @else
        <div class="row" style="margin:0px; width:100%" >
            <div class="col-lg-12" style="text-align: center">
                <p >No data available in table</p>
            </div>
        </div>
    @endif
    </div>

{{-- Album detail Modal --}}
@include('artist.artistDetail.album.modals.albumDetail')

{{-- Add Album  Modal --}}
@include('artist.artistDetail.album.modals.addAlbum')

{{-- Delete album Modal --}}
@include('artist.artistDetail.album.modals.deleteAlbum')

{{-- Select Song  Modal --}}
@include('artist.artistDetail.album.modals.selectSong')

{{-- Edit Album  Modal --}}
@include('artist.artistDetail.album.modals.editAlbum')

{{--Edit Select Song  Modal --}}
@include('artist.artistDetail.album.modals.editSelectSong')

{{-- Delete Song from album Modal --}}
@include('artist.artistDetail.album.modals.deleteSongFromAlbum')

{{-- Edit Song Modal --}}
@include('artist.artistDetail.album.modals.editSongFromAlbum')
