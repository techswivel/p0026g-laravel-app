{{-- Notification Table --}}
<table id="notificationTable" class="table display "  style="font-size: 12px;">
    <thead class="border-none">
        <tr class="border-none">
            <th class="border-none pt-0 pl-3">Date</th>
            <th class="border-none pt-0 pl-3">Status</th>
            <th class="border-none pt-0 pl-3">Notification title</th>
            <th class="border-none pt-0 pl-3">Notification</th>
            <th class="border-none pt-0 pl-3"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($notifications as $notification)
        <tr id="notificationId{{$notification->id}}">
            <td class="p-3">{{Carbon\Carbon::parse($notification->createdAt)->format('d/m/Y')}}</td>
            @if ($notification->status == 'Pending')
                <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Pending border-radius8">Pending</span></td>
            @elseif ($notification->status == 'Rejected')
                <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
            @elseif ($notification->status == 'Approved')
                <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Approved border-radius8">Approved</span></td>
            @elseif ($notification->status == 'DeletedByAdmin')
                <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-deleted border-radius8">Deleted By Admin</span></td>
            @elseif ($notification->status == 'DeletedByArtist')
                <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-deleted border-radius8">Deleted By Artist</span></td>
            @endif
            <td class="p-3">{{Str::limit($notification->title, 36)}}</td>
            <td class="p-3">{{ Str::limit($notification->message, 87) }}</td>
            <td class="p-3 d-flex justify-content-start" >
                    <a class="btn text-left btn-xs red-button border-radius8 small-button" style="margin-right: 4px" onclick="notificationDetail({{$notification->id}})"><i class="bi bi-eye-fill"></i></a>
                    <a class="btn text-left btn-xs orange-button border-radius8 small-button"   onclick="notificationDelete({{$notification->id}})"><span class="bi bi-trash3"  id="trash-iconq"></span></a>
            </td>
        </tr>       
        @endforeach
    </tbody>
</table>

{{-- Add Notification Modal --}}
@include('artist.Notification.modals.addNotification')

{{-- Notification detail Modal --}}
@include('artist.Notification.modals.notificationDetail')

{{-- Delete Notification Modal --}}
@include('artist.Notification.modals.deleteNotification')
