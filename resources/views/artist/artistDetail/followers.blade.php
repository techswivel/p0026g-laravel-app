 {{-- Follower Table --}}
<table id="followerTable" class="table display "  style="font-size: 12px;">
    <thead class="border-none">
        <tr class="border-none">
            <th class="border-none pt-0 pl-3">#No.</th>
            <th class="border-none pt-0 pl-3">User Name</th>
            <th class="border-none pt-0 pl-3">Email</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($followers as $index =>$follower)
            <tr>
                <td class="border-none p-3 pt-4">{{ ++$index}}</td>
                <td class="border-none p-3">
                    <div class="d-flex flex-row">
                        @if ($follower->user->avatar)
                            <img src="{{Helper::urlPath($follower->user->avatar)}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  
                        @else    
                            <img src="{{ asset('artist/image/avatar/default-avatar.png')}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  
                        @endif
                        <div class="ml-2 padding-top5">{{$follower->user->firstName}} {{$follower->user->lastName}}</div>
                    </div>
                </td>
                <td class="border-none p-3 pt-4">{{$follower->user->email}}</td>
            </tr>    
        @endforeach
    </tbody>
</table>


    