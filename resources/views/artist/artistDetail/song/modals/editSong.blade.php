{{-- Edit Song Modal --}}
<div class="modal fade" id="editSong">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2">
                <p class="modal-title add-bank-text"><b class="font-size12">Edit Song</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editSongForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" id="id" name="id">
                            <input type="hidden" id="editPrice" name="editPrice" value="">
                            <input type="hidden" id="artistId" name="artistId" value="{{ $artist->id }}">

                            <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span
                                        class="text-dark add-song-label-text">Title</span></label>
                                <input type="text" class="form-control input-sm border-radius8 add-song-input-text"
                                    value="{{ old('editSongName') }}" placeholder="Enter song title"
                                    style="font-size: 12.5px" id="editSongName" name="editSongName" required>
                                <div class="invalid-feedback" id="editSongNameError"></div>
                            </div>

                            <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Is Song
                                        ?</span></label>
                                <select id="editSongStatus" name="editSongStatus"
                                    class="form-control border-radius8 add-song-input-text"
                                    onchange="editStatusValue();" required>
                                    <option class="add-song-input-text" value="Paid Song">Paid Song</option>
                                    <option class="add-song-input-text" value="Free Song">Free Song</option>
                                </select>
                                <div class="invalid-feedback" id="editSongStatusError"></div>
                            </div>

                            <div id="edit-price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Price</span></label>
                                <select name="priceTier" id="editPriceTier" class="form-control"></select>
                                <div class="invalid-feedback" id="editPriceError"></div>
                            </div>

                            <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span
                                        class="text-dark add-song-label-text">Category</span></label>
                                <select class="form-control font-size12 border-radius8 add-song-input-text"
                                    id="editSongCategory" value="" name="editSongCategory" required>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback" id="editSongCategoryError"></div>
                            </div>
                            <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span
                                        class="text-dark add-song-label-text">Language</span></label>
                                <select class="form-control font-size12 border-radius8 add-song-input-text"
                                    id="editSongLanguage" value="" name="editSongLanguage" required>
                                    @foreach ($languages as $language)
                                        <option value="{{ $language['id'] }}">{{ $language['name'] }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback" id="editSongLanguageError"></div>
                            </div>
                            <div class="mb-2 mt-1 form-group input-group-sm input_field">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Thumbnail
                                    </span></label>
                                <div class="d-flex flex-row">
                                    <div id="editSongThumbnail" class="roundedImage rounded-circle  mt-0 editSongModal">
                                    </div>
                                    <div class="d-flex align-items-end image-div">
                                        <label>
                                            <span class="dot d-flex justify-content-center">
                                                <span class="bi bi-pencil-square fa-md" id="pencil-iconq"
                                                    style="color: white;font-size:11px"></span>
                                            </span>
                                            <input type="file" id="editThumbnail" name="editThumbnail"
                                                style="display: none;" accept="image/*">
                                        </label>
                                    </div>
                                </div>
                                <small class="add-song-span-text mt-1" style="font-size: 9px">Max. Size 2MB ( Only Jpg, Png, Jpeg | Dimensions 1:1 )</small>
                                <div class="invalid-feedback" id="editThumbnailError"></div>
                            </div>
                            <div class=" mb-2 mt-1 form-group input-group-sm input_field">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Audio File
                                    </span></label>
                                <div class="d-flex flex-row">
                                    <div class="roundedImage rounded-circle  mt-0 editSongModal "
                                        style="background-image: url({{ url('artist/image/music.jpg') }});"></div>
                                    <div class="d-flex align-items-end image-div">
                                        <label>
                                            <span class="dot d-flex justify-content-center">
                                                <span class="bi bi-pencil-square fa-md" id="pencil-iconq"
                                                    style="color: white;font-size:11px"></span>
                                            </span>
                                            <input type="file" id="editAudioFile" name="editAudioFile"
                                                style="display: none;" accept="audio/*">
                                        </label>
                                    </div>
                                </div>
                                <div id="songAudioName" class="add-song-span-text mt-1"></div>
                                <div class="invalid-feedback" id="editAudioFileError"></div>
                                <div class="add-song-span-text mt-1">Max. Size 10MB ( Only audio/mpeg, mp3, wav )</div>
                            </div>
                            <div class="videoFormRound mb-2 mt-1 form-group input-group-sm input_field">
                                <label class="videoFormRound m-0 mb-1"> <span
                                        class="videoFormRound text-dark add-song-label-text">Vidoe File <span
                                            class="videoFormRound add-song-span-text ">( Optional
                                            )</span></span></label>
                                <div class="videoFormRound d-flex flex-row">
                                    <div class="roundedImage rounded-circle videoFormRound  mt-0 editSongModal "
                                        style="background-image: url({{ url('artist/image/video.png') }});"></div>
                                    <div class="d-flex align-items-end videoFormRound image-div">
                                        <label>
                                            <span class="dot d-flex justify-content-center videoFormRound">
                                                <span class="bi bi-pencil-square fa-md videoFormRound"
                                                    id="pencil-iconq" style="color: white;font-size:11px"></span>
                                            </span>
                                            <input type="file" name="editVideoFile1" id="editVideoFile1"
                                                style="display: none;" accept="video/mp4">
                                        </label>
                                    </div>
                                </div>
                                <div class="add-song-span-text mt-1 ">Max. Size 10MB (Only MP4) </div>
                                <div class="invalid-feedback " id="editVideoFile1Error"></div>
                            </div>
                            <div class="videoForm mb-2 mt-1 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Video File <span
                                            class="add-song-span-text ">( Optional )</span></span></label>
                                <input type="file" class="form-control-file input-sm " value=""
                                    placeholder="Choose file" style="font-size: 12.5px" id="editVideoFile"
                                    name="editVideoFile" accept="video/mp4">
                                <div class="add-song-span-text mt-1">Max. Size 10MB (Only MP4) </div>
                                <div class="invalid-feedback " id="editVideoFileError"></div>
                                <div class="invalid-feedback">Please fill out this field.</div>
                            </div>
                            <div class="mb-3 mt-2 input-group-sm input_field ">
                                <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"><b>Lyrics <span
                                                class="add-song-span-text "> ( Optional )</span></b></span></label>
                                <textarea rows="3" cols="50" name="editSongLyrics" id="editSongLyrics" value=""
                                    class="form-control add-song-input-text input-xs rounded border-radius8" placeholder="Enter Lyrics.."></textarea>
                                <div class="invalid-feedback">Please fill out this field.</div>
                                <div class="invalid-feedback" id="editSongLyricsError"></div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                    <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey"
                        data-dismiss="modal">Canel</button>
                    <input type="submit"
                        class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button"
                        value="Update">
                </div>
            </form>
        </div>
    </div>
</div>
