 {{-- Song Table --}}
<table id="songTable" class="table display " >
    <thead id="song-thead">
        <tr>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($songs as $song)
            <tr id="id{{$song['id']}}" style="background-color: white !important;">
                <td  class="p-0 pl-2">
                    <ul class="products-list product-list-in-card pl-2 pr-1">
                        <li class="item">
                            <div id="imageDiv{{$song['id']}}" class="product-img pt-1">
                                <img src="{!! Helper::urlPath($song->thumbnail) !!}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                            </div>
                            <div id="titleDiv{{$song['id']}}" class="product-info ml-5">


                                @if ($song->isPaid)
                                    <span class="dot-prime" style="margin-left: -30px"><i class="fas fa-crown"></i></span>
                                    <small  class="product-title font-szie10" style="margin-left: 5px !important">{{Str::limit($song->name, 12)}}<span class="light-grey-text font-weight-lighter" > ({{$song->songLength}})</span>
                                        @if ($song->status != 'PENDING')
                                            <i class="bi bi-play-circle-fill mediaPlayer" id="playInTable{{$song->id}}" onclick="playMusic({{$song['id']}})"></i>
                                        @endif
                                    </small>
                                    <small class="isPaided product-description font-szie10 light-grey-text" style="margin-left: -5px !important">Paid. ${{$song->price}}</small>
                                @else
                                    <small  class="product-title font-szie10" style="margin-left: -5px !important">{{Str::limit($song->name, 12)}}<span class="light-grey-text font-weight-lighter" > ({{$song->songLength}})</span>
                                        @if ($song->status != 'PENDING')
                                            <i class="bi bi-play-circle-fill mediaPlayer playInTable" id="playInTable{{$song->id}}" onclick="playMusic({{$song['id']}})"></i>
                                        @endif
                                    </small>
                                    <small  class="isPaided product-description font-szie10 light-grey-text" style="margin-left: -5px !important">Free song</small>
                                @endif

                                @if ($song->status == 'PENDING')
                                    <div class="light-grey-text font-weight-lighter" style="margin-left: -5px !important"> <i class="fa fa-clock text-orange"></i> Pending</div>
                                @endif

                            </div>
                        </li>
                    </ul>
                </td>
                <td id="isVideo{{$song['id']}}" class="pl-0 pr-0">
                    <span class=" p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                    @if ($song->videoFile)
                        <span class="p-1 pl-2 pr-2 text-Audio text-video border-radius8 font-size12">Video</span>
                    @endif
                </td>
                <td class="pb-2 " >
                    <div class="float-right">
                    <a class="btn text-left btn-xs orange-button border-radius8 small-button"  href="javascript:void(0)"  onclick="deleteSong({{$song->id}})" ><span class="bi bi-trash3"  id="trash-iconq"></span></a>
                    <a class="btn text-left btn-xs red-button border-radius8 small-button" style="margin-right: 7px" href="javascript:void(0)"  onclick="editSong({{$song->id}})"><span class="bi bi-pencil-square fa-md" id="pencil-iconq"></span></a>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@include('artist.artistDetail.song.modals.addSong')

@include('artist.artistDetail.song.modals.editSong')

@include('artist.artistDetail.song.modals.deleteSong')
