{{-- Pre order song detail Modal --}}
<div class="modal fade" id="preorderSongBuyerDetail">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" id="preorderSongDetail" style="background-color: #242424;color:white">
                
            </div>
            <div class="modal-body p-0 ">
                <div class="row">
                    <div class="col-12 table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table responsive table-striped ">
                            <tbody id="preorderSongbuyerDetail">
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
