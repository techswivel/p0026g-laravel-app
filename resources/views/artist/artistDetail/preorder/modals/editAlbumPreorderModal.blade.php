{{-- Edit Album pre Order Modal --}}
<div class="modal fade" id="editAlbumPreorderModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Edit Pre Order</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editAlbumPreorderForm">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" id="editPreorderAlbumId" name='editPreorderAlbumId' />
                        <input type="hidden" id="editAlbumInAppPurchasePrice" name="iAppPurchasePrice">


                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Order Type ?</span></label>
                            <select id="orderTypeAlbum" name="editOrderTypeAlbum" class="form-control border-radius8 add-song-input-text"  onchange="resetPreorderAlbumStatusValue();">
                                <option class="add-song-input-text">Song</option>
                                <option class="add-song-input-text" selected>Album</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Album title" value="Life is fire" style="font-size: 12.5px" maxlength="30" id="editPreorderAlbumName" name="editPreorderAlbumName" required>
                            <div class="invalid-feedback" id="editPreorderAlbumNameError"></div>
                        </div>
                        <input type="hidden" id="preorderAlbumId" name="preorderAlbumId" >
                        <div class="mb-2 mt-1 form-group input-group-sm input_field" >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Thumbnail </span></label>
                            <div class="d-flex flex-row">
                                <div id="editPreorderAlbumThumbnailDisplay" class ="roundedImage rounded-circle  mt-0 editSongModal" ></div>
                                <div class="d-flex align-items-end image-div">
                                <label>
                                    <span class="dot d-flex justify-content-center">
                                        <span class="bi bi-pencil-square fa-md" id="pencil-iconq" style="color: white;font-size:11px"></span>
                                    </span>
                                    <input type="file" id="editPreorderAlbumThumbnail" name="editPreorderAlbumThumbnail" style="display: none;" accept="image/*">
                                </label>
                                </div>
                            </div>
                            <div class="invalid-feedback" id="editPreorderAlbumThumbnailError"></div>
                            <div class="add-song-span-text mt-1">Max. Size 2MB ( Only Jpg, Png, Jpeg | Dimensions 1:1 )</div>
                        </div>
                        <div class=" mt-0 mb-1 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Date:</b></span></label>
                            <div class="input-group date" id="editAlbumReservationdate" data-target-input="nearest">
                                <input type="text" class="form-control input-sm border-left datetimepicker-input" placeholder="Select publish date" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#editAlbumReservationdate" id="editPreorderAlbumDate" name="editPreorderAlbumDate" required>
                                <div class="input-group-append border-right" data-target="#editAlbumReservationdate" data-toggle="datetimepicker">
                                    <div class="input-group-text red-text border-right" style="background-color: white;border-left:none;height:31px;"><i class="bi bi-calendar3"></i></div>
                                </div>
                            </div>
                            <div class="invalid-feedback" id="editPreorderAlbumDateError"></div>
                        </div>
                        <div class="mt-0 mb-1 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Time:</b></span></label>
                            <div class="input-group date" id="editAlbumTimepicker" data-target-input="nearest">
                                <input type="text" class="form-control input-sm border-left datetimepicker-input" placeholder="Select publish time" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#editAlbumTimepicker" id="editPreorderAlbumTime" name="editPreorderAlbumTime" required>
                                <div class="input-group-append border-right" data-target="#editAlbumTimepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text border-right red-text" style="background-color: white;border-left:none;height:31px;"><i class="far fa-clock"></i></div>
                                </div>
                            </div>
                            <div class="invalid-feedback" id="editPreorderAlbumTimeError"></div>
                        </div>
                        <div class="mb-3 mt-3 mb-0 input-group-sm input_field " >
                            <button type="button" onclick="editStoreAlbumSong()"  class=" btn ml-0 pink-button btn-xs border-radius8 pt-1 pb-1 pl-2 pr-2"><i class="fas fa-plus"></i> Add Songs Detail</button>
                        </div>
                        <div class="invalid-feedback" id="editPreorderSelectedSongsError"></div>
                        <select style="display: none" class="select form-control input-sm border-radius8 add-song-input-text" multiple data-mdb-placeholder="Example placeholder" id="editPreorderSelectedSongs" name="editPreorderSelectedSongs[]" multiple>
                        </select>
                        <select style="display: none" class="select form-control input-sm border-radius8 add-song-input-text" multiple data-mdb-placeholder="Example placeholder" id="deleteAlbumPreorderSelectedSongs" name="deleteAlbumPreorderSelectedSongs[]" multiple>
                        </select>
                        <div class="row" id="editPreorderalbumSongList">
                        </div>

                        <div class="mb-2 mt-3 form-group input-group-sm input_field ">
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Album Price:</span></label>
                            <p class="text-dark add-song-label-text" id="editAlbumSongsCost"></p>
                        </div>

                        <div id="price-div" class="mb-2 mt-1 input-group-sm input_field ">
                            <label class="m-0 mb-1"><span class="text-dark add-song-label-text">Price</span></label>
                            <select name="priceTier" id="editAlbumPreorderPrice" class="form-control"></select>
                            <div class="invalid-feedback" id="albumPreorderSongPriceError"></div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>
