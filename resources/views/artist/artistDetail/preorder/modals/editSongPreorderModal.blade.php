{{-- Edit Song pre Order Modal --}}
<div class="modal fade" id="editSongPreorderModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2">
                <p class="modal-title add-bank-text"><b class="font-size12">Edit Pre Order</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editSongPreorderForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" id="preorderEditPrice" name="editPreorderSongPrice" value="">
                            <input type="hidden" id="preorderEditSongId" name="preorderSongId" />

                            <div class="mb-2 mt-0 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Order Type
                                        ?</span></label>
                                <select id="editOrderTypeSong" name="edtiOrderTypeSong"
                                    class="form-control border-radius8 add-song-input-text"
                                    onchange="resetPreorderSongStatusValue();">
                                    <option class="add-song-input-text" selected>Song</option>
                                    <option class="add-song-input-text">Album</option>
                                </select>
                            </div>

                            <div class="mb-2 mt-0 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span
                                        class="text-dark add-song-label-text">Title</span></label>
                                <input type="text" class="form-control input-sm border-radius8 add-song-input-text"
                                    maxlength="30" placeholder="Enter song title" style="font-size: 12.5px"
                                    id="editPreorderSongName" name="editPreorderSongName" required>
                                <div class="invalid-feedback" id="editPreorderSongNameError"></div>
                            </div>
                            <div id="price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Price</span></label>
                                <select name="priceTier" id="editPreorderPriceTier" class="form-control"></select>
                                <div class="invalid-feedback" id="editPreorderSongPriceError"></div>
                            </div>
                            <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span
                                        class="text-dark add-song-label-text">Category</span></label>
                                <select class="form-control font-size12 border-radius8 add-song-input-text"
                                    id="editPreorderSongCategory" name="editPreorderSongCategory" required>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback" id="editPreorderSongCategoryError"></div>
                            </div>
                            <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span
                                        class="text-dark add-song-label-text">Language</span></label>
                                <select class="form-control font-size12 border-radius8 add-song-input-text"
                                    id="editPreorderSongLanguage" name="editPreorderSongLanguage" required>
                                    @foreach ($languages as $language)
                                        <option value="{{ $language['id'] }}">{{ $language['name'] }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback" id="editPreoderSongLanguageError"></div>
                            </div>
                            <div class="mb-2 mt-1 form-group input-group-sm input_field">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Thumbnail
                                    </span></label>
                                <div class="d-flex flex-row">
                                    <div id="editPreorderSongThumbnailDisplay"
                                        class="roundedImage rounded-circle  mt-0 editSongModal"></div>
                                    <div class="d-flex align-items-end image-div">
                                        <label>
                                            <span class="dot d-flex justify-content-center">
                                                <span class="bi bi-pencil-square fa-md" id="pencil-iconq"
                                                    style="color: white;font-size:11px"></span>
                                            </span>
                                            <input type="file" id="editPreorderSongThumbnail"
                                                name="editPreorderSongThumbnail" style="display: none;"
                                                accept="image/*">
                                        </label>
                                    </div>
                                </div>
                                <div class="add-song-span-text mt-1">Max. Size 2MB ( Only Jpg, Png, Jpeg | Dimensions 1:1 ) </div>
                                <div class="invalid-feedback" id="editPreorderSongThumbnailError"></div>
                            </div>
                            <div class=" mb-2 mt-1 form-group input-group-sm input_field">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Audio File
                                    </span></label>
                                <div class="d-flex flex-row">
                                    <div class="roundedImage rounded-circle  mt-0 editSongModal "
                                        style="background-image: url({{ url('artist/image/music.jpg') }});"></div>
                                    <div class="d-flex align-items-end image-div">
                                        <label>
                                            <span class="dot d-flex justify-content-center">
                                                <span class="bi bi-pencil-square fa-md" id="pencil-iconq"
                                                    style="color: white;font-size:11px"></span>
                                            </span>
                                            <input type="file" id="editPreorderAudioFile"
                                                name="editPreorderAudioFile" style="display: none;" accept="audio/*">
                                        </label>
                                    </div>
                                </div>
                                <div id="songAudioName" class="add-song-span-text mt-1"></div>
                                <div class="invalid-feedback" id="editPreorderAudioFileError"></div>
                                <div class="add-song-span-text mt-1">Max. Size 10MB ( Only audio/mpeg, mp3, wav )</div>
                            </div>
                            <div class="videoFormRound mb-2 mt-1 form-group input-group-sm input_field">
                                <label class="videoFormRound m-0 mb-1"> <span
                                        class="videoFormRound text-dark add-song-label-text">Vidoe File <span
                                            class="videoFormRound add-song-span-text ">( Optional
                                            )</span></span></label>
                                <div class="videoFormRound d-flex flex-row">
                                    <div class="roundedImage rounded-circle videoFormRound  mt-0 editSongModal "
                                        style="background-image: url({{ url('artist/image/video.png') }});"></div>
                                    <div class="d-flex align-items-end videoFormRound image-div">
                                        <label>
                                            <span class="dot d-flex justify-content-center videoFormRound">
                                                <span class="bi bi-pencil-square fa-md videoFormRound"
                                                    id="pencil-iconq" style="color: white;font-size:11px"></span>
                                            </span>
                                            <input type="file" name="editPreorderVideoFile1"
                                                id="editPreorderVideoFile1" style="display: none;"
                                                accept="video/mp4">
                                        </label>
                                    </div>
                                </div>
                                <div class="add-song-span-text mt-1 ">Max. Size 10MB (Only MP4) </div>
                                <div class="invalid-feedback " id="editPreorderVideoFile1Error"></div>
                            </div>
                            <div class="videoForm mb-2 mt-1 form-group input-group-sm input_field ">
                                <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Video File <span
                                            class="add-song-span-text ">( Optional )</span></span></label>
                                <input type="file" class="form-control-file input-sm " value=""
                                    placeholder="Choose file" style="font-size: 12.5px" id="editPreorderVideoFile"
                                    name="editPreorderVideoFile" accept="video/mp4">
                                <div class="add-song-span-text mt-1">Max. Size 10MB (Only MP4) </div>
                                <div class="invalid-feedback " id="editPreorderVideoFileError"></div>
                            </div>
                            <div class="mb-3 mt-2 mb-0 input-group-sm input_field ">
                                <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"><b>Lyrics <span
                                                class="add-song-span-text "> ( Optional )</span></b></span></label>
                                <textarea rows="3" cols="50" name="editPreorderSongLyrics" id="editPreorderSongLyrics" maxlength="3000"
                                    class="form-control add-song-input-text input-xs rounded border-radius8" placeholder="Enter Lyrics.."></textarea>
                                <div class="invalid-feedback" id="editPreorderSongLyricsError"></div>
                            </div>
                            <div class="mb-3 mt-0 mb-0 input-group-sm input_field ">
                                <label class="m-0  mb-1"> <span
                                        class="text-dark add-song-label-text"><b>Date:</b></span></label>
                                <div class="input-group date" id="editReservationdate" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input"
                                        style="font-size: 12.5px;border-right: none;height:31px; "
                                        data-target="#reservationdate" id="editPreorderSongDate"
                                        name="editPreorderSongDate" required>
                                    <div class="input-group-append" data-target="#editReservationdate"
                                        data-toggle="datetimepicker">
                                        <div class="input-group-text red-text"
                                            style="background-color: white;border-left:none;height:31px; "><i
                                                class="bi bi-calendar3"></i></div>
                                    </div>
                                </div>
                                <div class="invalid-feedback" id="editPreorderSongDateError"></div>
                            </div>
                            <div class="mb-3 mt-0 mb-0 input-group-sm input_field ">
                                <label class="m-0  mb-1"> <span
                                        class="text-dark add-song-label-text"><b>Time:</b></span></label>
                                <div class="input-group date" id="editTimepicker" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input"
                                        placeholder="Select publish time"
                                        style="font-size: 12.5px;border-right: none;height:31px; "
                                        data-target="#editTimepicker" id="editPreorderSongTime"
                                        name="editPreorderSongTime" required>
                                    <div class="input-group-append" data-target="#editTimepicker"
                                        data-toggle="datetimepicker">
                                        <div class="input-group-text red-text"
                                            style="background-color: white;border-left:none;height:31px; "><i
                                                class="far fa-clock"></i></div>
                                    </div>
                                </div>
                                <div class="invalid-feedback" id="editPreorderSongTimeError"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                    <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey"
                        data-dismiss="modal">Canel</button>
                    <input type="submit"
                        class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button"
                        value="Update">
                </div>
            </form>
        </div>
    </div>
</div>
