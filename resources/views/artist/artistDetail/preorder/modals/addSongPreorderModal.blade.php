{{-- Add Song pre Order Modal --}}
<div class="modal fade" id="addSongPreorderModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2 p-0 border-0">
                <p class="modal-title add-bank-text"><b class="font-size12">Create Pre Order</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <ul class="nav nav-pills border-bottom" role="tablist">
                <li class="nav-link add-preorder-song-progress-step-1 w-50 active"></li>
                <li class="nav-link add-preorder-song-progress-step-2 w-50"></li>
            </ul>

            <form id="addSongPreorderForm">
                <div class="modal-body">
                    <input type="hidden" id="preorderStepNo" name="stepNo" value="1">
                    <input type="hidden" id="preorderSongPrice" name="preorderSongPrice" value="">
                    <input type="hidden" id="preorderSongId" name="songId" value="">

                    <div class="row">
                        <div class="col-12">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="preorder-pills-home" role="tabpanel"
                                    aria-labelledby="pills-home-tab">
                                    <input name="artistId" value="{{ $artist->id }}" type="hidden">

                                    <div class="mb-2 mt-0 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span
                                                class="preorderType text-dark add-song-label-text">Order Type
                                                ?</span></label>
                                        <select id="preorderTypeSong" id="preorderTypeSong" name="orderTypeSong"
                                            class="form-control border-radius8 add-song-input-text"
                                            onchange="setPreorderSongStatusValue();">
                                            <option class="add-song-input-text" selected>Song</option>
                                            <option class="add-song-input-text">Album</option>
                                        </select>
                                    </div>

                                    <div class="mb-2 mt-0 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span
                                                class="text-dark add-song-label-text">Title</span></label>
                                        <input type="text"
                                            class="form-control input-sm border-radius8 add-song-input-text"
                                            maxlength="30" placeholder="Enter song title" style="font-size: 12.5px"
                                            id="preorderSongName" name="preorderSongName" required>
                                        <div class="invalid-feedback" id="preorderSongNameError"></div>
                                    </div>

                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span
                                                class="text-dark add-song-label-text">Category</span></label>
                                        <select class="form-control font-size12 border-radius8 add-song-input-text"
                                            id="preorderSongCategory" name="preorderSongCategory" required>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback" id="preorderSongCategoryError"></div>
                                    </div>

                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span
                                                class="text-dark add-song-label-text">Language</span></label>
                                        <select class="form-control font-size12 border-radius8 add-song-input-text"
                                            id="preorderSongLanguage" name="preorderSongLanguage" required>
                                            @foreach ($languages as $language)
                                                <option value="{{ $language['id'] }}">{{ $language['name'] }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback" id="preorderSongLanguageError"></div>
                                    </div>

                                    <div class="modal-footer justify-content-between p-0 pt-2" style="margin-left: 4px">
                                        <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                                        <button type="button" class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" id="preorder-song-next-btn">Next</button>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="preorder-pills-profile" role="tabpanel"
                                    aria-labelledby="pills-profile-tab">
                                    <div id="price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                        <label class="m-0 mb-1"><span
                                                class="text-dark add-song-label-text">Price</span></label>
                                        <select name="priceTier" id="preorderPriceTier" class="form-control"></select>
                                        <div class="invalid-feedback" id="preorderSongPriceError"></div>
                                    </div>

                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Thumbnail
                                                <span class="add-song-span-text ">Max. Size 2MB ( Only Jpg, Png, Jpeg | Dimensions 1:1 )</span></span></label>
                                        <input type="file" class="form-control-file input-sm "
                                            placeholder="Choose Thumbnail (Max 1MB)" style="font-size: 12.5px"
                                            id="preorderSongThumbnail" name="preorderSongThumbnail" accept="image/*"
                                            required>
                                        <div class="invalid-feedback" id="preorderSongThumbnailError"></div>
                                    </div>

                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Audio
                                                File <span class="add-song-span-text "> Max. Size 10MB ( Only audio/mpeg, mp3, wav )</span></span></label>
                                        <input type="file" class="form-control-file input-sm "
                                            placeholder="Choose file" style="font-size: 12.5px"
                                            id="preorderAudioFile" name="preorderAudioFile" accept="audio/*"
                                            required>
                                        <div class="invalid-feedback" id="preorderAudioFileError"></div>
                                    </div>

                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Video
                                                File <span class="add-song-span-text "> ( Max 5MB Only Mp4 ) ( Optional
                                                    )</span></span></label>
                                        <input type="file" class="form-control-file input-sm "
                                            placeholder="Choose file" style="font-size: 12.5px"
                                            id="preorderVideoFile" name="preorderVideoFile" accept="video/mp4">
                                        <div class="invalid-feedback" id="preorderVideoFileError"></div>
                                    </div>

                                    <div class="mb-3 mt-2 mb-0 input-group-sm input_field ">
                                        <label class="m-0  mb-1"> <span
                                                class="text-dark add-song-label-text"><b>Lyrics <span
                                                        class="add-song-span-text "> ( Optional
                                                        )</span></b></span></label>
                                        <textarea rows="3" cols="50" name="preorderSongLyrics" id="preorderSongLyrics" maxlength="3000"
                                            class="form-control add-song-input-text input-xs rounded border-radius8" placeholder="Enter Lyrics.."></textarea>
                                        <div class="invalid-feedback" id="preorderSongLyricsError"></div>
                                    </div>

                                    <div class="mb-3 mt-0 mb-0 input-group-sm input_field ">
                                        <label class="m-0  mb-1"> <span
                                                class="text-dark add-song-label-text"><b>Date:</b></span></label>
                                        <div class="input-group date" id="reservationdate"
                                            data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                style="font-size: 12.5px;border-right: none;height:31px; "
                                                data-target="#reservationdate" id="preorderSongDate"
                                                name="preorderSongDate" required>
                                            <div class="input-group-append" data-target="#reservationdate"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text red-text"
                                                    style="background-color: white;border-left:none;height:31px; "><i
                                                        class="bi bi-calendar3"></i></div>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback" id="preorderSongDateError"></div>
                                    </div>

                                    <div class="mb-3 mt-0 mb-0 input-group-sm input_field ">
                                        <label class="m-0  mb-1"> <span
                                                class="text-dark add-song-label-text"><b>Time:</b></span></label>
                                        <div class="input-group date" id="timepicker" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                placeholder="Select publish time"
                                                style="font-size: 12.5px;border-right: none;height:31px; "
                                                data-target="#timepicker" id="preorderSongTime"
                                                name="preorderSongTime" required>
                                            <div class="input-group-append" data-target="#timepicker"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text red-text"
                                                    style="background-color: white;border-left:none;height:31px; "><i
                                                        class="far fa-clock"></i></div>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback" id="preorderSongTimeError"></div>
                                    </div>

                                    <div class="modal-footer justify-content-between p-0 pt-2 "
                                        style="margin-left: 4px">
                                        <button type="button"
                                            class="btn btn-xs py-1 px-3 border-radius8 light-grey preorder-song-previous-btn">Previous</button>
                                        <input type="submit"
                                            class="btn red-button p3 btn-xs mr-2 py-1 px-3 border-radius8 account-Create-button"
                                            value="Create">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
