{{-- Add Song Modal --}}
<div class="modal fade" id="addPreorderSongModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pr-2 p-0 border-0">
                <p class="modal-title add-bank-text"><b class="font-size12">Add Song</b></p>
                <button type="button" class="close preorderAlbumSongCanelbutton">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <ul class="nav nav-pills border-bottom" role="tablist">
                <li class="nav-link add-preorder-album-song-progress-step-1 w-50 active"></li>
                <li class="nav-link add-preorder-album-song-progress-step-2 w-50"></li>
            </ul>

            <form id="addSongToAlbumPreorderForm">
                <div class="modal-body">
                    <div class="row">

                        <div class="col-12">

                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="preorder-album-song-pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <input type="hidden" id="artistId" name="artistId" value="{{ $artist->id }}">
                                    <input type="hidden" id="modalStatus" name="modalStatus">
                                    <input type="hidden" id="preorderAlbumSongId" name="songId">
                                    <input type="hidden" id="preorderAlbumSongPrice" name="songPrice">
                                    <input type="hidden" id="preorderAlbumSongStepNo" name="stepNo" value="1">

                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span
                                                class="text-dark add-song-label-text">Title</span></label>
                                        <input type="text" class="form-control input-sm border-radius8 add-song-input-text"
                                            placeholder="Enter song title" style="font-size: 12.5px" maxlength="30"
                                            id="albumSongPreorderName" name="albumSongPreorderName" required>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                        <div class="invalid-feedback" id="albumSongPreorderNameError"></div>
                                    </div>
                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span
                                                class="text-dark add-song-label-text">Category</span></label>
                                        <select class="form-control font-size12 border-radius8 add-song-input-text"
                                            id="albumSongPreorderCategory" name="albumSongPreorderCategory" required>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                        <div class="invalid-feedback" id="albumSongPreorderCategoryError"></div>
                                    </div>
                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span
                                                class="text-dark add-song-label-text">Language</span></label>
                                        <select class="form-control font-size12 border-radius8 add-song-input-text"
                                            id="albumSongPreorderLanguage" name="albumSongPreorderLanguage" required>
                                            @foreach ($languages as $language)
                                                <option value="{{ $language['id'] }}">{{ $language['name'] }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                        <div class="invalid-feedback" id="albumSongPreorderLanguageError"></div>
                                    </div>

                                    <div class="modal-footer justify-content-between p-0 pt-2 " style="margin-left: 4px">
                                        <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey"  data-dismiss="modal">Canel</button>
                                        <input type="button" class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button preorder-album-song-next-btn" value="Next">
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="preorder-album-song-pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

                                    <div id="price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                        <label class="m-0 mb-1"><span class="text-dark add-song-label-text">Price</span></label>
                                        <select name="priceTier" id="albumSongPreorderPrice" class="form-control"></select>
                                        <div class="invalid-feedback" id="songPriceError"></div>
                                    </div>

                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span
                                                class="text-dark add-song-label-text">Thumbnail</span></label>
                                        <input type="file" class="form-control-file input-sm "
                                            placeholder="Choose Thumbnail (Max 1MB)" style="font-size: 12.5px"
                                            id="albumSongPreorderThumbnail" name="albumSongPreorderThumbnail" accept="image/*"
                                            required>
                                        <div class="add-song-span-text mt-1">Max. Size 2MB ( Only Jpg, Png, Jpeg | Dimensions 1:1 ) </div>
                                        <div class="invalid-feedback" id="albumSongPreorderThumbnailError"></div>
                                    </div>
                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Audio File
                                            </span></label>
                                        <input type="file" class="form-control-file input-sm " placeholder="Choose file"
                                            style="font-size: 12.5px" id="albumSongPreorderAudio" name="albumSongPreorderAudio"
                                            accept="audio/*" required>
                                        <div class="add-song-span-text mt-1">Max. Size 10MB ( Only audio/mpeg, mp3, wav )</div>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                        <div class="invalid-feedback" id="albumSongPreorderAudioError"></div>
                                    </div>
                                    <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                        <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Video File <span
                                                    class="add-song-span-text ">( Optional )</span></span></label>
                                        <input type="file" class="form-control-file input-sm" placeholder="Choose file"
                                            style="font-size: 12.5px" id="albumSongPreorderVideo"
                                            name="albumSongPreorderVideo" accept="video/mp4">
                                        <div class="add-song-span-text mt-1">Max. Size 10MB (Only MP4)</div>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                        <div class="invalid-feedback" id="albumSongPreorderVideoError"></div>
                                    </div>
                                    <div class="mb-3 mt-2 input-group-sm input_field ">
                                        <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"><b>Lyrics <span
                                                        class="add-song-span-text "> ( Optional )</span></b></span></label>
                                        <textarea rows="3" cols="50" name="albumSongPreorderLyrics" id="albumSongPreorderLyrics"
                                            class="form-control add-song-input-text input-xs rounded border-radius8" placeholder="Enter Lyrics.."></textarea>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                        <div class="invalid-feedback" id="albumSongPreorderLyricsError"></div>
                                    </div>


                                    <div class="modal-footer justify-content-between p-0 pt-2 " style="margin-left: 4px">
                                        <button type="button" class="btn btn-xs py-1 px-3 border-radius8 light-grey preorder-album-song-previous-btn" >Previous</button>
                                        <input type="submit" class="btn red-button p3 btn-xs mr-2 py-1 px-3 border-radius8 account-Create-button" value="Create">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
