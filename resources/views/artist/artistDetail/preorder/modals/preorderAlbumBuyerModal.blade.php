{{-- Pre order Album detail Modal --}}
<div class="modal fade" id="preorderAlbumDetailModal">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" id="preorderAlbumInDetail" style="background-color: #242424;color:white">
                
            </div>
            <div class="modal-body p-0 my-custom-scrollbar">
                <div class="row m-0" style="float: left">
                    <ul class="nav nav-pills ml-auto " style="margin-bottom: 0px !important;font-size: 11px;">
                        <li class="nav-item"><span class="nav-link active" href="#tab_21" data-toggle="tab" style="">All Songs</span></li>
                        <li class="nav-item"><span class="nav-link " href="#tab_24" data-toggle="tab" style="">Orders</span></li>
                    </ul>
                </div>
                <div class="card-body pt-2 pl-0 pr-0">
                    <div class="tab-content">
                        <div class="tab-pane  active" id="tab_21">
                            <table class="table responsive ">
                                <tbody id="preorderAlbumListOfSong">
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane " id="tab_24">
                            <div class="col-12 table-wrapper-scroll-y my-custom-scrollbar p-0" >
                                <table class="table responsive table-striped ">
                                    <tbody id="preorderAlbumBuyerList">
                                    
                                    </tbody>
                                </table>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
