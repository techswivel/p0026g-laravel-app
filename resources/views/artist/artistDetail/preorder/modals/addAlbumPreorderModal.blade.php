{{-- Add Album pre Order Modal --}}
<div class="modal fade" id="addAlbumPreorderModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pr-2 p-0 border-0">
                <p class="modal-title add-bank-text" ><b class="font-size12">Create Pre Order</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <ul class="nav nav-pills border-bottom" role="tablist">
                <li class="nav-link add-preorder-album-progress-step-1 w-50 active"></li>
                <li class="nav-link add-preorder-album-progress-step-2 w-50"></li>
            </ul>
            <form id="addAlbumPreorderForm">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" id="addPreorderAlbumSongStepNo" name="stepNo" value="1">
                        <input type="hidden" id="addPreorderAlbumId" name="albumId">
                        <input type="hidden" id="albumIAppPurchasePrice" name="iAppPurchasePrice">
                        <input type="hidden" name="artistId" value="{{ $artist->id }}">

                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="add-album-pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                                <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                                    <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Order Type ?</span></label>
                                    <select id="preorderTypeAlbum" name="preorderTypeAlbum" class="form-control border-radius8 add-song-input-text preorderType"  onchange="setPreorderAlbumStatusValue();">
                                        <option class="add-song-input-text" selected>Album</option>
                                        <option class="add-song-input-text">Song</option>
                                    </select>
                                </div>

                                <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                                    <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                                    <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Album title" style="font-size: 12.5px" maxlength="30" id="preorderAlbumName" name="preorderAlbumName">
                                    <div class="invalid-feedback" id="preorderAlbumNameError"></div>
                                </div>

                                <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                                    <label class="m-0 mb-0"> <span class="text-dark add-song-label-text" >Thumbnail </span></label><br>
                                    <input type="file" style="font-size: 12.5px;margin: .4rem 0;color:gray" id="preorderAlbumThumbnail" name="preorderAlbumThumbnail" accept="image/*"><br>
                                    <span class="add-song-span-text "> Max. Size 2MB ( Only Jpg, Png, Jpeg | Dimensions 1:1 ) </span>
                                    <div class="invalid-feedback" id="preorderAlbumThumbnailError"></div>
                                </div>

                                <div class="mb-3 mt-0 mb-0 input-group-sm input_field " >
                                    <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Date:</b></span></label>
                                    <div class="input-group date" id="albumReservationdate" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input"   style="font-size: 12.5px;border-right: none;height:31px; " data-target="#albumReservationdate" id="preorderAlbumDate" name="preorderAlbumDate">
                                        <div class="input-group-append" data-target="#albumReservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text red-text" style="background-color: white;border-left:none;height:31px; "><i class="bi bi-calendar3"></i></div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback" id="preorderAlbumDateError"></div>
                                </div>

                                <div class="mb-3 mt-0 mb-0 input-group-sm input_field " >
                                    <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Time:</b></span></label>
                                    <div class="input-group date" id="albumTimepicker" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" placeholder="Select publish time" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#albumTimepicker" id="preorderAlbumTime" name="preorderAlbumTime">
                                        <div class="input-group-append" data-target="#albumTimepicker" data-toggle="datetimepicker">
                                            <div class="input-group-text red-text" style="background-color: white;border-left:none;height:31px; "><i class="far fa-clock"></i></div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback" id="preorderAlbumTimeError"></div>
                                </div>

                                <div class="mb-3 mt-3 mb-0 input-group-sm input_field " >
                                    <button type="button" onclick="storeAlbumSong()"  class=" btn ml-0 pink-button btn-xs border-radius8 pt-1 pb-1 pl-2 pr-2"><i class="fas fa-plus"></i> Add Songs Detail</button>
                                </div>
                                <div class="invalid-feedback" id="preorderSelectedSongsError"></div>

                                <select style="display: none" class="select form-control input-sm border-radius8 add-song-input-text" multiple data-mdb-placeholder="Example placeholder" id="preorderSelectedSongs" name="preorderSelectedSongs[]" multiple>
                                    @foreach ($preorderAlbumSongs as $preorderAlbumSong)
                                        <option value="{{$preorderAlbumSong['id']}}" id="preorderAlbumSongReomve{{$preorderAlbumSong['id']}}" selected>{{$preorderAlbumSong['id']}}</option>
                                    @endforeach
                                </select>

                                <div class="row" id="preorderalbumSongList">
                                    @foreach ($preorderAlbumSongs as $preorderAlbumSong)
                                        <div id="albumSongImageAddModal{{$preorderAlbumSong->id}}" class="col-2 pb-1 albumSongRemove{{$preorderAlbumSong->id}}" >
                                            <img src="{!! Helper::urlPath($preorderAlbumSong->thumbnail) !!}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                        </div>
                                        <div id="albumSongNameAddModal{{$preorderAlbumSong->id}}" class="col-7 p-2 albumSongRemove{{$preorderAlbumSong->id}}" >
                                            <small>{{$preorderAlbumSong->name}}</small>
                                        </div>
                                        <div class="col 2 p-2 pr-3 albumSongRemove{{$preorderAlbumSong->id}}">
                                            <i class="float-right red-text bi bi-pencil-fill"></i>
                                        </div>
                                        <div class="col-1 p-2 albumSongRemove{{$preorderAlbumSong->id}}">
                                            <i class="float-right bi bi-x-lg" onclick="removePreorderAlbumSong({{$preorderAlbumSong->id}})"></i>
                                        </div>
                                    @endforeach
                                </div>

                                <div class="modal-footer justify-content-between p-0 pt-2 " style="margin-left: 4px">
                                    <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey"  data-dismiss="modal">Canel</button>
                                    <input type="button" class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button add-preorder-album-next-btn" value="Next">
                                </div>

                            </div>
                            <div class="tab-pane fade" id="add-album-pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="mb-2 mt-0 form-group input-group-sm input_field ">
                                    <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Album Price:</span></label>
                                    <span class="text-dark add-song-label-text" id="albumPreorderSongsCost"></span>
                                </div>

                                <div id="price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                    <label class="m-0 mb-1"><span class="text-dark add-song-label-text">Price</span></label>
                                    <select name="priceTier" id="addAlbumPreorderPrice" class="form-control"></select>
                                    <div class="invalid-feedback" id="albumPreorderSongPriceError"></div>
                                </div>

                                <div class="modal-footer justify-content-between p-0 pt-2 " style="margin-left: 4px">
                                    <button type="button" class="btn btn-xs py-1 px-3 border-radius8 light-grey add-preorder-album-previous-btn" >Previous</button>
                                    <input type="submit" class="btn red-button p3 btn-xs mr-2 py-1 px-3 border-radius8 account-Create-button" value="Create">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            </form>
        </div>
    </div>
</div>
