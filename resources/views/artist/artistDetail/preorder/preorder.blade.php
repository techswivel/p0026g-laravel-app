<div class="ml-3 mr-3">
    <ul class="nav nav-pills-preorder ml-auto ">
        <li class="nav-item"><button type="button"  href="#tab_11" data-toggle="tab"  class="nav-link-preorder active btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8" >Songs</button></li>
        <li class="nav-item"><button type="button"  href="#tab_22"  data-toggle="tab"  class="nav-link-preorder btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8"> Albums</button></li>
    </ul>

    <div class="tab-content mt-4 ml-2 mr-3">
        <div class="tab-pane active" id="tab_11">
            <div class="row">
                @if (count($preorderSongs) != 0)
                @foreach ($preorderSongs as $preorderSong)
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="card border-radius8" style="background-color: #F9F9F9" >
                            <div class="card-header border-radius-top p-2 pl-2 " >
                                <div class="row pb-1 m-0">
                                    <div class="col-3 m-0 p-0">
                                    <div>
                                        <img src="{!! Helper::urlPath($preorderSong->thumbnail) !!}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                    </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="p-1 pl-0 " >
                                            <small class="d-flex align-items-start" style="font-weight: 500">{{Str::limit($preorderSong->name, 18)}}</small>
                                            <small class="d-flex align-items-end " style="font-weight: 500">{{Carbon\Carbon::parse($preorderSong->preOrderReleaseDate)->format('d M Y')}}</small>
                                        </div>
                                        @if ($preorderSong->status == 'PENDING')
                                            <div class="light-grey-text font-weight-lighter" style="margin-left: -5px !important"> <i class="fa fa-clock text-orange"></i> Pending</div>
                                        @endif
                                    </div>
                                    <div class="col-3 p-0">
                                        <div class="dropdown dropdown-block-aside" id="checkplaylis" style="float: right !important">
                                            <i class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('/svg/doticon.png')}}"></i>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item btn-block-aside"  href="javascript:void(0)"  onclick="editPreorderSong({{$preorderSong->id}})">Edit Pre Order</a>
                                                <a class="dropdown-item btn-block-aside"  href="javascript:void(0)"  onclick="deletePreorderSong({{$preorderSong->id}})">Delete Pre Order</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-6">
                                        <small style="font-weight: 500">Preorders: {{$preorderSong->preOrderCount}}/{{$preorderSong->totalPreOrderCount}}</small>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" onclick="preorderSongPurchaseList({{$preorderSong->id}})"  class="float-right btn ml-2 p-1 pl-3 pr-3 red-button btn-xs border-radius8" ><small>View Detail</small></button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-2 border-radius-bootom" style="text-align:center;background-color:#212529">
                                <small style="font-size:14px;font-weight: 500;color:white" id="preorderSongtimeCounter{{$preorderSong->id}}"></small>
                            </div>
                        </div>
                    </div>
                @endforeach
                @else
                    <div class="row" style="margin:0px; width:100%" >
                        <div class="col-lg-12" style="text-align: center">
                            <p >No data available in table</p>
                        </div>
                    </div>
                @endif
            </div>
            <div class="container">
                <div class="row m-0 mt-5">
                  <div class="col text-center">
                    <button type="button" data-toggle="modal" data-target="#addSongPreorderModal" class="shadow btn ml-2 red-button btn-xs border-radius8 pt-2 pb-2 pl-4 pr-4"><i class="fas fa-plus"></i> Pre Order</button>
                  </div>
                </div>
              </div>
        </div>
        <div class="tab-pane " id="tab_22">
            <div class="row">
                @if (count($preorderAlbums) != 0)
                @foreach ($preorderAlbums as $preorderAlbum)
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card border-radius8" style="background-color: #F9F9F9" >
                        <div class="card-header border-radius-top p-2 pl-2 " >
                            <div class="row pb-1 m-0">
                                <div class="col-3 m-0 p-0">
                                <div>
                                    <img src="{!! Helper::urlPath($preorderAlbum->thumbnail) !!}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                </div>
                                </div>
                                <div class="col-6 p-0">
                                    <div class="p-1 pl-0 " >
                                        <small class="d-flex align-items-start" style="font-weight: 500">{{Str::limit($preorderAlbum->name, 9)}} ({!! Helper::totalSong($preorderAlbum->id) !!} Songs)</small>
                                        <small class="d-flex align-items-end " style="font-weight: 500">{{Carbon\Carbon::parse($preorderAlbum->preOrderReleaseDate)->format('d M Y')}}</small>
                                    </div>
                                </div>
                                <div class="col-3 p-0">
                                    <div class="dropdown dropdown-block-aside" id="checkplaylis" style="float: right !important">
                                        <i class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('/svg/doticon.png')}}"></i>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item btn-block-aside"  href="javascript:void(0)"  onclick="editPreorderAlbum({{$preorderAlbum->id}})">Edit Pre Order</a>
                                            <a class="dropdown-item btn-block-aside"  href="javascript:void(0)"  onclick="deletePreorderAlbum({{$preorderAlbum->id}})">Delete Pre Order</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <small style="font-weight: 500">Preorders: {{$preorderAlbum->preOrderCount}}/{{$preorderAlbum->totalPreOrderCount}}</small>
                                </div>
                                <div class="col-6">
                                    <button type="button" onclick="preorderAlbumPurchaseList({{$preorderAlbum->id}})"  class="float-right btn ml-2 p-1 pl-3 pr-3 red-button btn-xs border-radius8" ><small>View Detail</small></button>
                                </div>
                            </div>

                        </div>
                        <div class="card-body p-2 border-radius-bootom" style="text-align:center;background-color:#212529">
                            <small style="font-size:14px;font-weight: 500;color:white" id="preorderAlbumtimeCounter{{$preorderAlbum->id}}"></small>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                    <div class="row" style="margin:0px; width:100%" >
                        <div class="col-lg-12" style="text-align: center">
                            <p >No data available in table</p>
                        </div>
                    </div>
                @endif
            </div>
            <div class="container">
                <div class="row m-0 mt-5">
                  <div class="col text-center">
                    <button type="button" onclick="addPreorderAlbum({{$artist->id}})" class="shadow btn ml-2 red-button btn-xs border-radius8 pt-2 pb-2 pl-4 pr-4"><i class="fa fa-plus"></i> Pre Order</button>
                  </div>
                </div>
              </div>
        </div>
    </div>
</div>

{{-- Add Song pre Order Modal --}}
@include('artist.artistDetail.preorder.modals.addSongPreorderModal')
{{-- Delete Pre order Song/Album Modal --}}
@include('artist.artistDetail.preorder.modals.deleteSongPreorderModal')
{{-- Edit Song pre Order Modal --}}
@include('artist.artistDetail.preorder.modals.editSongPreorderModal')
{{-- Pre order song detail Modal --}}
@include('artist.artistDetail.preorder.modals.preorderSongBuyerModal')
{{-- Add Album pre Order Modal --}}
@include('artist.artistDetail.preorder.modals.addAlbumPreorderModal')
{{-- Add Song To Album pre Order Modal --}}
@include('artist.artistDetail.preorder.modals.addSongToAlbumPreorderModal')
{{-- Delete Album pre Order Modal --}}
@include('artist.artistDetail.preorder.modals.deleteAlbumPreorderModal')
{{-- Edit Album pre Order Modal --}}
@include('artist.artistDetail.preorder.modals.editAlbumPreorderModal')
{{-- Edit Song To Album pre Order Modal --}}
@include('artist.artistDetail.preorder.modals.editSongToAlbumPreorderModal')
{{-- Pre order Album detail Modal --}}
@include('artist.artistDetail.preorder.modals.preorderAlbumBuyerModal')


