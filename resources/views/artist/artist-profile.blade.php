@extends('artist.layouts.master')

@section('title')
    Q Music
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/customnotification.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('artist/css/artistProfile.css') }}">

@endsection

@section('content')
    <div class="row  pt-1" style="padding-bottom: 13px !important">
        <div class="col">
            <span class="withdrawal-text pl-0">Artist Profile</span>
        </div>
    </div>
    <div class="row pt-1 pb-3 modal-position-relative">
        @role('artist')
            <div class="col-lg-3 col-md-12">
                <div class="card border-radius8">
                    <div class="card-body border-radius8 pt-3 pl-4 pr-4 pb-3">
                        <div class="row d-flex justify-content-center mb-2">
                            @if (auth()->user()->avatar)
                                <div id="image-show" class="roundedImage rounded-circle mt-2"
                                    style="background-image: url({!! Helper::urlPath(auth()->user()->avatar) !!});"></div>
                            @else
                                <div id="image-show" class="roundedImage rounded-circle mt-2"
                                    style="background-image: url({{ url('artist/image/avatar/default-avatar.png') }});"></div>
                            @endif
                        </div>
                        <div class="row d-flex justify-content-center">
                            <small class="text-center"
                                style="font-weight:400;color:#212529">{{ auth()->user()->firstName . ' ' . auth()->user()->lastName }}</small>
                        </div>
                        <div class="text-center mb-2">
                            <small class="text-center light-grey-text ">{{ auth()->user()->email }}</small>
                        </div>

                        <div class="dropdown-divider"></div>
                        <div class="pl-0 pr-0">
                            <div class="row no-gutters">
                                <div class="col-md-1">
                                    <i class="bi bi-calendar4-event icon-blue"></i>
                                </div>
                                <div class="col-md-5 md-pl-2">
                                    <small class="light-grey-text">Join Date</small>
                                </div>
                                <div class="col-md-6 text-center">
                                    <small class="float-right font-weight500" style="font-size:10px;padding-top:8px">{{Carbon\Carbon::parse($artist->createdAt)->format('d-m-Y')}}</small>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown-divider"></div>
                        <div class="pl-0 pr-0">
                            <div class="row no-gutters">
                                <div class="col-md-1">
                                    <i class="bi bi-tag icon-blue"></i>
                                </div>
                                <div class="col-md-5 md-pl-2">
                                    <small class="light-grey-text">User ID</small>
                                </div>
                                <div class="col-md-6 text-center">
                                    <small class="float-right font-weight500" style="font-weight:bold;font-size:10px;padding-top:6px">{{ $artist->id  }}</small>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown-divider"></div>
                        <div class="small-space col-12 pl-0 pr-0">
                            <small class=" light-grey-text">
                                {{ auth()->user()->aboutArtist }}
                            </small>
                            <div class="mt-3">
                                <a href="#" id="editProfile"
                                    class="btn btn-block rounded-lg red-button font-weight-lighter text-white border-radius8 edit-profile"
                                    onclick="editArtistProfile({{ auth()->user() }})">Edit Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endrole
        @role('admin')
            <div class="col-lg-3 col-md-12">
                <div class="card border-radius8">
                    <div class="card-body border-radius8 pt-3 pl-4 pr-4 pb-3">
                        <div class="row">
                            @if ($artist->status == 'Active')
                                <small class="p-1 pl-2 pr-2 float:end"
                                    style="background-color: #01ba70;color:white;border-radius:8px">Active</small>
                            @else
                                <small class="p-1 pl-2 pr-2 float:end"
                                    style="background-color: #ba011a;color:white;border-radius:8px">Block</small>
                            @endif
                        </div>
                        <div class="row d-flex justify-content-center mt-2 mb-2">
                            @if ($artist->avatar)
                                <div id="image-show" class="roundedImage rounded-circle mt-2"
                                    style="background-image: url({!! Helper::urlPath($artist->avatar) !!});"></div>
                            @else
                                <div id="image-show" class="roundedImage rounded-circle mt-2"
                                    style="background-image: url({{ url('artist/image/avatar/default-avatar.png') }});"></div>
                            @endif
                        </div>
                        <div class="row d-flex justify-content-center">
                            <small class="text-center"
                                style="font-weight:400;color:#212529">{{ $artist->firstName . ' ' . $artist->lastName }}</small>
                        </div>
                        <div class="text-center mb-2">
                            <small class="text-center light-grey-text ">{{ $artist->email }}</small>
                        </div>

                        <div class="dropdown-divider"></div>
                        <div class="pl-0 pr-0">
                            <div class="row no-gutters">
                                <div class="col-md-1">
                                    <i class="bi bi-calendar4-event icon-blue"></i>
                                </div>
                                <div class="col-md-5 md-pl-2">
                                    <small class="light-grey-text">Join Date</small>
                                </div>
                                <div class="col-md-6 text-center">
                                    <small class="float-right font-weight500" style="font-size:10px;padding-top:8px">{{Carbon\Carbon::parse($artist->createdAt)->format('d-m-Y')}}</small>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown-divider"></div>
                        <div class="pl-0 pr-0">
                            <div class="row no-gutters">
                                <div class="col-md-1">
                                    <i class="bi bi-tag icon-blue"></i>
                                </div>
                                <div class="col-md-5 md-pl-2">
                                    <small class="light-grey-text">User ID</small>
                                </div>
                                <div class="col-md-6 text-center">
                                    <small class="float-right font-weight500" style="font-weight:bold;font-size:10px;padding-top:6px">{{ $artist->id  }}</small>
                                </div>
                            </div>
                        </div>


                        <div class="dropdown-divider"></div>
                        <div class="small-space col-12 pl-0 pr-0">
                            <small class=" light-grey-text">
                                {{ $artist->aboutArtist }}
                            </small>
                            <div class="mt-2">
                                <div class="row" style="margin: 0px">
                                    <div style="width:100%;">
                                        <a style="font-size: 12px"
                                            class="btn btn-danger mt-2 red-button font-weight-lighter text-white border-radius8"
                                            href="javascript:void(0)" onclick="artistDetail({{ $artist->id }})">Edit Artist
                                            Detail</a>
                                        @if ($artist->status == 'Active')
                                            <a style="font-size: 12px"
                                                class="btn  orange-button mt-2 font-weight-lighter text-white border-radius8"
                                                href="javascript:void(0)" onclick="artistBlock({{ $artist->id }})">Block</a>
                                        @else
                                            <a style="font-size: 12px"
                                                class="btn  orange-button mt-2 font-weight-lighter text-white border-radius8"
                                                href="javascript:void(0)"
                                                onclick="artistUnblock({{ $artist->id }})">Unblock</a>
                                        @endif
                                        <a style="font-size: 12px"
                                            class="btn orange-button mt-2 font-weight-lighter text-white border-radius8"
                                            href="javascript:void(0)" onclick="artistDelete({{ $artist->id }})">
                                            <span class="bi bi-trash3" id="trash-iconq"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endrole
        <div class="col-lg-9 col-md-12 pl-lg-0 pr-lg-4">
            <div class="card border-radius8">
                <div class="card-header pb-0"
                    style="background-color: white; border-top-left-radius:8px !important;border-top-right-radius:8px !important;">
                    <div class="row pb-1">
                        <div class="col-lg-2  pt-2">
                            <div class="card earn-box text-center pb-3">
                                <span class="info-box-text font-weight-lighter earn-box-title">No. Of Songs</span>
                                <span
                                    class="info-box-number font-weight-lighter earn-box-info">{{ Helper::artistTotalSong($artist->id) }}</span>
                            </div>
                        </div>
                        <div class="col-lg-2 pt-2">
                            <div class="card earn-box text-center pb-3">
                                <span class="info-box-text font-weight-lighter earn-box-title">Album</span>
                                <span
                                    class="info-box-number font-weight-lighter earn-box-info">{{ Helper::artistTotalAlbum($artist->id) }}</span>
                            </div>
                        </div>
                        <div class="col-lg-2 pt-2">
                            <div class="card earn-box text-center pb-3">
                                <span class="info-box-text font-weight-lighter earn-box-title">Paid Songs</span>
                                <span
                                    class="info-box-number font-weight-lighter earn-box-info">{{ Helper::artistPaidSong($artist->id) }}</span>
                            </div>
                        </div>
                        <div class="col-lg-2 pt-2">
                            <div class="card earn-box text-center pb-3">
                                <span class="info-box-text font-weight-lighter earn-box-title">Paid Album</span>
                                <span
                                    class="info-box-number font-weight-lighter earn-box-info">{{ Helper::artistPaidAlbum($artist->id) }}</span>
                            </div>
                        </div>
                        <div class="col-lg-2 pt-2">
                            <div class="card earn-box text-center pb-3">
                                <span class="info-box-text font-weight-lighter earn-box-title">Earnings</span>
                                <span
                                    class="info-box-number font-weight-lighter earn-box-info">${{ Helper::totalEarning($artist->id) }}</span>
                            </div>
                        </div>
                        <div class="col-lg-2 pt-2">
                            <div class="card earn-box text-center pb-3">
                                <span class="info-box-text font-weight-lighter earn-box-total-title">Pre Booking
                                    Earnings</span>
                                <span
                                    class="info-box-number font-weight-lighter earn-box-info">${{ Helper::preorderBookEarning($artist->id) }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row ml-0">
                        <ul class="nav nav-pills ml-auto "
                            style="margin-left: -12px !important;margin-bottom: -1px !important;font-size: 11px;">
                            <li class="nav-item"><span class="nav-link active" href="#tab_1" data-toggle="tab"
                                    style="">All Songs</span></li>
                            <li class="nav-item"><span class="nav-link" href="#tab_2" data-toggle="tab">All
                                    Albums</span></li>
                            <li class="nav-item"><span class="nav-link" href="#tab_3" data-toggle="tab">Earning
                                    Stats</span></li>
                            <li class="nav-item"><span class="nav-link" href="#tab_4" data-toggle="tab">Pre
                                    Orders</span></li>
                            @role('artist')
                                <li class="nav-item"><span class="nav-link" href="#tab_5"
                                        data-toggle="tab">Followers</span></li>
                            @endrole
                            @role('admin')
                                <li class="nav-item"><span class="nav-link" href="#tab_6"
                                        data-toggle="tab">Notifications</span></li>
                            @endrole
                        </ul>
                    </div>
                </div>
                <div class="card-body pt-2 pl-0 pr-0">
                    <div class="tab-content">
                        <div class="tab-pane active table-responsive" id="tab_1">
                            @include('artist.artistDetail.song.song')
                        </div>
                        <div class="tab-pane " id="tab_2">
                            @include('artist.artistDetail.album.album')
                        </div>
                        <div class="tab-pane table-responsive" id="tab_3">
                            @include('artist.artistDetail.earningStat')
                        </div>
                        <div class="tab-pane mt-1 ml-1" id="tab_4">
                            @include('artist.artistDetail.preorder.preorder')
                        </div>
                        <div class="tab-pane table-responsive" id="tab_5">
                            @include('artist.artistDetail.followers')
                        </div>
                        <div class="tab-pane table-responsive" id="tab_6">
                            @include('artist.artistDetail.notification')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('artist.modals.player')

        @include('artist.modals.playerPlaylist')

    </div>
    </div>

    @include('artist.modals.editArtistProfile')
    @include('admin.artists.modals.editArtistDetail')
    @include('admin.artists.modals.deleteArtist')
    <div id="loader" class="lds-dual-ring hidden overlay"></div>
@endsection
@section('script')
    @include('scripts.artist.artistProfileJs')
@endsection
