<div class="modal fade" id="songListModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 396px">
            <div class="modal-header p-0 py-2 px-3 d-block">
                <div class="row">
                    <div class="col-md-10 col-10">
                        <h6 class="modal-title font-weight-bold">Albums & Songs</h6>
                    </div>
                    <div class="col-md-2 col-2 text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <small class="font-weight-bold font-szie10">Albums List</small>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="swiper mySwiper">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">

                            </div>

                            <!-- If we need navigation buttons -->
                        </div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-3">
                        <small class="font-weight-bold font-szie10">Songs List</small>
                    </div>
                </div>

                <div class="songListsModal">

                </div>
            </div>

        </div>
    </div>
</div>
