{{-- Edit Profile Modal --}}
<div class="modal fade" id="edit-Profile">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b>Edit Artist Profile</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editProfileForm" >
            @csrf
                <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <div class=" mb-4">
                    <div class=" mb-1  mt-3 input-group-sm input_field" >
                        <div class="d-flex flex-row">
                            @if (auth()->user()->avatar)
                                <div id="image-show-detail" class ="roundedImage rounded-circle mt-2" style="background-image: url({!! Helper::urlPath(auth()->user()->avatar) !!});"></div>
                            @else
                                <div id="image-show-detail" class ="roundedImage rounded-circle mt-2" style="background-image: url({{url('artist/image/avatar/default-avatar.png')}});">    </div>
                            @endif
                            <div class="d-flex align-items-end image-div">
                                <label>
                                    <span class="dot d-flex justify-content-center">
                                        <b class="plus">+</b>
                                    </span>
                                    <input type="file" id="avatar" name="avatar" style="display: none;">
                                </label>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="invalid-feedback" id="avatarError"></div>
                    <div class="col-12">
                        <div class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500;color:black">Name</span></label>
                            <input type="text" class="form-control input-sm border-radius8" value=""  placeholder="Enter name" style="font-size: 12.5px" id="name" name="name" >
                            <div class="invalid-feedback" id="nameError"></div>
                        </div>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500">Email</span></label>
                            <input type="email" class="form-control input-sm border-radius8 add-bank-feild" value="{{ old('email')  ?? auth()->user()->email}}" placeholder="Enter email" style="font-size: 12.5px" id="email" name="email" disabled>
                            <div class="invalid-feedback" id="emailError"></div>
                        </div>
                        <div class="mb-3 mt-2 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark "  style="font-size: 13.5px;font-weight:500"><b>About Artist</b></span></label>
                            <textarea rows="3" cols="50" name="aboutArtist"  id="aboutArtist"   class="form-control input-xs rounded border-radius8"  placeholder="Enter About Artist" form="editProfileForm" >{{ old('editAboutArtist')  ?? auth()->user()->aboutArtist}}</textarea>
                            <div class="invalid-feedback" id="aboutArtistError"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>
