<!-- Note: Do Not Change Class Or Any Class Sequence Otherwise Music Player Will Start Making Errors-->
<div class="modal fade" id="musicBar" tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="musicBar"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content border-0">
            <div class="modal-body p-0 px-3 py-2">
                <div class="row no-gutters">
                    <div class="col-md-1 col-3 playBtnDiv">
                        <i class="bi bi-skip-start-fill mediaPlayer prevPlay"></i>
                        <i class="bi bi-play-circle-fill mediaPlayer playPause" id="playPause"></i>
                        <i class="bi bi-skip-end-fill mediaPlayer nextPlay"></i>
                    </div>
                    <div class="col-md-6 col-3 px-3 border-bar-left">
                        <div class="row no-gutters">
                            <div class="col-md-1 mt-1 currentTimeDiv">
                                <span class="time"></span>
                            </div>
                            <div class="col-md-10 hideInMobile">
                                <div id="waveform"></div>
                            </div>
                            <div class="col-md-1 mt-1 hideInMobile">
                                <span class="totalTime ml-1"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-4">
                        <div class="row no-gutters">
                            <div class="col-md-2 col-3 text-center volumeBtnDiv border-bar-left border-bar-right">
                                <i class="bi bi-volume-up-fill mediaPlayer volume" id="unmute"></i>
                            </div>
                            <div class="col-md-10 col-9">
                                <div class="row no-gutters border-bar-right">
                                    <div class="col-md-3 col-3 text-center">
                                        <img src="" class="border-radius8 songImageModal" alt="Product Image"
                                            style="width: 35.5px; height:35.5px">
                                    </div>
                                    <div class="col-md-9 col-9 h-0">
                                        <small class="font-szie10 position-absolute songArtistName"></small>
                                        <p class="font-szie14 font-weight-bold m-0 position-absolute top-18px songName"
                                            data-toggle="tooltip" title=""></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-2">
                        <div class="row no-gutters pr-1">
                            <div class="col-md-10 col-6 songListDiv">
                                <a type="button" class="songList" data-toggle="modal" data-backdrop="false" data-target="#songListModal">
                                    <div class="row no-gutters ml-4">
                                        <div class="col-2">
                                            <img src="{{ asset('artist/image/songlist.svg') }}"
                                                class="mt-2 songListIcon" width="18px" height="14px">
                                        </div>
                                        <div class="col-10 hideInMobile">
                                            <p class="font-weight-bold m-0 my-1 ml-2">Songs List</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-2 col-6 mt-1">
                                <button type="button" class="close closeMusicBar" data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
