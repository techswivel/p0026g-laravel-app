@extends('artist.layouts.master')

@section('title')
Terms And Conditions
@endsection


@section('style')

@endsection
@section('content')

<div class="row">
    <div class="col-lg-6 offset-lg-3" style="margin-top: 35px">
        @if ($term)
            <h5 class="mb-3"><b>{!! $term->title !!}</b></h5>
            <p class=" grey-text">{!! $term->description !!}</p>
        @else
        @endif
    </div>
</div>
@endsection


@section('script')

@endsection