@extends('artist.layouts.authRegisterMaster')

@section('title')
    Sign Up
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('artist/css/register.css')}}">
@endsection

@section('content')
<div class="text-center">
    <div class="padding-top110">
        <div class="data-content mt-5">
            <div>
                <span class="font-weight-bold h6 signup-text font-weight-bold" style="font-size: 19px;">Sign Up</span>
            </div>
            <p class="m-0 text-secondary small pl-1 pb-1" style="margin-top: -1.5px !important;font-size: 13.5px;">Q the Music Artist Dashboard.</p>
            <div class="mt-4">
                <div>
                    <h4 class="mb-0">A link has been sent to your given email <b>{{auth()->user()->email}}.</b></h4>
                    <h4>Kindly confirm your email.</h4>
                </div>
            <div id="email-success-message" class="alert alert-success text-center" hidden>A new email verification link has been emailed to you!</div>
            <div class="mt-4">
                <h4 class="pt-3">Didn't receive link?</h4>   
            </div>   
            <div class="row mt-1">
                <div class="col-md-6 offset-md-3">
                    <form id="artistLogout" method="POST" action="{{ route('verification.send') }}" class="text-center">
                        @csrf
                        <span class="invalid-feedback" id="email-message" style="display: block;" role="alert" hidden>
                            <strong>Wait verification link is sending to your email</strong>
                        </span>
                        <button type="submit" id="theBtn" class="btn light-red-button" >Resend</button>
                    </form> 
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$("#theBtn").click(function () {
    $("#artistLogout").submit(function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $("#theBtn").attr("disabled", true);
        $("#email-message").attr("hidden", false);
        $.ajax({
            url: "{{ route('verification.send') }}",
            type: "POST",
            contentType: false,
            processData: false,
            success: function (response) {
                $("#theBtn").attr("disabled", false);
                $("#email-message").attr("hidden", true);
                $("#email-success-message").attr("hidden", false);
            },
        });
    });
});
</script>
@endsection

