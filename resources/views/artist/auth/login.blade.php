@extends('artist.layouts.authLoginMaster')

@section('title')
    Sign In
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('artist/css/login.css')}}">
{{--        <link rel="stylesheet" href="{{ asset('admin/css/login.css')}}">--}}

@endsection

@section('content')
<div class="card p-2 mt-4 login_card" id="signin-card">
  <div class="card-body login-card-body " id="login_card_body">
    <div class="data-content mt-1">
      <div>
        <h6 class="font-weight-bold font-szie1 pl-1" id="signin-text">Sign In</h6>
      </div>
      <p class="m-0 text-secondary small pl-1" style="margin-top: -9px !important;font-size:13.5px ">Welcome to Q the Music Artist Dashboard.</p>
    </div>
    <div class="container p-1 pt-3">
      @if(session()->has('message'))
      <div class="alert alert-danger">
       {{ session()->get('message') }}
    </div>
     @endif
      <form method="POST" action="{{ route('login')}}" class="needs-validation" novalidate>
        @csrf
        <label class="m-0 pt-1"><small class="text-dark mb-0 size10">Email address</small></label>
        <input type="hidden" name="role" value="artist">
        <div class="mb-1 mt-1 input-group-sm input_field " >
          <input type="email" class="form-control input-sm border-radius8 " style="font-size: 12px"  placeholder="Email" id="email" name="email" required>
          @if ($errors->has('email'))
            <span class="invalid-feedback" style="display: block;" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
        </div>
        <label class="m-0 mt-1"><small class="text-dark m-0 size10">Password</small></label>
        <div class="input-group mt-1 mb-3 input-group-sm round-7" id="show_hide_password">
          <input type="password" class="form-control input-sm round-7 password-radius" style="font-size: 12px" placeholder="Password" id="password" name="password" required>
            <div class="input-group-append rounded" style="height: 28px">
              <div class="input-group-text eye-icon pt-2" id="password-eye" style=""><a href="#">
                <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
              </div>
            </div>
            @if ($errors->has('password'))
              <span class="invalid-feedback" style="display: block;" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
        </div>
        <input type="submit" class="btn btn-block btn-danger btn-sm rounded-lg small font-weight-lighter red-button" value="LOGIN">
      </form>
      <div class="pb-3">
        <a  href="{{ route('password.request') }}" ><small class="float-right text-xs pt-2 red-text font-szie1">Forget Password?</small></a>
      </div>
      <div class="mt-5 mb-1 rounded-lg">
        <a  href="{{ route('register')}}"  class="btn btn-block text-center btn-sm  rounded-lg light-red-button pt-2 pb-2">Sign Up as Artist</a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{ asset('artist/js/login.js')}}"></script>
@endsection
