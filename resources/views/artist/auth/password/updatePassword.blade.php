@extends('artist.layouts.master')

@section('title')
    Update password
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Change Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('user-password.update') }}">
                        @csrf
                        @method('PUT')
                        @if (session('status') == "password-updated")
                            <div class="alert alert-success">
                                Password updated successfully.
                            </div>
                        @endif
                        <div class="form-group row">
                            <label for="current_password" class="col-md-4 col-form-label text-md-right">{{ __('Current Password') }}</label>

                            <div class="col-md-6">
                                <div class="input-group mt-1 mb-3 input-group-sm round-7 " id="show_hide_current_password">
                                    <input type="password" class="form-control input-sm round-7 password-radius @error('current_password', 'updatePassword') is-invalid @enderror" style="font-size: 12px" placeholder="Password" id="current_password" name="current_password">
                                    <div class="input-group-append rounded " style="height: 28px">
                                        <div class="input-group-text eye-icon pt-2" id="current_password_eye" style=""><a href="#">
                                            <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
                                        </div>
                                    </div>
                                    @error('current_password', 'updatePassword')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="col-md-6">
                                <div class="input-group mt-1 mb-3 input-group-sm round-7 " id="show_hide_password">
                                    <input type="password" class="form-control input-sm round-7 password-radius @error('password', 'updatePassword') is-invalid @enderror" style="font-size: 12px" placeholder="Password" id="password" name="password">
                                    <div class="input-group-append rounded " style="height: 28px">
                                        <div class="input-group-text eye-icon pt-2" id="password_eye" style=""><a href="#">
                                            <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
                                        </div>
                                    </div>
                                    @error('password', 'updatePassword')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <div class="input-group mt-1 mb-3 input-group-sm round-7 " id="show_hide_confirm_password">
                                    <input type="password" class="form-control input-sm round-7 password-radius" style="font-size: 12px" placeholder="Password" id="password_confirmation" name="password_confirmation" autocomplete="new-password">
                                    <div class="input-group-append rounded " style="height: 28px">
                                        <div class="input-group-text eye-icon pt-2" id="password_confirmation_eye" style=""><a href="#">
                                            <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-0 form-group row">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success btn-sm" style="width: 100%">
                                    {{ __('Save') }}
                                </button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('artist/js/updatePassword.js')}}"></script>
@endsection
