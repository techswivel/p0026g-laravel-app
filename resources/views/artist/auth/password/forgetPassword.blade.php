@extends('artist.layouts.authLoginMaster')


@section('title')
    Forget Password
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('artist/css/forgetPassword.css')}}">
@endsection


@section('content')
<div class="card p-2 mt-4 login_card" style="height: 19rem ;" id="signin-card">
    <div class="card-body login-card-body" id="login_card_body">
      <div class="data-content mt-1">
        <div>
          <h6 class="font-weight-bold font-szie1 pl-1" id="signin-text">Send Email Link</h6>
        </div>
        <p class="m-0 text-secondary small pl-1" style="margin-top: -9px !important;font-size:13.5px ">Enter your email to get link.</p>
      </div>
      <div class="container p-1 pt-4">
        @if (session('status'))
        <div class="alert alert-success text-center">{{ session()->get('status')}}</div>
      @endif
        @if(session()->has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Error!</strong> {{ session()->get('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif  
        <form action="{{ route('password.email') }}" method="post" class="needs-validation" novalidate>
          @csrf
          <label class="m-0"><small class="text-dark mb-0 font-wegiht600">Email Address</small></label>
            <div class="mb-3 mt-1 input-group-sm input_field " >
              <input type="email" class="form-control input-sm border-radius8 " style="font-size: 12px"  placeholder="Email" id="email" name="email" required>
               @if ($errors->has('email'))
                        <span class="invalid-feedback" style="display: block;" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                 </span>
                    @endif
            </div>
          <input type="submit" class="btn btn-block btn-danger btn-sm rounded-lg small font-weight-lighter red-button" value="Send Link">
        </form>
        <div class="text-center pt-2">
          <a  href="{{ route('login')}}" ><small class=" text-xs pt-2 red-text font-szie1 font-wegiht600">Back to Login</small></a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script src="{{ asset('artist/js/forgetPassword.js')}}"></script>
@endsection
