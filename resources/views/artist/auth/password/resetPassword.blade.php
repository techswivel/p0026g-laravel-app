@extends('artist.layouts.authLoginMaster')


@section('title')
    Reset Password
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('artist/css/resetPassword.css')}}">
@endsection

@section('content')
<div class="card p-2 mt-4 login_card" id="signin-card">
    <div class="card-body login-card-body" id="login_card_body">
      <div class="data-content mt-1">
        <div>
          <h6 class="font-weight-bold font-szie1 pl-1" id="signin-text">New Password</h6>
        </div>
        <p class="m-0 text-secondary small pl-1" style="margin-top: -9px !important;font-size:13.5px ">Set New password to your account.</p>
      </div>
      <div class="container p-1 pt-3">  
        <form action="{{route('password.update')}}" method="post" class="needs-validation" novalidate>
          @csrf
          <input type="hidden" name="token" value="{{ request()->route('token') }}">
           <input id="email" type="hidden" class="form-control" name="email" value="{{  request()->email }}"  autocomplete="email" readonly autofocus>
             <label class="m-0 mt-1"><small class="text-dark m-0 font-wegiht600">Password</small></label>
          <div class="input-group mt-1  mb-1 input-group-sm round-7" id="show_hide_password">
            <input type="password" class="form-control input-sm round-7 password-radius" style="font-size: 12px" placeholder="New Password" id="password" name="password" required>
            <div class="input-group-append rounded" style="height: 28px">
              <div class="input-group-text eye-icon pt-2" id="password-eye" style=""><a href="#">
                <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
              </div>
            </div>
            @if ($errors->has('email'))
              <span class="invalid-feedback" style="display: block;" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
            @if ($errors->has('password'))
              <span class="invalid-feedback" style="display: block;" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
          </div>
           <label class="m-0 mt-1"><small class="text-dark m-0 font-wegiht600">Confirm Password</small></label>
          <div class="input-group mt-1 mb-3 input-group-sm round-7" id="show_hide_confirmPassword">
            <input type="password" class="form-control input-sm round-7 password-radius" style="font-size: 12px" placeholder="Confirm Password" id="password_confirmation" name="password_confirmation" required>
            <div class="input-group-append rounded" style="height: 28px">
              <div class="input-group-text eye-icon pt-2" id="confirmPassword-eye" style=""><a href="#">
                <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
              </div>
            </div>
            @if ($errors->has('password_confirmation'))
              <span class="invalid-feedback" style="display: block;" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
          </div>
          <input type="submit" class="btn btn-block btn-danger btn-sm rounded-lg small font-weight-lighter red-button" value="Done">
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script src="{{ asset('artist/js/resetPassword.js')}}"></script>
@endsection
