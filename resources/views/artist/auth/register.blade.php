@extends('artist.layouts.authRegisterMaster')

@section('title')
    Sign Up
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('artist/css/register.css')}}">
@endsection

@section('content')
<div class="text-center">
    <div class="padding-top30">
        <div class="data-content mt-5">
            <div>
                <span class="font-weight-bold h6 signup-text font-weight-bold" style="font-size: 19px;">Sign Up</span>
            </div>
            <p class="m-0 text-secondary small pl-1" style="margin-top: -1.5px !important;font-size: 13.5px;">Q the Music Artist Dashboard.</p>
        </div>
    </div>
</div>
<form method="post" action="{{ route('register') }}"  enctype="multipart/form-data" class="needs-validation" novalidate>
    @csrf
    <div class="d-flex justify-content-center @error('avatar') is-invalid @enderror">
        <div class=" mb-1  mt-3 input-group-sm input_field" >
            <div class="d-flex flex-row">
                <div id="image-show" class ="roundedImage rounded-circle mt-2">
                </div>
                <div class="d-flex align-items-end image-div " style="">
                    <label>
                        <span class="dot d-flex justify-content-center">
                            <b class="plus">+</b>
                        </span>
                        <input type="file" id="avatar" name="avatar" class="form-control" value="{{ old('avatar') }}" style="display: none;">
                    </label>
                </div>
            </div>
        </div>
    </div>
    @error('avatar')
        <span class="invalid-feedback text-center" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div class="row">
        <div class="col-md-6 pl-1 pr-1 offset-md-3 mt-3">
            <div class="row ml-0 mr-0">
                <div class="col-12 col-md-6 pl-0 pr-1 firstNameField" >
                    <label class="m-0"> <span class="text-dark font-weight-lighter pb-1 size13">First name <span class="text-danger">*</span></span></label>
                    <div class="mb-1 input-group-sm input_field round-7">
                        <input type="text" class="form-control input-xs round-7 border-radius8 @error('firstName') is-invalid @enderror"  placeholder="First name" id="firstName" name="firstName" value="{{ old('firstName') }}" autocomplete="firstName" autofocus required>
                        @error('firstName')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>
                </div>
                <div class="col-12 col-md-6 pl-1 pr-0 lastNameField">
                    <label class="m-0"> <span class="text-dark font-weight-lighter pb-1 size13" >Last name <span class="text-danger">*</span></span></label>
                    <div class="mb-1 input-group-sm input_field rounded" >
                        <input type="text" class="form-control input-xs border-radius8 @error('lastName') is-invalid @enderror" placeholder="Last name" id="lastName" name="lastName" value="{{ old('lastName') }}"  autocomplete="lastName" autofocus required>
                            @error('lastName')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                    </div>
                </div>
            </div>
            <div class="col-12 pl-0 pr-0 pt-2">
                <label class="m-0"> <span class="text-dark font-weight-lighter size13" >Email address <span class="text-danger">*</span></span></label>
                <div class=" mb-1 input-group-sm input_field mt-1" >
                    <input type="email" class="form-control input-xs border-radius8 @error('email') is-invalid @enderror"  placeholder="Email" id="email" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus required>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-12 pl-0 pr-0 pt-2">
                <label class="m-0"> <span class="text-dark font-weight-lighter size13" ><strong>About Artist</strong></span></label>
                <div class=" mb-1 input-group-sm input_field rounded mt-1" >
                    <textarea rows="3" cols="50" name="aboutArtist"  id="aboutArtist"  class="form-control input-xs rounded border-radius8 @error('aboutArtist') is-invalid @enderror"  placeholder="Enter About Artist"   autocomplete="aboutArtist" autofocus>{{ old('aboutArtist') }}</textarea>
                    @error('aboutArtist')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-12 pl-0 pr-0 pt-2">
                <label class="m-0"> <span class="text-dark font-weight-lighter size13">Password <span class="text-danger">*</span></span></label>
                <div class="input-group mb-1 input-group-sm round-7 mt-1" id="show_hide_password">
                    <input type="password" class="form-control input-xs password-radius @error('password') is-invalid @enderror"  placeholder="Password" id="password" name="password"  autofocus required>
                    <div class="input-group-append rounded">
                        <div class="input-group-text eye-icon" id="password-eye"><a href="#">
                            <span span class="fas fa-eye-slash eye-grey" ></span></a>
                        </div>
                    </div>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-12 pl-0 pr-0 pt-2">
                <label class="m-0"> <span class="text-dark font-weight-lighter size13" >Confirm Password <span class="text-danger">*</span></span></label>
                <div class="input-group mb-3 input-group-sm round-7 mt-1" id="show_hide_password_confirm">
                    <input type="password" class="form-control input-xs password-radius @error('password_confirmation') is-invalid @enderror"  placeholder="Enter Confirm Password" id="confirmPassword" name="password_confirmation"  autofocus required>
                    <div class="input-group-append rounded">
                        <div class="input-group-text eye-icon" id="confirmPassword-eye" ><a href="#">
                            <span class="fas fa-eye-slash eye-grey" ></span></a>
                        </div>
                    </div>
                    @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-12 pl-0 pr-0">
                <input type="submit" class="btn btn-block btn-danger btn-sm rounded-lg red-button" value="Sign Up">
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="mt-3 mb-1 rounded-lg">
                        <a  href="{{ route('login')}}"  class="btn btn-block btn-sm rounded-lg light-red-button pt-2 pb-1">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script src="{{ asset('artist/js/register.js')}}"></script>
@endsection
