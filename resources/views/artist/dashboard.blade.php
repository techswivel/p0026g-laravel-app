@extends('artist.layouts.master')

@section('title')
Dashboard
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('artist/css/dashboard.css')}}">
@endsection
@section('content')
<div class="row pt-0 pl-2">
    <p class="withdrawal-text ">Dashboard</p>
</div>
<div class="row pt-0 pl-0">
    <div class="col-lg-6">
       <div class="card">
              <div class="card-header whiteBackground border-0 pl-2 pt-3">
                <div class="d-flex justify-content-between">
                    <small class="float-left ml-2 pt-1"  style="font-weight:600">Sales Report</small>
                     <select class="  month-select" id="saleReportChart" name="saleReportChart" onchange="changeStatusOfChart();"></select>
                </div>
              </div>
              <div class="card-body pl-2 pr-2 pt-0 pb-3">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <small class="float-left ml-2 pt-1"  style="font-weight:600">Total Sales</small>
                    <small class="float-left ml-2 pt-1"  style="font-weight:800">$<small id="totalPriceOfSale"style="font-weight:800;font-size:12px"></small>
                         <span id="revenueColor" class="text-success" style="font-weight:500"><i id="revenueArrow"class="fas fa-arrow-up"></i><span id="revenueValue"> % </span><span class="text-muted" style="font-weight:400">Since last Year</span></span>
                    </small>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-1">
                  <canvas id="sales-chart" height="170"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square black-red-text"></i> <small class=" ml-0 pt-1"  style="font-weight:500" id="aboutChart">This Year</small>
                  </span>
                </div>
              </div>
            </div>
    </div>
    <div class="col-lg-6">
         <div class="card" style="height: 328px">
              <div class="card-header whiteBackground border-0">
               <div class="d-flex justify-content-between">
                    <small class="  pt-1"  style="font-weight:600">New Buying</small>
                    <a href="{{ route('artist.profile')}}"><small class="  pt-1"  style="font-weight:500">View Details</small></a>
               </div> 
              </div>
              <div class="card-body table-responsive newBuy p-0">
                <table class="table table-striped table-valign-middle" style="font-size: 12px">
                  <thead>
                  <tr>
                    <th>#No</th>
                    <th>User</th>
                    <th>Purchased at</th>
                    <th>Song/Album</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($purchases as $purchase)
                      <tr>
                        <td class="border-none p-2 ">00001</td>
                        <td class="border-none p-2">
                          <div class="d-flex flex-row">
                            @if ($purchase->user->avatar)
                              <img src="{{Helper::urlPath($purchase->user->avatar)}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  
                            @else    
                              <img src="{{ asset('artist/image/avatar/default-avatar.png')}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  
                            @endif
                            <div class="ml-2 padding-top5">{{$purchase->user->firstName}} {{$purchase->user->lastName}}</div>
                          </div>
                        </td>
                        <td class="border-none p-2 ">{{Carbon\Carbon::parse($purchase->createdAt)->format('d/m/Y.g:i A')}}</td>
                        <td class="border-none p-2 ">{{Helper::checkItem($purchase->id)}}</td>
                        <td class="border-none p-2 "><button type="button" style="cursor:pointer" onclick="buyerDetail({{$purchase->id}})" id="viewEarn" data-toggle="modal" data-target="#" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
    </div>
</div>

<div id="mySidenav" class="sidenav shadow">
    
  <div class="row m-0 mb-1 mt-1">
    <div class="col ">
        <small class="float-left ml-0 pt-1"  style="font-weight:500">User Detail</small> 
    <br>
    <ul class="products-list product-list-in-card pl-2 pr-1">
        <li class="item">
            <div class="product-img pt-1">
                <img id="buyerImage" src="" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  id="buyerName" class="product-title font-szie10">Khawar Hussain</small>
                <small id="buyerEmail" class="product-description font-szie10 light-grey-text">khawar.hussain@gmail.com</small>
            </div>
        </li>
    </ul>
    <small class="float-left ml-0 pt-1 pl-0"  style="font-weight:500">Purchased items </small> 
    <br>
    <ul id="purchaseList" class="products-list product-list-in-card pl-0 pr-1">
        <li class="item pt-1 pb-1" style="border-bottom: none">
            <div class="product-img pt-1">
                <img src="" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10 ">Life on Mars<span class="float-right ">$5</span></small>
                <small class="product-description font-szie10 light-grey-text">3:56</small>
            </div>
        </li>
    </ul>
    <div  class="card p-0 m-0 mt-3 border-radius8 off-white " style="width: 100%">
        <div class="card-body m-2 m-0 p-0 border-radius8">
            <div class="row">
                <div class="col-lg-12">
                    <div class="d-flex align-items-start">
                        <small class="float-left ml-0 pt-1"  style="font-weight:500">Address</small>
                    </div>
                    <div  class="d-flex align-items-end">
                         <p id="buyerAddress" class="pb-2  userAccountNumber">885B Faisal Town Lahore,Pakistan</p>
                    </div>
               </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>    

    

<div id="mySideClose" class="shadow">
  <div class="row m-0 ">
    <div class="col " style="padding-left:7px">
         <a href="javascript:void(0)"  onclick="closeNav()"><i class="bi bi-x"></i></a>
    </div>
 </div>
</div> 
@endsection


@section('script')
<script>
function thumbnailUrl(path) {
    return`<?php $path = Helper::urlPath(`+path +`);echo "$path";?>`+path;
    }
</script>
<script src="{{ asset('js/Chart.js')}}"></script>
<script src="{{ asset('artist/js/dashboard.js')}}" ></script>
@endsection