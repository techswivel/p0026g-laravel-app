<aside class="main-sidebar sidebar-dark-primary elevation-4 text-xs" style="background-color: #ba011a;">
    <!-- Brand Logo -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-1  pb-0 mb-0 d-flex" style="margin-top: 4px !important;">
            <div class="text-center">
                <div class="row d-flex align-items-center">
                    <p class="" style="margin-left: 30px;color: white;font-size: 15px;margin-top: 1px !important;">Q the Music</p>
                </div>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item width180">
                    <a href="{{ route('admin.dashboard')}}"  class="nav-link white-color width180">
                        <i class=""></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item width180">
                    <a href="{{ route('users.index')}}" class="nav-link white-color width180">
                        <i class=""></i>
                        <p>
                            App Users
                        </p>
                    </a>
                </li>
                <li class="nav-item width180">
                    <a href="{{url('admin/categories/')}}" class="nav-link white-color width180">
                        <i class=""></i>
                        <p>
                            Category
                        </p>
                    </a>
                </li>
                <li class="nav-item width180">
                    <a href="{{url('admin/notifications')}}" class="nav-link white-color width180">
                        <i class=""></i>
                        <p>
                            Notifications
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link white-color width180">
                        <i class=""></i>
                        <p>
                            Artists
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="ml-1 nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{ route('activeArtists') }}" class="nav-link white-color width180">
                            <i class=""></i>
                            <p class="ml-2">Actives Artists</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{ route('blockArtists') }}" class="nav-link white-color width180">
                            <i class=""></i>
                            <p class="ml-2">Blocked Artists</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{ route('admin.artist.notification') }}" class="nav-link white-color width180">
                            <i class=""></i>
                            <p class="ml-2">Notifications</p>
                        </a>
                      </li>
                    </ul>
                </li>
                <li class="nav-item width180">
                    <a href="{{ route('admin.packages')}}" class="nav-link white-color width180">
                        <i class=""></i>
                        <p>
                            Packages
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link white-color width180">
                        <i class=""></i>
                        <p>
                            Earnings
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="ml-1 nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{ route('admin.purchased.history') }}" class="nav-link white-color width180">
                            <i class=""></i>
                            <p class="ml-2">Purchased History</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{ route('admin.artist.earning') }}" class="nav-link white-color width180">
                            <i class=""></i>
                            <p class="ml-2">Artist Earnings</p>
                        </a>
                      </li>
                    </ul>
                </li>
                <li class="nav-item width180">
                    <a href="{{ url('admin/content-type/create?content_type=TermAndCondition') }}" class="nav-link white-color width180">
                        <i class=""></i>
                        <p>
                            Terms & Conditions
                        </p>
                    </a>
                </li>
                <li class="nav-item width180">
                    <a href="{{ url('admin/content-type/create?content_type=PrivacyPolicy') }}" class="nav-link white-color width180">
                        <i class="nav-icon "></i>
                        <p>
                            Privacy policy
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('telescope')}}"
                       class="nav-link white-color width180 {{ (request()->is('telescope/*')) ? 'active-nav' : '' }}" target="_blank">
                        <p>
                            <i class="nav-icon "></i>

                            Telescope
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

