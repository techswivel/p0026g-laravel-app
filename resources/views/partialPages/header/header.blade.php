<nav  class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>

    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <div class="user-panel  pb-3  d-flex">
                  <img src="{{asset('img/default-avatar.png')}}"  class="img-size-32 img-circle img-22"  alt="User Image" >
              </div>
        </a>
        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
          <div class="dropdown-item disabled">
            <div class="media">
              <img src="{{asset('img/default-avatar.png')}}" alt="User Avatar"  class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <p class="dropdown-item-title">
                    {{auth()->user()->name? auth()->user()->name : "Administrator"}}
                </p>
                <small class="text-sm">{{auth()->user()->email? auth()->user()->email : "Email"}}</small>
                
              </div>
            </div></div>
          <div class="dropdown-divider"></div>
          <div class="dropdown-item" >
              {{-- <a class="btn btn-primary" style="float: left" href="{{ url('/forgot-password') }}">Password Reset</a> --}}
              <form action="{{route('logout')}}" method="post" enctype="multipart/form-data">
                  @csrf
                  <button class="btn btn-block btn-danger red-button btn-sm rounded">Logout</button>
              </form>
           </div>
        </div>
      </li>
       </ul>
  </nav>
  <!-- /.navbar -->

 
