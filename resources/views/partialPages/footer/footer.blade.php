<div>
    <footer class="main-footer no-print">
        <div class="float-right d-none d-sm-block">
            <b>{{ config('app.name', 'TechSwivel') }}</b>
        </div>
        <strong>Copyright &copy; {{date('Y')}} <a href="https://www.techswivel.com/" target="_blank">TechSwivel Pvt</a>.</strong> All rights reserved.
    </footer>
</div>
