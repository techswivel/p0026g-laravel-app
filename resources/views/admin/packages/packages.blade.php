@extends('artist.layouts.master')

@section('title')
    packages
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/packages.css') }}">
    <link rel="stylesheet" href="{{ asset('css/customnotification.css') }}">
@endsection
@section('content')
    <div class="row pl-3 pr-3  pt-1 pb-3">
        <div class="col-lg-12 pl-0">
            <span class="withdrawal-text pl-0">Packages</span>
        </div>
    </div>
    <div class="row pt-1 pb-3">
        <div class="col-lg-3 col-md-12 pl-2 pr-2">
            <div class="card border-radius8">
                <div class="card-header topBorderRadius border-0" style="background-color: white">
                    <p class="mb-0" style="font-weight:200">Create Package Plan</p>
                </div>

                <ul class="nav nav-pills border-bottom" id="pills-tab" role="tablist">
                    <li class="p-0 nav-link progress-step-1 w-50 active"></li>
                    <li class="p-0 nav-link progress-step-2 w-50"></li>
                </ul>

                <div class="card-body border-radius8 " style="padding:12px">
                    <form id="createPackageForm">

                        <input type="hidden" id="stepNo" name="stepNo" value="1">
                        <input type="hidden" id="subscriptionPrice" name="subscriptionPrice" value="">
                        <input type="hidden" id="packageId" name="packageId" value="">

                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                                <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                    <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px "><b>Title</b></span></label>
                                    <input type="text" class="form-control input-sm border-radius8 add-song-input-text" placeholder="Enter Plan Title" style="font-size: 13.5px" id="planTitle" name="planTitle">
                                    <div class="invalid-feedback" id="planTitleError"></div>
                                </div>

                                <div class="mb-2 mt-1 form-group input-group-sm input_field ">
                                    <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px ">Duration</span></label>
                                    <select class="form-control font-size12 border-radius8 add-song-input-text" id="planDuration" name="planDuration" required>
                                        <option value="ONE_WEEK">1 Week</option>
                                        <option value="ONE_MONTH">1 Month</option>
                                        <option value="TWO_MONTHS">2 Months</option>
                                        <option value="THREE_MONTHS">3 Months</option>
                                        <option value="SIX_MONTHS">6 Months</option>
                                        <option value="ONE_YEAR">1 Year</option>
                                    </select>
                                    <div class="invalid-feedback" id="planDurationError"></div>
                                </div>


                                <div class="pt-2 mb-2 mt-1 form-group input-group-sm input_field " >
                                    <input type="button" id="fd" class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-4 pr-4 border-radius8 account-Create-button package-next-btn" style="font-size: 12px" value="Next">
                                    <span class="clearText">Clear all</span>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

                                <div id="price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                    <label class="m-0 mb-1"><span class="text-dark add-song-label-text">Price</span></label>
                                    <select name="priceTier" id="priceTier" class="form-control"></select>
                                    <div class="invalid-feedback" id="songPriceError"></div>
                                </div>


                                <div class="pt-2 mb-2 mt-1 form-group input-group-sm input_field ">
                                    <button type="button" class="btn btn-xs py-1 px-3 border-radius8 light-grey package-previous-btn" >Previous</button>
                                    <input type="submit" id="fd" class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-4 pr-4 border-radius8 account-Create-button" style="font-size: 12px" value="Activate Plan">
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-12 pl-2 pr-2">
            <div class="card border-radius8">
                <div class="card-header topBorderRadius" style="background-color: white;border-bottom: none;">
                    <p class="mb-0" style="font-weight:200">Active Plans</p>
                </div>
                <div class="card-body border-radius8 " style="padding:12px">
                    <div class="row">
                        @if (count($packages) != 0)
                            @foreach ($packages as $index => $package)
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="card border-radius8" style="background-color: white">
                                        <div class="card-body border-radius-top p-2 pl-2 ">
                                            <div class="row pb-1 m-0">
                                                <ul class="products-list product-list-in-card pl-2 pr-2">
                                                    <li class="item">
                                                        <div class="product-img">
                                                            <h3 class="packageCount border-radius8">
                                                                {{ sprintf('%02d', ++$index) }}</h3>
                                                        </div>
                                                        <div class="product-info" style="margin-top: 6px;">
                                                            <h5 class="product-title" style="margin-bottom: 0px;">
                                                                {{ $package->name }}</h5>
                                                            <small class="product-description" style="color: black">
                                                                ${{ $package->price }}/ {{ $package->duration }} Month
                                                            </small>
                                                            <small class="product-description" style="color: black">
                                                                @if ($package->status == 'In-Process')
                                                                    <i class="fa fa-clock text-orange"></i> {{ $package->status }}
                                                                @elseif ($package->status == 'Failed')
                                                                    <i class="fa fa-info-circle text-danger"></i> {{ $package->status }}
                                                                @else

                                                                @endif
                                                            </small>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="row m-0 pb-2 text-center">
                                                <div class="col-2 p-0 pl-2">
                                                    <div class="dropdown dropdown-block-aside" id="checkplaylis">
                                                        <i class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('/svg/doticon.png') }}"></i>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item btn-block-aside"  href="javascript:void(0)"  onclick="editPackage({{$package->id}})">Edit Plan</a>
                                                            <a class="dropdown-item btn-block-aside"  href="javascript:void(0)"  onclick="packageDelete({{$package->id}})">Delete Plan</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-10 p-0 pl-2">
                                                    <a href="/admin/purchased/history/{{ $package->id }}" id="editProfile" class="btn btn-block rounded-lg red-button font-weight-lighter text-white border-radius8 edit-profile edit-profile-package">
                                                        View Earnings
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row" style="margin:0px; width:100%">
                                <div class="col-lg-12" style="text-align: center">
                                    <p>No data available in table</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Delete Package Modal --}}
    @include('admin.packages.modals.deletePackage')
    {{-- Edit Package Modal --}}
    @include('admin.packages.modals.editPackage')

@endsection


@section('script')
    <script>
        function thumbnailUrl(path) {
            return `<?php $path = Helper::urlPath(`+path +`);
            echo "$path"; ?>` + path;
        }
    </script>
    <script src="{{ asset('admin/js/package.js') }}"></script>
@endsection
