{{-- Edit Package  Modal --}}
<div class="modal fade" id="editPackageModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Edit Plan</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editPackageForm">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <input id="editPackageId" name="packageId" type="hidden">
                        <input type="hidden" id="editPlanPrice" name="editPlanPrice" value="">

                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px "><b>Title</b></span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text" placeholder="Enter Plan Title" maxlength="30" style="font-size: 12.5px" id="editPlanTitle" name="editPlanTitle" >
                            <div class="invalid-feedback" id="editPlanTitleError"></div>
                        </div>
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <div id="price-div" class="mb-2 mt-1 input-group-sm input_field ">
                                <label class="m-0 mb-1"><span class="text-dark add-song-label-text">Price</span></label>
                                <select id="editPlanPriceTier" name="priceTier" class="form-control" disabled></select>
                                <div class="invalid-feedback" id="editPlanPriceError"></div>
                            </div>
                        </div>
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px "><b>Duration</b></span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="editPlanDuration" name="editPlanDuration" required>
                                <option value="ONE_WEEK">1 Week</option>
                                <option value="ONE_MONTH">1 Month</option>
                                <option value="TWO_MONTHS">2 Months</option>
                                <option value="THREE_MONTHS">3 Months</option>
                                <option value="SIX_MONTHS">6 Months</option>
                                <option value="ONE_YEAR">1 Year</option>
                            </select>
                            <div class="invalid-feedback" id="editPlanDurationError"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button p3 btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>
