<div class="modal fade" id="deletePackageModal">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete This Package?</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form id="deletePackageForm">    
                    <input id="deleteSongButton" type="submit" id class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Yes">
                </form>
            </div>
        </div>
    </div>
</div>