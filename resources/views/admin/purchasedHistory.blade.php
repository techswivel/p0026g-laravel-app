@extends('artist.layouts.master')

@section('title')
    Purchased History
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/purchasedHistory.css')}}">
@endsection
@section('content')
<div class="row pl-3 pr-3  pt-1 pb-3">
    <div class="col-lg-10 pl-0">
        <span class="withdrawal-text pl-0">Purchased History</span>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header p-2 " style="background-color: white;">
                <div class="row m-0">
                    <div class="col-lg-4 p-2 pr-1">
                        <div class="card border-radius8 m-0" style="background-color: #fbf2f3" >
                            <div class="card-body border-radius-top p-1 pl-2 pr-3" >
                                <div class="row m-0">
                                    <div class="col-1 px-0 py-1">
                                        <div class="line"></div>
                                    </div>
                                    <div class="col-7 pl-0 pt-3 pb-3">
                                        <h5 class="float-left mb-0">Songs Earnings</h5>
                                    </div>
                                    <div class="col-4 pr-0 pt-3 pb-3">
                                        <h5 class="float-right mb-0"><b>${{ Helper::number_format_short($songPurchase) }}</b></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-2">
                        <div class="card border-radius8 m-0" style="background-color: #fbf2f3" >
                            <div class="card-body border-radius-top p-1 pl-2 pr-3" >
                                <div class="row m-0">
                                    <div class="col-1 px-0 py-1">
                                        <div class="line"></div>
                                    </div>
                                    <div class="col-7 pl-0 pt-3 pb-3">
                                        <h5 class="float-left mb-0">Albums Earnings</h5>
                                    </div>
                                    <div class="col-4 pr-0 pt-3 pb-3">
                                        <h5 class="float-right mb-0"><b>${{ Helper::number_format_short($albumPurchase) }}</b></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-2">
                        <div class="card border-radius8 m-0" style="background-color: #fbf2f3" >
                            <div class="card-body border-radius-top p-1 pl-2 pr-3" >
                                <div class="row m-0">
                                    <div class="col-1 px-0 py-1">
                                        <div class="line"></div>
                                    </div>
                                    <div class="col-7 pl-0 pt-3 pb-3">
                                        <h5 class="float-left mb-0">Packages Earnings</h5>
                                    </div>
                                    <div class="col-4 pr-0 pt-3 pb-3">
                                        <h5 class="float-right mb-0"><b>${{ Helper::number_format_short($planPurchase) }}</b></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body p-0">
                {{-- Purchaed History Table --}}
                <table id="purchasedHistoryTable" class="table display table-striped table-valign-middle"  style="font-size: 12px;">
                    <thead class="border-none">
                        <tr class="border-none">
                            <th class="border-none pt-2 pl-3">#No.</th>
                            <th class="border-none pt-0 pl-3">User Name</th>
                            <th class="border-none pt-0 pl-3">Email</th>
                            <th class="border-none pt-0 pl-3">Song/Album/Plan</th>
                            <th class="border-none pt-0 pl-3">Spent Amount</th>
                            <th class="border-none pt-0 pl-3">Purchaed at</th>
                            <th class="border-none pt-0 pl-3"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($purchases as $index => $purchase)
                            <tr>
                                <td class="border-none p-3 pt-4">{{ ++$index }}</td>
                                <td class="border-none p-3">
                                    <div class="d-flex flex-row">
                                        @if ($purchase->user->avatar)
                                            <img src="{{ Helper::urlPath($purchase->user->avatar) }}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  
                                        @else
                                            <img src="{{ asset('artist/image/avatar/default-avatar.png')}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  
                                        @endif
                                        <div class="ml-2 padding-top5">{{ $purchase->user->firstName }} {{ $purchase->user->lastName }}</div>
                                    </div>
                                </td>
                                <td class="border-none p-3 pt-4">{{ $purchase->user->email }}</td>
                                <td class="border-none p-3 pt-4">{{ Helper::purchaseItem($purchase) }}</td>
                                <td class="border-none p-3 pt-4">${{ $purchase->amount}}</td>
                                <td class="border-none p-3 pt-4">{{ Carbon\Carbon::parse($purchase->createdAt)->format('d/m/Y .g:i A') }}</td>
                                <td class="border-none p-3 pt-4">
                                    <button type="button" style="cursor:pointer;float: right;"   data-toggle="modal" data-target="#" onclick="buyerDetail({{ $purchase->id}})" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10" ><i class="bi bi-folder-fill "></i> View</button>
                                </td>
                            </tr>
                        @endforeach     
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="mySidenav" class="sidenav shadow">
    
    <div class="row m-0 mb-1 mt-1">
      <div class="col ">
          <small class="float-left ml-0 pt-1"  style="font-weight:500">User Detail</small> 
      <br>
      <ul class="products-list product-list-in-card pl-2 pr-1">
          <li class="item">
              <div class="product-img pt-1">
                  <img id="buyerImage" src="" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
              </div>
              <div class="product-info ml-5">
                  <small  id="buyerName" class="product-title font-szie10">Khawar Hussain</small>
                  <small id="buyerEmail" class="product-description font-szie10 light-grey-text">khawar.hussain@gmail.com</small>
              </div>
          </li>
      </ul>
      <small class="float-left ml-0 pt-1 pl-0"  style="font-weight:500">Purchased items </small> 
      <br>
      <ul id="purchaseList" class="products-list product-list-in-card pl-0 pr-1">
          <li class="item pt-1 pb-1" style="border-bottom: none">
              <div class="product-img pt-1">
                  <img src="" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
              </div>
              <div class="product-info ml-5">
                  <small  class="product-title font-szie10 ">Life on Mars<span class="float-right ">$5</span></small>
                  <small class="product-description font-szie10 light-grey-text">3:56</small>
              </div>
          </li>
      </ul>
        <div id="plan">
        </div>
      </div>
    </div>
  </div>    
  
      
  
  <div id="mySideClose" class="shadow" style="height: 40px;">
    <div class="row m-0 ">
      <div class="col " style="padding-left:7px">
           <a href="javascript:void(0)"  onclick="closeNav()"><i class="bi bi-x"></i></a>
      </div>
   </div>
  </div>
@endsection


@section('script')
    <script>
function thumbnailUrl(path) {
    return`<?php $path = Helper::urlPath(`+path +`);echo "$path";?>`+path;
    }
</script>
<script src="{{ asset('admin/js/purchasedHistory.js')}}" ></script>
@endsection
