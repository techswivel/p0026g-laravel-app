{{-- Notification detail Modal --}}
<div class="modal fade" id="notificationShowDetail">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title notification-detail-text" ><b>Notification Detail </b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="notificationShowDetailValue">
            </div>
            <div class="modal-footer p-0" style="margin-left: 4px">
                <div class="" style="width: 100%;">
                    <div class="pr-0 p-2 pr-0 mr-0 float-right d-flex justify-content-start" >
                        <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
