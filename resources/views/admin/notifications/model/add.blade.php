{{-- Add Notification Modal --}}
<style>
    input[type=checkbox]{
        height:10px;
        width:10px;
        background-color: red;
    }
</style>
<div class="modal fade" id="myNotification">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content border-radius12">
            <div class="modal-header p-3 pb-2 " >
                <small class="modal-title"><b>New Notification </b></small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addNotificationFormAdminTopic">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <for name="title"></for>
                            <label for="title" class="m-0"><small class="text-dark mb-0 font-wegiht600"><b>Title</b></small></label>
                            <div class="mb-1 mt-1 input-group-sm input_field " >
                                <input type="text" class="form-control input-sm border-radius8"  placeholder="Enter title" id="title" name="title">
                                <div class="invalid-feedback" id="notificationTitleError"></div>
                            </div>
                            <for name="description"></for>
                            <label  for="description" class="m-0 mt-1"><small class="text-dark mb-0 font-wegiht600"><b>Message</b></small></label>
                            <div class="mb-1 mt-1 input-group-sm input_field ">
                                <textarea name="description" id="description"
                                          rows="7" cols="50"  placeholder="Write notification description here..."  class="form-control input-xs rounded border-radius8" required></textarea>
                                <div class="invalid-feedback" id="notificationMessageError"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="icheck-primary d-inline">
                            <for name="Android_Users"></for>
                            <input type="checkbox" id="Android_Users" value="Android_Users" checked name="Android_Users">
                            <label for="Android_Users">
                                Android Users</label>
                        </div>
                        &nbsp; &nbsp;
                        <div class="icheck-primary d-inline">
                            <for name="iOS_Users"></for>
                            <input type="checkbox" id="iOS_Users" value="iOS_Users" name="iOS_Users">
                            <label for="iOS_Users">
                                iOS Users</label>
                        </div>
                        &nbsp; &nbsp;
                        <div class="icheck-primary d-inline">
                            <for name="free_plan_user"></for>
                            <input type="checkbox" id="free_plan_user" value="free_plan_user" name="free_plan_user" >
                            <label for="free_plan_user">
                                Free Plan User</label>
                        </div>
                        &nbsp; &nbsp;

                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        @foreach($packages as $package)
                            <div class="icheck-primary d-inline">
                                <for name="packageName"></for>
                                <input type="checkbox" name="packageName" checked value="{{$package->name}}">
                                <label for="packageName[]">
                                    {{$package->name}}</label>
                            </div> &nbsp;
                        @endforeach
                </div>
                <div class="modal-footer justify-content-between ">
                    <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Send">
                </div>
                </div>
            </form>
        </div>
    </div>
</div>



