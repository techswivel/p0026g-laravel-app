@extends('artist.layouts.master')

@section('title')
Notification
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('css/customnotification.css')}}">
<link rel="stylesheet" href="{{ asset('artist/css/notification.css')}}">
@endsection

@section('content')
<div class="row pl-4 pr-4  pt-1 pb-3">
    <div class="col-lg-8 pl-0">
        <span class="withdrawal-text pl-0">Send Notification</span>
        <button type="button" data-toggle="modal" data-target="#myNotification" class="btn ml-2 red-button btn-xs border-radius8 "><i class="fas fa-plus"></i> Add new</button>
    </div>
        <div class="col-md-12">
            <div class="table-responsive">
                {!! $dataTable->table(['class' => 'table table-bordered table-striped ', 'id' => 'notification-table']) !!}
            </div>
        </div>
</div>

@include('admin.notifications.model.add')
@include('admin.notifications.model.notificationShowDetail')
@include('admin.notifications.model.deleteNotificationTopic')

@endsection

@section('script')
    {!! $dataTable->scripts() !!}
    <script src="{{ asset('admin/js/notification.js')}}" ></script>
@endsection
