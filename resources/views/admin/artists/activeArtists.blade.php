@extends('artist.layouts.master')

@section('title')
    Active Artists
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/activeArtist.css')}}">
@endsection
@section('content')
<div class="row pl-3 pr-3  pt-1 pb-3">
    <div class="col-lg-10 pl-0">
        <span class="withdrawal-text pl-0">Artists</span>
    </div>
    <div class="col-lg-2 pl-0 pr-0">
        <input type= "search" class="form-control float-right" id="activeArtistSearch"  placeholder="Search">
    </div>
</div>
    <table id="example" style="width:100%" class=" display table-striped " >
        <thead>
            <tr >
                <th class="p-2 " style="font-size: 12px">#No.</th>
                <th class="p-2 " style="font-size: 12px">User Name</th>
                <th class="p-2 " style="font-size: 12px">Email</th>
                <th class="p-2 " style="font-size: 12px">Songs&Albums</th>
                <th class="p-2 " ></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($artists as $index => $artist)
                <tr>
                    <td class="p-3">{{ ++$index }}</td>
                    <td class="p-3">
                        <div class="row no-gutters text-center">
                            <div class="col-xl-3 col-md-6">
                                <img class=" img-fluid img-circle img-size-32" src="{{$artist->AvatarFullUrl}}" style="height:35px;">
                            </div>
                            <div class="col-xl-9 col-md-6 pl-1">
                                <span>{{$artist->firstName}} {{$artist->lastName}}</span>
                            </div>
                        </div>

                    </td>
                    <td class="p-3"><p class="text-center email-overflow-above-992" data-toggle="tooltip" data-placement="top" title="{{$artist->email}}">{{$artist->email}}</p></td>
                    <td class="p-3">{{ Helper::artistTotalSong($artist->id) }} Songs .{{Helper::artistTotalAlbum($artist->id)}} album</td>
                    <td class="p-3 float-right text-center">
                        <div class="outer">
                            <div class="inner">
                                <div class="dropdown dropdown-block-aside" id="checkplaylis" >
                                    <i class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('/svg/doticon.png')}}" class="mt-2"></i>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item btn-block-aside"  href="javascript:void(0)"  onclick="artistDetail({{$artist->id}})">Edit Artist Detail</a>
                                        <a class="dropdown-item btn-block-aside"  href="javascript:void(0)"  onclick="artistDelete({{$artist->id}})">Delete Artist</a>
                                    </div>
                                </div>
                            </div>
                            <div class="inner">
                                <a type="button" style="cursor:pointer" href="/admin/artists/detail/{{$artist->id}}" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10 mt-2"><i class="bi bi-folder-fill "></i> View</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

{{-- Edit Artist Modal --}}
@include('admin.artists.modals.editArtistDetail')
{{-- Delete Artist Modal --}}
@include('admin.artists.modals.deleteArtist')
{{-- Block Artist Modal --}}
@include('admin.artists.modals.blockArtist')
{{-- UnBlock Artist Modal --}}
@include('admin.artists.modals.unblockArtist')

@endsection


@section('script')
    <script>
function thumbnailUrl(path) {
    return`<?php $path = Helper::urlPath(`+path +`);echo "$path";?>`+path;
    }
</script>
<script src="{{ asset('admin/js/activeArtist.js')}}" ></script>
<script src="{{ asset('admin/js/blockArtist.js')}}" ></script>
{{-- <script src="{{ asset('js/user/rightSideForm.js')}}" ></script> --}}
@endsection
