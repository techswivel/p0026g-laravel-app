@extends('artist.layouts.master')

@section('title')
    Artist Earning
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/artistEarning.css')}}">
@endsection
@section('content')
<div class="row pl-3 pr-3  pt-1 pb-3">
    <div class="col-lg-10 pl-0">
        <span class="withdrawal-text pl-0">Artist Earnings</span>
    </div>
</div>

<table id="artistEarningTable" class="table table-striped table-valign-middle" style="font-size: 12px">
    <thead>
    <tr>
      <th>#No</th>
      <th>Artist Name</th>
      <th>Email</th>
      <th>Total earnings</th>
      <th>Withdrawal amount</th>
      <th>Available amount</th>
      <th>Requested</th>
      <th></th>
    </tr>
    </thead>
    <tbody>
        @foreach ($artists as $index => $artist)
            <tr>
                <td class="border-none p-3 text-center">{{ ++$index }}</td>
                <td class="border-none p-3">
                    <div class="row no-gutters text-center">
                        <div class="col-xl-3 col-md-6">
                            <img class=" img-fluid img-circle img-size-32" src="{{$artist->AvatarFullUrl}}" style="height:35px;">
                        </div>
                        <div class="col-xl-9 col-md-6 pl-1">
                            <span>{{$artist->firstName}} {{$artist->lastName}}</span>
                        </div>
                    </div>
                </td>
                <td class="p-3"><p class="text-center email-overflow-above-992" data-toggle="tooltip" data-placement="top" title="{{$artist->email}}">{{$artist->email}}</p></td>
                <td class="border-none p-3 ">${{ Helper::totalEarning($artist->id)}}</td>
                <td class="border-none p-3 ">${{ Helper::totalWithdrawalAmount($artist->id)}}</td>
                <td class="border-none p-3 ">${{ Helper::totalAvailableAmount($artist->id) }}</td>
                <td class="border-none p-3 ">{{ Helper::artistRequest($artist->id) }}</td>
                <td class="border-none p-3 float-right d-flex justify-content-start">
                    <button type="button" style="cursor:pointer;"   onclick="amount({{$artist->id}})"  class="btn ml-2 p-1 pl-2 pr-2 greenButton btn-xs border-radius8 font-size10">Send amount</button>
                    <a type="button" style="cursor:pointer" href="/admin/artists/detail/{{$artist->id}}" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</a>
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>

{{-- Create Earning Modal --}}
@include('admin.artists.modals.transferConfirmation')

@endsection

@section('script')
    <script>
function thumbnailUrl(path) {
    return`<?php $path = Helper::urlPath(`+path +`);echo "$path";?>`+path;
    }
</script>
<script src="{{ asset('admin/js/artistEarning.js')}}" ></script>
@endsection
