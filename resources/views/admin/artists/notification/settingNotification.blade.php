@extends('artist.layouts.master')

@section('title')
    Artist Notification
@endsection

@section('style')

    <link rel="stylesheet" href="{{ asset('admin/css/artistNotification.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
        .toggle.ios .toggle-handle { border-radius: 20px; }
        .toggle-off{
            background-color: #BA011A;
        }
        .toggle-on{
            background-color: #BA011A;
        }
    </style>
@endsection


@section('content')
    <div class="row pl-3 pr-3  pt-1 pb-3">
        <div class="col-lg-8 pl-0">
            <span class="withdrawal-text pl-0">Notifications Permissions</span>
        </div>
    </div>
    <div class="" style="background:white;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- translations card -->
            <div class="container" style="    background-color: white">
                <div class="content-centered">
                    <div class="col-12">
                    <div class=" col-md-12 col-lg-12 col-sm-12 mt-3" style="background-color: white;border: 1px">
                        <form id="perdayNotification">
                        <div class="form-group">
                            <for name="numberPerDay"></for>
                            <small for="title">Notification per day for artist</small>
                                <input type="number" id="numberPerDay" name="numberPerDay" style="width: 75%;"  class="input-sm form-control" placeholder="Enter Value" required>

                        </div>
                            <div class="float-right">
                                <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 float-right" style="width: 100px;margin-top: -51px" value="Send">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <strong>All Artist</strong><br>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col" class="border-0"></th>
                                <th scope="col" class="border-0"></th>
                                <th scope="col" class="border-0"></th>
                                <th scope="col" class="border-0"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($artists as $artist)
                            <tr class="p-1">
                                <td scope="row" class="border-0 p-1">
                                    <img class=" img-fluid img-circle img-size-32" src="{{$artist->AvatarFullUrl}}" style="height:35px;">&nbsp;
                                    {{$artist->firstName.' '.$artist->lastName}}</td>
                                <td class="border-0 p-1"></td>
                                <td class="border-0 p-1">
                                </td>
                                <td class="border-0 p-1" style="float: right !important">
                                    @if($artist->isNotificationEnabled !=0)
                                    <div class="checkbox">
                                        <input type="hidden" value="{{$artist->isNotificationEnabled}}"  name="isNotificationEnabled">
                                        <label >
                                            <input type="checkbox"  class="artistStatus"  name="testing" id="testing"  data-on="" data-off="" value="{{$artist->id}}" data-onstyle="danger" checked data-toggle="toggle" data-style="ios">
                                        </label>
                                    </div>
                                    @else
                                    <div class="checkbox1">
                                        <input type="hidden" value="{{$artist->isNotificationEnabled}}" id="isNotificationEnabledOne" name="isNotificationEnabledOne">
                                        <label >
                                            <input type="checkbox" class="noActiveStatus" value="{{$artist->id}}" name="abc" data-onstyle="danger"   data-toggle="toggle" data-style="ios">
                                        </label>
                                    </div>

                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
@section('script')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{ asset('admin/js/notificationSetting.js')}}" ></script>
@endsection
