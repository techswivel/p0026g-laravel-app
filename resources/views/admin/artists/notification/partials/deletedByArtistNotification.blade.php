{{--Deleted By Artist Notification Table --}}
<table id="deletedByArtistNotificationTable" class=" display table-striped" >
    <thead>
        <tr style="height:30px">
            <th class="pl-3">Date </th>
            <th class="pl-3">Artist Name </th>
            <th class="pl-3">Notification title</th>
            <th class="pl-3">Notification</th>
            <th class="pl-3"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($deletedByArtistNotifications as $deletedByArtistNotification)
            <tr>
                <td class="p-3">{{Carbon\Carbon::parse($deletedByArtistNotification->createdAt)->format('d/m/Y')}}</td>
                <td class="p-3">
                    <div class="row text-center">
                        <div class="col-xl-3 col-md-3">
                            @if ($deletedByArtistNotification->getArtist->avatar)
                                <img src="{{Helper::urlPath($deletedByArtistNotification->getArtist->avatar)}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                            @else
                                <img src="{{ asset('artist/image/avatar/default-avatar.png')}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                            @endif

                        </div>
                        <div class="col-xl-9 col-md-9">
                            <span>{{ $deletedByArtistNotification->getArtist->firstName }} {{$deletedByArtistNotification->getArtist->lastName}}</span>
                        </div>
                    </div>
                </td>
                <td class="p-3">{{ Str::limit($deletedByArtistNotification->title, 25) }}</td>
                <td class="p-3">{{ Str::limit($deletedByArtistNotification->message, 35) }}</td>
                <td class="p-3 float-right d-flex justify-content-start" >
                    <a class="btn text-left btn-xs red-button border-radius8 small-button px-2" onclick="notificationDetail({{ $deletedByArtistNotification->id }})" style="margin-right: 4px" ><i class="bi bi-eye-fill"></i></a>
                    <a class="btn text-left btn-xs orange-button border-radius8 small-button px-2" onclick="deleteNotification({{$deletedByArtistNotification->id}})"  style="margin-right: 4px"><span class="bi bi-trash3"  id="trash-iconq"></span></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
