@extends('artist.layouts.master')

@section('title')
    Artist Notification
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/artistNotification.css')}}">
@endsection


@section('content')
<div class="row pl-3 pr-3  pt-1 pb-3">
    <div class="col-lg-8 pl-0">
        <span class="withdrawal-text pl-0">Artist Notifications</span>
    </div>
    <div class="col-lg-2 pl-0 pr-1">
        <a href="{{url('admin/notification-setting')}}" class="btn btn-outline-dark  float-right" style="background-color:#FFFFFF "><img src="{{asset('/svg/settings.svg')}}" height="20px" width="20px" class="mr-2">setting</a>
    </div>
    <div class="col-lg-2 pl-0 pr-0">
        <input type= "search" class="form-control float-right" id="filterSearch"  placeholder="Search">
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header p-0">
                <div class="row ml-0">
                    <ul class="nav nav-pills ml-auto " style="margin-left: 0px !important;margin-bottom: -1px !important;font-size: 11px;">
                        <li class="nav-item"><a class="nav-link active pl-4 pr-4" href="#tab_1" data-toggle="tab" style="">Pending</a></li>
                        <li class="nav-item"><a class="nav-link pl-4 pr-4" href="#tab_2" data-toggle="tab">Approved</a></li>
                        <li class="nav-item"><a class="nav-link pl-4 pr-4" href="#tab_3" data-toggle="tab">Rejected</a></li>
                        <li class="nav-item"><a class="nav-link pl-4 pr-4" href="#tab_5" data-toggle="tab">Deleted By Artist</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="tab-content">
                    <div class="tab-pane active table-responsive" id="tab_1">
                        @include('admin.artists.notification.partials.pendingNotification')
                    </div>
                    <div class="tab-pane table-responsive" id="tab_2">
                        @include('admin.artists.notification.partials.approvedNotification')
                    </div>
                    <div class="tab-pane table-responsive" id="tab_3">
                        @include('admin.artists.notification.partials.rejectNotification')
                    </div>
                    <div class="tab-pane table-responsive" id="tab_5">
                        @include('admin.artists.notification.partials.deletedByArtistNotification')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Notification Detail --}}
@include('admin.artists.notification.modals.notificationDetail')
{{-- Notification Reject --}}
@include('admin.artists.notification.modals.rejectNotification')
{{-- Notification Approve --}}
@include('admin.artists.notification.modals.approveNotification')
{{-- Notification Delete --}}
@include('admin.artists.notification.modals.deleteNotification')

@endsection

@section('script')
<script>
function thumbnailUrl(path) {
    return`<?php $path = Helper::urlPath(`+path +`);echo "$path";?>`+path;
}
</script>
<script src="{{ asset('admin/js/pendingNotification.js')}}" ></script>
<script src="{{ asset('admin/js/approvedNotification.js')}}" ></script>
<script src="{{ asset('admin/js/rejectNotification.js')}}" ></script>
<script src="{{ asset('admin/js/deletedByArtistNotification.js')}}" ></script>
@endsection
