{{-- Transfer Confirmation  Modal --}}
<div class="modal fade" id="transferConfirmationModal">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Transfer Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <div class="row mt-2 ml-2 mr-2">
                    <div id="userAccount" class="card mt-2 mb-0 border-radius8 off-white" style="width: 100%;">
                        <div class="card-body p-0 border-radius8">
                            <div class="row" style="margin: 0px">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="d-flex align-items-start">
                                        <p  class="pt-2 pl-2 userAccountName" id="userAccountName" >xxxx</p>
                                    </div>
                                    <div class="d-flex align-items-end">
                                        <p class="pb-2 pl-2 userAccountNumber" id="userAccountNumber">xxxx</p>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            <form id="transferConfirmForm">    
            <div class="row mt-3 ml-2 mr-2">
                <div class="col-8 pl-0 pr-0">
                    <small style="color: gray;">Did you send money to this buyer</small>
                </div>
                <div class="col-4 pl-0 pr-0">
                    <input type="radio" id="age1" name="age" value="30">
                    <label for="age1" class="pr-3" style="color: gray;">Yes</label>
                    <input type="radio"  id="age1" name="age" value="30">
                    <label for="age1" style="color: gray;">No</label>
                </div>
            </div>
            <div class="row mt-3 ml-2 mr-2">
                <div class="col-8 pl-0 pr-0">
                    <small style="color: gray;">How much did you send?</small>
                </div>
                <div class="col-4 pl-0 pr-0">
                    <input type="hidden" id="artistId" name="artistId" >
                    <input type="text" id="amount" name="amount" placeholder="Enter amount" style="width: 100%;border-radius:8px">
                </div>
            </div>
            <div class="row mt-0 ml-2 mr-2 mb-3">
                <div class="invalid-feedback" id="amountError"></div>
            </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                    <input id="deleteSongButton" type="submit" id class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Done">
                </form>
            </div>
        </div>
    </div>
</div>