{{-- Add Notification Modal --}}
<div class="modal fade" id="editArtistModal">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content border-radius12">
            <div class="modal-header p-3 pb-2 " >
                <p class="mb-0" style="font-weight:200" >Edit Artist Detail</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editArtistForm">
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px "><b>First Name</b></span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter First Name" style="font-size: 13.5px" id="editArtistFirstName" name="editArtistFirstName" >
                            <div class="invalid-feedback" id="editArtistFirstNameError"></div>
                        </div>
                    </div>
                    @if ($artist)
                        <input type="hidden" id="artistId" name="artistId" value="{{$artist->id}}">
                    @endif
                    <div class="col-lg-6">
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px "><b>Last Name</b></span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Last Name" style="font-size: 13.5px" id="editArtistLastName" name="editArtistLastName" >
                            <div class="invalid-feedback" id="editArtistLastNameError"></div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="mb-2 mt-1 form-group input-group-sm input_field" >
                            <div class="d-flex flex-row">
                                <div id="editArtistAvatarDisplay" class ="roundedImage rounded-circle  mt-0 editSongModal" style="border-radius:23% !important;"></div>
                                <div class="d-flex align-items-end image-div">
                                    <label>
                                        <span class="dot d-flex justify-content-center">
                                            <span class="bi bi-pencil-square fa-md" id="pencil-iconq" style="color: white;font-size:11px"></span>
                                        </span>
                                        <input type="file" id="editArtistAvatar" name="editArtistAvatar" style="display: none;" accept="image/*">
                                    </label>
                                </div>
                            </div>
                            <div class="invalid-feedback" id="editArtistAvatarError"></div>
                            <small class="add-song-span-text mt-1">Max. Size 2MB ( Only Jpg, Png, Jpeg )</small>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" style="font-size: 12px "><b>Email</b></span></label>
                            <input type="email" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Email" style="font-size: 13.5px" id="editArtistEmail" name="email" readonly>
                            <div class="invalid-feedback" id="editArtistEmailError"></div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <label class="m-0 mt-1"><span class="text-dark add-song-label-text" style="font-size: 12px ">Password</span></label>
                        <div class="input-group mt-1 mb-3 input-group-sm round-7" id="show_hide_editPassword">
                            <input type="password" class="form-control input-sm round-7 password-radius" style="font-size: 13px" placeholder="Set Password" id="editArtistPassword" name="editArtistPassword">
                            <div class="input-group-append rounded" style="height: 29.5px">
                                <div class="input-group-text eye-icon pt-2" id="editArtistPassword-eye" style=""><a href="#">
                                    <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
                                </div>
                            </div>
                            <div class="invalid-feedback" id="editArtistPasswordError"></div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0  mb-1"><span class="text-dark add-song-label-text" style="font-size: 12px ">About Artist</span></label>
                            <textarea rows="3" cols="50" name="editArtistBio"  id="editArtistBio"   class="form-control add-song-input-text input-xs rounded border-radius8"  placeholder="Enter Bio.."></textarea>
                            <div class="invalid-feedback" id="editArtistBioError"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Close</button>
                <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>
