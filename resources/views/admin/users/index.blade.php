@extends('artist.layouts.master')
@section('title')
    App User
@endsection
@section('style')
    <style>
        .dataTables_length {
            float: right;
        }

        .page-item.active .page-link {
            z-index: 3;
            color: #fff;
            background-color: #e3342f;
            border-color: #e3342f;
        }

        hr {
            margin-right: -202px;
            margin-top: -10px;
            width: 197px;
        }
        #usersTable_paginate{
            margin-right: 0 !important
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/customnotification.css') }}">
@endsection
@section('content')
    <div class="row pt-0 pl-1">
        <p class="withdrawal-text ">App Users</p>
    </div>
    <div class="row pt-0 pl-1 pr-1">
        <div class="col-12">
            <div class="table-responsive">
                <table id="usersTable" class="display table-striped border border-left-0 border-bottom-0 border-right-0 w-100" >
                    <thead>
                        <tr >
                            <th class="p-2 " style="font-size: 12px; width:55px"># No. </th>
                            <th class="p-2 " style="font-size: 12px">User Name</th>
                            <th class="p-2 " style="font-size: 12px">Email</th>
                            <th class="p-2 " style="font-size: 12px">Phone</th>
                            <th class="p-2 " style="font-size: 12px">Social Login</th>
                            <th class="p-2 " style="font-size: 12px">Join Date</th>
                            <th class="p-2 " style="font-size: 12px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $index => $user)
                            <tr>
                                <td class="p-3">{{ ++ $index }}</td>
                                <td class="p-1">
                                    <div class="row no-gutters text-center">
                                        <div class="col-xl-3 col-md-6">
                                            <img class=" img-fluid img-circle img-size-32" src="{{$user->AvatarFullUrl}}" style="height:35px;">
                                        </div>
                                        <div class="col-xl-9 col-md-6 pl-1">
                                            <span>{{$user->firstName}} {{$user->lastName}}</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3"><p class="text-center email-overflow" data-toggle="tooltip" data-placement="top" title="{{$user->email}}">{{$user->email}}</p></td>
                                <td class="p-3">{{$user->phoneNumber}}</td>
                                <td class="p-3">
                                    @if ($user->fbId != null)
                                        Facebook
                                    @elseif ($user->gmailId != null)
                                        Google
                                    @elseif ($user->appleId != null)
                                        Apple
                                    @endif
                                </td>
                                <td>{{\Carbon\Carbon::parse($user->createdAt)->setTimezone(Session::get('timeZone'))->format('d F Y ')}}</td>
                                <td class="p-3 float-right text-center">
                                    @if($user->status=="Active")
                                         <a href="" class="del_ text-danger" data-id="{{$user->id}}" title="Block user!">
                                            <img src="{{asset('/svg/banned.svg')}}" class="mt-2">
                                        </a>

                                        <button data-id="{{$user->id}}" data-name="{{ucfirst($user->firstName)}}"
                                            class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10 viewSeller mt-1" title="View Details">
                                           <i class="fa fa-folder"></i>
                                            View
                                        </button>


                                    @else
                                        <a href="" class="unBlock_ text-danger" data-id="{{$user->id}}" title="UnBlock user!">
                                            <img src="{{asset('/svg/banned.svg')}}" class="mt-2">
                                        </a>

                                        <button data-id="{{$user->id}}" data-name="{{ucfirst($user->firstName)}}"
                                            class="text-white btn btn-danger btn-sm btn-flat viewSeller mt-1"
                                           title="View Details"><i class="fa fa-folder"></i> View</button>

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('admin.users.modals.userDeleteModalFrom')
    @include('admin.users.inc.aside')
@endsection

@section('script')
    <script>
        function thumbnailUrl(path) {
            return `<?php $path = Helper::urlPath(`+path +`);
            echo "$path"; ?>` + path;
        }


    </script>
    <script src="{{ asset('js/user/aside.js') }}"></script>
    <script>
        $(function() {
            let $modal = $('#myModalDelete');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // show the modal when delete button clicked
            $('#users-table').on('click', '.del_', function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                $modal.modal('show');
                $('#formClick').on('click', function() {
                    var base_url = "{{ url('admin/userBlock') }}";
                    var Url = base_url + '/' + id;
                    $.ajax({
                        type: 'GET',
                        url: Url,
                        success: function(response) {
                            $modal.modal('hide');
                            toastr.success(response.message);
                            window.location.reload();
                        },
                        error: function(data) {
                            toastr.error(data.message)
                        }
                    })
                });
            });
        });
    </script>
    <script>
        $(function() {
            let $modal = $('#myModalUnblock');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // show the modal when delete button clicked
            $('#users-table').on('click', '.unBlock_', function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                $modal.modal('show');
                $('#formClickUnblock').on('click', function() {
                    var base_url = "{{ url('admin/userBlock') }}";
                    var Url = base_url + '/' + id;
                    $.ajax({
                        type: 'GET',
                        url: Url,
                        success: function(response) {
                            $modal.modal('hide');
                            toastr.success(response.message);
                        },
                        error: function(data) {
                            toastr.error(data.message);
                            $modal.modal('show');

                        }
                    })
                });
            });
        });



        table = $("#usersTable").DataTable({
            dom: '<"top"fli>rt<"bottom"p><"clear">',
        });

    </script>
@endsection
