@extends('artist.layouts.master')

@section('title')
    Dashboard
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('artist/css/dashboard.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('css/customnotification.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/css/withdrawalEarning.css')}}">
    <style>
    a:focus{
        color:#e3342f;
    }
    .nav-pills .nav-link.active, .nav-pills .show > .nav-lin{
        font-weight: 600;
    }
</style>
@endsection
@section('content')
    <div class="row  pt-1" style="padding-bottom: 13px !important">
        <div class="col">
            <span class="withdrawal-text pl-0">User Detail</span>
        </div>
    </div>
    <div class="row  pt-1 pb-3">
        <div class="col-lg-3 col-md-12">
            <div class="card border-radius8">
                <div class="card-body border-radius8 pt-3 pl-4 pr-4 pb-3">
                    <div class="row d-flex justify-content-center mb-2">
                        @if ($user->avatar)
                            <div id="image-show" class ="roundedImage rounded-circle mt-2" style="background-image: url({{$user->avatarFullUrl}});"></div>
                        @else
                            <div id="image-show" class ="roundedImage rounded-circle mt-2" style="background-image: url({{url('artist/image/avatar/default-avatar.png')}});">    </div>
                        @endif
                    </div>
                    <input type="hidden" id="id" value="{{$user->id}}">

                    <div class="row d-flex justify-content-center">
                        <small class="text-center" style="font-weight:400;color:#212529">{{$user->name}}</small>
                    </div>
                    <div class="mb-2">
                        <p class="text-center light-grey-text">{{$user->email}}</p>
                    </div>
                    <div  class="card p-0 m-0 mt-3 border-radius8 off-white " style="width: 100%;background-color: #f2e3e3">
                        <div class="card-body m-2 m-0 p-0 border-radius8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex align-items-start">
                                        <small class="float-left ml-0 pt-1"  style="font-weight:500">Current Plan</small>
                                    </div>
                                    @if($subscription==null)
                                    No Plan Selected
                                    @else
                                        <div class="d-flex align-items-start">
                                            <small class="float-left ml-0 pt-1"  style="font-weight:800">{{$subscription->name}}</small>
                                        </div>
                                        <div  class="d-flex align-items-end">
                                            <p id="buyerAddress" class="pb-2  userAccountNumber">${{$subscription->price}}/{{$subscription->duration}}</p>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="dropdown-divider"></div>
                    <div class="pl-0 pr-0">
                        <div class="row no-gutters">
                            <div class="col-md-1">
                                <i class="bi bi-calendar4-event icon-blue"></i>
                            </div>
                            <div class="col-md-5 md-pl-2">
                                <small class="light-grey-text">Join Date</small>
                            </div>
                            <div class="col-md-6 text-center">
                                <small class="float-right font-weight500" style="font-size:10px;padding-top:8px">{{Carbon\Carbon::parse($user->createdAt)->format('d-m-Y')}}</small>
                            </div>
                        </div>
                    </div>

                    <div class="dropdown-divider"></div>
                    <div class="pl-0 pr-0">
                        <div class="row no-gutters">
                            <div class="col-md-1">
                                <i class="bi bi-tag icon-blue"></i>
                            </div>
                            <div class="col-md-5 md-pl-2">
                                <small class="light-grey-text">User ID</small>
                            </div>
                            <div class="col-md-6 text-center">
                                <small class="float-right font-weight500" style="font-weight:bold;font-size:10px;padding-top:6px">{{ $user->id  }}</small>
                            </div>
                        </div>
                    </div>

                    <div class="dropdown-divider"></div>
                    <div class="pl-0 pr-0">
                        <div class="row no-gutters">
                            <div class="col-md-1">
                                <i class="bi bi-phone icon-blue"></i>
                            </div>
                            <div class="col-md-5 md-pl-2">
                                <small class="light-grey-text">Phone No</small>
                            </div>
                            <div class="col-md-6 text-center">
                                <small class="float-right font-weight500 phone-number-overflow" style="font-weight:bold;font-size:10px;padding-top:6px" data-toggle="tooltip" data-placement="top" title="{{$user->phoneNumber}}">{{$user->phoneNumber}}</small>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="small-space col-12 pl-0 pr-0">
                        <label>Interest</label><br>
                        <small class=" light-grey-text">
                           @foreach($user->userInterests as $interest)
                               @foreach($interest->categroy as $cat)
                                    <span style="background-color: white;color: black; border: 1px solid;border-color: #E1E5F1" class="btn btn-outline-dark p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">{{$cat->name}}</span>
                                @endforeach
                            @endforeach
                        </small>&nbsp;&nbsp;

                    </div><br>
                    <div class="row">
                        <div class="col-12 text-center">
                            <div class="btn-group">
                                <a href="#" id="editProfile"  class="btn btn-danger rounded-lg text-white border-radius8 edit-profile" data-toggle="modal" data-target="#edit-Profile" >Edit Detail</a>&nbsp;&nbsp;
                                <a id="deleteAppUser" href="#" style="background-color: #FF6250;"  class="btn btn-danger rounded-lg text-white border-radius8 edit-profile">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-md-12 pl-lg-0 pr-lg-4" >
            <div class="card border-radius8">
                <div class="card-header pb-0" style="background-color: white; border-top-left-radius:8px !important;border-top-right-radius:8px !important;">
                    <div class="row pb-1">
                    </div>
                    <div class="row ml-0">
                        <ul class="nav nav-pills ml-auto " style="margin-left: -12px !important;margin-bottom: 0px !important;font-size: 11px;">
                            <li class="nav-item"><span class="nav-link active" href="#tab_12" data-toggle="tab" style="">Playlists</span></li>
                            <li class="nav-item"><span class="nav-link " href="#tab_1" data-toggle="tab" style="">Favourite Songs</span></li>
                            <li class="nav-item"><span class="nav-link" href="#tab_2" data-toggle="tab">Purchased Album</span></li>
                            <li class="nav-item"><span class="nav-link" href="#tab_3" data-toggle="tab">Purchased Songs</span></li>
                            <li class="nav-item"><span class="nav-link" href="#tab_4" data-toggle="tab">Following Artist</span></li>
                            <li class="nav-item"><span class="nav-link" href="#tab_5" data-toggle="tab">Downloaded</span></li>
                            <li class="nav-item"><span class="nav-link" href="#tab_6" data-toggle="tab">Buying History</span></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body pt-2 pl-0 pr-0">
                    <div class="tab-content">
                        <div class="tab-pane active show  table-responsive" id="tab_12">
                            @include('admin.users.partials.playlist')
                        </div>
                        <div class="tab-pane  table-responsive" id="tab_1">
                            @include('admin.users.partials.album')
                        </div>
                        <div class="tab-pane " id="tab_2">
                           @include('admin.users.partials.purchasedAlbum')
                        </div>
                        <div class="tab-pane table-responsive" id="tab_3">
                            @include('admin.users.partials.purchasedSong')

                        </div>
                        <div class="tab-pane mt-1 ml-1" id="tab_4">
                        @include('admin.users.partials.followingArtist')
                        </div>
                        <div class="tab-pane table-responsive" id="tab_5">
                     @include('admin.users.partials.downloadSong')
                        </div>
                        <div class="tab-pane table-responsive" id="tab_6">
                            @include('admin.users.partials.buyingHistroy')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>



    @include('admin.users.partials.editProfile')
    <div class="modal fade" id="myModalDeleteUser">
        <div class="modal-dialog modal-dialog-centered modal-md card-middle">
            <div class="modal-content border-radius12">
                <div class="modal-header pl-3 pt-3 pb-2" >
                    <p class="modal-title notification-detail-text"><b>Confirmation </b></p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col notification-detail-textt">
                            <small>Are You Sure You Want To Delete This User</small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between ">
                    <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                    <input id="userDelete" type="submit"  class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Confirm">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="{{asset('js/user/appUserProfile.js')}}"></script>
    <script src="{{asset('js/user/actionBtn.js')}}"></script>
    <script src="{{ asset('js/user/album.js')}}" ></script>
    <script src="{{ asset('js/user/playlist.js')}}" ></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
