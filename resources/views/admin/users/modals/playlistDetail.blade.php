{{-- Album detail Modal --}}
<div class="modal fade" id="playlistSongDetail">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div id="playlistThumbnail" class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" style="background-color: #BA011A;color:white">

            </div>
            <div class="modal-body p-0 ">
                <div class="row">
                    <div class="col-12 table-wrapper-scroll-y">
                        <table id="playSongListTable" class="table responsive ">
                            <tbody id="playSongListTableBody">
                            </tbody>
                        </table>
                        <div class="no-record d-none" id="no-record" ></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
