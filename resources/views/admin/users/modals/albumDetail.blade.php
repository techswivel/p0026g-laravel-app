{{-- Album detail Modal --}}
<div class="modal fade" id="albumDetail">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div id="albumThumbnail" class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" style="background-color: #BA011A;color:white">

            </div>
            <div class="modal-body p-0 ">
                <div class="row">
                    <div class="col-12 table-wrapper-scroll-y">
                        <table id="albumSongListTable" class="table responsive ">
                            <tbody id="albumSongListTableBody">
                            </tbody>
                        </table>
                        <div class="no-record d-none" id="no-record" style="display: none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

