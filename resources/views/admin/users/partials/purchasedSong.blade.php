{{-- Song Table --}}
<table id="songTable" class="table display " >
    <thead id="song-thead">
    <tr>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if($user->purchasedALbum->count()!=0)
    @foreach ($user->purchasedALbum as $album)
        @foreach ($album->getSongs as $song)
            <tr id="id{{$song->id}}" style="background-color: white !important;">
                <td  class="p-0 pl-2">
                    <ul class="products-list product-list-in-card pl-2 pr-1">
                        <li class="item">
                            <div id="imageDiv{{$song->id}}" class="product-img pt-1">
                                <img src="{!! Helper::urlPath($song->thumbnail) !!}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                            </div>
                            <div id="titleDiv{{$song['id']}}" class="product-info ml-5">
                                    <span class="dot-prime" style="margin-left: -30px"><i class="fas fa-crown"></i></span>
                                    <small  class="product-title font-szie10" style="margin-left: 5px !important">{{$song->name }}
                                    </small>
                                    <small class="isPaided product-description font-szie10 light-grey-text" style="margin-left: -5px !important">({{$song->songLength}})</small>
                            </div>
                        </li>
                    </ul>
                </td>
                <td id="isVideo{{$song['id']}}" class="pl-0 pr-0">
                    <span class="btn btn-outline-dark p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                    @if ($song->videoFile)
                        <span class="btn btn-outline-dark p-1 pl-2 pr-2 text-Audio text-video border-radius8 font-size12">Video</span>
                    @endif
                </td>

            </tr>
        @endforeach
    @endforeach
    @else
        <div style="margin-left: 40%">No record found</div>

    @endif
    </tbody>
</table>
