<div style="margin-left: 24px;margin-bottom: 10px" class="row">
    @if($playlists->count()!=0)
    @foreach($playlists as $playlist)
    <ul class="products-list product-list-in-card pl-2 pr-1"  style="border: 1px solid #DCDCDC;width: 314px;margin-left: 20px;margin-top: 8px;border-radius: 10px">
    <li class="item">
{{--        //image code--}}
        <div class="product-img pt-1" style="background: #838385;border-radius: 10px; height: 50px;width: 50px">
            <a href="javascript:void(0)" onclick="albumDetail123({{$playlist->id}})">
            <span class="border-radius8"><i class="fa fa-music" style="color: white;margin-top: 16px;margin-left: 20px"></i></span>
            </a>
        </div>

        <div class="product-info" style="padding: 4px">
            <small  id="buyerName" class="product-title font-szie10"><input type="hidden" name="title" id="title" value="{{$playlist->title}}">{{$playlist->title}}</small>
            <small id="buyerEmail" class="product-description font-szie10 light-grey-text">{{$playlist->playlistSongs->count()}} Songs</small>
            <small id="buyerEmail" style="margin-left: 150px;margin-top: -20px" class="product-description font-szie10 light-grey-text">
                <input type="hidden" name="id" id="fplaylistId" value="{{$playlist->id}}">
            </small>
            <div class="dropdown dropdown-block-aside" id="checkplaylist" style="float:right;margin-top: -11px;">
                <i class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('/svg/doticon.png')}}"></i>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item btn-block-aside" onClick="divFunction({{$playlist->id}},'{{$playlist->title}}')"  id="playlistEdit" href="#">Edit</a>
                    <a class="dropdown-item btn-block-aside" onclick="divFunction2({{$playlist->id}})"  id="playlistDelete" href="#">Delete</a>
                </div>
            </div>
        </div>
    </li>
</ul><br><br>
    @endforeach
    @else
        <div style="margin-left: 40%">No record found</div>

    @endif
        @include('admin.users.modals.playlistDetail')
        @include('admin.users.modals.deleteSongFromPlaylist')

</div>
@include('admin.users.modals.playlistModalFrom')
