
<table id="songTable" class="table display " >
    <thead id="song-thead">
    <tr>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if($user->userFavouritSongs->count()!=0)

    @foreach ($user->userFavouritSongs as $favourites)
<input type="hidden" id="favoruiteId" value="{{$favourites->id}}">
        <tr id="id{{$favourites->song->id}}" style="background-color: white !important;">
            <td  class="p-0 pl-2">
                <ul class="products-list product-list-in-card pl-2 pr-1">
                    <li class="item">
                        <div id="imageDiv{{$favourites->song->id}}" class="product-img pt-1">
                            <img src="{!! Helper::urlPath($favourites->song->thumbnail) !!}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                        </div>
                        <div id="titleDiv{{$favourites->song['id']}}" class="product-info ml-5">
                            @if ($favourites->song->isPaid)
                                <span class="dot-prime" style="margin-left: -30px"><i class="fas fa-crown"></i></span>
                                <small  class="product-title font-szie10" style="margin-left: 5px !important">{{$favourites->song->name }}
                                </small>
                                <small class="isPaided product-description font-szie10 light-grey-text" style="margin-left: -5px !important">({{$favourites->song->songLength}})</small>
                            @else
                                <small  class="product-title font-szie10" style="margin-left: -5px !important">{{$favourites->song->name }}
                                </small>
                                <small  class="isPaided product-description font-szie10 light-grey-text" style="margin-left: -5px !important">({{$favourites->song->songLength}})</small>
                            @endif

                        </div>
                    </li>
                </ul>
            </td>
            <input type="hidden" value="{{$favourites->song->id}}">
            <td id="isVideo{{$favourites->song->id}}" class="pl-0 pr-0">
                <span class=" p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                @if ($favourites->song->videoFile)
                    <span class="p-1 pl-2 pr-2 text-Audio text-video border-radius8 font-size12">Video</span>
                @endif
            </td>
            <td class="pb-2 " >
                <div class="float-right">
                    <a class="text-left  border-radius8 deleteUser" href="#" id="abc"   style="color:#BA011A">Remove From favourite</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
    @else
        <div style="margin-left: 40%">No record found</div>

    @endif
</table>
@include('admin.users.modals.favouriteSongDelete')
