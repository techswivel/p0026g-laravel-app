{{-- Edit Profile Modal --}}

<div class="modal fade" id="edit-Profile">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b>Edit App User Profile</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editProfileFormAppUser">
                @csrf
                <div class="modal-body">
                    <div class="row d-flex justify-content-center">
                 <input type="hidden" id="id" value="{{$user->id}}">
                        <div class="invalid-feedback" id="avatarError"></div>
                        <div class="col-12">
                            <div class="mb-2 mt-1 input-group-sm input_field " >
                                <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500;color:black">Name</span></label>
                                <input type="text" class="form-control input-sm border-radius8" value="{{ old('name')  ?? $user->name}}"  placeholder="Enter name" style="font-size: 12.5px" id="name" name="name" >
                                <div class="invalid-feedback" id="nameError"></div>
                            </div>
                            <div class="abc mb-4">
                                <div class=" mb-1  mt-3 input-group-sm input_field" >
                                    <div class="d-flex flex-row">
                                        @if ($user->avatar)
                                            <div id="image-show-detail" class ="roundedImage   rounded mt-2" style="background-image: url({{$user->avatarFullUrl}});"></div>
                                        @else
                                            <div id="image-show-detail" class ="roundedImage rounded mt-2" style="background-image: url({{url('artist/image/avatar/default-avatar.png')}});">    </div>
                                        @endif
                                        <div class="d-flex align-items-end image-div">
                                            <label><span class="dot d-flex justify-content-center"><b class="plus">+</b></span>
                                                <input type="file" id="avatar" name="avatar" style="display: none;">

                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <small class="add-song-span-text mt-1">Max. Size 2MB ( Only Jpg, Png, Jpeg )</small>
                            </div>
                            <div class="mb-1 mt-1 input-group-sm input_field " >
                                <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500">Gender</span></label>
                               <select name="gender" id="gender" class="form-control">
                                   <option value="{{$user->gender}}"selected> {{$user->gender}}</option>
                                   @if($user->gender=="Male")
                                       <option value="Female">Female</option>
                                   @else
                                       <option value="Male"> Male</option>
                                   @endif
                               </select>
                                <div class="invalid-feedback" id="genderError"></div>
                            </div>
                            <div class="mb-3 mt-2 input-group-sm input_field">
                                <label class="m-0  mb-1"> <span class="text-dark "  style="font-size: 13.5px;font-weight:500"><b>Date Of Birth</b></span></label>
                                <div class="input-group"  >
{{--                                    <!-- Sets the calendar icon -->--}}
{{--                                    <!-- Accepts the input from calendar -->--}}
                                    <input class="form-control" type="text"
                                           value="{{$user->formattedDate}}" placeholder="Enter birthDate"  id="birthDate" name="birthDate">
                                    <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar"
                                            style="float: right">
                                        </i>
                                    </span>
                                </span>
                                </div>

                               <div class="invalid-feedback" id="birthDateError"></div>
                            </div>
                            <div class="mb-3 mt-2 input-group-sm input_field " >
                                <label class="m-0  mb-1"> <span class="text-dark "  style="font-size: 13.5px;font-weight:500"><b>Address</b></span></label>
                                <input type="text" class="form-control input-sm border-radius8 add-bank-feild" value="{{$user->address}}" placeholder="Enter address" style="font-size: 12.5px" id="address" name="address" >

                                <div class="invalid-feedback" id="addressError"></div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom03">City</label>
                                    <input type="text" class="form-control" name="city" id="validationCustom03" value="{{$user->city}}" placeholder="Enter City" required>
                                    <div class="invalid-feedback" id="cityError"></div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom04">State</label>
                                    <input type="text" class="form-control" id="state" name="state" value="{{$user->state}}" placeholder="Enter State" required>
                                    <div class="invalid-feedback" id="stateError"></div>

                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom03">Country</label>
                                    <input type="text" class="form-control" id="validationCustom03" placeholder="Enter Country" name="country" value="{{$user->country}}" required>
                                    <div class="invalid-feedback" id="countryError"></div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom04">Zip Code</label>
                                    <input type="text" class="form-control" id="zipCode" name="zipCode" value="{{$user->zipCode}}" placeholder="Enter zipCode" required>
                                    <div class="invalid-feedback" id="zipCodeError"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                    <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button"  value="Update">
                </div>
            </form>
        </div>
    </div>
</div>

