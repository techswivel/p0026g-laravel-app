<style>
    .page-item.active .page-link {
    background-color: #ba011a;
    }
    div.dataTables_wrapper div.dataTables_length select{
        padding: 5px 15px;
    }
</style>
<div class="">
    <div class="card border-none" style="height: 328px">
        <div class="card-header whiteBackground border-0">
            <div class="d-flex justify-content-between">

            </div>
        </div>

        <div class=" table-responsive newBuy p-0 border-none">
            <table id="followerTable" class="table table-striped table-valign-middle" style="font-size: 12px">
                <thead>
                <tr>
                    <th>#No</th>
                    <th>User</th>

                </tr>
                </thead>
                <tbody>
                @foreach ($follower as $user)
                    <tr>
                        <td class="border-none p-2 ">000{{$user->id}}</td>
                        <td class="border-none p-2">
                            <div class="d-flex flex-row">
                                    <img src="{{ $user->avatarFullUrl}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                <div class="ml-2 padding-top5">{{$user->name}}</div>
                            </div>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

</div>
<div class="" style="float: right" >
    <span class="nav-link" href="#tab_4" data-toggle="tab"> {{$follower->links('pagination::bootstrap-4')}}</span>
</div>
