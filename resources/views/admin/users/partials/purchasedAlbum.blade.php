<div class="row m-0 mt-4" id="albumList">
    @if($user->purchasedALbum->count()!=0)
    @foreach ($user->purchasedALbum as $abc)
    @foreach ($abc->getAlbums as $album)
        <div class=" box ml-2" style="width: 88px" id="album{{$album->id}}">
            <center class=" float-left">
                @if ($album->albumStatus == 'Paid')
                    <a href="javascript:void(0)" onclick="albumDetail12({{$album->id}})"><img src="{!! Helper::urlPath($album->thumbnail) !!}"  class="border-radius5" width="80px" height="80px" alt="">
                        <small  class="name" style="font-size: 10px;color:black">{{$album->name}} </small></a>
            </center>
            <span class="dot-prime" style="margin-left:-22px"><i class="fas fa-crown"></i></span>
            @else
                <a href="javascript:void(0)" onclick="albumDetail12({{$album->id}})"><img src="{!! Helper::urlPath($album->thumbnail) !!}"  class="border-radius5" width="80px" height="80px" alt="">
                    <small  class="name" style="font-size: 10px;color:black">{{$album->name}} </small></a>
                </center>
            @endif
        </div>
    @endforeach
    @endforeach
    @else
        <div style="margin-left: 40%">No record found</div>
    @endif
</div>
@include('admin.users.modals.albumDetail')
