<div class="card-body pl-0 pt-2 pr-2">
    <div class="row" style="margin: 0px !important;">
        <div class="col-md-12 payout-history-text" style="width: 100% !important;">
        </div>
        <div class="col-md-12 pt-5  pl-0 pr-5" style="width: 100% !important;">
            @if($user->userBuyingHistories->count()!=0)

            <div class="timeline" style="padding-left: 20px;">
            @foreach ($user->userBuyingHistories as $buying)
                @foreach ($buying->getSongs as $song)
            <div class="dot-left all" id="" style="margin-bottom: 9px">
                        <span class="dot-earn"></span>
                        <small class="payout-time pr-5">{{Carbon\Carbon::parse($song->createdAt)->format('d/m/Y H:i A')}}</small>
                        <span style="margin-left: 70px">
                            <img src="{!! Helper::urlPath($song->thumbnail) !!}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                        </span>
                        <p style="margin-top: -39px;margin-left: 311px">{{$song->name}}<br>({{$song->songLength}})</p>
                        <small  class="payout-price text-warning timeline-right" style="margin-top: -38px;color: orange !important"> ${{$song->price}}</small>
            </div>
            @endforeach
               @endforeach
            </div>
            @else
                <div style="margin-left: 40%">No record found</div>
            @endif
        </div>
    </div>
</div>
