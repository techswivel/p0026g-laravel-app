@extends('artist.layouts.master')

@section('title')
    Music Category
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/customnotification.css')}}">
    <link rel="stylesheet" href="{{ asset('css/customcategory.css')}}">
@endsection

@section('content')
    <div class="row  pt-1 pb-3">
        <div class="col-lg-12">
            <span class="withdrawal-text">Music Category</span> <button type="button" data-toggle="modal" data-target="#addMusicCategory" class="btn ml-2 red-button btn-xs border-radius8 add-noti"><i class="bi bi-plus-lg"></i> Add New</button>
        </div>
    </div>
    <div class="row pt-1" >
        <div class="col-lg-3 col-md-12 ">
            <div class="card border-radius8 music-category-card">
                <div class="card-body border-radius8 pt-3 pl-4 pr-4 pb-3">
                    <div class="row">
                        <small class="pl-0 text-dark font-weight500" >Categories List</small>
                    </div>
                    <div class="row">
                        @foreach($categories as $category)
                        <div class="col-lg-6 col-md-4 pl-0">
                            <input type="hidden" value="{{$category->id}}" id="pageLoadId">
                            <a href="#" data-id="{{$category->id}}" onclick="listingCat({{$category->id}})" id="listingCat">
                                <div class="background" style="background-image:url({{$category->avatarFullUrl}});  ">
                                    <div class="text-center transbox">
                                        <p>{{$category->name}} </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
        @include('admin.category.modals.listingCategories')
    </div>


    {{-- Add Music Category Modal --}}
    @include('admin.category.modals.createCategory')
    {{-- Edit Music Category Modal --}}

    @include('admin.category.modals.deleteCategoryModal')
    @include('admin.category.modals.editCategoryModal')
    @include('admin.category.modals.NotDeleteCategory')

    @include('admin.users.modals.albumDetail')


    @include('artist.modals.player')
    @include('artist.modals.playerPlaylist')

@endsection

@section('script')
    <script>
        var songs = @json([])

        var songIds = @json([])

        var albums = @json([])
    </script>
    @include('scripts.artist.playerJs')
    <script src="{{asset('js/category/category.js')}}"></script>
    <script src="{{asset('js/category/listing.js')}}"></script>
    <script src="{{ asset('js/user/album.js')}}" ></script>
@endsection


