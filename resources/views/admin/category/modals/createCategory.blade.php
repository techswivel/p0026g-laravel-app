<div class="modal fade" id="addMusicCategory">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b>Add Category</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editProfileForm" class="needs-validation" enctype="multipart/form-data" novalidate>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-2 mt-1 input-group-sm input_field " >
                                <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500;color:black">Title</span></label>
                                <input type="text" class="form-control input-sm border-radius8" placeholder="Type category name" style="font-size: 12.5px" id="name" name="name" required>
                                <div class="invalid-feedback">Please fill out this field.</div>
                            </div>
                            <div class="mb-1 mt-1 input-group-sm input_field " >
                                <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500"><b>Image</b> <span class="add-song-span-text ">( Max 1MB )</span></span></label>
                               <input type="file" name="image" id="image" class="form-control input-sm border-radius8" placeholder="Choose Image (Max 1MB)" style="font-size: 12.5px">
                                <div class="invalid-feedback">Please fill out this field.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                    <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>
