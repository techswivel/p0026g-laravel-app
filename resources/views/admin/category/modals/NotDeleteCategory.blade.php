<div class="modal fade" id="NotdeleteMusicCategory">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Notic</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>First either delete the songs and albums</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button style="margin-left: 100px" type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" id="NotplaylistTrash" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
