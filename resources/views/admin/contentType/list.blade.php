<html>
<head>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
<nav  class="navbar navbar-expand navbar-white navbar-light text-white" style="border-bottom:1px solid">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a href="{{url('admin/dashboard')}}">
            <p class="" style="margin-left: 30px;color: #BA011A;font-size: 15px;margin-top: 1px !important;">Q the Music</p>
            </a>
        </li>

    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        &nbsp&nbsp&nbsp
        <div class="dropdown">

        </div>

    </ul>
</nav>

<div  class="justify-content-center container  " style="width:50%">
    <div style="padding-top: 18px;">
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(is_null($content))

            @else
                    <h2><b>{{$content->title}}</b> </h2>
            @endif
            @if(is_null($content))
            @else
                {!!$content->description  !!}
            @endif
        </div>


    </div>
</div>
</body>
</html>
