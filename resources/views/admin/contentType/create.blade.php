@extends('artist.layouts.master')
@section('title','Q the Music')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
@endsection
@section('content')
    <div class="" style="background:white;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        @if($contentType===\App\Enums\PageTypeEnum::TermAndCondition)
                            <h1>How To Term Condition</h1>
                        @elseif($contentType===\App\Enums\PageTypeEnum::PrivacyPolicy)
                            <h1>Privacy Policy</h1>
                        @endif
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- translations card -->
            <div class="container">
                <div class="content-centered">
                    <div class="col-12">
                        @if(is_null($content))
                            <form  method="post" action="{{ route('content-type.store') }}" id="notificationsForm" class="needs-validation">
                                @csrf
                                @else
                                    <form method="post" action="{{route('content-type.update',$content->id)}}">
                                        @csrf
                                        @method('PATCH')
                            @endif
                            <input type="hidden" name="contentType" value="{{$contentType}}">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12">
                                    <div class="form-group">
                                        <for name="title"></for>
                                        <label for="title">Title</label>
                                        @if(is_null($content))
                                            <input type="text" id="title" name="title"  class="input-sm form-control" placeholder="Write the Title" required>
                                        @else
                                            <input type="text" name="title" value="{{$content->title}}" class="form-control" placeholder="write the description about the apologetics" required>

                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12 col-sm-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        @if(is_null($content))

                                        <textarea class="summernote required" id="description" required="required" name="description"  cols="5" rows="6"></textarea>
                                        @else
                                            <textarea class="summernote required" id="description" required="required" name="description"  cols="5" rows="6">{{$content->description}}</textarea>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12 float-right">
                                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 float-right" style="width: 100px;" value="Send">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                @if($contentType===\App\Enums\PageTypeEnum::TermAndCondition)
                                        <a href="{{ url('admin/termsConditions') }}" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 float-right"  target="_blank" id="uploader" style="margin-right: 10px;background-color: #efefef;color: #ba011a;border-color: #ba011a">View
                                        </a>
                                    @elseif($contentType===\App\Enums\PageTypeEnum::PrivacyPolicy)
                                        <a href="{{ url('admin/privacyPolicy') }}" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey float-right" target="_blank" id="uploader" style="margin-right: 10px;background-color: #efefef;color: #ba011a;border-color: #ba011a">View
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                        </form>
                    </div>
                </div>
                </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
    <script src="{{ asset('js/page/page.js')}}" ></script>
@endsection
