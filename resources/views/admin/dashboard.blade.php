@extends('artist.layouts.master')

@section('title')
    Dashboard
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('admin/css/dashboard.css')}}">
@endsection
@section('content')
    <div class="row pt-0 pl-2">
        <p class="withdrawal-text ">Dashboard</p>
    </div>
    <div class="row pt-0 pl-0">
        <div class="col-lg-3 col-md-6 pl-1 pl-lg-2 pr-1">
            <div class="card border-radius8" style="background-color: white" >
                <div class="card-body border-radius-top p-0 " >
                    <div class="row m-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2" style="width: 100%">
                            <li class="item p-0 pt-2 pb-2">
                              <div class="product-img">
                                <img class="packageCount border-radius8" src="{{asset('/svg/sales.svg')}}" style="width: 55px !important;height:55px !important">
                              </div>
                              <div class="product-info pt-1 pl-2">
                                <small class="product-title" >
                                    Total Subscriptions Sales
                                </small>
                                <small class=" product-description" style="color: black;font-weight: 900;">
                                    ${{ Helper::totatSubscriptionsSales() }}  
                                </small>
                              </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 pl-1 pr-1">
            <div class="card border-radius8" style="background-color: white" >
                <div class="card-body border-radius-top p-0 " >
                    <div class="row m-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2" style="width: 100%">
                            <li class="item p-0 pt-2 pb-2">
                              <div class="product-img">
                                <img class="packageCount border-radius8" src="{{asset('/svg/sales.svg')}}" style="width: 55px !important;height:55px !important">
                              </div>
                              <div class="product-info pt-1 pl-2">
                                <small class="product-title" >
                                    Buying Sales
                                </small>
                                <small class=" product-description" style="color: black;font-weight: 900;">
                                    ${{ Helper::totatBuyingSales() }}  
                                </small>
                              </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 pl-1 pr-1">
            <div class="card border-radius8" style="background-color: white" >
                <div class="card-body border-radius-top p-0 " >
                    <div class="row m-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2" style="width: 100%">
                            <li class="item p-0 pt-2 pb-2">
                              <div class="product-img">
                                <img class="packageCount border-radius8" src="{{asset('/svg/user.svg')}}" style="width: 55px !important;height:55px !important">
                              </div>
                              <div class="product-info pt-1 pl-2">
                                <small class="product-title" >
                                    App Users
                                </small>
                                <small class=" product-description" style="color: black;font-weight: 900;">
                                    {{ Helper::countUser() }}  
                                </small>
                              </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 pl-1 pr-1 pr-lg-2">
            <div class="card border-radius8" style="background-color: white" >
                <div class="card-body border-radius-top p-0 " >
                    <div class="row m-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2" style="width: 100%">
                            <li class="item p-0 pt-2 pb-2">
                              <div class="product-img">
                                <img class="packageCount border-radius8" src="{{asset('/svg/user.svg')}}" style="width: 55px !important;height:55px !important">
                              </div>
                              <div class="product-info pt-1 pl-2">
                                <small class="product-title" >
                                    Total Artists
                                </small>
                                <small class=" product-description" style="color: black;font-weight: 900;">
                                    {{ Helper::countArtist() }}  
                                </small>
                              </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row pt-0 pl-0">
        <div class="col-lg-6 pl-1 pl-lg-2 pr-1">
           <div class="card">
                  <div class="card-header whiteBackground border-0 pl-2 pt-3">
                    <div class="d-flex justify-content-between">
                        <small class="float-left ml-2 pt-1"  style="font-weight:600">Subscriptions</small>
                         <select class="  month-select" id="saleReportChart" name="saleReportChart" onchange="changeStatusOfChart();"></select>
                    </div>
                  </div>
                  <div class="card-body pl-2 pr-2 pt-0 pb-3">
                    <div class="d-flex">
                      <p class="d-flex flex-column">
                      <small class="float-left ml-2 pt-1"  style="font-weight:800">
                         <span id="revenueColor" class="text-success" style="font-weight:500"><i id="revenueArrow"class="fas fa-arrow-up"></i><span id="revenueValue"> % </span><span class="text-muted" style="font-weight:400">Since last Year</span></span>
                    </small>
                      </p>
                    </div>
                    <!-- /.d-flex -->
    
                    <div class="position-relative mb-1">
                      <canvas id="sales-chart" height="170"></canvas>
                    </div>
    
                    <div class="d-flex flex-row justify-content-end">
                      <span class="mr-2">
                        <i class="fas fa-square chartRed"></i> <small class=" ml-0 pt-1"  style="font-weight:500" id="aboutChart">This Year</small>
                      </span>
                    </div>
                  </div>
                </div>
        </div>
        <div class="col-lg-6 pl-1 pr-1 pr-lg-2">
             <div class="card" style="height: 304px">
                  <div class="card-header whiteBackground border-0">
                   <div class="d-flex justify-content-between">
                        <small class="  pt-1"  style="font-weight:600">New Users</small>
                        <a href="{{ url('admin/users')}}"><small class="  pt-1"  style="font-weight:500">View All Users</small></a>
                   </div> 
                  </div>
                  <div class="card-body table-responsive newBuy p-0">
                    <table class="table table-striped table-valign-middle" style="font-size: 12px">
                      <thead>
                      <tr>
                        <th>#No</th>
                        <th>User</th>
                        <th>Join Date</th>
                        <th>Phone#</th>
                        <th></th>
                      </tr>
                      </thead>
                      <tbody>
                        @foreach ($users as $index => $user)
                          <tr>
                            <td class="border-none p-1 text-center">{{++$index}}</td>
                            <td class="border-none p-1">
                              <div class="d-flex flex-row">
                                @if ($user->avatar)
                                  <img src="{{Helper::urlPath($user->avatar)}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  
                                @else    
                                  <img src="{{ asset('artist/image/avatar/default-avatar.png')}}" id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">  
                                @endif
                                  <div class="ml-2 padding-top5">{{$user->firstName}} {{$user->lastName}}</div>
                              </div>
                            </td>
                            <td class="border-none p-1 ">{{Carbon\Carbon::parse($user->createdAt)->format('d/m/Y')}}</td>
                            <td class="border-none p-1 ">{{$user->phoneNumber}}</td>
                            <td class="border-none p-1 ">
                              {{-- <button data-id="{{$user->id}}" data-name="{{ucfirst($user->firstName)}}"
                                class="text-white btn btn-danger btn-sm btn-flat viewSeller"
                               title="View Details"><i class="fa fa-folder"></i> View</button> --}}
                              <button type="button" style="cursor:pointer;"  id="viewEarn"  data-id="{{ $user->id }}" data-name="{{ucfirst($user->firstName)}}" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10 viewSeller"><i class="bi bi-folder-fill "></i> View</button>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
          </div>
    </div>

    <aside id="dashboardSideBar" class="control-sidebar control-sidebar-light" style="width: 280px">
      <!-- Control sidebar content goes here -->
      <div class=" control-sidebar-content os-host os-theme-light os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition os-host-overflow os-host-overflow-y"
           style="height: 100%;margin-top: 50px">
          <div class="os-scrollbar-corner">
            <div class="row m-2">
              <small class="float-left ml-0 " style="font-weight:500;font-size: 14px;">User Detail</small>
          </div>
           <div class="seller-details" id="seller-details"></div>
            <div class="row" id="seller-documents" style="margin-bottom:50px;">
            </div>
            <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6">
          </div>
            </div>
          </div>
      </div>
  </aside>

  <div id="mySideClose" class="shadow" style="height: 40px;right:300px !important">
    <div class="row m-0 ">
      <div class="col " style="padding-left:7px">
           <a href="javascript:void(0)"  onclick="closeNav()"><i class="bi bi-x"></i></a>
      </div>
   </div>
  </div>
@endsection


@section('script')
    <script>
function thumbnailUrl(path) {
    return`<?php $path = Helper::urlPath(`+path +`);echo "$path";?>`+path;
    }
</script>
<script src="{{ asset('js/Chart.js')}}"></script>
<script src="{{ asset('admin/js/dashboard.js')}}" ></script>
<script src="{{ asset('admin/js/aside.js') }}"></script>
@endsection
