@component('mail::message')
# Welcome to Q the Music

Your artist account is created on Q the Music. Here are the credentials to login in artist dashboard.
<br>
Email: {{$email}}
<br>
Password: {{$password}}
@component('mail::button', ['url' => ''])
Visit the Website
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
