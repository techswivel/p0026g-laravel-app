## Todo

## Done
1) Remove .idea dir
2) Remove composer.lock file
3) Remove package-lock.json
4) Add above mention file name in Git ignore file
5) Buttons Plugin Installation for yajra datatable for php artisan command
   Ref link: https://yajrabox.com/docs/laravel-datatables/master/buttons-installation
6) Fortify Package use in user login and registration
7) Telescope URL tags
8) Telescope publish migration
9) Artisan commands panel
10) Enum Integration