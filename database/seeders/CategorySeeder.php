<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $category1 = Category::create(["name"=>"Hip Hop","imageUrl"=>"uploads/categories/hiphop.jpg"]);
        $this->uploadCategoryImages('hiphop.jpg');

        $category2 = Category::create(["name"=>"Rock","imageUrl"=>"uploads/categories/rock.jpg"]);
        $this->uploadCategoryImages('rock.jpg');

        $category3 = Category::create(["name"=>"Pop","imageUrl"=>"uploads/categories/pop.jpg"]);
        $this->uploadCategoryImages('pop.jpg');

        $category3 = Category::create(["name"=>"Jazz","imageUrl"=>"uploads/categories/jazz.jpg"]);
        $this->uploadCategoryImages('jazz.jpg');

        $category3 = Category::create(["name"=>"Classical","imageUrl"=>"uploads/categories/classic.jpg"]);
        $this->uploadCategoryImages('classic.jpg');
    }

    function uploadCategoryImages($img){
        $path=rtrim("uploads/categories", '/').'/'.$img;
      
        $imageGetPath = public_path($path);

        $imageSavePath = 'uploads/categories/'.$img;
        $image = Storage::disk('s3')->put($imageSavePath, fopen($imageGetPath, 'r+'));
    }

}
