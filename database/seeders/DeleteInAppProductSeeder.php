<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Google_Client;
use Google_Service_AndroidPublisher;
use Google_Service_AndroidPublisher_InAppProduct;
use Google_Service_AndroidPublisher_Price;
use Illuminate\Support\Facades\Log;

class DeleteInAppProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Google_Client();
        $client->setAuthConfig(config('inapppurchase.serviceAccountJsonFile.file'));
        $client->addScope('https://www.googleapis.com/auth/androidpublisher');

        $service = new Google_Service_AndroidPublisher($client);
        $allProducts = $service->inappproducts->listInappproducts(config('inapppurchase.packageName.name'));
        $endloop = 1;
        $i = 0 ;
        while(!$endloop == 0){
            foreach($allProducts as $product){ 
                $deleteProduct = $service->inappproducts->delete(
                    config('inapppurchase.packageName.name'),
                    $product['sku']
                ); 
            }
            $array = array();
            $array['token'] = $allProducts['tokenPagination']['nextPageToken'];
            if($array['token'] == null){
                $endloop = 0;
            }
            $allProducts = $service->inappproducts->listInappproducts(config('inapppurchase.packageName.name'),$array);
        }
    }
}
