<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserFirebaseDeleteSeeder::class);
        $this->call([
            LanguageSeeder::class,
            CategorySeeder::class,
            PrivacyAndPolicySeeder::class,
            TermsAndConditionSeeder::class,
            NotificationCountSeeder::class,
        ]);
        $this->call(RolesTableSeeder::class);
        $this->call(SuperAdminSeeder::class);
        $this->call(PackageSeeder::class);
        if (env('APP_ENV')=="local" || env('APP_ENV')=="staging" || env('APP_ENV')=="acceptance") {
            $this->call(DeleteInAppProductSeeder::class);
        }     

    }
}
