<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = ['English','Hmong','Laotian','Thai','Chinese','Others'];
        foreach ($languages as $language) {
            Language::create([
                'name' => $language,
            ]);
        }
    }
}
