<?php

namespace Database\Seeders;

use App\Models\Package;
use App\Traits\InAppPurchaseTrait;
use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    use InAppPurchaseTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $package_1 = Package::create([
            "name"=>"Premium Subs",
            "firebaseTopic"=>"premium_subs",
            "price"=> 120,
            "duration"=>"1",
            "sku"=>"premium_subs",
        ]);   
        $package_2 = Package::create([
            "name"=>"Preofessional Subscription",
            "firebaseTopic"=>"preofessional_subscription",
            "price"=> 15,
            "duration"=>"2",
            "sku"=>"preofessional_subscription",
        ]); 
        $package_3 = Package::create([
            "name"=>"Stander Plan Subscription",
            "firebaseTopic"=>"stander_plan_subscription",
            "price"=> 20,
            "duration"=>"3",
            "sku"=>"stander_plan_subscription",
        ]); 

    }
}
