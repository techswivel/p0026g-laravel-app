<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=User::create([
            'firstName' => 'TechSwivel',
            'lastName' => 'QA',
            'email' => 'qa@techswivel.com',
            'phoneNumber'=>'+923415353533',
            'password' => Hash::make('TechSwivel#001'),
            'status'=>'Active',
            'email_verified_at'=>1654584216
        ]);
        $user->assignRole('admin');
    }
}
