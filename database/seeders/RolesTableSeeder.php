<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::where('name', '=', 'admin')->first() === null) {
            Role::create(['name' => 'admin']);
        }
        if (Role::where('name', '=', 'artist')->first() === null) {
            Role::create(['name' => 'artist']);

        }
        if (Role::where('name', '=', 'user')->first() === null) {
            Role::create(['name' => 'user']);
        }

    }
}
