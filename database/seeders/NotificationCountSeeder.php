<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MonthlyNotificationCount;

class NotificationCountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MonthlyNotificationCount::create([
            'totalNotificationCount' => 10,
        ]);
    
    }
}
