<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Page;
use App\Enums\PageTypeEnum;


class TermsAndConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $title = "Terms And Conditions";
        $description = "You have choices about how your personal information is accessed, collected, shared, and stored by Rabboni Books, which are discussed below. You will make choices about our use and processsing of your information when you first engage CPB and when you enage certain CPB functionality and may also make certain choices in the settings menu of your CPB Member account.";
        
        Page::create([
            'title' => $title,
            'description' => $description,
            'type' => PageTypeEnum::TermAndCondition,
        ]);
        
    }
}
