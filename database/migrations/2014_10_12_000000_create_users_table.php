<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstName',150);
            $table->string('lastName',150);
            $table->string('email',150)->unique();
            $table->string('gender', 150)->nullable();
            $table->string('phoneNumber',100)->unique()->nullable();
            $table->unsignedInteger('birthDate')->nullable();
            $table->string('fbId',250)->unique()->nullable();
            $table->string('gmailId',250)->unique()->nullable();
            $table->string('appleId',250)->unique()->nullable();
            $table->string('password',255);
            $table->string('avatar', 255)->nullable();
            $table->string('uid', 100)->unique()->nullable();
            $table->string('paypalCustomerId',250)->unique()->nullable();
            $table->enum('status', ['Active', 'Block'])->default('Active');
            $table->text('aboutArtist')->nullable();
            $table->string('address', 255)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('state', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('zipCode', 100)->nullable();
            $table->boolean('isNotificationEnabled')->nullable();
            $table->boolean('isArtistUpdateEnabled')->nullable();
            $table->unsignedInteger('email_verified_at')->nullable();
            $table->rememberToken();
            $table->text('jwtToken')->nullable();
            $table->string('jwtExpiredTime', 150)->nullable();
            $table->unsignedInteger('createdAt')->nullable();
            $table->unsignedInteger('updatedAt')->nullable();
            $table->unsignedInteger('deletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
