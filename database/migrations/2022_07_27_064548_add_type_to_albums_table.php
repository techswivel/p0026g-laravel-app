<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->enum('type',['Album','PreOrder'])->change();
            $table->enum('createdType',['Album','PreOrder'])->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->enum('type',['Song','PreOrder','PreOrderAlbum'])->change();
            $table->enum('createdType',['Song','PreOrder','PreOrderAlbum'])->change();
        });
    }
}
