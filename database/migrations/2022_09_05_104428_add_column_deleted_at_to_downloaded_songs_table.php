<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDeletedAtToDownloadedSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('downloaded_songs', function (Blueprint $table) {
            $table->unsignedInteger('deletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('downloaded_songs', function (Blueprint $table) {
            Schema::table('downloaded_songs', function (Blueprint $table) {
                $table->dropColumn('deletedAt');
            });
        });
    }
}
