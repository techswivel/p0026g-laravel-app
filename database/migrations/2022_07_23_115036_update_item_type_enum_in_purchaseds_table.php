<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateItemTypeEnumInPurchasedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchaseds', function (Blueprint $table) {
            $table->enum('itemType',['SONG','ALBUM','PLAN_SUBSCRIPTION','PRE_ORDER_SONG','PRE_ORDER_ALBUM'])->change(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchaseds', function (Blueprint $table) {
            $table->enum('itemType',['SONG','ALBUM','PLAN_SUBSCRIPTION'])->change(); 
        });
    }
}
