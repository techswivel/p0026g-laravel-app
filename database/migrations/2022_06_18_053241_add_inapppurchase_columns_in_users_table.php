<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInapppurchaseColumnsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // in app purchase columns
            $table->string('gplay_order_token')->nullable();
            $table->string('gplay_order_id')->nullable();
            $table->string('apple_order_id')->nullable();
            $table->dateTime('datetime_subscribed')->nullable();    
            $table->dateTime('lastpayment_datetime')->nullable();
            // in app purchase columns
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['gplay_order_token','gplay_order_id','apple_order_id','datetime_subscribed','lastpayment_datetime']);
        });
    }
}
