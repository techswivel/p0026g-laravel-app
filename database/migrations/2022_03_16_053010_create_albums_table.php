<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('albums', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('userId');
            $table->foreign('userId')->references('id')->on('users');
            $table->string('name',250);
            $table->string('thumbnail')->nullable();
            $table->integer('preOrderCount')->nullable();
            $table->integer('totalPreOrderCount')->nullable();
            $table->double('preOrderReleaseDate')->nullable();
            $table->enum('type',['Song','PreOrder','PreOrderAlbum']);
            $table->enum('createdType',['Song','PreOrder','PreOrderAlbum']);
            $table->enum('albumStatus',['Free','Partially','Paid']);
            $table->string('sku',200)->nullable();
            $table->string('iAppPurchaseIdWd',200)->nullable();
            $table->string('iAppPurchaseIdWod',200)->nullable();
            $table->string('iAppPurchasePrice',200)->nullable();
            $table->string('discountedPrice',200)->nullable();
            $table->enum('status',['PENDING','COMPLETED'])->nullable();
            $table->unsignedInteger('createdBy')->nullable();
            $table->unsignedInteger('deletedBy')->nullable();
            $table->unsignedInteger('createdAt')->nullable();
            $table->unsignedInteger('updatedAt')->nullable();
            $table->unsignedInteger('deletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('albums');
    }
}
