<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('userId');
            $table->foreign('userId')->references('id')->on('users');
            $table->string('name',250);
            $table->boolean('isPaid');
            $table->decimal('price',12,2)->default(0);
            $table->decimal('discountedPrice',12,2)->default(0);
            $table->unsignedBigInteger('categoryId');
            $table->foreign('categoryId')->references('id')->on('categories');
            $table->unsignedBigInteger('languageId');
            $table->foreign('languageId')->references('id')->on('languages');
            $table->string('thumbnail')->nullable();
            $table->string('audioFile')->nullable();
            $table->string('songLength')->nullable();
            $table->string('videoFile')->nullable();
            $table->integer('preOrderCount')->nullable();
            $table->integer('totalPreOrderCount')->nullable();
            $table->text('lyrics')->nullable();
            $table->string('demoAudioFile')->nullable();
            $table->string('demoVideoFile')->nullable();
            $table->double('preOrderReleaseDate')->nullable();
            $table->enum('type',['Song','PreOrder','PreOrderAlbum']);
            $table->enum('createdType',['Song','PreOrder','PreOrderAlbum']);
            $table->string('sku',200)->nullable();
            $table->string('iAppPurchaseIdWd',200)->nullable();
            $table->string('iAppPurchaseIdWod',200)->nullable();
            $table->enum('status',['PENDING','COMPLETED'])->nullable();
            $table->unsignedInteger('createdBy')->nullable();
            $table->unsignedInteger('deletedBy')->nullable();
            $table->unsignedInteger('createdAt')->nullable();
            $table->unsignedInteger('updatedAt')->nullable();
            $table->unsignedInteger('deletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
