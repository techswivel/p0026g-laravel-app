<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('name',200);
            $table->string('firebaseTopic',200);
            $table->decimal('price',12,2)->default(0);
            $table->string('priceId',255)->nullable()->comment('Apple AppStoreConnect Subscription Price Id');
            $table->string('duration',200);
            $table->string('sku',200)->nullable();
            $table->string('subscriptionId',200)->nullable()->comment('Apple AppStoreConnect Subscription Id');
            $table->enum('status',['In-Process','Failed'])->nullable();
            $table->unsignedInteger('createdAt')->nullable();
            $table->unsignedInteger('updatedAt')->nullable();
            $table->unsignedInteger('deletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
