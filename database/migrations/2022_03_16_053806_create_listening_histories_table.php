<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListeningHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listening_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('userId');
            $table->foreign('userId')->references('id')->on('users');
            $table->unsignedBigInteger('songId')->nullable();
            $table->foreign('songId')->references('id')->on('songs');
            $table->unsignedBigInteger('albumId')->nullable();
            $table->foreign('albumId')->references('id')->on('albums');
            $table->unsignedBigInteger('artistId')->nullable();
            $table->foreign('artistId')->references('id')->on('users');
            $table->boolean('isRecentlyPlayed')->default(0)->nullable();
            $table->boolean('isListeningHistory')->default(0)->nullable();
            $table->enum('type',['Song','Album','Artist']);
            $table->unsignedInteger('createdAt')->nullable();
            $table->unsignedInteger('updatedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listening_histories');
    }
}
