<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchaseds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('userId')->nullable();
            $table->foreign('userId')->references('id')->on('users');
            $table->unsignedBigInteger('songId')->nullable();
            $table->foreign('songId')->references('id')->on('songs');
            $table->unsignedBigInteger('albumId')->nullable();
            $table->foreign('albumId')->references('id')->on('albums');
            $table->unsignedBigInteger('packageId')->nullable();
            $table->foreign('packageId')->references('id')->on('packages');
            $table->unsignedBigInteger('cardId')->nullable();
            $table->foreign('cardId')->references('id')->on('cards');
            $table->enum('purchaseType',['GOOGLE_PAY','APPLE_PAY','PAYPAL','CARD']);  //Paypal and Card
            $table->enum('itemType',['SONG','ALBUM','PLAN_SUBSCRIPTION']);
            $table->decimal('amount',12,2)->default(0);
            $table->longText('transactionId');
            $table->decimal('paidAmount',12,2)->default(0);
            $table->boolean('isSong'); //instead of type we use bool 0 for song, 1 for album
            $table->string('purchaseStatus')->nullable();
            $table->string('isAcknowledged')->nullable();
            $table->string('subType')->nullable();
            $table->longText('response')->nullable();
            $table->unsignedInteger('createdAt')->nullable();
            $table->unsignedInteger('updatedAt')->nullable();
            $table->unsignedInteger('deletedAt')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchaseds');
    }
}
