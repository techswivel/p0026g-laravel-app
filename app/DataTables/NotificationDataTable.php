<?php

namespace App\DataTables;

use App\Enums\NotificationTypeEnum;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class NotificationDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                if($row){
                    $btn = '<a class="btn text-left btn-xs red-button border-radius8 small-button" style="margin-right: 4px" onclick="notificationShowDetail('.$row->id.')" title="View notification"><i class="bi bi-eye-fill"></i></a>&nbsp;&nbsp;
                    <a class="btn text-left btn-xs orange-button border-radius8 small-button" onclick="notificationTopicDelete('.$row->id.')" title="Delete notification"><span class="bi bi-trash3"  id="trash-iconq"></span></a>';
                    return $btn;
                }
                return '';
            })
            ->addColumn('date', function ($row) {
                if($row){
                    $dateFormat= Carbon::parse($row->createdAt)->setTimezone(Session::get('timeZone'))->format('d/m/Y');
                    return $dateFormat;
                }
                return '';
            })
            ->addColumn('title',function ($row){
                return Str::words($row->title,2);
            })
            ->addColumn('message',function ($row){
                return Str::words($row->message,9);
            })

            ->rawColumns(['action','date','title','message']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Notification $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Notification $model)
    {
        $notification=Notification::where('notificationType',NotificationTypeEnum::Admin);
        return $this->applyScopes($notification);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('notification-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
            ->lengthMenu([10, 50, 100, 250, 500, 1000, "All"])
            ->dom(  '<"top"fli>rt<"bottom"p><"clear">')
            ->parameters([
                'responsive'=>
                    [
                        'details'=> [
                            'display'=> '$.fn.dataTable.Responsive.display.childRowImmediate',
                            'type'=> 'none',
                            'target'=> ''
                        ]
                    ],
                'autoWidth' => false,
                'searching'=>true,
                'display'=>true,
                "processing"=> '<i class="fa fa-spinner fa-spin" style="font-size:24px;color:rgb(75, 183, 245);"></i>'
            ])
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('date')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('title'),
            Column::make('message'),
            Column::make('action'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Notification_' . date('YmdHis');
    }
}
