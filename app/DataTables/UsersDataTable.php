<?php

namespace App\DataTables;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('user_name',function ($row){
                $img='<img class=" img-fluid img-circle img-size-32" src="' .$row->AvatarFullUrl . '" style="height:35px;">&nbsp;';
                $name = $img.' '.ucfirst($row->firstName) .' '. ucfirst($row->lastName);
                //code comment to one row record shoow
                return $name;
            })
            ->addColumn('social_login', function ($row) {
                    if ($row->fbId){
                        $btn = ' <span class="">Facebook</span>&nbsp;&nbsp;';
                        return $btn;
                    }elseif ($row->twitterId){
                        $btn = ' <span class="">Twitter</span>&nbsp;&nbsp;';
                        return $btn;
                    }elseif ($row->appleId){
                        $btn = ' <span class="">Apple</span>&nbsp;&nbsp;';
                        return $btn;
                    }else{
                        return '';
                    }

            })->editColumn('createdAt',function ($row){
                $dateFormat= Carbon::parse($row->createdAt)->setTimezone(Session::get('timeZone'))->format('d F Y ');
                return $dateFormat;
            })
            ->addColumn('action', function ($row) {
                $btn = '';
                $img=asset('/svg/banned.svg');
                if($row->status=="Active"){
                    $btn .='<a href="" class="del_ text-danger" data-id="'.$row->id.'" title="Block user!"><img src="'.$img.'"></a>&nbsp;&nbsp';
                    $btn .= '
                        <button data-id="' . $row->id  . '" data-name="' . ucfirst($row->firstName)  . '"
                        class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10 viewSeller"
                       title="View Details"><i class="fa fa-folder"></i> View</button>
                        ';
                    return $btn;
                }else{
                    $btn .='<a href="" class="unBlock_ text-danger" data-id="'.$row->id.'" title="UnBlock user!"><img src="'.$img.'"></a>&nbsp;&nbsp';
                    $btn .= '
                        <button data-id="' . $row->id  . '" data-name="' . ucfirst($row->firstName)  . '"
                        class="text-white btn btn-danger btn-sm btn-flat viewSeller"
                       title="View Details"><i class="fa fa-folder"></i> View</button>
                        ';
                    return $btn;
                }
                return '';

            })->filterColumn('user_name', function($query, $keyword) {
                    $sql = "CONCAT(users.firstName,'-',users.lastName)  like ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->rawColumns(['user_name','email','Phone','social_login','join_date','action']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $users = $model->whereDoesntHave('roles', function ($q) {
            $q->whereIn('name', ['admin', 'artist']);
        })->orderBy('id','desc');

        return $users->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('users-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->lengthMenu([10, 50, 100, 250, 500, 1000, "All"])
                    ->dom('<"top"fli>rt<"bottom"p><"clear">')
                    ->parameters([
                        'responsive'=>
                            [
                                'details'=> [
                                    'display'=> '$.fn.dataTable.Responsive.display.childRowImmediate',
                                    'type'=> 'none',
                                    'target'=> ''
                                ]
                            ],
                        'autoWidth' => false,
                        'ordering' => false,
                        'display'=> true,
                        "processing"=> '<i class="fa fa-spinner fa-spin" style="font-size:24px;color:rgb(75, 183, 245);"></i>'
                    ])
                    ->orderBy(1)
                    ->serverSide(true)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('id')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('user_name'),
            Column::make('email'),
            Column::make('phoneNumber')->title('Phone'),
            Column::make('social_login'),
            Column::make('createdAt')->title('Join Date'),
            Column::make('action'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
