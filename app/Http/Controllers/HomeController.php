<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Purchased;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function artistLogin(){
        return view('artist.auth.login');

    }
    public function forgetPassword(){
        return view('auth.passwords.email');
    }

    public function getUsers()
    {
        return view('datatable');
    }
   public function loginPage(){
        return view('auth.login');
   }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        return Datatables::of(User::select('id','name','email','createdAt','updatedAt')->get())->make(true);
    }


}
