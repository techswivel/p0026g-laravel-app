<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Requests\Artist\Album\AlbumAddRequest;
use App\Http\Requests\Artist\Album\AlbumSelectRequest;
use App\Http\Requests\Artist\Album\AlbumEditRequest;
use Illuminate\Support\Facades\DB;
use App\Repositories\Eloquent\AlbumRepository;

class AlbumController extends Controller
{
    use loggerExceptionTrait;
    protected $albumRepository;

    public function __construct(AlbumRepository $albumRepository)
    {
        $this->albumRepository = $albumRepository;
    }

    public function store(AlbumAddRequest $request)
    {
        try {
            DB::beginTransaction();
            $album = $this->albumRepository->store($request);
            DB::commit();
            // After the changing retrun the Album to
            return response()->json([
                'status' => 200,
                'album' => $album,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $msg = $exception->getMessage();
            $this->saveExceptionLog($exception, 'Save Album error exception'.$msg);

            return response()->json([
                'status' => 500,
                'message' => 'Fail to stored Album! '.$msg,
            ]);
        }
    }

    public function selectSong(AlbumSelectRequest $request)
    {
        try {
            DB::beginTransaction();
            // select song from list
            $songs = $this->albumRepository->selectSong($request);
            DB::commit();
            return response()->json([
                'status' => 200,
                'songs' => $songs,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Select song error exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to select Song '.$exception->getMessage(),
            ]);
        }
    }

    public function albumDetail($id)
    {
        try {
            DB::beginTransaction();
            $value = $this->albumRepository->albumDetail($id);
            DB::commit();
            return response()->json([
                'status'=> 200,
                'album'=>$value[0],
                'songs'=>$value[1],
                'count' =>$value[2]
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $msg=$exception->getMessage();
            $errorMsg=json_decode($msg);
            $this->saveExceptionLog($exception, 'Album detail error exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show Album Detail '.$errorMsg->error->message,
            ]);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $this->albumRepository->delete($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message'=> 'Album is delete successfully',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $msg=$exception->getMessage();
            $this->saveExceptionLog($exception, 'Album delete error exception'.$msg);
            return response()->json([
                'status' =>  500,
                'message'=> 'Fail to delete Album '.$msg,
            ]);
        }
    }

    public function listTheSong($id)
    {
        try {
            DB::beginTransaction();
            $selectsongs = $this->albumRepository->listTheSong($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'selectsongs' => $selectsongs,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Listing album song error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to list Song Album '.$exception->getMessage(),
            ]);
        }

    }

    public function removeSong($id)
    {
        try {
            DB::beginTransaction();
            // remove the song from the  album
            $album = $this->albumRepository->removeSong($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'album'=> $album,
                'message'=> 'Song is deleted from Album ',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $msg=$exception->getMessage();
            $errorMsg=json_decode($msg);
            $this->saveExceptionLog($exception, 'Remove album song error exception'.$errorMsg->error->message);
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to delete Album '.$errorMsg->error->message,
            ]);
        }
    }


    public function show($id)
    {
        try {
            DB::beginTransaction();
            // show the album and all the songin the album
            $value = $this->albumRepository->show($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'album'=>$value[0],
                'songs'=> $value[1]
            ]);
        } catch (\Exception $exception) {
             DB::rollBack();
            $this->saveExceptionLog($exception, 'Show album the song error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to show Album '.$exception->getMessage(),
            ]);
        }

    }

    public function update(AlbumEditRequest $request)
    {
        try {
            DB::beginTransaction();
            $updateAlbum = $this->albumRepository->update($request);
            DB::commit();
            return response()->json([
                'status' => 200,
                'updateAlbum' => $updateAlbum]);
        } catch (\Exception $exception) {
             DB::rollBack();
            $msg=$exception->getMessage();
            $errorMsg=json_decode($msg);
            $this->saveExceptionLog($exception, 'Album update error '.$errorMsg->error->message);
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to update Album '.$errorMsg->error->message,
            ]);
        }

    }

    public function AlbumStatus($id)
    {
        try {
            DB::beginTransaction();
            $album = $this->albumRepository->AlbumStatus($id);
            DB::commit();
            return response()->json([
                'status'=> 200,
                'album'=>$album]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Album the status error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to update Album '.$exception->getMessage(),
            ]);
        }
    }
}
