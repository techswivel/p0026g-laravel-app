<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Artist\Song\SongAddRequest;
use App\Http\Requests\Artist\Song\SongEditRequest;
use App\Repositories\Eloquent\SongRepository;

class SongController extends Controller
{
    use loggerExceptionTrait;
    protected $songRepository;

    public function __construct(SongRepository $songRepository)
    {
        $this->songRepository = $songRepository;
    }

    public function store(SongAddRequest $request)
    {
        try{

            DB::beginTransaction();
            $song = $this->songRepository->store($request);
            DB::commit();
            return response()->json($song);
        } catch (\Exception $exception) {
            DB::rollBack();
            $msg=$exception->getMessage();
            $this->saveExceptionLog($exception, 'Song store error  exception'.$msg);
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to stored Song '.$msg,
            ]);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $this->songRepository->delete($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message'=> 'Song is delete successfully',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $msg=$exception->getMessage();
            $errorMsg=json_decode($msg);
            $this->saveExceptionLog($exception, 'Delete song error  exception'.$errorMsg->error->message);
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to delete Song '.$errorMsg->error->message,
            ]);
        }
    }

    public function show($id)
    {
        $song = $this->songRepository->show($id);
        return response()->json($song);
    }


    public function update(SongEditRequest $request)
    {
        try {
            DB::beginTransaction();
            $song = $this->songRepository->update($request);
            DB::commit();
            return response()->json($song);
        } catch (\Exception $exception) {
            DB::rollBack();
            $msg= $exception->getMessage();
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to Update the Song '.$msg,
            ]);
        }
    }

}
