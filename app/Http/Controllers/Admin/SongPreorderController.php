<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Requests\Artist\Preorder\Song\PreorderSongAddRequest;
use App\Http\Requests\Artist\Preorder\Song\PreorderSongEditRequest;
use Illuminate\Support\Facades\DB;
use App\Repositories\Eloquent\SongPreorderRepository;
use App\Repositories\Eloquent\SongRepository;

class SongPreorderController extends Controller
{
    use loggerExceptionTrait;
    protected $songPreorderRepository,$songRepository;

    public function __construct(SongPreorderRepository $songPreorderRepository,SongRepository $songRepository)
    {
        $this->songPreorderRepository = $songPreorderRepository;
        $this->songRepository = $songRepository;
    }

    public function store(PreorderSongAddRequest $request)
    {
        try{
            DB::beginTransaction();
            $song = $this->songPreorderRepository->store($request);
            DB::commit();
            return response()->json($song);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'SongPreOrder store error  exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to stored Song '.$exception->getMessage(),
            ]);
        }
    }

    public function update(PreorderSongEditRequest $request)
    {
        try {
            DB::beginTransaction();
            $song = $this->songPreorderRepository->update($request);
            DB::commit();
            return response()->json($song);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'SongPreOrder update error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to Update the Song '.$exception->getMessage(),
            ]);
        }
    }

    public function preorderSongPurchaseList($id)
    {
        try {
            $value = $this->songPreorderRepository->preorderSongPurchaseList($id);
            return response()->json([
                'status' =>200,
                'song' => $value[0],
                'purchase' =>$value[1]
            ]);
        } catch (\Throwable $th) {
            $this->saveExceptionLog($th, 'SongPreOrder purchase list error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to Show the List '.$th->getMessage(),
            ]);
        }
    }

    public function carbonDate(Request $request,$id)
    {
        try {
            $value = $this->songPreorderRepository->carbonDate($request,$id);
            return response()->json([
                'status' =>200,
               'preorderSongsCountDown' =>$value[0],
                'preorderAlbumsCountDown' => $value[1],
            ]);
        } catch (\Throwable $th) {
            $this->saveExceptionLog($th, 'Carbon date  error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to show the preorder time '.$th->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        $song = $this->songRepository->show($id);
        return response()->json($song);
    }


    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $this->songRepository->delete($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message'=> 'Song is delete successfully',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Delete preOrder song error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to delete Song '.$exception->getMessage(),
            ]);
        }
    }
}
