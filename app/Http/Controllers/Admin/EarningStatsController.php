<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Repositories\Eloquent\EarningStatsRepository;
use Illuminate\Support\Facades\DB;

class EarningStatsController extends Controller
{
    use loggerExceptionTrait;
    protected $earningStatsRepository;

    public function __construct(EarningStatsRepository $earningStatsRepository)
    {
        $this->earningStatsRepository = $earningStatsRepository;
    }

    public function purchaseItemDetail($id)
    {
        try {
            DB::beginTransaction();
            $value = $this->earningStatsRepository->purchaseItemDetail($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'purchase' => $value[0],
                'albumSongs' => $value[1],
                'price' => $value[2],
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Purchase Item detail error  exception');
            return response()->json([
                'status' => 500,
                'message' => 'Fail to show purchase detail! '.$exception->getMessage(),
            ]);
        }
    }
}
