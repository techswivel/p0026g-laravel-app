<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\UpdatePasswordRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin.dashboard');
    }
    public function changePassword(){
        return view('auth.passwords.change-password');
    }
    public function updatePassword(UpdatePasswordRequest $request)
    {

        if (Hash::check($request->oldPassword, auth()->user()->password)) {
            User::find(auth()->user()->id)->update(['password'=> Hash::make($request->password)]);
            toastr()->success('Password changed successfully!');
            return redirect('admin/dashboard');
        }else{
            toastr()->error('Old password not matched!');
            return back();
        }




    }

}
