<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\FellowerArtistDataTable;
use App\DataTables\UsersDataTable;
use App\Enums\UserStatusEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\UserProfileRequest;
use App\Models\Album;
use App\Models\Favorite;
use App\Models\Follower;
use App\Models\Package;
use App\Models\Playlist;
use App\Models\PlaylistSongs;
use App\Models\Purchased;
use App\Models\Song;
use App\Models\User;
use App\Traits\FileUploadTrait;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    use FileUploadTrait,loggerExceptionTrait;

    protected $imageDirectory = "uploads/users";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(UsersDataTable $dataTable)
    {

        $users = User::whereDoesntHave('roles', function ($q) {
                    $q->whereIn('name', ['admin', 'artist']);
                 })->orderBy('id','desc')->get();

        return view('admin.users.index',get_defined_vars());
    }

    public  function update(UserProfileRequest $request,$id){
        try {
            DB::beginTransaction();
            $user = User::find($id);
            $name = explode(" ", $request['name']);
            $firstName = $name[0];
            $secondName = ' ';
            if(str_replace($firstName,"",$request['name'])){
                $secondName = ltrim(str_replace($firstName,"",$request['name']));
            }
            $avatar = NULL;
            if ($request->has('avatar')) {
                $this->deleteFile($user->avatar);
                $avatar = $this->uploadFile($request->avatar, $this->imageDirectory);
            }else{
                $avatar = $user->avatar;
            }
            $date = $request['birthDate'];
            $timestamp = strtotime($date);
            if ($timestamp === FALSE) {
                $timestamp = strtotime(str_replace('/', '-', $date));
            }

            User::where("id",$user->id)->update([
                    'firstName' => $firstName,
                    'lastName' => $secondName,
                    'gender' => $request['gender'],
                    'address' => $request['address'],
                    'avatar' => $avatar,
                    'city'=>$request['city'],
                    'birthDate'=>$timestamp,
                    'state'=>$request['state'],
                    'country'=>$request['country'],
                    'zipCode'=>$request['zipCode']
                ]);
            DB::commit();
            return response()->json(['status'=>true,'message'=>'User profile updated successfully!']);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'User profile update error  exception');
            return back();
        }
    }
    public function userBlock($id){
        try {
            $user=User::find($id);
            if ($user->status == UserStatusEnum::Active) {
                $user->update([
                    'status' => UserStatusEnum::Block
                ]);
                $user->tokens()->delete();
                toastr()->success('User blocked successfully!');
                return response()->json(['status'=>true,'message'=>'User blocked successfully!']);

            } elseif ($user->status == UserStatusEnum::Block) {
                $user->update([
                    'status' => UserStatusEnum::Active
                ]);
                toastr()->success('User unblocked successfully!');
                return response()->json(['status'=>true,'message'=>'User unblocked successfully!']);
            }
        }catch (\Exception $e){
            $this->saveExceptionLog($e, 'User block and unBlock error  exception');
            return back();
        }

    }
    public function destroy($id){
        try {
            $user= User::find($id);
            $user->delete();
            return response()->json(['status'=>true,'message'=>'User deleted successfully!']);
        }catch (\Exception $e ){
            $this->saveExceptionLog($e, 'User delete error  exception');
            return  false;
        }

    }
    public function listing($id){
        $user = User::with('userBuyingHistories','userBuyingHistories.getSongs','userActivePackages','userActivePackages.package')->find($id);
        $subscription=null;
        if($user->gplay_order_token != null && $user->isSubscribed == 1){
            $subscriptionId = Purchased::where('userId',$user->id)->where('transactionId',$user->gplay_order_token)->first();
            if ($subscriptionId!=null) {
                $subscription = Package::where('id', $subscriptionId->packageId)->first();
            }
        }
        return response()->json(['status'=>true,'data'=>$user,'package'=>$subscription]);
    }
    public function userDetail($id){
        $user=User::with('userInterests','userInterests.categroy','userActivePackages','userActivePackages.package','userFavouritSongs',
            'userFavouritSongs.song','purchasedALbum','purchasedALbum.getAlbums','purchasedALbum.getSongs','downloadSongs','downloadSongs.getSongs',
            'userBuyingHistories','userBuyingHistories.getSongs','userArtist')
            ->find($id);

        $playlists = Playlist::where('userId',$user->id)->get();
        $artistIds = Follower::where('userId',$user->id)->pluck('artistId')->toArray();
        $follower = User::whereIn('id',$artistIds)->paginate(10);
        $subscription=null;
        if($user->gplay_order_token != null && $user->isSubscribed == 1){
            $subscriptionId = Purchased::where('userId',$user->id)->where('transactionId',$user->gplay_order_token)->first();
            if ($subscriptionId!=null) {
                $subscription = Package::where('id', $subscriptionId->packageId)->first();
            }
        }
        return view('admin.users.userDetail',compact('user','follower','playlists','subscription'));
    }

    public function favouriteDelete($id){
        $favourite=Favorite::find($id);
        if ($favourite){
            $favourite->delete();
            return response()->json(['status'=>true,'message'=>'Favourite song deleted!']);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went Wrong!']);
        }

    }
    public function serverTimeZone(){
        $value = config('app.timezone');

        $abc=date('Y-d-m H:i:s');

        dd($value,$abc);
    }
}
