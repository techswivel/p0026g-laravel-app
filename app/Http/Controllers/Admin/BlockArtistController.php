<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Models\User;
use App\Enums\UserStatusEnum;

class BlockArtistController extends Controller
{
    use loggerExceptionTrait;
    public function index()
    {
        $artists = User::role('artist')->where('status',UserStatusEnum::Block)->get();
        $artist = auth()->user();
        return view('admin.artists.blockArtists',['artists'=>$artists,'artist'=>$artist]);
    }

    public function unblock($id)
    {
        try {
            $artist = User::where('id',$id)->update([
                'status' => UserStatusEnum::Active,
            ]);
            return response()->json([
                'status' => 200,
                'message' => 'Artist is unblock',
            ]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Artist block and unBlock error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to block Artist '.$exception->getMessage(),
            ]);
        }
    }
}
