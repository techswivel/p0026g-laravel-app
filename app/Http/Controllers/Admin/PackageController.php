<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Api\AppleStoreConnect\Subscription\SubscriptionController;
use App\Http\Controllers\Api\AppleStoreConnect\Subscription\SubscriptionPriceController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Package;
use Illuminate\Support\Facades\DB;
use App\Traits\InAppPurchaseTrait;
use App\Http\Requests\admin\packages\createPackageRequest;
use App\Http\Requests\admin\packages\updatePackageRequest;
use App\Jobs\AppleInAppSubscription;
use App\Jobs\AppleInAppSubscriptionAvailability;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PackageController extends Controller
{
    use InAppPurchaseTrait;

    public function index()
    {
        $packages = Package::all();
        return view('admin.packages.packages',['packages'=>$packages]);
    }

    public function store(createPackageRequest $request)
    {
        try {

            if($request->packageId == null){
                $sku = str_replace(' ', '_', strtolower($request->planTitle));

                $package = Package::create([
                    "name"=>$request->planTitle,
                    "firebaseTopic"=> $sku,
                    "price"=> 0,
                    "duration" => $request->planDuration,
                    "sku"=> $sku,
                ]);

                $request['name'] = $request->planTitle;
                $request['productId'] = $sku;
                $request['subscriptionPeriod'] = $request->planDuration;
                $request['groupLevel'] = Package::all()->count();
                $request['reviewNote'] = 'reviewNote';
                $request['locale'] = 'en-US';
                $request['localeName'] = 'English';
                $request['localeDescription'] = 'English language';

                $request['promotionIcon'] = new UploadedFile(public_path('appleAppStore/1024.png'),'1024.png','image/png',filesize(public_path('appleAppStore/1024.png')),true,TRUE);
                $request['reviewScreenshot'] = new UploadedFile(public_path('appleAppStore/Simulator-Screenshot.png'),'Simulator-Screenshot.png','image/png',filesize(public_path('appleAppStore/Simulator-Screenshot.png')),true,TRUE);

                $subscriptionId = (new SubscriptionController)->storeSubscription($request,env('APP_STORE_SUBSCRIPTION_GROUP_ID'));

                Package::find($package->id)->update([
                    "subscriptionId" => $subscriptionId,
                    "status" => "In-Process",
                ]);


                $getPriceTier = Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                ->get('https://api.appstoreconnect.apple.com/v1/subscriptions/'.$subscriptionId.'/pricePoints?include=territory&filter[territory]=USA&limit=800');

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());
                    $prices = [];

                    foreach($data->data as $i => $price){
                        $prices[$i]['priceTier'] = $price->id;
                        $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                        $prices[$i]['packageId'] = $package->id;
                    }

                   return $prices;

                }elseif($getPriceTier->failed()){

                    Package::find($package->id)->update([
                        "status" => "Failed",
                    ]);

                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get Apple Subscription Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

            }else{
                $package = Package::find($request->packageId);
            }



            if($request->stepNo == 2){

                Package::find($package->id)->update([
                    "price" => $request->subscriptionPrice,
                    "priceId" => $request->priceTier,
                ]);


                dispatch(new AppleInAppSubscriptionAvailability($request->all(), $package, JwtTokenController::getJwtToken()));
                dispatch(new AppleInAppSubscription($request->all(), $package, JwtTokenController::getJwtToken()));
            }


            return response()->json([
                'status' => 200,
                'package' => $package,
            ]);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to stored Plan ' . $exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $package = Package::find($id);
            if($package){
                $package->delete();
            }

            if($package->subscriptionId != null){
               (new SubscriptionController)->deleteSubscription($package->subscriptionId);
            }

            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Package is delete',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to delete Package '.$exception->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        try {
            $prices = [];
            $package = Package::find($id);

            if($package->subscriptionId != null){
                $getPriceTier = Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                ->get('https://api.appstoreconnect.apple.com/v1/subscriptions/'.$package->subscriptionId.'/pricePoints?include=territory&filter[territory]=USA&limit=800');

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());

                    foreach($data->data as $i => $price){
                        $prices[$i]['priceTier'] = $price->id;
                        $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                    }

                }elseif($getPriceTier->failed()){
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get Apple Subscription Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                $package->prices = $prices;
            }

            return response()->json([
                'status' => 200,
                'package' => $package,
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show package '.$exception->getMessage(),
            ]);
        }
    }

    public function package()
    {
        try {
            $package = Package::all();
            return response()->json([
                'status' => 200,
                'package' => $package,
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show package '.$exception->getMessage(),
            ]);
        }
    }

    public function update(updatePackageRequest $request)
    {
        try {
            $package = Package::find($request->packageId);


            if($package){
                $sku = str_replace(' ', '_', strtolower($request->editPlanTitle));

                Package::where('id',$request->packageId)->update([
                    "name"=>$request->editPlanTitle,
                    "firebaseTopic"=> $sku,
                    "duration"=>$request->editPlanDuration,
                ]);

                $request['name'] = $request->editPlanTitle;
                $request['subscriptionPeriod'] = $request->editPlanDuration;

                (new SubscriptionController)->updateSubscription($request,$package->subscriptionId);

            }

            return response()->json([
                'status' => 200,
                'package' => $package,
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to update Plan '.$exception->getMessage(),
            ]);
        }
    }
}
