<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\admin\SendAmountRequest;
use Illuminate\Support\Facades\DB;
use App\Models\Purchased;
use App\Models\Earning;
use App\Models\Card;
use App\Enums\WithdrawalStatusEnum;

class ArtistEarningController extends Controller
{
    public function index()
    {
        $artists = User::role('artist')->with('card')->get();
        return view('admin.artists.artistEarning',['artists'=>$artists]);
    }

    public function sendAmount(SendAmountRequest $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find($request->artistId);
            if($user){
            $id = $request->artistId;
            $totalEarning =  Purchased::whereHas('song', function($buyer) use ($id){
                    $buyer->where('userId', $id);
                })->orWhereHas('album', function($buyer) use ($id){
                    $buyer->where('userId', $id);
                })->get()->sum('amount');
            $withdrawalEarning = Earning::where('userId',$id)->where('withdrawalStatus',WithdrawalStatusEnum::Paid)->sum('amount');
            $availableAmount = round($totalEarning - $withdrawalEarning,2);
            if($request->amount <= $availableAmount){
                $earningRequest =  Earning::where('userId',$id)->where('withdrawalStatus',WithdrawalStatusEnum::Requested)->delete();
                Earning::create([
                    'userId' => $id,
                    'withdrawalStatus' => WithdrawalStatusEnum::Paid,
                    'amount' => $request->amount,
                ]);
            }else{
                return response()->json([
                    'status' => 500,
                    'message' => 'fail to send amount low available balance',
                ]);     
            }
            }
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Amount is send to artist',
            ]); 
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to send amount '.$exception->getMessage(),
            ]);
        }
    }

    public function artistCard($id)
    {
        try {
            $card = Card::where('userId',$id)->first();
            return response()->json([
                'status' => 200,
                'card'=> $card,
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to get card '.$exception->getMessage(),
            ]);
        }
    
    }
}

