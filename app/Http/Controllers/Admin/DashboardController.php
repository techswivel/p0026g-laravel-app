<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Purchased;
use Carbon\Carbon;
use App\Enums\PurchasedItemTypeEnum;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::role('user')->latest('id')->limit(5)->get();
        return view('admin.dashboard',['users'=>$users]);
    }

    public function salesReport($report)
    {
        if($report == 'Last Week'){
            return $this->saleReportWeek();
        }elseif($report == 'Last month'){
            return $this->saleReportMonthly();
        }else{
            return $this->saleReportYear($report);
        }
    }

    protected function saleReportYear($year)
    {
        try {
            $previousYearSales = 0;
            $date = Carbon::parse($year.'-01-01 00:00');
            $monthly = array();
            for ($x = 0; $x <= 11; $x++) {
                $subscriptionsSales = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addMonth($x)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addMonth($x)->endOfMonth()->timestamp))->sum('amount');
                array_push($monthly,($subscriptionsSales));
            }
            $previousDate = Carbon::parse(($year-1).'-01-01 00:00');
            $previousYearSales =  Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addMonth(0)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addMonth(11)->endOfMonth()->timestamp))->sum('amount');
            return response()->json([
                    'status' => 200,
                    'monthly' =>$monthly,
                    'previousYearSales' => $previousYearSales,
                ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to show sales report'.$exception->getMessage(),
            ]);
        }
    }

    protected function saleReportMonthly()
    {
        try {
            $previousMonthValue = 0;
            $day = array();
            $date = Carbon::now()->subMonth()->startofMonth();
            for ($x = 0; $x <= 24; $x++) {
                $y = $x + 4;
                $subscriptionsSales = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay($x)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addDay($y)->timestamp))->sum('amount');
                array_push($day,($subscriptionsSales));
                $x = $y;
                if($x == 24){
                    $subscriptionsSales = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay($x + 1)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->sum('amount');
                    array_push($day,($subscriptionsSales));
                }
            }
            $previousMonthdate = Carbon::now()->subMonth()->subMonth()->startofMonth();
            $previousMonthValue = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay(0)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->sum('amount');
            return response()->json([
            'status' => 200,
            'day' => $day,
            'previousMonthValue' => $previousMonthValue
        ]);
        } catch (\Exception $exception) {
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show sales report '.$exception->getMessage(),
            ]);
        }
    }

    protected function saleReportWeek()
    {
        try {
            $date = Carbon::now()->subWeek()->startOfWeek();
            $weekDays = array();
            for ($x = 0; $x <= 6; $x++) {
                $subscriptionsSales  = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay($x)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addDay($x)->timestamp))->sum('amount');
                array_push($weekDays,($subscriptionsSales));
            }
            $previousWeek = Carbon::now()->subWeek(1)->startOfWeek();
            $previousWeekValue = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay(0)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addDay(6)->timestamp))->sum('amount');
            return response()->json([
                'status' => 200,
                'weekDays' => $weekDays,
                'previousWeekValue' => $previousWeekValue,
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show sales report '.$exception->getMessage(),
            ]);
        }
    }

}
