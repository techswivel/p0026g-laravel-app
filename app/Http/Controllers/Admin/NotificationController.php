<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\NotificationDataTable;
use App\Enums\UserStatusEnum;
use App\Http\Controllers\Controller;
use App\Models\MonthlyNotificationCount;
use App\Models\NotificationTopic;
use Illuminate\Http\Request;
use App\Http\Requests\Artist\Notification\NotificationAddRequest;
use Illuminate\Support\Facades\DB;
use App\Repositories\Eloquent\NotificationRepository;
use App\Enums\NotificationStatusEnum;
use App\Enums\NotificationTypeEnum;
use App\Enums\UserTypeNotificationEnum;
use App\Http\Requests\Notifications\NotificationRequest;
use App\Http\Resources\User\SubscriptionResource;
use App\Models\FcmToken;
use App\Models\Follower;
use App\Models\Notification;
use App\Models\Package;
use App\Models\User;
use App\Services\FcmTokenService;
use App\Services\FirebaseService;
use Carbon\Carbon;
use Facades\App\Helpers\FcmHelper;
use Illuminate\Support\Facades\Auth;
use App\Traits\NotificationTrait;

class NotificationController extends Controller
{
    use NotificationTrait;
    protected $notificationRepository;

    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function store(NotificationAddRequest $request)
    {
        try {
            DB::beginTransaction();
            $notification = $this->notificationRepository->store($request);
            try {
                $this->approveNotificationSend($notification->userId,$notification->id);
            } catch (\Exception $exception) {
                throw new \ErrorException('Fail to send notification '.$exception->getMessage());
            }
            DB::commit();
        } catch (\Exception $exception) {
             DB::rollBack();
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to stored Notification '.$exception->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();
            $notification = $this->notificationRepository->show($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'notification' => $notification]);
        } catch (\Exception $exception) {
             DB::rollBack();
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show Notification '.$exception->getMessage(),
            ]);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $notification = $this->notificationRepository->delete($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message'=> 'Notification is delete successfully',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'status' =>  500,
                'message'=> 'Fail to delete Notification '.$exception->getMessage(),
            ]);

        }
    }
    /**
     * index show create form for notifictions
     *
     * @return void
     */
    public function index(NotificationDataTable $dataTable)

    {
        $packages=Package::get();
        return $dataTable->render('admin.notifications.index',compact(['packages']));
    }

    public function send(NotificationRequest $request)
    {
        DB::beginTransaction();
        $userId = Auth::id();
        $data = $request->description;
        $title = $request->title;
        $planName=$request->packageName;
        $ios=$request->get('IOS_Users');
        $freePlan=$request->get('free_plan_user');
        $android=$request->get('Android_Users');
        $allNotifications = ($planName !=  null) ? $planName : [];
        array_push($allNotifications,
            $android != null ? $android : "",
            $ios != null ? $ios : "",
            $freePlan != null ? $freePlan : "",
        );
        if($allNotifications != null){
            $notification=Notification::create([
                'userId'=>$userId,
                'title'=>$title,
                'message'=>$data,
                'notificationType'=>NotificationTypeEnum::Admin
            ]);
            foreach ($allNotifications as $notify){
                if($notify != ""){
                    if (FcmHelper::topic($data, $title, $notify)) {

                    }
                    NotificationTopic::create([
                        'notificationId'=>$notification->id,
                        'topicName'=>$notify
                    ]);
                }
            }
            DB::commit();
            toastr()->success('Notification is sent successfully');
            return  response()->json(['status'=>200,'message'=>'Notification is sent successfully']);
        } else{
            DB::rollBack();
            return  response()->json(['status'=>500,'message'=>'Not select type notification']);

        }
        /* send notification to both users  */

    }

    
    public function notificationDetail($notificationId)
    {
        try {
            DB::beginTransaction();
            $notification = Notification::where('notificationType',NotificationTypeEnum::Admin)->find($notificationId);
            $topics=NotificationTopic::where('notificationId',$notification->id)->get();
            DB::commit();
            return response()->json([
                'status' => 200,
                'notification' => $notification,
                'topics'=>$topics
                ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to show Notification '.$exception->getMessage(),
            ]);
        }
    }
    public function notificationDeleteTopic($notificationId)
    {
        try {
            DB::beginTransaction();
            $notification = Notification::where('notificationType',NotificationTypeEnum::Admin)->find($notificationId);
            $topics=NotificationTopic::where('notificationId',$notification->id)->get();
            foreach ($topics as $topic){
             $topic->delete();
            }
            $notification->delete();
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Notification is delete successfully',

                ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to show Notification '.$exception->getMessage(),
            ]);
        }
    }
    public function notificationSetting(Request $request){
        $artists=User::role('artist')->where('status',UserStatusEnum::Active)->get();
        return view('admin.artists.notification.settingNotification',compact('artists'));
    }
    public function artistLimitPerDayNotification(Request $request){

        try {
            DB::beginTransaction();
            MonthlyNotificationCount::create([
                'totalNotificationCount' => $request->numberPerDay
            ]);
            DB::commit();
            return response()->json(['status' => 200, 'message' => 'Notification limit set per day by artist']);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['status' => 500, 'message' => 'Notification limit not correct']);

        }
    }
    public function notificationStatusCheck(Request $request){
        try {
            DB::beginTransaction();
            $user=User::find($request->id);
            if ($request->isNotificationEnabled==0){
                $user->update([
                    'isNotificationEnabled' => 1
                ]);
                DB::commit();
                return response()->json(['status'=>200,'message'=>'Notification is disable successfully!']);
            }else{
                $user->update([
                    'isNotificationEnabled' =>0,
                ]);
                DB::commit();
                return response()->json(['status'=>200,'message'=>'Notification is isNotificationEnabled successfully!']);
            }

        }catch (\Exception $e){
            DB::rollBack();
            $this->saveExceptionLog($e, 'User block and unBlock error  exception');
            return response()->json(['status'=>500,'message'=>'User block and unBlock error  exception!']);

        }
    }
}
