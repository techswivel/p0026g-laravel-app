<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\admin\artist\createArtistRequest;
use App\Http\Requests\admin\artist\updateArtistRequest;
use App\Traits\FileUploadTrait;
use App\Models\User;
use App\Models\Song;
use App\Models\AlbumSongs;
use App\Models\Category;
use App\Models\Language;
use App\Models\Follower;
use App\Models\Album;
use App\Models\Notification;
use App\Models\Purchased;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Enums\SongPreOrderEnum;
use App\Enums\AlbumPreOrderEnum;
use App\Enums\UserStatusEnum;
use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Mail\ArtistWelcomeMail;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Models\MonthlyNotificationCount;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class ActiveArtistController extends Controller
{
    use FileUploadTrait,loggerExceptionTrait;

    public function index()
    {
        $artists = User::role('artist')->where('status',UserStatusEnum::Active)->get();
        $artist = auth()->user();
        return view('admin.artists.activeArtists',['artists'=>$artists,'artist'=>$artist]);
    }

    public function store(createArtistRequest $request)
    {
        try {
            DB::beginTransaction();
            $avatar = NULL;
            if(request()->hasFile('artistAvatar')){
                $file = request()->file('artistAvatar');
                $path = 'artist/image/avatar';
                $avatar = $this->uploadFile($file,$path);
            }
            $artist = User::create([
                'firstName' => $request->artistFirstName,
                'lastName' => $request->artistLastName,
                'email' => $request->email,
                'password' => Hash::make($request->artistPassword),
                'aboutArtist' => $request->artistBio,
                'avatar' => $avatar,
                'isNotificationEnabled' => 1,
            ]);
            $artist->assignRole('artist');
            Mail::to($request->email)->send(new ArtistWelcomeMail($request->email,$request->artistPassword));
            DB::commit();
            return response()->json([
                'status' => 200,
                'artist' => $artist,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Save Active artist exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to stored Artist '.$exception->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        try {
            $artist = User::find($id);
            return response()->json([
                'status' => 200,
                'artist' => $artist,
            ]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Show Active artist exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show Artist '.$exception->getMessage(),
            ]);
        }

    }

    public function update(updateArtistRequest $request)
    {
        try {
            DB::beginTransaction();
            $artist = User::find($request->artistId);
            $avatar = $artist->avatar;
            if(request()->hasFile('editArtistAvatar')){
                $file = request()->file('editArtistAvatar');
                $path = 'artist/image/avatar';
                $avatar = $this->uploadFile($file,$path);
                $this->deleteFile($artist->avatar);
            }
            $password = $artist->password;
            if($request->editArtistPassword){
                $password = Hash::make($request->editArtistPassword);
            }
            $artistUpdate = User::where('id',$request->artistId)->update([
                'firstName' => $request->editArtistFirstName,
                'lastName' => $request->editArtistLastName,
                'password' => $password,
                'aboutArtist' => $request->editArtistBio,
                'avatar' => $avatar,
            ]);
            DB::commit();
            return response()->json([
                'status' => 200,
                'artist' => $artistUpdate,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Update Active artist exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to update Artist '.$exception->getMessage(),
            ]);
        }
    }

    public function delete($id)
    {
        try {
            $artist = User::find($id);
            $albums = Album::where('userId',$id)->get();
            foreach ($albums as $album) {
                $albumSongs = AlbumSongs::where('albumId',$album->id)->delete();
                $album->delete();
            }
            $songs = Song::where('userId',$id)->get();
            foreach ($songs as $song) {
                $song->delete();
            }
            $artist->delete();
            return response()->json([
                'status' => 200,
                'message' => 'Artist is delete',
            ]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Delete Active artist exception');

            return response()->json([
                'status' => 500,
                'message'=> 'Fail to delete Artist '.$exception->getMessage(),
            ]);
        }
    }

    public function detail($id)
    {
        try {
            $isNotificationLimit = TRUE;
            $artist = User::find($id);
            $role = auth()->user()->getRoleNames()->first();
            $selectsongs = Song::where('userId',$id)->doesntHave("songs")->get();
            $songs = Song::with(['getArtist', 'getSongRelation'])->where('userId',$id)->where('type',SongPreOrderEnum::Song)->get();
            $categories = Category::all();
            $languages = Language::all();
            $followers = Follower::with('user')->where('artistId',$id)->get();
            $notifications = Notification::where('userId',$id)->get();
            $date = Carbon::now()->startofMonth();
            $notification = Notification::withTrashed()->where('userId',$id)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->count();
            $monthlyNotificationCount = MonthlyNotificationCount::first();
            if($monthlyNotificationCount){
                if($notification < $monthlyNotificationCount->totalNotificationCount){
                    $isNotificationLimit = TRUE;
                }else{
                    $isNotificationLimit = FALSE;
                }
            }
            if($artist->isNotificationEnabled){
                $isNotificationLimit = TRUE;
            }else{
                $isNotificationLimit = FALSE;
            }
            $albums = Album::with(['getSongRelation', 'getArtist'])->where('userId',$id)->where('type',AlbumPreOrderEnum::Album)->get();
            $purchases = Purchased::with('song','album','user')->whereHas('getSongs', function($buyer) use ($artist){
            $buyer->where('userId',$artist->id);
            })->orWhereHas('getAlbums', function($buyer) use ($artist)
            {
                $buyer->where('userId',$artist->id);
            })->get();
            $preorderAlbumSongs = Song::where('userId',$id)->where('type',SongPreOrderEnum::PreOrderAlbum)->doesntHave("songs")->get();
            $preorderSongs = Song::where('userId',$id)->where('type',SongPreOrderEnum::PreOrder)->get();
            $preorderAlbums = Album::where('userId',$id)->where('type',AlbumPreOrderEnum::PreOrder)->get();


            return view('artist.artist-profile', get_defined_vars());
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Detail Active artist exception');
            return redirect()->route('activeArtists');
        }
    }

    public function artistCompleteDetail($id)
    {
        try {
            $isNotificationLimit = TRUE;
            $artist = User::find($id);
            $role = auth()->user()->getRoleNames()->first();
            $selectsongs = Song::where('userId',$id)->doesntHave("songs")->get();
            $songs = Song::where('userId',$id)->where('type',SongPreOrderEnum::Song)->get();
            $categories = Category::all();
            $languages = Language::all();
            $followers = Follower::with('user')->where('artistId',$id)->get();
            $notifications = Notification::where('userId',$id)->get();
            $date = Carbon::now()->startofMonth();
            $notification = Notification::withTrashed()->where('userId',$id)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->count();
            $monthlyNotificationCount = MonthlyNotificationCount::first();
            if($monthlyNotificationCount){
                if($notification < $monthlyNotificationCount->totalNotificationCount){
                    $isNotificationLimit = TRUE;
                }else{
                    $isNotificationLimit = FALSE;
                }
            }
            if($artist->isNotificationEnabled){
                $isNotificationLimit = TRUE;
            }else{
                $isNotificationLimit = FALSE;
            }
            $albums = Album::with('getSongRelation')->where('userId',$id)->where('type',AlbumPreOrderEnum::Album)->get();
            $purchases = Purchased::with('song','album','user')->whereHas('getSongs', function($buyer) use ($artist){
            $buyer->where('userId',$artist->id);
            })->orWhereHas('getAlbums', function($buyer) use ($artist)
            {
                $buyer->where('userId',$artist->id);
            })->get();
            $preorderAlbumSongs = Song::where('userId',$id)->where('type',SongPreOrderEnum::PreOrderAlbum)->doesntHave("songs")->get();
            $preorderSongs = Song::where('userId',$id)->where('type',SongPreOrderEnum::PreOrder)->get();
            $preorderAlbums = Album::where('userId',$id)->where('type',AlbumPreOrderEnum::PreOrder)->get();
            return view('artist.artist-profile-new',['role'=>$role,'artist'=>$artist,'albums'=>$albums,'categories'=>$categories,'languages'=>$languages,'songs'=>$songs,'followers'=>$followers,'selectsongs'=>$selectsongs,'purchases'=>$purchases,'preorderSongs'=>$preorderSongs,'preorderAlbumSongs'=>$preorderAlbumSongs,'preorderAlbums'=>$preorderAlbums,'notifications'=>$notifications,'isNotificationLimit'=>$isNotificationLimit]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Detail Active artist exception');
            return redirect()->route('activeArtists');
        }
    }

    public function block($id)
    {
        try {
            $artist = User::where('id',$id)->update([
                'status' => UserStatusEnum::Block,
            ]);
            return response()->json([
                'status' => 200,
                'message' => 'Artist is Block',
            ]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Artist  block and unBlock exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to block Artist '.$exception->getMessage(),
            ]);
        }
    }
}
