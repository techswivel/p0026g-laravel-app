<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Purchased;
use App\Models\Package;
use App\Models\Song;
use App\Models\Album;
use App\Models\AlbumSongs;
use Illuminate\Support\Facades\DB;
use App\Enums\PurchasedItemTypeEnum;

class PurchasedHistoryController extends Controller
{
    
    public function index()
    {
        $songPurchase = Purchased::whereIn('itemType',[PurchasedItemTypeEnum::SONG,PurchasedItemTypeEnum::PRE_ORDER_SONG])->sum('amount');
        $albumPurchase = Purchased::whereIn('itemType',[PurchasedItemTypeEnum::ALBUM,PurchasedItemTypeEnum::PRE_ORDER_ALBUM])->sum('amount');
        $planPurchase = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->sum('amount');
        $purchases = Purchased::with('user','song','album')->get();
        return view('admin.purchasedHistory',['purchases'=>$purchases,'songPurchase'=>$songPurchase,'albumPurchase'=>$albumPurchase,'planPurchase'=>$planPurchase,]);
    }

    public function purchaseDetail($id)
    {
        try {
            $purchase = Purchased::withTrashed()->with('song','album','user','package')->find($id);
            $albumSongs = Song::withTrashed()->find(AlbumSongs::where('albumId',$purchase->albumId)->get('songId'));
            $price = $albumSongs->sum('price');
            return response()->json([
                'status' => 200,
                'purchase' => $purchase,
                'albumSongs' => $albumSongs,
                'price' => number_format($price, 2, '.', ''),
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'message' => 'Fail to show purchase detail '.$exception,
            ]);
        }
        
    }

    public function purchaseStatus($name)
    {
        if($name == 'All'){
            $songPurchase = Purchased::whereIn('itemType',[PurchasedItemTypeEnum::SONG,PurchasedItemTypeEnum::PRE_ORDER_SONG])->sum('amount');
            $albumPurchase = Purchased::whereIn('itemType',[PurchasedItemTypeEnum::ALBUM,PurchasedItemTypeEnum::PRE_ORDER_ALBUM])->sum('amount');
            $planPurchase = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->sum('amount');
            $purchases = Purchased::with('user','song','album')->get();
            return view('admin.purchasedHistory',['purchases'=>$purchases,'songPurchase'=>$songPurchase,'albumPurchase'=>$albumPurchase,'planPurchase'=>$planPurchase,'selectVale'=>$name]);
        }else if($name == 'Songsearnings'){
            $songPurchase = Purchased::where('itemType',PurchasedItemTypeEnum::SONG)->sum('amount');
            $albumPurchase = 0;
            $planPurchase = 0;
            $purchases = Purchased::where('itemType',PurchasedItemTypeEnum::SONG)->get();
            return view('admin.purchasedHistory',['purchases'=>$purchases,'songPurchase'=>$songPurchase,'albumPurchase'=>$albumPurchase,'planPurchase'=>$planPurchase,'selectVale'=>$name]);
        }else if($name == 'Albumearnings'){
            $songPurchase = 0;
            $albumPurchase = Purchased::where('itemType',PurchasedItemTypeEnum::ALBUM)->sum('amount');
            $planPurchase = 0;
            $purchases = Purchased::where('itemType',PurchasedItemTypeEnum::ALBUM)->get();
            return view('admin.purchasedHistory',['purchases'=>$purchases,'songPurchase'=>$songPurchase,'albumPurchase'=>$albumPurchase,'planPurchase'=>$planPurchase,'selectVale'=>$name]);
        }else if($name == 'Preorderearnings'){
            $songPurchase = Purchased::where('itemType',PurchasedItemTypeEnum::PRE_ORDER_SONG)->sum('amount');
            $albumPurchase = Purchased::where('itemType',PurchasedItemTypeEnum::PRE_ORDER_ALBUM)->sum('amount');
            $planPurchase = 0;
            $purchases = Purchased::whereIn('itemType',[PurchasedItemTypeEnum::PRE_ORDER_ALBUM,PurchasedItemTypeEnum::PRE_ORDER_SONG])->get();
            return view('admin.purchasedHistory',['purchases'=>$purchases,'songPurchase'=>$songPurchase,'albumPurchase'=>$albumPurchase,'planPurchase'=>$planPurchase,'selectVale'=>$name]);
        }else{
            $songPurchase = 0;
            $albumPurchase = 0;
            $planPurchase = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->where('packageId',$name)->sum('amount');
            $purchases = Purchased::where('packageId',$name)->get();
            return view('admin.purchasedHistory',['purchases'=>$purchases,'songPurchase'=>$songPurchase,'albumPurchase'=>$albumPurchase,'planPurchase'=>$planPurchase,'selectVale'=>$name]);
        }
    }
}
