<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Models\Playlist;
use App\Models\PlaylistSongs;
use App\Models\Song;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlaylistController extends Controller
{
    use loggerExceptionTrait;
    public function playlistUpdate(Request $request){
     $playlist=Playlist::where('id',$request->get('id'))->first();
     if ($playlist){
         $playlist->update([
             'title'=>$request->get('title'),
         ]);
         return response()->json(['status'=>true,'message'=>'Playlist song updated successfully!']);
     }else{
         return response()->json(['status'=>false,'message'=>'Something Went Wrong!']);

     }
    }
    public function playlistDelete($id){
     $playlist=Playlist::find($id);
     if ($playlist){
         $playlist->delete();
         return response()->json(['status'=>true,'message'=>'Playlist song Deleted successfully!']);
     }else{
         return response()->json(['status'=>false,'message'=>'Something Went Wrong!']);

     }
    }
     public function listTheSong()
    {
        try {
            DB::beginTransaction();
            // List all song in the song List to select song
            $selectsongs = Song::where('userId',auth()->user()->id)->doesntHave("songs")->get();
            DB::commit();
            return response()->json([
                'status' => 200,
                'selectsongs' => $selectsongs,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Play list the song error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to list Song Album '.$exception->getMessage(),
            ]);
        }

    }
    public function albumDetail($id)
    {
        try {
            DB::beginTransaction();
            $album = Album::find($id);
            $albumSongs = AlbumSongs::where('albumId',$id)->get('songId');
            $count = count($albumSongs);
            //list the all song of the album in albumDetail
            $songs = Song::find($albumSongs);
            DB::commit();
            return response()->json([
                'status'=> 200,
                'album'=>$album,
                'songs'=>$songs,
                'count' =>$count
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Album  detail the song error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to show Album Detail '.$exception->getMessage(),
            ]);
        }
    }
    public function playlistDetail($id)
    {

        try {
            DB::beginTransaction();
            $album = Playlist::find($id);
            $albumSongs = PlaylistSongs::where('playlistId',$id)->get('songId');
            $count = count($albumSongs);
            //list the all song of the album in PlayListDetail
            $songs = Song::find($albumSongs);
            DB::commit();
            return response()->json([
                'status'=> 200,
                'album'=>$album,
                'songs'=>$songs,
                'count' =>$count
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Playlist detail list the song error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to show Album Detail '.$exception->getMessage(),
            ]);
        }
    }
    public function removeSong($id)
    {
        try {
            DB::beginTransaction();
            // remove the song from the  Playlist
            $albumSong = PlaylistSongs::where('songId',$id)->first();
            $albumSong->delete();
            $album = Playlist::find($albumSong->playlistId);
            DB::commit();
            return response()->json([
                'status' => 200,
                'album'=> $album,
                'message'=> 'Song is deleted from Playlist',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Remove album the song error  exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to delete Album '.$exception->getMessage(),
            ]);
        }
    }

}
