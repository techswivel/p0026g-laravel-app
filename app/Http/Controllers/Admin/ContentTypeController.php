<?php

namespace App\Http\Controllers\Admin;

use App\Enums\PageTypeEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\ContentType\ContentTypeRequest;
use App\Http\Requests\admin\ContentType\ContentTypeUpdateRequest;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentTypeController extends Controller
{

    public function termConditions(Request $request){
        $content=Page::where('type',PageTypeEnum::TermAndCondition)->first();
        return view('admin.contentType.list',compact('content'));
    }
    public function privacyPolicy(Request $request){
        $content=Page::where('type',PageTypeEnum::PrivacyPolicy)->first();
        return view('admin.contentType.list',compact('content'));
    }
    public function webViewTermConditions(Request $request){
        $content=Page::where('type',PageTypeEnum::TermAndCondition)->first();
        return view('admin.web.list',compact('content'));
    }
    public function webViewPrivacyPolicy(Request $request){
        $content=Page::where('type',PageTypeEnum::PrivacyPolicy)->first();
        return view('admin.web.list',compact('content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */

    public function create(Request $request){
        $contentType=$request->content_type;
        $content=Page::where('type',$request->content_type)->first();
        if ($contentType == PageTypeEnum::TermAndCondition){
            return view('admin.contentType.create',compact('contentType','content'));
        }elseif($contentType == PageTypeEnum::PrivacyPolicy) {
            return view('admin.contentType.create', compact('contentType','content'));
        }else{
            return abort(404);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContentTypeRequest $request)
    {
        try {
            DB::beginTransaction();
                Page::create([
                    'title'=>$request->title,
                    'description'=>$request->description,
                    'type'=>$request->contentType,
                ]);
                DB::commit();
                toastr()->success('Content Type added successfully!');
                return redirect()->back();
        }catch (\Exception $e){
            DB::rollBack();
            toastr()->error($e->getMessage());
            return redirect()->back();

        }
    }
    public function update(ContentTypeUpdateRequest $request, $id){
       DB::beginTransaction();
        $content=Page::find($id);
        if ($content){
            if ($content->type==PageTypeEnum::TermAndCondition){
                $content->update([
                    'title'=>$request->title,
                    'description'=>$request->description,
                ]);
                DB::commit();
                toastr()->success('Content has been updated successfully!');
                return redirect()->back();

            }elseif ($content->type==PageTypeEnum::PrivacyPolicy){
                $content->update([
                    'title'=>$request->title,
                    'description'=>$request->description,
                ]);
                DB::commit();
                toastr()->success('Content has been updated successfully!');
                return redirect()->back();
            }else{
                toastr()->error('Content type not found!');
                return redirect()->back();
            }
        }else{
            DB::rollBack();
            toastr()->error('Content type id is not found!');
            return redirect()->back();
        }

    }
}
