<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\Categories\StoreRequest;
use App\Http\Requests\admin\Categories\UpdateRequest;
use App\Models\Album;
use App\Models\Category;
use App\Models\Song;
use App\Models\UserInterest;
use App\Traits\FileUploadTrait;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    use FileUploadTrait,loggerExceptionTrait;
    /**
     * imageDirectory  path of folder where image will be stored on s3 e.g uploads/categories
     *
     * @var string
     */
    protected $imageDirectory = "uploads/categories";

    public function index()
    {
        $categories=Category::get();
        return view('admin.category.index',compact('categories'));
    }

    public function store(StoreRequest $request)
    {
     try{
         DB::beginTransaction();
         $imageName = uniqid().'.'.$request->image->getClientOriginalExtension();
         $imageCategory = Image::make($request->image)->resize(524, 524);
         $imageName=rtrim($this->imageDirectory, '/').'/'.$imageName;
         Storage::disk('s3')->put($imageName, $imageCategory->stream());
         Category::create(['name' => ucwords($request->name), 'imageUrl' =>  $imageName]);
         DB::commit();
         return response()->json(['status' => true, 'data' => ucwords($request->name) . ' category added successfully!'], JsonResponse::HTTP_OK);
     }catch (\Exception $e){
         DB::rollBack();
         $this->saveExceptionLog($e, 'Category store error  exception');
         return response()->json(['status' => false, 'data' => $e->getMessage()], JsonResponse::HTTP_OK);
     }

    }
    public function categoryDetail($id){
        $cat=Category::with('categorySongs')->find($id);
        $song=$cat->categorySongs->pluck('id')->toArray();
        $albums = Album::with(['getSongRelation', 'getArtist'])->wherehas('albumSongs',function($q) use($song){
            $q->whereIn('songId',$song);
        })->get();
        $albumCount=$albums->count();
        return response()->json(['status'=>true,
            'data'=>$cat,
            'albumCount'=>$albumCount,
            'albums'=>$albums
        ]);
    }
    public function edit($id){
        try {
            $cat = Category::find($id);
            return response()->json(['status' => true, 'data' => $cat], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Category edit error  exception');
            return response()->json(['status' => false, 'data' => $e->getMessage()], JsonResponse::HTTP_OK);
        }
    }
    public function update(UpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $imageName = '';
            $category = Category::find($request->id);
            if ($request->has('image')) {
                $this->deleteFile($category->imageUrl);
                $imageName = uniqid().'.'.$request->image->getClientOriginalExtension();
                $imageCategory = Image::make($request->image)->resize(524, 524);
                $imageName=rtrim($this->imageDirectory, '/').'/'.$imageName;
                Storage::disk('s3')->put($imageName, $imageCategory->stream());
            }
            $category->update(
                [
                    'name' => $request->has('name') ? ucwords($request->name) : $category->name,
                    'imageUrl' => $request->has('image') ? $imageName : $category->imageUrl
                ]
            );
            toastr()->success('Category updated  successfully!');
            DB::commit();
            return back();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'category update error  exception');
            toastr()->error($e->getMessage());
            return back();
        }
    }
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $category = Category::find($id);
            if ($category) {
                $songs = Song::where('categoryId', $id)->exists();
                if ($songs){
                    return response()->json(['status' => false, 'data' => 'You cant delete a category with Songs to You can not delete this category, because it exists Songs or Albums!'], JsonResponse::HTTP_OK);
                }
                $interestChecks=UserInterest::where('categoryId',$category->id)->get();
                if ($interestChecks){
                    foreach ($interestChecks as $interestCheck) {
                        $interestCheck->delete();
                    }
                }
                $this->deleteFile($category->imageUrl);
                $category->delete();
                DB::commit();
                return response()->json(['status' => true, 'data' => 'Category deleted successfully!'], JsonResponse::HTTP_OK);
            } else {
                return response()->json(['status' => false, 'data' => 'You cant delete a category with Songs to You can not delete this category, because it exists Songs or Albums!'], JsonResponse::HTTP_OK);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Category delete error  exception');
            return response()->json(['status' => false, 'data' => $e->getMessage()], JsonResponse::HTTP_OK);
        }
    }

}
