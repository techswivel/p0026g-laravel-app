<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Enums\NotificationTypeEnum;
use App\Enums\NotificationStatusEnum;
use Illuminate\Support\Facades\DB;
use App\Traits\NotificationTrait;

class ArtistNotificationController extends Controller
{
    use NotificationTrait;
    public function index()
    {
        $approveNotifications = Notification::with('getArtist')->where('notificationType',NotificationTypeEnum::Artist)->where('status',NotificationStatusEnum::Approved)->get();
        $rejectNotifications = Notification::with('getArtist')->where('notificationType',NotificationTypeEnum::Artist)->where('status',NotificationStatusEnum::Rejected)->get();
        $pendingNotifications = Notification::with('getArtist')->where('notificationType',NotificationTypeEnum::Artist)->where('status',NotificationStatusEnum::Pending)->get();
        $deletedByArtistNotifications = Notification::with('getArtist')->where('notificationType',NotificationTypeEnum::Artist)->onlyTrashed()->where('status','!=',NotificationStatusEnum::DeletedByAdmin)->get();
        return view('admin.artists.notification.notification',['approveNotifications'=>$approveNotifications,'rejectNotifications'=>$rejectNotifications,'pendingNotifications'=>$pendingNotifications,'deletedByArtistNotifications'=>$deletedByArtistNotifications]);
    }

    public function notificationDetail($id)
    {
        try {
            DB::beginTransaction();
            $notification = Notification::withTrashed()->with('getArtist')->find($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'notification' => $notification]);
        } catch (\Exception $exception) {
             DB::rollBack();
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show Notification '.$exception->getMessage(),
            ]);
        }
    }

    public function rejectNotification($id)
    {
        try {
            DB::beginTransaction();
            $notification = Notification::where('id',$id)->update([
                'status' => NotificationStatusEnum::Rejected,
            ]);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Notification is Reject']);
        } catch (\Exception $exception) {
             DB::rollBack();
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to reject Notification '.$exception->getMessage(),
            ]);
        }
    }

    public function approveNotification($id)
    {
        try {
            DB::beginTransaction();
            $notification = Notification::find($id);
            try {
                $this->approveNotificationSend($notification->userId,$notification->id);
            } catch (\Exception $exception) {
                throw new \ErrorException('Fail to send notification '.$exception->getMessage());
            }
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Notification is Approve']);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to approve Notification '.$exception->getMessage(),
            ]);
        }
    }

    public function deleteNotification($id)
    {
        try {
            DB::beginTransaction();
            $notification = Notification::withTrashed()->where('id',$id)->update([
                    'status' => NotificationStatusEnum::DeletedByAdmin,
                ]);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Notification is Delete']);
        } catch (\Exception $exception) {
             DB::rollBack();
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to delete Notification '.$exception->getMessage(),
            ]);
        }
    }
}
