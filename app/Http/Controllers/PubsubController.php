<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Google\Cloud\PubSub\PubSubClient;
use App\User;
use App\Jobs\SubscribeUser;

class PubsubController extends Controller
{
    public function subscribe()
    {
        $project_id = config('services.google_cloud.project_id');
        $config_path = config('services.google_cloud.config_path');

        $key_file = file_get_contents(base_path().$config_path);
        $pubsub = new PubSubClient([
            'projectId' => $project_id,
            'keyFile' => json_decode($key_file, true)
        ]);        

        $req_body = file_get_contents('php://input');
        $req_data = json_decode($req_body, true);

        $data = json_decode(base64_decode($req_data['message']['data']), true);

        $purchase_token = $notificationType = $type = null;
        if (array_key_exists('subscriptionNotification',$data)){
            $type = "SUBSCRIPTION";
            $purchase_token = $data['subscriptionNotification']['purchaseToken'];
            $notificationType = $data['subscriptionNotification']['notificationType'];
        }elseif(array_key_exists('oneTimeProductNotification',$data)){
            $type = "ONETIMEPRODUCT";
            $purchase_token = $data['oneTimeProductNotification']['purchaseToken'];
            $notificationType = $data['oneTimeProductNotification']['notificationType'];
        }else{
            return 'testing';
        }        

        $pubsub->consume($req_data);
        SubscribeUser::dispatch($notificationType,$purchase_token,$type)->delay(now()->addSeconds(5));

        return 'ok';
    }
}