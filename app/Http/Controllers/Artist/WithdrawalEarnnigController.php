<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Models\Card;
use Illuminate\Support\Facades\DB;
use App\Models\Purchased;
use App\Models\Earning;
use Helper;
use App\Enums\WithdrawalStatusEnum;

class WithdrawalEarnnigController extends Controller
{
    use loggerExceptionTrait;
    public function index()
    {
        try {
            $withdrawalEarning = Earning::where('userId',auth()->user()->id)->where('withdrawalStatus',WithdrawalStatusEnum::Paid)->get()->sum('amount');
            $withwithdrawalLists = Earning::where('userId',auth()->user()->id)->withTrashed()->get();
            $purchased =  Purchased::whereHas('song', function($buyer){
                $buyer->where('userId', auth()->user()->id);
            })->orWhereHas('album', function($buyer)
            {
                $buyer->where('userId', auth()->user()->id);
            })->get()->sum('amount');
            $totalWithdrawalAmount = $withdrawalEarning;
            $withdrawalPrice = number_format($purchased - $withdrawalEarning,1);
            $availableBalance = ($purchased - $withdrawalEarning);
            $card = Card::where('userId',auth()->user()->id)->first();
            return view('artist.withdrawalEarning.withdrawalEarning',['availableBalance'=>$availableBalance,'totalWithdrawalAmount'=>$totalWithdrawalAmount,'card'=>$card,'withdrawalPrice'=>$withdrawalPrice,'withwithdrawalLists'=>$withwithdrawalLists]);

        }catch (\Exception $e){
            $this->saveExceptionLog($e, 'Withdrawal earning show error exception');
            return back();
        }

    }

    public function withdrawalRequest()
    {
        try {
            $withdrawalEarning = Earning::where('userId',auth()->user()->id)->where('withdrawalStatus',WithdrawalStatusEnum::Paid)->get()->sum('amount');
            $withdrawalEarningRequest = Earning::where('userId',auth()->user()->id)->where('withdrawalStatus',WithdrawalStatusEnum::Requested)->get()->sum('amount');
            $purchased =  Purchased::whereHas('song', function($buyer){
                $buyer->where('userId', auth()->user()->id);
            })->orWhereHas('album', function($buyer)
            {
                $buyer->where('userId', auth()->user()->id);
            })->get()->sum('amount');
            $withdrawalPrice = ( ( floor($purchased * 100) - floor($withdrawalEarning * 100) ) / 100 );

            $currentWithdrawal =( ( floor($withdrawalPrice * 100) - floor($withdrawalEarningRequest * 100) ) / 100 );

            // Check Two Float point is equal
            if(abs($withdrawalPrice-$withdrawalEarningRequest) < 0.00001) {
                return response()->json([
                    'status' => 100,
                    'message' => 'You already request the Balance',
                ]);
            }else if($withdrawalEarningRequest){
                return response()->json([
                    'status' => 300,
                    'currentWithdrawal' => $currentWithdrawal
                ]);
            }else{
                $earning = Earning::create([
                    'userId' => auth()->user()->id,
                    'withdrawalStatus' => WithdrawalStatusEnum::Requested,
                    'amount' => $withdrawalPrice,
                ]);
                return response()->json([
                    'status' => 200,
                    'earning' => $earning,
                ]);
            }
        }catch (\Exception $e){
            $this->saveExceptionLog($e, 'Withdrawal earning Request error exception');
            return back();
        }

    }

    public function withdrawalRequestAgain()
    {
        try {
            $withdrawalEarning = Earning::where('userId',auth()->user()->id)->where('withdrawalStatus',WithdrawalStatusEnum::Paid)->get()->sum('amount');
            $withdrawalEarningRequest = Earning::where('userId',auth()->user()->id)->where('withdrawalStatus',WithdrawalStatusEnum::Requested)->get()->sum('amount');
            $purchased =  Purchased::whereHas('song', function($buyer){
                $buyer->where('userId', auth()->user()->id);
            })->orWhereHas('album', function($buyer)
            {
                $buyer->where('userId', auth()->user()->id);
            })->get()->sum('amount');
            $withdrawalPrice = ( ( floor($purchased * 100) - floor($withdrawalEarning * 100) ) / 100 );

            $currentWithdrawal =( ( floor($withdrawalPrice * 100) - floor($withdrawalEarningRequest * 100) ) / 100 );

            $earning = Earning::create([
                'userId' => auth()->user()->id,
                'withdrawalStatus' => WithdrawalStatusEnum::Requested,
                'amount' => $currentWithdrawal,
            ]);
            return response()->json([
                'status' => 200,
                'currentWithdrawal' => $currentWithdrawal
            ]);
        }catch (\Exception $exception){
            $this->saveExceptionLog($exception, 'Song show error exception');
            return back();
        }

    }
}
