<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Requests\Artist\Preorder\Album\PreorderAlbumAddRequest;
use App\Http\Requests\Artist\Preorder\Album\PreorderAlbumEditRequest;
use App\Http\Requests\Artist\Preorder\Album\PreorderAlbumSongAddRequest;
use App\Http\Requests\Artist\Preorder\Album\PreorderAlbumSongEditRequest;
use Illuminate\Support\Facades\DB;
use App\Repositories\Eloquent\AlbumPreorderRepository;


class AlbumPreorderController extends Controller
{
    use loggerExceptionTrait;
    public function __construct(AlbumPreorderRepository $albumPreorderRepository)
    {
        $this->albumPreorderRepository = $albumPreorderRepository;
    }

    public function store(PreorderAlbumAddRequest $request)
    {
        try {
            DB::beginTransaction();
            $album = $this->albumPreorderRepository->store($request);
            DB::commit();
            // After the changing retrun the Album to
            return response()->json([
                'status' => 200,
                'album' => $album,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Album preOrder store the song exception');
            return response()->json([
                'status' => 500,
                'message' => 'Fail to stored Album! '.$exception->getMessage(),
            ]);
        }
    }

    public function storeAlbumSong(PreorderAlbumSongAddRequest $request)
    {
        try{
            DB::beginTransaction();
            $song = $this->albumPreorderRepository->storeAlbumSong($request);
            DB::commit();
            if($request->modalStatus == 'editModal'){
                return response()->json([
                    'status' => 300,
                    'song' =>$song]);
            }else{
                return response()->json([
                    'status' => 200,
                    'song' =>$song]);
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Store Album the song exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to stored Song '.$exception->getMessage(),
            ]);
        }
    }

    public function updateAlbumSong(PreorderAlbumSongEditRequest $request)
    {
        try {
            DB::beginTransaction();
            $song = $this->albumPreorderRepository->updateAlbumSong($request);
            DB::commit();
            if($request->editModalStatus == 'AddModal'){
                return response()->json([
                    'status' => 200,
                    'song'=>$song]);
            }else{
                return response()->json([
                    'status' => 300,
                    'song'=>$song]);
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Update Album the song exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to Update the Song '.$exception->getMessage(),
            ]);
        }
    }

    public function removeAlbumSong($id)
    {
        try {
            DB::beginTransaction();
            // remove the song from the  album
            $this->albumPreorderRepository->removeAlbumSong($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message'=> 'Song is remove from List ',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Remove the song exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to remove song '.$exception->getMessage(),
            ]);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $this->albumPreorderRepository->delete($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message'=> 'Album is delete successfully',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Delete Album error exception');
            return response()->json([
                'status' =>  500,
                'message'=> 'Fail to delete Album '.$exception->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        try {
            $value = $this->albumPreorderRepository->show($id);
            return response()->json([
                'status' => 200,
                'album'=> $value[0],
                'songs'=> $value[1],
            ]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Album show error exception');
            return response()->json([
                'status' =>  500,
                'message'=> 'Fail to show Album '.$exception->getMessage(),
            ]);
        }
    }

    public function getPreorderSong($id)
    {
        $preorderAlbumSongs = $this->albumPreorderRepository->getPreorderSong($id);
        return response()->json([
            'status' => 200,
            'preorderAlbumSongs' => $preorderAlbumSongs,
        ]);
    }

    public function update(PreorderAlbumEditRequest $request)
    {
        try {
            DB::beginTransaction();
            $album = $this->albumPreorderRepository->update($request);
            DB::commit();
            // After the changing retrun the Album to
            return response()->json([
                'status' => 200,
                'album' => $album,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Update Album  exception');
            return response()->json([
                'status' => 500,
                'message' => 'Fail to update Album! '.$exception->getMessage(),
            ]);
        }
    }

    public function preorderAlbumPurchaseList($id)
    {
        try {
            $value = $this->albumPreorderRepository->preorderAlbumPurchaseList($id);
            return response()->json([
                'status' => 200,
                'album' => $value[0],
                'sum' => $value[1],
                'purchase' => $value[2],
                'albumSongs' => $value[3],
            ]);
        } catch (\Excpetion $exception) {
            $this->saveExceptionLog($exception, 'Album purchase list error exception');
            return response()->json([
                'status' => 500,
                'message' => 'Fail to show Album  detail! '.$exception->getMessage(),
            ]);
        }
    }
}
