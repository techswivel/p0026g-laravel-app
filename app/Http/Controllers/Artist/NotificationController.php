<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Requests\Artist\Notification\NotificationAddRequest;
use App\Models\Notification;
use App\Models\MonthlyNotificationCount;
use App\Enums\NotificationStatusEnum;
use App\Enums\NotificationTypeEnum;
use App\Enums\UserTypeNotificationEnum;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Repositories\Eloquent\NotificationRepository;


class NotificationController extends Controller
{
    use loggerExceptionTrait;
    
    protected $notificationRepository;
    
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function index(Request $request)
    {
        $artist = auth()->user();
        $isNotificationLimit = TRUE;
        $notifications = Notification::where('userId',auth()->user()->id)->get();
        $date = Carbon::now()->startofMonth();
        $notification = Notification::withTrashed()->where('userId',auth()->user()->id)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->count();
        $monthlyNotificationCount = MonthlyNotificationCount::first();
        if($monthlyNotificationCount){
            if($notification < $monthlyNotificationCount->totalNotificationCount){
                $isNotificationLimit = TRUE;
            }else{
                $isNotificationLimit = FALSE;
            }
        }
        return view('artist.Notification.notification',['artist'=>$artist,'notifications'=>$notifications,'isNotificationLimit'=>$isNotificationLimit]);
    }

    public function store(NotificationAddRequest $request)
    {
        try {
            DB::beginTransaction();
            $notification = $this->notificationRepository->store($request);
            DB::commit();
        } catch (\Exception $exception) {
             DB::rollBack();
            $this->saveExceptionLog($exception, 'notification sent error exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to stored Notification '.$exception->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();
            $notification = $this->notificationRepository->show($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'notification' => $notification]);
        } catch (\Exception $exception) {
             DB::rollBack();
            $this->saveExceptionLog($exception, 'notification show error exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show Notification '.$exception->getMessage(),
            ]);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $notification = $this->notificationRepository->delete($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message'=> 'Notification is delete successfully',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'notification delete error exception');
            return response()->json([
                'status' =>  500,
                'message'=> 'Fail to delete Notification '.$exception->getMessage(),
            ]);
        }
    }
}
