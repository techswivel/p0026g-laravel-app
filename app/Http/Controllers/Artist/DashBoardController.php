<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Models\Purchased;
use App\Models\User;
use App\Models\Song;
use App\Models\Album;
use App\Models\AlbumSongs;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    use loggerExceptionTrait;

    protected function getBuyerData()
    {
        return Purchased::with('song','album','user')->whereHas('getSongs', function($buyer)
        {
            $buyer->where('userId', auth()->user()->id);
        })->orWhereHas('getAlbums', function($buyer)
        {
            $buyer->where('userId', auth()->user()->id);
        });

    }

    public function index()
    {
        $purchases = $this->getBuyerData()->latest('id')->limit(5)->get();
        return view('artist.dashboard',['purchases'=>$purchases]);
    }

    public function purchaseDetail($id)
    {
        try {
            $purchase = Purchased::with('song','album','user')->find($id);
            $albumSongs = Song::withTrashed()->find(AlbumSongs::where('albumId',$purchase->albumId)->get('songId'));
            $price = $albumSongs->sum('price');
            return response()->json([
                'status' => 200,
                'purchase' => $purchase,
                'albumSongs' => $albumSongs,
                'price' => number_format($price, 2, '.', ''),
            ]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Purchase detail error exception');
            return response()->json([
                'status' => 500,
                'message' => 'Fail to show purchase detail '.$exception,
            ]);
        }

    }

    public function salesReport($report)
    {
        if($report == 'Last Week'){
            return $this->saleReportWeek();
        }elseif($report == 'Last month'){
            return $this->saleReportMonthly();
        }else{
            return $this->saleReport($report);
        }
    }

    protected function getArtistSong()
    {
        return Purchased::whereHas('getSong', function($buyer){
                    $buyer->where('userId', auth()->user()->id);
        });
    }

    protected function getArtistAlbum()
    {
        return Purchased::whereHas('getAlbum', function($buyer){
            $buyer->where('userId', auth()->user()->id);
        });
    }

    protected function saleReport($year)
    {
        try {
            $previousYearSales = 0;
            $date = Carbon::parse($year.'-01-01 00:00');
            $monthly = array();
            for ($x = 0; $x <= 11; $x++) {
                $artistSong = $this->getArtistSong()->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addMonth($x)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addMonth($x)->endOfMonth()->timestamp))->sum('amount') ;
                $artistAlbum = $this->getArtistAlbum()->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addMonth($x)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addMonth($x)->endOfMonth()->timestamp))->sum('amount') ;
                array_push($monthly,($artistSong + $artistAlbum));
            }
            $previousDate = Carbon::parse(($year-1).'-01-01 00:00');
            $previousYearSongSales = $this->getArtistSong()->whereBetween('createdAt', array(Carbon::createFromDate($previousDate->format('Y-m-d')." 00:00")->addMonth(0)->timestamp,Carbon::createFromDate($previousDate->format('Y-m-d')." 23:59")->addMonth(11)->endOfMonth()->timestamp))->sum('amount');
            $previousYearAlbumSales = $this->getArtistAlbum()->whereBetween('createdAt', array(Carbon::createFromDate($previousDate->format('Y-m-d')." 00:00")->addMonth(0)->timestamp,Carbon::createFromDate($previousDate->format('Y-m-d')." 23:59")->addMonth(11)->endOfMonth()->timestamp))->sum('amount');
            $previousYearSales = ($previousYearSongSales + $previousYearAlbumSales);
            return response()->json([
                    'status' => 200,
                    'monthly' =>$monthly,
                    'previousYearSales' => $previousYearSales,
                ]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Sale Report error exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to show sales report'.$exception->getMessage(),
            ]);
        }
    }

    protected function saleReportMonthly()
    {
        try {
            $previousMonthValue = 0;
            $day = array();
            $date = Carbon::now()->subMonth()->startofMonth();
            for ($x = 0; $x <= 24; $x++) {
                $y = $x + 4;
                $artistSong = $this->getArtistSong()->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay($x)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addDay($y)->timestamp))->sum('amount');
                $artistAlbum = $this->getArtistAlbum()->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay($x)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addDay($y)->timestamp))->sum('amount');
                array_push($day,($artistSong + $artistAlbum));
                $x = $y;
                if($x == 24){
                    $artistSong = $this->getArtistSong()->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay($x + 1)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->sum('amount');
                    $artistAlbum = $this->getArtistAlbum()->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay($x + 1)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->sum('amount');
                    array_push($day,($artistSong + $artistAlbum));
                }
            }
            $previousMonthdate = Carbon::now()->subMonth()->subMonth()->startofMonth();
            $previousMonthSongValue = $this->getArtistSong()->whereBetween('createdAt', array(Carbon::createFromDate($previousMonthdate->format('Y-m-d')." 00:00")->addDay(0)->timestamp,Carbon::createFromDate($previousMonthdate->format('Y-m-d')." 23:59")->addDay(30)->timestamp))->sum('amount');
            $previousMonthAlbumValue = $this->getArtistAlbum()->whereBetween('createdAt', array(Carbon::createFromDate($previousMonthdate->format('Y-m-d')." 00:00")->addDay(0)->timestamp,Carbon::createFromDate($previousMonthdate->format('Y-m-d')." 23:59")->addDay(30)->timestamp))->sum('amount');
            $previousMonthValue = ($previousMonthSongValue + $previousMonthAlbumValue);
            return response()->json([
            'status' => 200,
            'day' => $day,
            'previousMonthValue' => $previousMonthValue
        ]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Sale report monthly error exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show sales report '.$exception->getMessage(),
            ]);
        }
    }

    protected function saleReportWeek()
    {
        try {
            $date = Carbon::now()->subWeek()->startOfWeek();
            $weekDays = array();
            for ($x = 0; $x <= 6; $x++) {
                $artistSong = $this->getArtistSong()->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay($x)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addDay($x)->timestamp))->sum('amount');
                $artistAlbum = $this->getArtistAlbum()->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->addDay($x)->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->addDay($x)->timestamp))->sum('amount');
                array_push($weekDays,($artistSong + $artistAlbum));
            }
            $previousWeek = Carbon::now()->subWeek(1)->startOfWeek();
            $previousWeekSongValue = $this->getArtistSong()->whereBetween('createdAt', array(Carbon::createFromDate($previousWeek->format('Y-m-d')." 00:00")->addDay(0)->timestamp,Carbon::createFromDate($previousWeek->format('Y-m-d')." 23:59")->addDay(6)->timestamp))->sum('amount');
            $previousWeekAlbumValue = $this->getArtistAlbum()->whereBetween('createdAt', array(Carbon::createFromDate($previousWeek->format('Y-m-d')." 00:00")->addDay(0)->timestamp,Carbon::createFromDate($previousWeek->format('Y-m-d')." 23:59")->addDay(6)->timestamp))->sum('amount');
            $previousWeekValue = ($previousWeekSongValue + $previousWeekAlbumValue);
            return response()->json([
                'status' => 200,
                'weekDays' => $weekDays,
                'previousWeekValue' => $previousWeekValue,
            ]);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Sale report weekly exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to show sales report '.$exception->getMessage(),
            ]);
        }
    }
}
