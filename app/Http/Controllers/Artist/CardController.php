<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Requests\Artist\WithdrawalEarning\AccountAddRequest;
use App\Http\Requests\Artist\WithdrawalEarning\AccountEditRequest;
use App\Models\Card;
use Illuminate\Support\Facades\DB;



class CardController extends Controller
{
    use loggerExceptionTrait;
    public function store(AccountAddRequest $request)
    {
        try {
            DB::beginTransaction();
            $card = Card::create([
                'userId' => auth()->user()->id,
                'cardId' => $request->accountNumber,
                'cardHolderName' => $request->accountName,
                'lastDigits' => substr($request->accountNumber, -4),
            ]);
            DB::commit();
            return response()->json([
                'status' => 200,
                'card' => $card,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Card store error exception');
            return response()->json([
                'status' => 500,
                'message' => 'Fail to stored account! '.$exception->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        try {
            $card = Card::findOrFail($id);
            return response()->json([
                'status' => 200,
                'card' => $card,
            ]);
        } catch (\Excpetion $exception) {
            $this->saveExceptionLog($exception, 'Card show error exception');
            return response()->json([
                'status' => 500,
                'message' => 'Fail to show account! '.$exception->getMessage(),
            ]);
        }
    }

    public function update(AccountEditRequest $request)
    {
        try {
            DB::beginTransaction();
            $card = Card::where('id',$request->id)->update([
                'cardId' => $request->editAccountNumber,
                'cardHolderName' => $request->editAccountName,
                'lastDigits' => substr($request->editAccountNumber, -4),
            ]);
            DB::commit();
            return response()->json([
                'status' => 200,
                'card' => $card,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Card show error exception');
            return response()->json([
                'status' => 500,
                'message' => 'Fail to stored account! '.$exception->getMessage(),
            ]);
        }
    }
}
