<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Enums\PageTypeEnum;


class TermsAndConditionsController extends Controller
{
    public function index()
    {
        $term = Page::where('type',PageTypeEnum::TermAndCondition)->first();
        return view('artist.termsAndCondition',['term'=>$term]);
    }
}
