<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Artist\Song\SongAddRequest;
use App\Http\Requests\Artist\Song\SongEditRequest;
use App\Repositories\Eloquent\SongRepository;
use Exception;

class SongController extends Controller
{
    use loggerExceptionTrait;
    protected $songRepository;
    public function __construct(SongRepository $songRepository)
    {
        $this->songRepository = $songRepository;
    }

    public function store(SongAddRequest $request)
    {
        try{
            DB::beginTransaction();

            $song = $this->songRepository->store($request);
            DB::commit();
            return response()->json($song);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Song artist store error exception');
            return response()->json([
                    'status' => 500,
                    'message'=> 'Fail to stored Song '.$exception->getMessage(),
            ]);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $this->songRepository->delete($id);
            DB::commit();
            return response()->json([
                'status' => 200,
                'message'=> 'Song is delete successfully',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Song delete error exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to delete Song '.$exception->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        try {
            $song = $this->songRepository->show($id);
            return response()->json($song);
        } catch (\Exception $exception) {
            $this->saveExceptionLog($exception, 'Song show exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to show Song '.$exception->getMessage(),
            ]);
        }
    }


    public function update(SongEditRequest $request)
    {
        try {
            DB::beginTransaction();
            $song = $this->songRepository->update($request);
            DB::commit();
            return response()->json($song);
        } catch (Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Update song error exception');
            return response()->json([
                'status' => 500,
                'message'=> 'Fail to Update the Song '.$exception->getMessage(),
            ]);
        }
    }

}
