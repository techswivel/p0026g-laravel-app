<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\Language;
use App\Models\Category;
use App\Models\Song;
use App\Models\User;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Models\Follower;
use App\Models\Notification;
use App\Models\Purchased;
use App\Traits\FileUploadTrait;
use App\Http\Requests\Artist\UserProfileRequest;
use Illuminate\Support\Facades\DB;
use App\Enums\SongPreOrderEnum;
use App\Enums\AlbumPreOrderEnum;

class ArtistProfileController extends Controller
{
    use FileUploadTrait, loggerExceptionTrait;
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function dashboard()
    {
        toastr()->success('Welcome back!', 'Login', ['timeOut' => 5000]);
        return view('artist.dashboard');
    }
    public function index()
    {
        $artist = auth()->user();
        $isNotificationLimit = TRUE;
        $role = auth()->user()->getRoleNames()->first();
        $selectsongs = Song::where('userId', auth()->user()->id)->doesntHave("songs")->get();
        $songs = Song::with(['getArtist', 'getSongRelation'])->where('userId', auth()->user()->id)->where('type', SongPreOrderEnum::Song)->get();
        $categories = Category::all();
        $languages = Language::all();
        $notifications = Notification::where('userId', auth()->user()->id)->get();
        $followers = Follower::with('user')->where('artistId', auth()->user()->id)->get();
        $albums = Album::with(['getSongRelation', 'getArtist'])->where('userId', auth()->user()->id)->where('type', AlbumPreOrderEnum::Album)->get();
        $purchases = Purchased::with('song', 'album', 'user')->whereHas('getSongs', function ($buyer) {
            $buyer->where('userId', auth()->user()->id);
        })->orWhereHas('getAlbums', function ($buyer) {
            $buyer->where('userId', auth()->user()->id);
        })->get();
        $preorderAlbumSongs = Song::where('userId', auth()->user()->id)->where('type', SongPreOrderEnum::PreOrderAlbum)->doesntHave("songs")->get();
        $preorderSongs = Song::where('userId', auth()->user()->id)->where('type', SongPreOrderEnum::PreOrder)->get();
        $preorderAlbums = Album::where('userId', auth()->user()->id)->where('type', AlbumPreOrderEnum::PreOrder)->get();

        return view('artist.artist-profile', ['role' => $role, 'artist' => $artist, 'albums' => $albums, 'categories' => $categories, 'languages' => $languages, 'songs' => $songs, 'followers' => $followers, 'selectsongs' => $selectsongs, 'purchases' => $purchases, 'preorderSongs' => $preorderSongs, 'preorderAlbumSongs' => $preorderAlbumSongs, 'preorderAlbums' => $preorderAlbums, 'notifications' => $notifications, 'isNotificationLimit' => $isNotificationLimit]);
    }

    /**
     * Validate and update the given user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    public function update(UserProfileRequest $input)
    {
        try {
            DB::beginTransaction();
            $user = auth()->user();
            $name = explode(" ", $input['name']);
            $firstName = $name[0];
            $secondName = ' ';

            if (str_replace($firstName, "", $input['name'])) {
                $secondName = ltrim(str_replace($firstName, "", $input['name']));
            }

            $avatar = NULL;

            if (request()->hasFile('avatar')) {
                $path = 'artist/image/' . auth()->user()->id . '/avatar';
                $file = request()->file('avatar');
                $avatar = $this->uploadFile($file, $path);
            } else {
                $avatar = $user->avatar;
            }

            if ( $input['email'] !== $user->email && $user instanceof MustVerifyEmail ) {
                $this->updateVerifiedUser($firstName, $secondName, $avatar, $input);
            } else {
                User::where("id", auth()->user()->id)->update([
                    'firstName' => $firstName,
                    'lastName' => $secondName,
                    'aboutArtist' => $input['aboutArtist'],
                    'avatar' => $avatar,
                ]);
            }

            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'User is update successfully',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Artist profile error exception');
            return response()->json([
                'status' => 200,
                'message' => 'Fail to update User ' . $exception,
            ]);
        }
    }

    /**
     * Update the given verified user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    protected function updateVerifiedUser($firstName, $secondName, $avatar, $input)
    {
        try {
            DB::beginTransaction();
            $user = auth()->user();
            User::where("id", auth()->user()->id)->update([
                'firstName' => $firstName,
                'lastName' => $secondName,
                'aboutArtist' => $input['aboutArtist'],
                'avatar' => $avatar,
                'email_verified_at' => null,
            ]);
            $user->sendEmailVerificationNotification();
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'User is update successfully',
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $this->saveExceptionLog($exception, 'Artist update verified user error exception');
            return response()->json([
                'status' => 200,
                'message' => 'Fail to update User ' . $exception,
            ]);
        }
    }
}
