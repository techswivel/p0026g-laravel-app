<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Enums\PageTypeEnum;

class PrivacyAndPolicyController extends Controller
{
    public function index()
    {
        $policy = Page::where('type',PageTypeEnum::PrivacyPolicy)->first();
        return view('artist.privacyAndPolicy',['policy'=>$policy]);
    }
}
