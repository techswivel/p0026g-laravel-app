<?php

namespace App\Http\Controllers\Api;

use App\Enums\ListeningTypeEnum;
use App\Enums\SongPreOrderEnum;
use App\Http\Requests\Api\Song\NextSongPlayListRequest;
use App\Http\Requests\Api\Song\SetFavoriteSongRequest;
use App\Http\Requests\Api\Song\SyncRecentlyPlayedRequest;
use App\Http\Resources\RecentlyPlayed\RecentlyPlayedCollection;
use App\Http\Resources\Song\NextPlaySongCollection;
use App\Http\Resources\Song\NextPlaySongResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Traits\loggerExceptionTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Song\SongRequest;
use App\Http\Requests\Api\Song\UpdateDownloadedSongsListRequest;
use App\Http\Resources\Song\SongResource;
use App\Http\Resources\Song\SongWithMetaCollection;
use App\Http\Resources\Song\SongWithMetaResource;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Models\Category;
use App\Models\DownloadedSongs;
use App\Models\Favorite;
use App\Models\ListeningHistory;
use App\Models\Playlist;
use App\Models\PlaylistSongs;
use App\Models\Purchased;
use App\Models\Song;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Traits\PaginationTrait;

class SongController extends Controller
{
    use loggerExceptionTrait, PaginationTrait;

    public function songs(SongRequest $request)
    {
        try {

            $limit = $request->limit;
            $paginate = $request->page;
            $songIds = [];
            // "TRENDING",
            if ($request->type == "TRENDING") {
                $songIds = ListeningHistory::select('songId', DB::raw('count(*) as songCount'))
                    ->where('type', ListeningTypeEnum::Song)
                    ->whereNotNull('songId')->groupBy('songId')
                    ->orderBy('songCount', 'DESC')->limit(30)
                    ->pluck('songId')->toArray();

            }
            // "PLAY_LIST",
            if ($request->type == "PLAY_LIST") {
                $checkPlaylist = Playlist::where('id', $request->playListId)->where('userId', Auth::user()->id)->count();
                if ($checkPlaylist == 0) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Playlist not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $songIds = PlaylistSongs::where('playlistId', $request->playListId)
                    ->whereHas('getPlaylist', function ($q) {
                        $q->where('userId', Auth::user()->id);
                    })
                    ->pluck('songId')->toArray();
            }

            // "FAVORITES",
            if ($request->type == "FAVOURITES") {
                $songIds = Favorite::where('userId', Auth::user()->id)->pluck('songId')->toArray();
            }
            // "PURCHASED",
            if ($request->type == "PURCHASED") {
                $songIds = Purchased::where('userId', Auth::user()->id)->whereNotNull('songId')->pluck('songId')->toArray();
                $userIds = Song::whereIn('id', $songIds)->pluck('userId')->toArray();
                $artistCheckBlock=User::whereIn('id',$userIds)->where('status','!=','Block')->withTrashed()->pluck('id')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->whereIn('userId',$artistCheckBlock)->orderBy('name','ASC')->withTrashed()->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new SongWithMetaCollection($songs)]], JsonResponse::HTTP_OK);
            }
            // "ALBUM",
            if ($request->type == "ALBUM") {
                $checkAlbum = Album::where('id', $request->albumId)->where('status','!=','PENDING')->count();
                if ($checkAlbum == 0) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Album not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $songIds = AlbumSongs::where('albumId', $request->albumId)
                    ->pluck('songId')->toArray();
            }
            // "DOWNLOADED"
            if ($request->type == "DOWNLOADED") {
                $songIds = DownloadedSongs::where('userId', Auth::user()->id)->where('isDownloaded', 1)
                    ->pluck('songId')->toArray();
                $songsCheckUser=Song::whereIn('id',$songIds)->where('status','!=','PENDING')->pluck('userId')->toArray();
                $userCheck=User::role('artist')->whereIn('id',$songsCheckUser)->where('status','!=','Block')->pluck('id')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->whereIn('userId',$userCheck)->where('status','!=','PENDING')->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new SongWithMetaCollection($songs)]], JsonResponse::HTTP_OK);

            }

            // "CATEGORY_ARTIST_SONGS"
            if ($request->type == "CATEGORY_ARTIST_SONGS") {
                $getCategory = Category::find($request->categoryId);
                if (!$getCategory) {
                    return response()->json(['response' => ['status' => false, 'message' => "Category not found."]], JsonResponse::HTTP_OK);
                }
                $getArtist = User::where('status','!=','Block')->find($request->artistId);
                if (!$getArtist) {
                    return response()->json(['response' => ['status' => false, 'message' => "Artist not found."]], JsonResponse::HTTP_OK);
                }
                $recommendedSongs = Song::with('category', 'language')
                    ->where('categoryId', $request->categoryId)
                    ->where('userId', $request->artistId)->orderBy('name','ASC')->get();
                return response()->json(['response' => ['status' => true, 'data' => [
                    'totalSongs' => (count($recommendedSongs)),
                    'songs' => SongResource::collection($recommendedSongs),
                ]]], JsonResponse::HTTP_OK);
            }
            $songsCheckUser=Song::whereIn('id',$songIds)->pluck('userId')->toArray();
            $userCheck=User::role('artist')->whereIn('id',$songsCheckUser)->where('status','!=','Block')->pluck('id')->toArray();
            $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->whereIn('userId',$userCheck)->where('status','!=','PENDING')->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
            return response()->json(['response' => ['status' => true, 'data' => new SongWithMetaCollection($songs)]], JsonResponse::HTTP_OK);
        } catch (Exception $e) {
            $this->saveExceptionLog($e, 'Songs  error exception');
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function downloadedSongs(UpdateDownloadedSongsListRequest $request)
    {
        try {
            $user = request()->user();

            if ($request->type == "ADD") {
                if ($user->isSubscribed==null || $user->isSubscribed==0){
                    return response()->json(['response' => ['status' => false, 'message' => 'Purchase subscription to download song!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $checkSong = Song::where('id', $request->songId)->count();
                if ($checkSong == 0) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Song not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $downloadSong = DownloadedSongs::where('userId', $user->id)
                    ->where('songId', $request->songId)->first();
                if ($downloadSong) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Song aleady downloaded!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                DB::beginTransaction();
                $downloadSong = DownloadedSongs::create([
                    'userId' => $user->id,
                    'songId' => $request->songId
                ]);
                DB::commit();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            }

            if ($request->type == "REMOVE") {
                $downloadedSong = DownloadedSongs::where('userId', $user->id)
                    ->where('songId', $request->songId)->first();
                if ($downloadedSong != null) {
                    $downloadedSong->delete();
                    return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
                } else {
                    return response()->json(['response' => ['status' => false, 'message' => 'Song not found in downloaded list!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
            return response()->json(['response' => ['status' => false, 'message' => 'Selected type not correct!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Download song exception');
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function nextPlaySongList(NextSongPlayListRequest $request)
    {
        try {
            $limit = $request->limit;
            $paginate = $request->page;
            $songIds = [];
            $currentSong = Song::where('id', $request->currentSongId)->withTrashed()->first();
            if (empty($currentSong)) {
                return response()->json(['response' => ['status' => false, 'message' => 'Song not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            if ($request->playedFrom == "PLAYLIST") {
                $checkPlaylist = Playlist::where('id', $request->playListId)->count();
                if ($checkPlaylist == 0) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Playlist not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $currentPlayListSong = PlaylistSongs::where('playListId', $request->playListId)->where('songId', $currentSong->id)->withTrashed()->first();
                if (empty($currentPlayListSong)) {
                    return response()->json(['response' => ['status' => false, 'message' => 'This song not added in the Playlist!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $playlistSongIds = PlaylistSongs::where('playlistId', $request->playListId)->orderBy('songId','ASC')->where('songId', '>', $currentPlayListSong->songId)->limit(3)->withTrashed()->pluck('id')->toArray();
                $songIds = PlaylistSongs::whereIn('id', $playlistSongIds)->orderBy('songId','ASC')->pluck('songId')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new NextPlaySongCollection($songs)]], JsonResponse::HTTP_OK);
            } elseif ($request->playedFrom == "ALBUM") {
                $checkAlbum = Album::where('id', $request->albumId)->count();
                if ($checkAlbum == 0) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Album not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $currentAlbumSong = AlbumSongs::where('albumId', $request->albumId)->where('songId', $currentSong->id)->first();
                if (empty($currentAlbumSong)) {
                    return response()->json(['response' => ['status' => false, 'message' => 'This song not added in the Album!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                //album check artist block pending
                $albumCheckArtist=Album::where('id',$request->albumId)->pluck('userId')->first();
                $artistCheckBlock= User::where('id',$albumCheckArtist)->where('status','Block')->pluck('id')->first();
               if ($artistCheckBlock){
                   return response()->json(['response' => ['status' => true, 'data' => 'Artist is blocked']], JsonResponse::HTTP_OK);
               }
                $songIds = AlbumSongs::where('albumId', $request->albumId)->orderBy('songId','ASC')->where('songId', '>', $currentAlbumSong->songId)->withTrashed()->limit(3)->pluck('songId')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);

                return response()->json(['response' => ['status' => true, 'data' => new NextPlaySongCollection($songs)]], JsonResponse::HTTP_OK);
            } elseif ($request->playedFrom == "CATEGORY") {
                $checkCategory = Category::where('id', $request->categoryId)->count();
                if ($checkCategory == 0) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Category not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $songIds = Song::where('categoryId', $request->categoryId)->orderBy('name','ASC')->where('id', '>', $currentSong->id)->limit(3)->pluck('id')->toArray();
                $songsCheckUser=Song::whereIn('id',$songIds)->pluck('userId')->toArray();
                $userCheck=User::role('artist')->whereIn('id',$songsCheckUser)->where('status','!=','Block')->pluck('id')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->whereIn('userId',$userCheck)->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new NextPlaySongCollection($songs)]], JsonResponse::HTTP_OK);
            } elseif ($request->playedFrom == "FAVOURITE") {
                $currentFavorite = Favorite::where('songId', $currentSong->id)->where('userId', auth()->user()->id)->withTrashed()->first();
                if (empty($currentFavorite)) {
                    return response()->json(['response' => ['status' => false, 'message' => 'This song not added in your favourite list!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $favouriteIds = Favorite::where('userId', auth()->user()->id)->orderBy('songId','ASC')->where('songId', '>', $currentFavorite->songId)->withTrashed()->limit(3)->pluck('id')->toArray();
                $songIds = Favorite::whereIn('id', $favouriteIds)->orderBy('songId','ASC')->pluck('songId')->toArray();
                $songsCheckUser=Song::whereIn('id',$songIds)->pluck('userId')->toArray();
                $userCheck=User::role('artist')->whereIn('id',$songsCheckUser)->where('status','!=','Block')->pluck('id')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->whereIn('userId',$userCheck)->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new NextPlaySongCollection($songs)]], JsonResponse::HTTP_OK);
            } elseif ($request->playedFrom == "PURCHASED_SONG") {
                $currentPurchased = Purchased::where('songId', $currentSong->id)->where('userId', auth()->user()->id)->first();
                if (empty($currentPurchased)) {
                    return response()->json(['response' => ['status' => false, 'message' => 'This song not added in your purchased list!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $songIds = Purchased::where('userId', auth()->user()->id)->orderBy('songId','ASC')->where('songId', '>', $currentPurchased->songId)->limit(3)->pluck('songId')->toArray();
                $songsCheckUser=Song::whereIn('id',$songIds)->pluck('userId')->toArray();
                $userCheck=User::role('artist')->whereIn('id',$songsCheckUser)->where('status','!=','Block')->withTrashed()->pluck('id')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->whereIn('userId',$userCheck)->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new NextPlaySongCollection($songs)]], JsonResponse::HTTP_OK);
            } elseif ($request->playedFrom == "PURCHASED_ALBUM") {
                $currentPurchased = Purchased::where('albumId', $currentSong->id)->where('userId', auth()->user()->id)->first();
                if (empty($currentPurchased)) {
                    return response()->json(['response' => ['status' => false, 'message' => 'This song not added in your purchased list!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $songIds = Purchased::where('userId', auth()->user()->id)->where('albumId', $request->albumId)->whereNotNull('songId')->orderBy('songId','ASC')->where('songId', '>', $currentPurchased->songId)->limit(3)->pluck('songId')->toArray();
                $songsCheckUser=Song::whereIn('id',$songIds)->pluck('userId')->toArray();
                $userCheck=User::role('artist')->whereIn('id',$songsCheckUser)->where('status','!=','Block')->withTrashed()->pluck('id')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->whereIn('userId',$userCheck)->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new NextPlaySongCollection($songs)]], JsonResponse::HTTP_OK);
            } elseif ($request->playedFrom == "DOWNLOADED") {
                $currentDownloadedSongs = DownloadedSongs::where('songId', $currentSong->id)->where('userId', auth()->user()->id)->withTrashed()->first();
                if (empty($currentDownloadedSongs)) {
                    return response()->json(['response' => ['status' => false, 'message' => 'This song not added in your download list!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $downloadedIds = DownloadedSongs::where('userId', auth()->user()->id)->where('isDownloaded', 1)->orderBy('songId','ASC')->where('songId', '>', $currentDownloadedSongs->songId)->withTrashed()->limit(3)->pluck('id')->toArray();
                $songIds = DownloadedSongs::whereIn('id', $downloadedIds)->orderBy('songId','ASC')->pluck('songId')->toArray();
                $songsCheckUser=Song::whereIn('id',$songIds)->pluck('userId')->toArray();
                $userCheck=User::role('artist')->whereIn('id',$songsCheckUser)->where('status','!=','Block')->pluck('id')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->whereIn('userId',$userCheck)->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new NextPlaySongCollection($songs)]], JsonResponse::HTTP_OK);
            } elseif ($request->playedFrom == "SEARCHED") {
                $songIds = Song::where('categoryId', $currentSong->categoryId)->orderBy('id','ASC')->where('id', '>', $currentSong->id)->limit(3)->withTrashed()->pluck('id')->toArray();
                $songsCheckUser=Song::whereIn('id',$songIds)->pluck('userId')->toArray();
                $userCheck=User::role('artist')->whereIn('id',$songsCheckUser)->where('status','!=','Block')->pluck('id')->toArray();
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->whereIn('userId',$userCheck)->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new NextPlaySongCollection($songs)]], JsonResponse::HTTP_OK);
            } elseif ($request->playedFrom == "EMPTY") {
                return response()->json(['response' => ['status' => true, 'data' => ['songs' => []]]], JsonResponse::HTTP_OK);
            } elseif ($request->playedFrom == "ARTIST_SONGS") {
                $artistCheck=User::role('artist')->where('id',$request->artistId)->where('status','!=','Block')->first();
                if (!$artistCheck) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Artist not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $songs = Song::where('type',SongPreOrderEnum::Song)->whereIn('id', $songIds)->where('userId',$artistCheck->id)->orderBy('name','ASC')->paginate($limit, ['*'], 'page', $paginate);
                return response()->json(['response' => ['status' => true, 'data' => new NextPlaySongCollection($songs)]], JsonResponse::HTTP_OK);
            } else {
                return response()->json(['response' => ['status' => false, 'message' => 'Value of playedFrom is not correct!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Next play song error exception');
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function setFavouriteSong(SetFavoriteSongRequest $request)
    {
        try {
            $user = request()->user();
            if ($request->isFavourite == true) {
                $song = Song::find($request->songId);
                if($song){
                    if($song->isPaid == 1){
                        $albumIdOfSong = AlbumSongs::where('songId',$request->songId)->first();
                        if($albumIdOfSong) {
                            $purchased = Purchased::where('userId',$user->id)->where('albumId',$albumIdOfSong->albumId)->first();
                            if(!$purchased) {
                                $purchased = Purchased::where('userId',$user->id)->where('songId',$request->songId)->first();
                                if(!$purchased){
                                    return response()->json(['response' => ['status' => false, 'message' => 'Song is not purcahsed!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                                }
                            }
                        }
                    }
                }else{
                    return response()->json(['response' => ['status' => false, 'message' => 'Song not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $favoriteSong = Favorite::where('userId', $user->id)
                    ->where('songId', $request->songId)->first();
                if ($favoriteSong) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Song aleady in favourite list!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                DB::beginTransaction();
                $favoriteSong = Favorite::create([
                    'userId' => $user->id,
                    'songId' => $request->songId
                ]);
                DB::commit();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            }

            if ($request->isFavourite == false) {
                $favoriteSong = Favorite::where('userId', $user->id)
                    ->where('songId', $request->songId)->first();
                if ($favoriteSong != null) {
                    $favoriteSong->delete();
                    return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
                } else {
                    return response()->json(['response' => ['status' => false, 'message' => 'Song not found in favourite list!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
            return response()->json(['response' => ['status' => false, 'message' => 'Selected is favorit value not correct!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Set favourite song error exception');
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
    public function syncRecentlyPlayed(SyncRecentlyPlayedRequest $request){
        try {
        DB::beginTransaction();
        $songsIds = array();
        $artistIds = array();
        $albumIds = array();
        $recentlySongsIds = array();
        $recentlyArtistIds = array();
        $recentlyAlbumIds = array();
        $songs = array();
        $data = array();
        $isListeningHistorySongsIds = array();
        $isListeningHistoryArtistIds = array();
        $isListeningHistoryAlbumIds = array();
        $data['isRecentlyPlayed'] = $request->isRecentlyPlayed;
        $data['isListeningHistory'] = $request->isListeningHistory;
        if($request->isRecentlyPlayed == false && $request->isListeningHistory == false){
            $recentlyHistories =ListeningHistory::where('isRecentlyPlayed',1)->where('userId',auth()->user()->id)->latest()->take(5)->get();
            if(!empty($recentlyHistories)){
                foreach($recentlyHistories as $recentlyHistory){
                    $songsIds[] = $recentlyHistory->songId;
                    $artistIds[] = $recentlyHistory->artistId;
                    $albumIds[] = $recentlyHistory->albumId;
                }
            }
            $listeningHistories =ListeningHistory::where('isListeningHistory',1)->where('userId',auth()->user()->id)->latest()->get();
            if(!empty($listeningHistories)){
                foreach($listeningHistories as $listeningHistory){
                    $isListeningHistorySongsIds[] = $listeningHistory->songId;
                    $isListeningHistoryArtistIds[] = $listeningHistory->artistId;
                    $isListeningHistoryAlbumIds[] = $listeningHistory->albumId;
                }
            }
            $songListening = Song::whereIn('id',$songsIds)->get();
            $checkArtistBlock=User::whereIn('id',$songListening)->where('status','!=','Block')->pluck('id')->toArray();
            $songs = Song::whereIn('id',$songsIds)->whereIn('userId',$checkArtistBlock)->get();
            $artists = User::whereIn('id',$artistIds)->where('status','!=','Block')->get();
            $albumArtist = Album::whereIn('id',$albumIds)->pluck('userId')->toArray();
            $checkBlock=User::whereIn('id',$albumArtist)->where('status','!=','Block')->pluck('id')->toArray();
            $albums = Album::whereIn('id',$albumIds)->where('userId',$checkBlock)->get();
            $isListeningHistorySonguserId = Song::whereIn('id',$isListeningHistorySongsIds)->pluck('userId')->toArray();
            $checkBlockSong=User::whereIn('id',$isListeningHistorySonguserId)->where('status','!=','Block')->pluck('id')->toArray();
            $isListeningHistorySongs = Song::whereIn('id',$isListeningHistorySongsIds)->whereIn('userId',$checkBlockSong)->get();
            $isListeningHistoryArtists = User::whereIn('id',$isListeningHistoryArtistIds)->where('status','!=','Block')->get();
            $isListeningHistoryAlbumArtist = Album::whereIn('id',$isListeningHistoryAlbumIds)->pluck('userId')->toArray();
            $checkBlockAlbumListing=User::whereIn('id',$isListeningHistoryAlbumArtist)->where('status','!=','Block')->pluck('id')->toArray();
            $isListeningHistoryAlbums = Album::whereIn('id',$isListeningHistoryAlbumIds)->whereIn('userId',$checkBlockAlbumListing)->get();
            $data['artists'] = $artists;
            $data['songs'] = $songs;
            $data['albums'] = $albums;
            $data['isListeningHistorySongs'] = $isListeningHistorySongs;
            $data['isListeningHistoryArtists'] = $isListeningHistoryArtists;
            $data['isListeningHistoryAlbums'] = $isListeningHistoryAlbums;
        }
        if($request->isRecentlyPlayed == true && $request->isListeningHistory == false) {
            if(!empty($request->recentlyPlayed)) {
                foreach ($request->recentlyPlayed as $recentlyPlayed) {
                    if (array_key_exists('songId', $recentlyPlayed) && array_key_exists('albumId', $recentlyPlayed) && array_key_exists('artistId', $recentlyPlayed)) {
                        $song = Song::where('id', $recentlyPlayed['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$recentlyPlayed['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $artist = User::where('id',$recentlyPlayed['artistId'])->first();
                        if(!$artist){
                            return response()->json(['response' => ['status' => false, 'message' => 'Artist not found against artistId = '.$recentlyPlayed['artistId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $album = Album::where('id',$recentlyPlayed['albumId'])->first();
                        if(!$album){
                            return response()->json(['response' => ['status' => false, 'message' => 'Album not found against albumId = '.$recentlyPlayed['albumId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song && $artist && $album){
                            $listeningHistory = ListeningHistory::where(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "albumId" => $recentlyPlayed['albumId'], "artistId" => $recentlyPlayed['artistId'], "isRecentlyPlayed" => 1])->first();
                            if($listeningHistory){
                                $listeningHistory->updatedAt = Carbon::now();
                                $listeningHistory->save();
                            }else{
                                ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "albumId" => $recentlyPlayed['albumId'], "artistId" => $recentlyPlayed['artistId'], "isRecentlyPlayed" => 1]);
                            }
                        }
                    }
                    elseif (array_key_exists('songId', $recentlyPlayed) && array_key_exists('albumId', $recentlyPlayed)) {
                        $song = Song::where('id', $recentlyPlayed['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$recentlyPlayed['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $album = Album::where('id',$recentlyPlayed['albumId'])->first();
                        if(!$album){
                            return response()->json(['response' => ['status' => false, 'message' => 'Album not found against albumId = '.$recentlyPlayed['albumId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song && $album) {
                            $listeningHistory = ListeningHistory::where(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "artistId" => null,"albumId" => $recentlyPlayed['albumId'], "isRecentlyPlayed" => 1])->first();
                            if ($listeningHistory) {
                                $listeningHistory->updatedAt = Carbon::now();
                                $listeningHistory->save();
                            } else {
                                ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "albumId" => $recentlyPlayed['albumId'], "isRecentlyPlayed" => 1]);
                            }
                        }
                    }
                    elseif (array_key_exists('songId', $recentlyPlayed)) {
                        $song = Song::where('id', $recentlyPlayed['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$recentlyPlayed['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song) {
                            $listeningHistory = ListeningHistory::where(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "artistId" => null,"albumId" => null, "isRecentlyPlayed" => 1])->first();
                            if ($listeningHistory) {
                                $listeningHistory->updatedAt = Carbon::now();
                                $listeningHistory->save();
                            } else {
                                ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "isRecentlyPlayed" => 1]);
                            }
                        }
                    }
                    if ($recentlyPlayed['songId']) {
                        $songsIds[] = $recentlyPlayed['songId'];
                    }
                    if (array_key_exists('artistId', $recentlyPlayed)) {
                        $artistIds[] = $recentlyPlayed['artistId'];
                    }
                    if (array_key_exists('albumId', $recentlyPlayed)) {
                        $albumIds[] = $recentlyPlayed['albumId'];
                    }
                }
            }
            $listeningHistories =ListeningHistory::where('isListeningHistory',1)->where('userId',auth()->user()->id)->latest()->get();
            if(!empty($listeningHistories)){
                foreach($listeningHistories as $listeningHistory){
                    $isListeningHistorySongsIds[] = $listeningHistory->songId;
                    $isListeningHistoryArtistIds[] = $listeningHistory->artistId;
                    $isListeningHistoryAlbumIds[] = $listeningHistory->albumId;
                }
            }
            $listeningSongCheckSong = Song::whereIn('id',$isListeningHistorySongsIds)->pluck('userId')->toArray();
            $checkArtistBlock=User::whereIn('id',$listeningSongCheckSong)->where('status','!=','Block')->pluck('id')->toArray();
            $listeningSongs = Song::whereIn('id',$isListeningHistorySongsIds)->whereIn('userId',$checkArtistBlock)->get();
            $listeningArtists = User::whereIn('id',$isListeningHistoryArtistIds)->where('status','!=','Block')->get();
            $listeningAlbumCheck = Album::whereIn('id',$isListeningHistoryAlbumIds)->pluck('userId')->toArray();
            $checkArtistAlbumBlock=User::whereIn('id',$listeningAlbumCheck)->where('status','!=','Block')->pluck('id')->toArray();
            $listeningAlbums = Album::whereIn('id',$isListeningHistoryAlbumIds)->whereIn('userId',$checkArtistAlbumBlock)->get();
            $songListening = Song::whereIn('id',$songsIds)->pluck('userId')->toArray();
            $checkArtistAlbumBlock=User::whereIn('id',$songListening)->where('status','!=','Block')->pluck('id')->toArray();
            $songs = Song::whereIn('id',$songsIds)->whereIn('userId',$checkArtistAlbumBlock)->get();
            $artists = User::whereIn('id',$artistIds)->where('status','!=','Block')->get();
            $albumCheck = Album::whereIn('id',$albumIds)->pluck('userId')->toArray();
            $checkAlbum=User::whereIn('id',$albumCheck)->where('status','!=','Block')->pluck('id')->toArray();
            $albums = Album::whereIn('id',$albumIds)->whereIn('userId',$checkAlbum)->get();
            $data['songs'] = $songs;
            $data['artists'] = $artists;
            $data['albums'] = $albums;
            $data['isListeningHistorySongs'] = $listeningSongs;
            $data['isListeningHistoryArtists'] = $listeningArtists;
            $data['isListeningHistoryAlbums'] = $listeningAlbums;
        }
        if($request->isListeningHistory == true && $request->isRecentlyPlayed == false){
            if(!empty($request->listeningHistory)) {
                foreach ($request->listeningHistory as $listeningHistory) {
                    if (array_key_exists('songId', $listeningHistory) && array_key_exists('albumId', $listeningHistory) && array_key_exists('artistId', $listeningHistory)) {
                        $song = Song::where('id', $listeningHistory['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$listeningHistory['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $artist = User::where('id',$listeningHistory['artistId'])->first();
                        if(!$artist){
                            return response()->json(['response' => ['status' => false, 'message' => 'Artist not found against artistId = '.$listeningHistory['artistId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $album = Album::where('id',$listeningHistory['albumId'])->first();
                        if(!$album){
                            return response()->json(['response' => ['status' => false, 'message' => 'Album not found against albumId = '.$listeningHistory['albumId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song && $artist && $album) {
                            ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $listeningHistory['songId'], "albumId" => $listeningHistory['albumId'], "artistId" => $listeningHistory['artistId'], "isListeningHistory" => 1]);
                        }
                    }
                    elseif (array_key_exists('songId', $listeningHistory) && array_key_exists('albumId', $listeningHistory)) {
                        $song = Song::where('id', $listeningHistory['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$listeningHistory['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $album = Album::where('id',$listeningHistory['albumId'])->first();
                        if(!$album){
                            return response()->json(['response' => ['status' => false, 'message' => 'Album not found against albumId = '.$listeningHistory['albumId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song && $album) {
                            ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $listeningHistory['songId'], "albumId" => $listeningHistory['albumId'], "isListeningHistory" => 1]);
                        }
                    }
                    elseif (array_key_exists('songId', $listeningHistory)) {
                        $song = Song::where('id', $listeningHistory['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$listeningHistory['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song) {
                            ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $listeningHistory['songId'], "isListeningHistory" => 1]);
                        }
                    }
                    if ($listeningHistory['songId']) {
                        $isListeningHistorySongsIds[] = $listeningHistory['songId'];
                    }
                    if (array_key_exists('artistId', $listeningHistory)) {
                        $isListeningHistoryArtistIds[] = $listeningHistory['artistId'];
                    }
                    if (array_key_exists('albumId', $listeningHistory)) {
                        $isListeningHistoryAlbumIds[] = $listeningHistory['albumId'];
                    }
                    if (!array_key_exists('albumId', $listeningHistory)) {
                        $listeningHistorySong = Song::where('id', $listeningHistory['songId'])->first();
                        if($listeningHistorySong){
                            $isListeningHistoryArtistIds[] = $listeningHistorySong->userId;
                        }
                    } else {
                        $listeningHistoryAlbum = Album::where('id', $listeningHistory['albumId'])->first();
                        if($listeningHistoryAlbum){
                            $isListeningHistoryArtistIds[] = $listeningHistoryAlbum->userId;
                        }
                    }
                }
            }
            $listeningHistories =ListeningHistory::where('isRecentlyPlayed',1)->where('userId',auth()->user()->id)->latest()->take(5)->get();
            if(!empty($listeningHistories)){
                foreach($listeningHistories as $listeningHistory){
                    $recentlySongsIds[] = $listeningHistory->songId;
                    $recentlyArtistIds[] = $listeningHistory->artistId;
                    $recentlyAlbumIds[] = $listeningHistory->albumId;
                }
            }
            $songCheckArtistRecently = Song::whereIn('id',$recentlySongsIds)->pluck('userId')->toArray();
            $checkArtistBlock=User::whereIn('id',$songCheckArtistRecently)->where('status','!=','Block')->pluck('id')->toArray();
            $recentlySongs = Song::whereIn('id',$recentlySongsIds)->whereIn('userId',$checkArtistBlock)->get();
            $recentlyArtists = User::whereIn('id',$recentlyArtistIds)->where('status','!=','Block')->get();
            $recentlyAlbumCheckBLock = Album::whereIn('id',$recentlyAlbumIds)->pluck('userId')->toArray();
            $checkArtistBlock=User::whereIn('id',$recentlyAlbumCheckBLock)->where('status','!=','Block')->pluck('id')->toArray();
            $recentlyAlbums = Album::whereIn('id',$recentlyAlbumIds)->where('userId',$checkArtistBlock)->get();
            $isListeningSongCheckArtist = Song::whereIn('id',$isListeningHistorySongsIds)->pluck('userId')->toArray();
            $checkArtistBlockHistory=User::whereIn('id',$isListeningSongCheckArtist)->where('status','!=','Block')->pluck('id')->toArray();
            $isListeningHistorySongs = Song::whereIn('id',$isListeningHistorySongsIds)->whereIn('userId',$checkArtistBlockHistory)->get();
            $isListeningHistoryArtists = User::whereIn('id',$isListeningHistoryArtistIds->where('status','!=','Block'))->get();
            $isListeningHistoryAlbumBlock = Album::whereIn('id',$isListeningHistoryAlbumIds)->pluck('userId')->toArray();
            $checkArtistAlbumIsHistory=User::whereIn('id',$isListeningHistoryAlbumBlock)->where('status','!=','Block')->pluck('id')->toArray();
            $isListeningHistoryAlbums = Album::whereIn('id',$isListeningHistoryAlbumIds)->whereIn('userId',$checkArtistAlbumIsHistory)->get();
            $data['songs'] = $recentlySongs;
            $data['artists'] = $recentlyArtists;
            $data['albums'] = $recentlyAlbums;
            $data['isListeningHistorySongs'] = $isListeningHistorySongs;
            $data['isListeningHistoryArtists'] = $isListeningHistoryArtists;
            $data['isListeningHistoryAlbums'] = $isListeningHistoryAlbums;
        }
        if($request->isRecentlyPlayed == true && $request->isListeningHistory == true) {
            if(!empty($request->listeningHistory)) {
                foreach ($request->listeningHistory as $listeningHistory) {
                    if (array_key_exists('songId', $listeningHistory) && array_key_exists('albumId', $listeningHistory) && array_key_exists('artistId', $listeningHistory)) {
                        $song = Song::where('id', $listeningHistory['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$listeningHistory['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $artist = User::where('id',$listeningHistory['artistId'])->first();
                        if(!$artist){
                            return response()->json(['response' => ['status' => false, 'message' => 'Artist not found against artistId = '.$listeningHistory['artistId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $album = Album::where('id',$listeningHistory['albumId'])->first();
                        if(!$album){
                            return response()->json(['response' => ['status' => false, 'message' => 'Album not found against albumId = '.$listeningHistory['albumId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song && $artist && $album) {
                            ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $listeningHistory['songId'], "albumId" => $listeningHistory['albumId'], "artistId" => $listeningHistory['artistId'], "isListeningHistory" => 1]);
                        }
                    }
                    elseif (array_key_exists('songId', $listeningHistory) && array_key_exists('albumId', $listeningHistory)) {
                        $song = Song::where('id', $listeningHistory['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$listeningHistory['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $album = Album::where('id',$listeningHistory['albumId'])->first();
                        if(!$album){
                            return response()->json(['response' => ['status' => false, 'message' => 'Album not found against albumId = '.$listeningHistory['albumId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song && $album) {
                            ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $listeningHistory['songId'], "albumId" => $listeningHistory['albumId'], "isListeningHistory" => 1]);
                        }
                    }
                    elseif (array_key_exists('songId', $listeningHistory)) {
                        $song = Song::where('id', $listeningHistory['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$listeningHistory['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song) {
                            ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $listeningHistory['songId'], "isListeningHistory" => 1]);
                        }
                    }
                    if ($listeningHistory['songId']) {
                        $isListeningHistorySongsIds[] = $listeningHistory['songId'];
                    }
                    if (array_key_exists('artistId', $listeningHistory)) {
                        $isListeningHistoryArtistIds[] = $listeningHistory['artistId'];
                    }
                    if (array_key_exists('albumId', $listeningHistory)) {
                        $isListeningHistoryAlbumIds[] = $listeningHistory['albumId'];
                    }
                    if (!array_key_exists('albumId', $listeningHistory)) {
                        $listeningHistorySong = Song::where('id', $listeningHistory['songId'])->first();
                        if($listeningHistorySong){
                            $isListeningHistoryArtistIds[] = $listeningHistorySong->userId;
                        }
                    } else {
                        $listeningHistoryAlbum = Album::where('id', $listeningHistory['albumId'])->first();
                        if($listeningHistoryAlbum){
                            $isListeningHistoryArtistIds[] = $listeningHistoryAlbum->userId;
                        }
                    }
                }
            }
            if(!empty($request->recentlyPlayed)) {
                foreach ($request->recentlyPlayed as $recentlyPlayed) {
                    if (array_key_exists('songId', $recentlyPlayed) && array_key_exists('albumId', $recentlyPlayed) && array_key_exists('artistId', $recentlyPlayed)) {
                        $song = Song::where('id', $recentlyPlayed['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$recentlyPlayed['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $artist = User::where('id',$recentlyPlayed['artistId'])->first();
                        if(!$artist){
                            return response()->json(['response' => ['status' => false, 'message' => 'Artist not found against artistId = '.$recentlyPlayed['artistId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $album = Album::where('id',$recentlyPlayed['albumId'])->first();
                        if(!$album){
                            return response()->json(['response' => ['status' => false, 'message' => 'Album not found against albumId = '.$recentlyPlayed['albumId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song && $artist && $album){
                            $listeningHistory = ListeningHistory::where(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "albumId" => $recentlyPlayed['albumId'], "artistId" => $recentlyPlayed['artistId'], "isRecentlyPlayed" => 1])->first();
                            if($listeningHistory){
                                $listeningHistory->updatedAt = Carbon::now();
                                $listeningHistory->save();
                            }else{
                                ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "albumId" => $recentlyPlayed['albumId'], "artistId" => $recentlyPlayed['artistId'], "isRecentlyPlayed" => 1]);
                            }
                        }
                    }
                    elseif (array_key_exists('songId', $recentlyPlayed) && array_key_exists('albumId', $recentlyPlayed)) {
                        $song = Song::where('id', $recentlyPlayed['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$recentlyPlayed['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        $album = Album::where('id',$recentlyPlayed['albumId'])->first();
                        if(!$album){
                            return response()->json(['response' => ['status' => false, 'message' => 'Album not found against albumId = '.$recentlyPlayed['albumId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song && $album) {
                            $listeningHistory = ListeningHistory::where(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "artistId" => null,"albumId" => $recentlyPlayed['albumId'], "isRecentlyPlayed" => 1])->first();
                            if ($listeningHistory) {
                                $listeningHistory->updatedAt = Carbon::now();
                                $listeningHistory->save();
                            } else {
                                ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "albumId" => $recentlyPlayed['albumId'], "isRecentlyPlayed" => 1]);
                            }
                        }
                    }
                    elseif (array_key_exists('songId', $recentlyPlayed)) {
                        $song = Song::where('id', $recentlyPlayed['songId'])->first();
                        if(!$song){
                            return response()->json(['response' => ['status' => false, 'message' => 'Song not found against songid = '.$recentlyPlayed['songId']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                        if($song) {
                            $listeningHistory = ListeningHistory::where(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "artistId" => null,"albumId" => null, "isRecentlyPlayed" => 1])->first();
                            if ($listeningHistory) {
                                $listeningHistory->updatedAt = Carbon::now();
                                $listeningHistory->save();
                            } else {
                                ListeningHistory::create(["userId" => auth()->user()->id, "songId" => $recentlyPlayed['songId'], "isRecentlyPlayed" => 1]);
                            }
                        }
                    }
                    if ($recentlyPlayed['songId']) {
                        $songsIds[] = $recentlyPlayed['songId'];
                    }
                    if (array_key_exists('artistId', $recentlyPlayed)) {
                        $artistIds[] = $recentlyPlayed['artistId'];
                    }
                    if (array_key_exists('albumId', $recentlyPlayed)) {
                        $albumIds[] = $recentlyPlayed['albumId'];
                    }
                }
            }
            $songArtistCheck = Song::whereIn('id',$songsIds)->pluck('userId')->toArray();
            $artistCheckBlock=User::whereIn('id',$songArtistCheck)->where('status','!=','Block')->pluck('id')->toArray();
            $recentlySongs = Song::whereIn('id',$songsIds)->whereIn('userId',$artistCheckBlock)->get();
            $recentlyArtists = User::whereIn('id',$artistIds)->where('status','!=','Block')->get();
            $albumsArtistCheck = Album::whereIn('id',$albumIds)->pluck('userId')->toArray();
            $artistAlbumBlockCheck=User::whereIn('id',$albumsArtistCheck)->where('status','!=','Block')->pluck('id')->toArray();
            $recentlyAlbums = Album::whereIn('id',$albumIds)->whereIn('userId',$artistAlbumBlockCheck)->get();
            $isListeningHistorySongArtist = Song::whereIn('id',$isListeningHistorySongsIds)->pluck('userId')->toArray();
            $artistCheckBlockArtistId=User::whereIn('id',$isListeningHistorySongArtist)->where('status','!=','Block')->pluck('id')->toArray();
            $isListeningHistorySongs = Song::whereIn('id',$isListeningHistorySongsIds)->whereIn('userId',$artistCheckBlockArtistId)->get();
            $isListeningHistoryArtists = User::whereIn('id',$isListeningHistoryArtistIds)->where('status','!=','Block')->get();
            $isListeningHistoryAlbums = Album::whereIn('id',$isListeningHistoryAlbumIds)->pluck('userId')->toArray();
            $artistCheckBlockArtistId=User::whereIn('id',$isListeningHistorySongArtist)->where('status','!=','Block')->pluck('id')->toArray();
            $isListeningHistoryAlbums = Album::whereIn('id',$isListeningHistoryAlbumIds)->whereIn('userId',$artistCheckBlockArtistId)->get();
            $data['songs'] = $recentlySongs;
            $data['artists'] = $recentlyArtists;
            $data['albums'] = $recentlyAlbums;
            $data['isListeningHistorySongs'] = $isListeningHistorySongs;
            $data['isListeningHistoryArtists'] = $isListeningHistoryArtists;
            $data['isListeningHistoryAlbums'] = $isListeningHistoryAlbums;
        }
        $isRecentlyPlayedCount = ListeningHistory::where('userId', Auth::user()->id)->where('isRecentlyPlayed',1)->count();
        if($isRecentlyPlayedCount > 5){
            $ids = ListeningHistory::where('userId', Auth::user()->id)->where('isRecentlyPlayed',1)->latest()->take(5)->pluck('id');
            ListeningHistory::where('userId', Auth::user()->id)->where('isRecentlyPlayed',1)->whereNotIn('id', $ids)->delete();
        }
        DB::commit();
        return response()->json(['response' => ['status' => true, 'data' => new RecentlyPlayedCollection($data)]], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Sync recently played song error exception');
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

}

