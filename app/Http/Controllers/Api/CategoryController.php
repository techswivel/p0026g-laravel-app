<?php

namespace App\Http\Controllers\Api;

use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Category\CategoryRequest;
use App\Http\Resources\Category\CategoryCollection;
use App\Models\Category;
use App\Models\UserInterest;
use App\Traits\PaginationTrait;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    use PaginationTrait,loggerExceptionTrait;

    public function categories(CategoryRequest $request){
        try {
            $limit = $request->limit;
            $paginate = $request->page;
            if($request->categoryType == "ALL"){
                $allCategory = Category::paginate($limit, ['*'], 'page', $paginate );
            }elseif($request->categoryType == "INTERESTS"){
                $allCategory = Category::join('songs', 'categories.id', '=', 'songs.categoryId')
                                        ->select(DB::raw('count(songs.id) as songs_count'),'categories.*')
                                        ->groupBy('songs.categoryId')
                                        ->havingRaw('count(songs.id)>4')
                                        ->paginate($limit, ['*'], 'page', $paginate );
            }elseif($request->categoryType == "RECOMMENDED"){
                $interestCategories = UserInterest::where('userId',Auth::user()->id)->pluck('categoryId')->toArray();

                $interestCategory = Category::whereIn('id', $interestCategories)->get();
                $limitInterest = 8-count($interestCategory);
                $nonInterestCategory = Category::whereNotIn('id', $interestCategories)->limit($limitInterest)->get();

                $selectedCategory = $interestCategory->merge($nonInterestCategory);
                $allCategory = $this->paginate($selectedCategory, $limit, $paginate);
            }else{
                return response()->json(['response' => ['status' => false, 'message' => 'Category type not correct!']], JsonResponse::HTTP_BAD_REQUEST);
            }
            return response()->json(['response' => ['status' => true,'data' => new CategoryCollection($allCategory)]], JsonResponse::HTTP_OK);
        } catch (Exception $e) {
            $this->saveExceptionLog($e, 'Categories exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
