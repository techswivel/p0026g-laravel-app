<?php

namespace App\Http\Controllers\Api;

use App\Enums\OtpTypes;
use App\Enums\UserStatusEnum;
use App\Models\Otp;
use App\Traits\loggerExceptionTrait;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\OtpPasswordRequest;

class ResetPasswordController extends Controller
{
    use loggerExceptionTrait;
    /*Email and password send to server to change password against given email. */
    public function resetPassword(OtpPasswordRequest $request)
    {
        try {
            $user = User::where('email', $request->email)->role('user')->first();
            if (is_null($user)) {
                // JsonResponse::HTTP_UNPROCESSABLE_ENTITY its ok with this
                return response()->json(['response' => ['status' => false, 'message' => 'Unable to find user with given Email!']], JsonResponse::HTTP_NOT_FOUND);
            }
            if ($user->status == UserStatusEnum::Block) {
                return response()->json(['response' => ['status' => false, 'message' => 'Your account is blocked. Kindly contact with admin support! ']], JsonResponse::HTTP_FORBIDDEN);
            }
            $tokenEmail = Otp::where('email_phoneNumber', $request->email)->where('otp', $request->otp)->first();
            if (empty($tokenEmail)) {
                return response()->json(['response' => ['status' => false, 'message' => 'Otp is Invalid!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            /* validate token for just only 10 minutes */
            if (now() >= $tokenEmail->createdAt->addMinutes(10)) {
                DB::beginTransaction();
                $tokenEmail->delete();
                DB::commit();
                return response()->json(['response' => ['status' => false, 'message' => 'Your otp  expired!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            $tokenEmail->delete();
            $user->update(['password' => bcrypt($request->password)]);
            return response()->json(null, JsonResponse::HTTP_NO_CONTENT);


        } catch (Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Reset password exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
