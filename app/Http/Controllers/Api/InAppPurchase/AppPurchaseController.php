<?php

namespace App\Http\Controllers\Api\InAppPurchase;

use App\Enums\PurchasedItemTypeEnum;
use App\Enums\PurchaseTypeEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AppStore\InAppPurchaseVerifyReceiptRequest;
use App\Http\Resources\User\SubscriptionResource;
use App\Models\Package;
use App\Models\Purchased;
use App\Models\User;
use App\Services\FcmTokenService;
use App\Services\FirebaseService;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class AppPurchaseController extends Controller
{

    /**
     * Verify inApp Purchase Receipts.
     *
     * @param InAppPurchaseVerifyReceiptRequest $request
     * @return \Illuminate\Http\Response
     */

    public function InAppPurchaseVerifyReceipt(InAppPurchaseVerifyReceiptRequest $request)
    {
        try {

            DB::beginTransaction();

            $errors = [
                '21000' => 'The request to the App Store didn’t use the HTTP POST request method.',
                '21001' => 'The App Store no longer sends this status code.',
                '21002' => 'The data in the receipt-data property is malformed or the service experienced a temporary issue. Try again.',
                '21003' => 'The system couldn’t authenticate the receipt.',
                '21004' => 'The shared secret you provided doesn’t match the shared secret on file for your account.',
                '21005' => 'The receipt server was temporarily unable to provide the receipt. Try again.',
                '21006' => 'This receipt is valid, but the subscription is in an expired state. When your server receives this status code, the system also decodes and returns receipt data as part of the response. This status only returns for iOS 6-style transaction receipts for auto-renewable subscriptions.',
                '21007' => 'This receipt is from the test environment, but you sent it to the production environment for verification.',
                '21008' => 'This receipt is from the production environment, but you sent it to the test environment for verification.',
                '21009' => 'Internal data access error. Try again later.',
                '21010' => 'The system can’t find the user account or the user account has been deleted.',
            ];

            if (env('APP_STORE_IOS_ENVIROMENT') == 'test') {
                $reciptURL = 'https://sandbox.itunes.apple.com/verifyReceipt';
            } else {
                $reciptURL = 'https://buy.itunes.apple.com/verifyReceipt';
            }

            $response =  Http::post($reciptURL, ["receipt-data" => $request->purchaseToken, "password" => env('APP_STORE_SHARED_SECRET')]);
            $responseData = json_decode($response->body());


            if ($responseData->status == 0) {
                DB::commit();

                return response(['response' => ['status' => 'true', 'data' => $responseData]], Response::HTTP_OK);
            } else {
                return response(['response' => ['status' => 'false', 'message' => $errors[$responseData->status]]], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response(['response' => ['status' => 'false', 'message' => $e->getMessage()]], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Verify inApp Purchase Receipts.
     *
     * @param InAppPurchaseVerifyReceiptRequest $request
     * @return \Illuminate\Http\Response
     */

    public function InAppPurchaseServerNotification(Request $request)
    {
        try {

            $header_payload_secret = explode('.', $request->signedPayload);

            //------------------------------------------
            // Header
            //------------------------------------------
            $header = json_decode(base64_decode($header_payload_secret[0]));

            $algorithm = $header->alg;
            $x5c = $header->x5c; // array
            $certificate = $x5c[0];
            $intermediate_certificate = $x5c[1];
            $root_certificate = $x5c[2];



            //------------------------------------------
            // Payload
            //------------------------------------------
            $payload = json_decode(base64_decode($header_payload_secret[1]));

            $notificationType = $payload->notificationType;
            $transactionInfo = explode('.', $payload->data->signedTransactionInfo);
            $signedRenewalDecodedData = explode('.', $payload->data->signedRenewalInfo);

            $notificationData = [
                'notificationType' => $notificationType,
                'subtype' => $payload->subtype,
                'notificationUUID' => $payload->notificationUUID,
                'transactionDecodedData' => json_decode(base64_decode($transactionInfo[1])),
                'signedRenewalDecodedData' => json_decode(base64_decode($signedRenewalDecodedData[1])),
            ];

            Log::info('Server CallBack Response:', ['notificationData' => $notificationData]);
            $subscription = Purchased::where('purchaseType',PurchaseTypeEnum::APPLE_PAY)->where('transactionId',$notificationData['transactionDecodedData']->transactionId)->first();

            if($subscription != null){

                $packages = Package::where('id',$subscription->packageId)->first();
                $user = User::find($subscription->userId);
                $subscription->update([
                    'purchaseStatus' => $notificationData['notificationType'],
                    'subType'  => $notificationData['subtype'],
                    'response'  => json_encode($notificationData)
                ]);

                if($subscription->purchaseStatus == 'RENEWAL_EXTENSION'){
                    if($subscription->subType == 'SUMMARY'){
                        $fcm['title'] = 'Subscription Renewal Date Extended';
                        $fcm['body'] = 'Request to extend the subscription renewal date is completed successfully';
                        $fcm['data']['title'] = 'Subscription Renewal Date Extended';
                        $fcm['data']['body'] = 'Request to extend the subscription renewal date is completed successfully';

                    }elseif($subscription->subType == 'FAILURE'){
                        $fcm['title'] = 'Subscription Renewal Date Extension Failed';
                        $fcm['body'] = 'Request to extend the subscription renewal date is failed';
                        $fcm['data']['title'] = 'Subscription Renewal Date Extension Failed';
                        $fcm['data']['body'] = 'Request to extend the subscription renewal date is failed';
                    }
                }elseif($subscription->purchaseStatus == 'REFUND_DECLINED'){
                    $fcm['title'] = 'Subscription Refund Declined';
                    $fcm['body'] = 'Request to subscription refund is declined';
                    $fcm['data']['title'] = 'Subscription Refund Declined';
                    $fcm['data']['body'] = 'Request to subscription refund is declined';
                }elseif($subscription->purchaseStatus == 'REFUND'){
                    $fcm['title'] = 'Subscription Successfully Refunded';
                    $fcm['body'] = 'Request to subscription refund is successfully completed';
                    $fcm['data']['title'] = 'Subscription Successfully Refunded';
                    $fcm['data']['body'] = 'Request to subscription refund is successfully completed';
                }elseif($subscription->purchaseStatus == 'PRICE_INCREASE'){
                    if($subscription->subType == 'ACCEPTED'){
                        $fcm['title'] = 'Subscription Price Increase Accepted';
                        $fcm['body'] = 'Thanks! to accept subscription price increase';
                        $fcm['data']['title'] = 'Subscription Price Increase Accepted';
                        $fcm['data']['body'] = 'Thanks! to accept subscription price increase';

                    }elseif($subscription->subType == 'PENDING'){
                        $fcm['title'] = 'Subscription Price Increase Pending';
                        $fcm['body'] = 'Subscription price increase not accepted yet';
                        $fcm['data']['title'] = 'Subscription Price Increase Pending';
                        $fcm['data']['body'] = 'Subscription price increase not accepted yet';
                    }
                }elseif($subscription->purchaseStatus == 'GRACE_PERIOD_EXPIRED'){
                    $fcm['title'] = 'Auto Renewing Subscription Ended';
                    $fcm['body'] = 'Billing grace period has ended without renewing the subscription';
                    $fcm['data']['title'] = 'Auto Renewing Subscription Ended';
                    $fcm['data']['body'] = 'Billing grace period has ended without renewing the subscription';

                }elseif($subscription->purchaseStatus == 'EXPIRED'){
                    if($subscription->subType == 'BILLING_RETRY'){

                        $fcm['title'] = 'Subscription Expired';
                        $fcm['body'] = 'The subscription expired because the subscription failed to renew before the billing retry period ended';
                        $fcm['data']['title'] = 'Subscription Expired';
                        $fcm['data']['body'] = 'The subscription expired because the subscription failed to renew before the billing retry period ended';

                    }elseif($subscription->subType == 'PRICE_INCREASE'){

                        $fcm['title'] = 'Subscription Expired';
                        $fcm['body'] = 'The subscription expired because the user didn’t consent to a price increase';
                        $fcm['data']['title'] = 'Subscription Expired';
                        $fcm['data']['body'] = 'The subscription expired because the user didn’t consent to a price increase';

                    }elseif($subscription->subType == 'PRODUCT_NOT_FOR_SALE'){

                        $fcm['title'] = 'Subscription Expired';
                        $fcm['body'] = 'The subscription expired because the product wasn’t available for purchase at the time the subscription attempted to renew';
                        $fcm['data']['title'] = 'Subscription Expired';
                        $fcm['data']['body'] = 'The subscription expired because the product wasn’t available for purchase at the time the subscription attempted to renew';

                    }elseif($subscription->subType == 'VOLUNTARY'){

                        $fcm['title'] = 'Subscription Expired';
                        $fcm['body'] = 'The subscription expired after the user disabled subscription auto-renewal';
                        $fcm['data']['title'] = 'Subscription Expired';
                        $fcm['data']['body'] = 'The subscription expired after the user disabled subscription auto-renewal';

                    }

                    $user->update([
                        'datetime_subscribed' => now(),
                        'lastpayment_datetime' => now()->toDateTimeString(),
                        'isSubscribed' => 0,
                    ]);


                }elseif($subscription->purchaseStatus == 'DID_RENEW'){
                    if($subscription->subType == 'BILLING_RECOVERY'){
                        $fcm['title'] = 'Subscription Renewed Successfully';
                        $fcm['body'] = 'The expired subscription that previously failed to renew has successfully renewed';
                        $fcm['data']['title'] = 'Subscription Renewed Successfully';
                        $fcm['data']['body'] = 'The expired subscription that previously failed to renew has successfully renewed';
                    }
                }elseif($subscription->purchaseStatus == 'DID_FAIL_TO_RENEW'){
                    if($subscription->subType == 'GRACE_PERIOD'){
                        $fcm['title'] = 'Subscription Renew Failed';
                        $fcm['body'] = 'The subscription failed to renew due to a billing issue';
                        $fcm['data']['title'] = 'Subscription Renew Failed';
                        $fcm['data']['body'] = 'The subscription failed to renew due to a billing issue';
                    }

                    $user->update([
                        'datetime_subscribed' => now(),
                        'lastpayment_datetime' => now()->toDateTimeString(),
                        'isSubscribed' => 0,
                    ]);

                }elseif($subscription->purchaseStatus == 'DID_CHANGE_RENEWAL_STATUS'){
                    if($subscription->subType == 'AUTO_RENEW_DISABLED'){
                        $fcm['title'] = 'Subscription Auto Renewal Disabled';
                        $fcm['body'] = 'Subscription auto-renewal disabled, or the App Store disabled subscription auto-renewal after you requested a refund';
                        $fcm['data']['title'] = 'Subscription Auto Renewal Disabled';
                        $fcm['data']['body'] = 'Subscription auto-renewal disabled, or the App Store disabled subscription auto-renewal after you requested a refund';
                    }elseif($subscription->subType == 'AUTO_RENEW_ENABLED'){
                        $fcm['title'] = 'Subscription Auto Renewal Enabled';
                        $fcm['body'] = 'Subscription auto-renewal enabled';
                        $fcm['data']['title'] = 'Subscription Auto Renewal Enabled';
                        $fcm['data']['body'] = 'Subscription auto-renewal enabled';
                    }
                }elseif($subscription->purchaseStatus == 'DID_CHANGE_RENEWAL_PREF'){
                    if($subscription->subType == 'DOWNGRADE'){
                        $fcm['title'] = 'Subscription Downgrade';
                        $fcm['body'] = 'The user downgraded their subscription. Downgrades take effect at the next renewal';
                        $fcm['data']['title'] = 'Subscription Downgrade';
                        $fcm['data']['body'] = 'The user downgraded their subscription. Downgrades take effect at the next renewal';
                    }elseif($subscription->subType == 'UPGRADE'){
                        $fcm['title'] = 'Subscription Upgrade';
                        $fcm['body'] = 'The user downgraded their subscription. Upgrades take effect at the next renewal';
                        $fcm['data']['title'] = 'Subscription Upgrade';
                        $fcm['data']['body'] = 'The user downgraded their subscription. Upgrades take effect at the next renewal';
                    }
                }

                $fcm['data']['type'] = 'Subscription';
                $fcm['data']['notificationType'] = $subscription->purchaseStatus;
                $fcm['data']['notificationSubType'] =  $subscription->subType;
                $fcm['data']['subscriptionCode'] =  4;
                $fcm['data']['notification'] = json_encode(new SubscriptionResource($packages));

                $FCMToken = new FcmTokenService();
                $firebase = new FirebaseService();
                $tokens = $FCMToken->getUserFCMRegistrationToken($user->id);

                foreach ($tokens as $key => $token) {
                    $fcm['FCMRegistrationToken'] = $token;
                    $firebase->cloudMessageToSingleDevice($fcm);
                }

            }
        } catch (Exception $e) {
            Log::error('Server Callback Notification Exception:',[$e->getMessage()]);
        }
    }
}
