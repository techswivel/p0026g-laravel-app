<?php

namespace App\Http\Controllers\Api;

use App\Enums\OtpTypes;
use App\Enums\SocialSitesEnum;
use App\Http\Requests\Api\User\UpdateProfileRequest;
use App\Http\Resources\SocialResource;
use App\Http\Resources\User\UserAuthResource;
use App\Models\Otp;
use App\Models\User;
use App\Models\FcmToken;
use App\Repositories\Interfaces\FCMTokenRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\FacebookService;
use App\Services\FcmTokenService;
use App\Services\GoogleAuthService;
use App\Services\OtpService;
use App\Traits\loggerExceptionTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use App\Enums\SignInTypeEnum;
use App\Enums\UserStatusEnum;
use App\Traits\FileUploadTrait;
use App\Services\FirebaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\User\UserResource;
use App\Http\Requests\Api\User\RegistrationRequest;
use App\Http\Resources\User\ProfileResource;
use App\Http\Requests\Api\User\logoutRequest;
use App\Http\Requests\Api\User\SigninRequest;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Redis;

class AuthController extends Controller
{
    use FileUploadTrait ,loggerExceptionTrait;

    protected $imageDirectory = "uploads/users";


    public function signUp(RegistrationRequest $request, FirebaseService $firebase)
    {
        try {
            DB::beginTransaction();
            $user = $request->only("zipcode","country","city","socialId","socialSite","state", 'name','dOb','gender','password', 'email', 'phoneNumber', 'avatar','completeAddress');
            $name = explode(" ", $user['name']);
            $user['firstName'] = $name[0];
            if(array_key_exists('1', $name)){
                $user['lastName'] = $name[1];
            }
            $user['birthDate'] = null;
            if(array_key_exists('dOb', $user)){
                $user['birthDate'] = $user['dOb'];
            }
            $user['address'] = null;
            if(array_key_exists('socialSite', $user)){
                if($user['socialSite'] == 'FACEBOOK'){
                    $user['fbId'] = $user['socialId'];

                }elseif ($user['socialSite'] == 'GMAIL'){
                    $user['gmailId'] = $user['socialId'];
                }
            }
            $user['address'] = null;
            if(array_key_exists('completeAddress', $user)){
                $user['address'] = $user['completeAddress'];
            }
            $user['zipCode'] = null;
            if(array_key_exists('zipcode', $user)){
                $user['zipCode'] = $user['zipcode'];
            }
            $user['isNotificationEnabled'] = true;
            $user['isArtistUpdateEnabled'] = true;
            if(array_key_exists('1', $name)){
                $user['lastName'] = $name[1];
            }
            $user['password'] = bcrypt($user['password']);
            $email = $user['email'];
            if ($email) {
                $otpEmail = Otp::where('email_phoneNumber', $email)->where('isVerified', 1)->first();
                if (is_null($otpEmail)) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Email  is not verified!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                } else {
                    $otpEmail->delete();
                }
            }
            if ($request->has('profile')) {
                $user['avatar'] = $this->uploadFile($request->profile, $this->imageDirectory);
            }

            $firebaseUser = $firebase->createUserAccount(['email' => $request->email, 'password' => $user['password']]);
            if (!$firebaseUser->status) {
                return response()->json(['response' => ['status' => false, 'message' => $firebaseUser->message]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            $user['uid'] = $firebaseUser->data->uid;
            $user = User::create($user);
            $token = $user->createToken($request->deviceName)->plainTextToken;
            $user['jwt'] = $token;
            $FCMToken = new FcmTokenService();
            $FCMToken->addFcmtoken($request, $user->id);
            $user->setAttribute('firebaseCustomToken', $firebase->getToken($user->uid));
            /* assign role as a user  */
            $user->assignRole('user');
            DB::commit();
            return response()->json(['response' => ['status' => true, 'data' => ['auth' => new UserResource($user)]]], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'SignUp exception');
            return response()->json(['response' => ['status' => false, 'message' =>'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function signIn(SigninRequest $request, FirebaseService $firebase, UserRepositoryInterface $userRepository, FCMTokenRepositoryInterface $fcmToken)
    {
        try {
            DB::beginTransaction();
            $user = null;
            $message = 'Unable to find user with given email!';
            if ($request->loginType == SignInTypeEnum::SOCIAL) {
                $user = false;
                if ($request->has('socialSite')) {
                    if ($request->socialSite == SocialSitesEnum::FACEBOOK) {
                        $userInfo = FacebookService::validateAccessToken($request->accessToken);

                        if (!$userInfo) {
                            return response()->json(['response' => ['status' => false, 'message' => 'Invalid Social Login']], JsonResponse::HTTP_BAD_REQUEST);
                        }
                        $userInfo['socialSite'] = SocialSitesEnum::FACEBOOK;
                        $user = $userRepository->getByFbSocialId($userInfo['id']);
                    } elseif ($request->socialSite == SocialSitesEnum::GMAIL) {
                        $userInfo = (new GoogleAuthService())->getInfo($request->accessToken);
                        if (!$userInfo) {
                            return response()->json(['response' => ['status' => false, 'message' => 'Invalid Social Login']], JsonResponse::HTTP_BAD_REQUEST);
                        }
                        $userInfo['socialSite'] = SocialSitesEnum::GMAIL;
                        if ($userInfo) {
                            $user = $userRepository->getByGmailSocialId($userInfo['id']);
                        }
                    } elseif ($request->socialSite == SocialSitesEnum::APPLE) {
                        $userInfo['socialSite'] = SocialSitesEnum::APPLE;
                        $userInfo['id'] = $request->accessToken;
                        $userInfo['firstName'] = $request->firstName;
                        $userInfo['lastName'] = $request->lastName;
                        $userInfo['email'] = $request->email;
                        if ($userInfo) {
                            $user = $userRepository->getByAppleSocialId($request->accessToken);

                            $password=Hash::make("TechSwivel#001");
                            if(!$user){
                                $dummyPhoneNumber='qa@techswivel'.$this->getRandomNumber(7,5).'.com';
                                $firebaseUser = $firebase->createUserAccount(['email' => $dummyPhoneNumber, 'password' => $password]);
                                if (!$firebaseUser->status) {
                                    return response()->json(['response' => ['status' => false, 'message' => $firebaseUser->message]], JsonResponse::HTTP_BAD_REQUEST);
                                }
                                $isEmailAlreadyExist=User::where('email',$request->email)->exists();
                                if ($isEmailAlreadyExist) {
                                    return response()->json(['response' => ['status' => false, 'message' => 'Sorry! Account could not be created as this email is already registered for some other account.']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                                }
                                $user = [
                                    'firstName' => $request->firstName?$request->firstName:"qa",
                                    'lastName' => $request->lastName?$request->lastName:"test",
                                    'phoneNumber' => $request->phoneNumber,
                                    'status' => UserStatusEnum::Active,
                                    'email' => $request->email?$request->email:'qa@techswivel'.$this->getRandomNumber(7,5).'.com',
                                    'password' => $password,
                                    'isEmailVerified'=>true,
                                    'uid' => $firebaseUser->data->uid,
                                    'avatar' => 'avatars/default-avatar.png',
                                    'appleId'=>$request->accessToken
                                ];
                                $user = $userRepository->add($user);
                                $user->assignRole('user');

                            }else{
                                $user->firstName=$request->firstName? $request->firstName: $user->firstName;
                                $user->lastName=$request->lastName? $request->lastName: $user->lastName;
                                if($user->email==null){
                                    $isEmailAlreadyExist=User::where('email',$request->email)->exists();
                                    if (!$isEmailAlreadyExist) {
                                        $user->email=$request->email? $request->email: $user->email;
                                    }
                                }
                                $user->save();
                            }
                        }
                        if(!$user->hasRole('user')){
                            $user->assignRole('user');
                        }
                    }

                    if ($user) {
                        if ($user->status == UserStatusEnum::Block) {
                            return response()->json(['response' => ['status' => false, 'message' => "Sorry! You can't login as your account is blocked. Kindly contact with q music Admin."]], JsonResponse::HTTP_FORBIDDEN);
                        }
                        $token = $user->createToken($request->deviceName)->plainTextToken;
                        $user['jwt'] = $token;
                        $FCMToken = new FcmTokenService();
                        $FCMToken->addFcmtoken($request, $user->id);
                        $user->setAttribute('firebaseCustomToken', $firebase->getToken($user->uid));
                        DB::commit();
                        return response()->json(['response' => ['status' => true, 'data' => ['auth' => new UserResource($user)]]], JsonResponse::HTTP_OK);
                    } else {
                        Redis::hmset('socialId:' . $userInfo['id'], $userInfo);
                        Redis::expireat('socialId:' . $userInfo['id'], Carbon::now()->addHour(1)->timestamp);
                        return response()->json(['response' => ['status' => false, 'data' => new SocialResource($userInfo)]], JsonResponse::HTTP_OK);
                    }
                }
            }
            if ($request->loginType == SignInTypeEnum::SIMPLE) {
                $user = User::where('email', $request->email)->first();
            }
            if (is_null($user)) {
                return response()->json(['response' => ['status' => false, 'message' => $message]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            if ($user->status == UserStatusEnum::Block) {
                return response()->json(['response' => ['status' => false, 'message' => 'Your account is blocked. Kindly contact with admin support! ']], JsonResponse::HTTP_FORBIDDEN);
            }
            if (!Hash::check($request->password, $user->password)) {
                return response()->json(['response' => ['status' => false, 'message' => 'Your password is wrong!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            /* generating token by usign Sanctum */
            $token = $user->createToken($request->deviceName)->plainTextToken;
            $user['jwt'] = $token;
            $FCMToken = new FcmTokenService();
            $FCMToken->addFcmtoken($request, $user->id);
            $user->setAttribute('firebaseCustomToken', $firebase->getToken($user->uid));
            DB::commit();
            $request['authId'] = $user->id;
            $request['isSubscribed'] = $user->isSubscribed;
            return response()->json(['response' => ['status' => true, 'data' => ['auth' => new UserResource($user)]]], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'SignIn exception');
            return response()->json(['response' => ['status' => false, 'data' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function logout(logoutRequest $request)
    {
        try {
            $user = request()->user();
            if (!is_null($user)) {
                FcmToken::where('userId', $user->id)->where('deviceidentifier', $request->deviceIdentifier)->delete();
                $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            }
            return response()->json(['response' => ['status' => false, 'message' => 'Unable to find sanctum token against requested user!']], JsonResponse::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'LogOut error exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function profile()
    {
        $user = Auth::user();
        return response()->json(['response' => ['status' => true, 'data' => ['auth' => new ProfileResource($user)]]], JsonResponse::HTTP_OK);
    }
    public function getRandomNumber($requiredLength = 11, $highestDigit = 8) {

        $x = true;
        do {
            $randomNumber = '';
            for ($i = 0; $i < $requiredLength; ++$i) {
                $randomNumber .= mt_rand(1, $highestDigit);
            }
            $isPhoneNumberExist=User::where('email',"qa@techswivel".$randomNumber.".com")->first();
            if(!$isPhoneNumberExist){
                $x=false;
            }
        } while ($x);

        return $randomNumber;
    }
    protected function addFcmtoken($fcmToken, $request, $userId)
    {
        FcmToken::where('token' , $request->fcmToken)->update([
            'isActive' => 0
        ]);
        $fcmToken->updateOrCreate(
            [
                'deviceidentifier' => $request->deviceIdentifier?$request->deviceIdentifier:"abcd",
            ], [
            'token' => $request->fcmToken,
            'deviceidentifier' => $request->deviceIdentifier?$request->deviceIdentifier:"abcd",
            'userId' => $userId,
            'isActive' => 1
        ]);
    }
    public function updateProfile(UpdateProfileRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = Auth::user();
            //phoneNumber is check verified
            if ($request->phoneNumber && $request->otp){
                $otpCode = Otp::where('email_phoneNumber', $request->phoneNumber)->where('otp', $request->otp)->first();
                if (!$otpCode) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Otp code is invalid!']], JsonResponse::HTTP_BAD_REQUEST);
                }
                if ($otpCode->otpType == OtpTypes::PHONE_NUMBER) {
                    $otpCode->delete();
                }else{
                    return response()->json(['response' => ['status' => false, 'message' => 'Otp type is invalid!']], JsonResponse::HTTP_BAD_REQUEST);
                }
            }
            if ($request['name']==null){
                $firstName = $data->firstName;
                $secondName = $data->lastName;
            }else{
                $splitName = explode(' ', $request['name'], 2); // Restricts it to only 2 values, for names like Billy Bob Jones
                $firstName = $splitName[0];
                $secondName = !empty($splitName[1]) ? $splitName[1] : '';
            }

            $avatar = $data->avatar??null;
            if ($request->has('profile')) {
                $this->deleteFile($data->avatar);
                $avatar = $this->uploadFile($request->profile, $this->imageDirectory);
            }
            $isEnableNotification=null;
            if (!empty($request['isEnableNotification'])){
                if ($request['isEnableNotification']=="true"){
                    $isEnableNotification=1;
                }else{
                    $isEnableNotification=0;
                }
            }
            $isArtistUpdateEnable=null;
            if (!empty($request['isArtistUpdateEnable'])) {
                if ($request['isArtistUpdateEnable'] == "true") {
                    $isArtistUpdateEnable = 1;
                } else {
                    $isArtistUpdateEnable = 0;
                }
            }

               $data->update([
                'firstName' => $firstName??$data->firstName,
                'lastName' => $secondName??$data->lastName,
                'gender' => $request['gender']??$data->gender,
                'address' => $request['completeAddress']??$data->address,
                'avatar' => $avatar,
                'city'=>$request['city']??$data->city,
                'birthDate'=>$request['dOb']??$data->birthDate,
                'state'=>$request['state']??$data->state,
                'country'=>$request['country']?? $data->country,
                'zipCode'=>$request['zipCode']??$data->zipCode,
                'phoneNumber'=>$request['phoneNumber']??$data->phoneNumber,
                'isNotificationEnabled'=>$isEnableNotification??$data->isNotificationEnabled,
                'isArtistUpdateEnabled'=>$isArtistUpdateEnable ??$data->isArtistUpdateEnabled
            ]);
            DB::commit();
            return response()->json(['response' => ['status' => true, 'data' => ['auth' => new ProfileResource($data)]]], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Update profile exception');
            return response()->json(['response' => ['status' => false, 'message' =>$e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

}
