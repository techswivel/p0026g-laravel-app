<?php

namespace App\Http\Controllers\Api;

use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\UserInterest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InterestController extends Controller
{
    use loggerExceptionTrait;
    public function saveInterest(Request $request){
        try {
            DB::beginTransaction();
            $deletePreviousinterests = UserInterest::where('userId',Auth::user()->id)->delete();

            $interestCount = count($request->interests);
            if($interestCount<2 || $interestCount>5){
                return response()->json(['response' => ['status' => false, 'message' => 'User can select minimum 2 and maximum 5 interests!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            foreach($request->interests as $interest){
                $category = Category::where('id',$interest['categoryId'])->count();
                if($category == 0){
                    return response()->json(['response' => ['status' => false, 'message' => 'Category not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $userinterest = UserInterest::where('userId',Auth::user()->id)
                ->where('categoryId',$interest['categoryId'])->first();

                if(!$userinterest){
                    UserInterest::create([
                        'userId' => Auth::user()->id,
                        'categoryId' => $interest['categoryId']
                    ]);
                }
            }
            DB::commit();
            return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
        } catch (Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Save interest exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
