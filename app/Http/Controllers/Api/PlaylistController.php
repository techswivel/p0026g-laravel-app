<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Playlist\SavePlaylistRequest;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Playlist\UpdatePlaylistRequest;
use App\Http\Resources\Playlist\AllPlaylistCollection;
use App\Http\Resources\Playlist\PlaylistResource;
use App\Models\AlbumSongs;
use App\Models\Playlist;
use App\Models\PlaylistSongs;
use App\Models\Purchased;
use App\Models\Song;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class PlaylistController extends Controller
{
    use loggerExceptionTrait;
    public function playlist(Request $request)
    {
        try {
            $user = request()->user();
            $limit = $request->limit;
            $paginate = $request->page;

            $playlists = Playlist::where('userId',$user->id)->paginate($limit, ['*'], 'page', $paginate );

            return response()->json(['response' => ['status' => true, 'data' => new AllPlaylistCollection($playlists), ]], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Playlist exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
    public function savePlaylist(SavePlaylistRequest $request)
    {
        try {
            $user = request()->user();
            $checkPlaylist = Playlist::where('userId',$user->id)->where('title',$request->title)->count();
            if($checkPlaylist > 0){
                return response()->json(['response' => ['status' => false, 'message' => 'Playlist aleady exist!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            DB::beginTransaction();

            $playlist = Playlist::create([
                    'userId' => $user->id,
                    'title' => $request->title
                ]);
            DB::commit();
            return response()->json(['response' => ['status' => true, 'data' =>['playlist' => new PlaylistResource($playlist)]  ]], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Save playlist error exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function updatePlaylist(UpdatePlaylistRequest $request)
    {
        try {
            $user = request()->user();

            if($request->type == "ADD"){
                $song = Song::find($request->songId);
                if($song){
                    if($song->isPaid == 1){
                        $albumIdOfSong = AlbumSongs::where('songId',$request->songId)->first();
                        if($albumIdOfSong) {
                            $purchased = Purchased::where('userId',$user->id)->where('albumId',$albumIdOfSong->albumId)->first();
                            if(!$purchased) {
                                $purchased = Purchased::where('userId',$user->id)->where('songId',$request->songId)->first();
                                if(!$purchased){
                                    return response()->json(['response' => ['status' => false, 'message' => 'Song is not purcahsed!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                                }
                            }
                        }
                    }
                }else{
                    return response()->json(['response' => ['status' => false, 'message' => 'Song not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $checkPlaylist = Playlist::where('id',$request->playListId)->where('userId',$user->id)->count();
                if($checkPlaylist == 0){
                    return response()->json(['response' => ['status' => false, 'message' => 'Playlist not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $checkSong = Song::where('id',$request->songId)->count();
                if($checkSong == 0){
                    return response()->json(['response' => ['status' => false, 'message' => 'Song not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $playlistSong = PlaylistSongs::whereHas('getPlaylist',function($q) use($user){
                    $q->where('userId',$user->id);
                })->where('songId',$request->songId)->first();
                if($playlistSong){
                    return response()->json(['response' => ['status' => false, 'message' => 'Song aleady exist in playlist!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                DB::beginTransaction();
                $playlistsong = PlaylistSongs::create([
                        'playlistId' => $request->playListId,
                        'songId' => $request->songId
                    ]);
                DB::commit();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            }

            if($request->type == "REMOVE"){
                $playlistsong = PlaylistSongs::where('playlistId',$request->playListId)
                ->where('songId',$request->songId)->first();
                if($playlistsong != null){
                    $playlistsong->delete();
                    return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
                }else{
                    return response()->json(['response' => ['status' => false, 'message' => 'Song not found in playlist!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
            return response()->json(['response' => ['status' => false, 'message' => 'Selected type not correct!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Update playlist error exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
    public function deletePlaylist(Request $request, int $playlistId)
    {
        try {
            $user = request()->user();
            DB::beginTransaction();
            $playlist = Playlist::with('playlistSongs')->where('id',$playlistId)->where('userId',$user->id)->first();
            if(!$playlist){
                return response()->json(['response' => ['status' => false, 'message' => 'Playlist not found!']], JsonResponse::HTTP_BAD_REQUEST);
            }
            foreach($playlist->playlistSongs as $playlistSong){
                $pSong = $playlistSong->delete();
            }
            $list = $playlist->delete();
            DB::commit();
            return response()->json('', JsonResponse::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Delete playlist exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
