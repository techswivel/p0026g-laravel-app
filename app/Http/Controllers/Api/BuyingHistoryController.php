<?php

namespace App\Http\Controllers\Api;

use App\Enums\PurchaseTypeEnum;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\History\BuyerHistoryRequest;
use App\Http\Resources\History\BuyingHistoryCollection;
use App\Models\Purchased;
use Illuminate\Http\JsonResponse;

class BuyingHistoryController extends Controller
{
    use loggerExceptionTrait;
    public function buyingHistory(BuyerHistoryRequest $request)
    {
        try {
            $user = request()->user();

            $limit = $request->limit;
            $paginate = $request->page;

            $purchased = Purchased::where('userId',$user->id)->where('itemType','!=','PLAN_SUBSCRIPTION');
            if($request->typeOfTransection != "ALL"){
                $purchased = $purchased->where('purchaseType',$request->typeOfTransection);
            }
            $purchased = $purchased->paginate($limit, ['*'], 'page', $paginate );
            return response()->json(['response' => ['status' => true, 'data' => new BuyingHistoryCollection($purchased), ]], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Buying history exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
