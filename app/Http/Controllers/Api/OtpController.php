<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\SendOtpRequest;
use App\Http\Requests\Api\User\ValidateOTPRequest;
use App\Services\TwilioService;
use App\Traits\loggerExceptionTrait;
use Facades\App\Services\OtpService;
use Facades\App\Services\SendOtpService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OtpController extends Controller
{
    use loggerExceptionTrait;
    public function sendOtpCode(SendOtpRequest $request)
    {
        try {
            return SendOtpService::sendOtp($request);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Send otp code exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function validateOTP(ValidateOTPRequest $request)
    {
        try {
            return OtpService::validate($request);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Validate opt code exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
