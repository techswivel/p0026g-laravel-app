<?php

namespace App\Http\Controllers\Api\AppleStoreConnect\Subscription;

use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionPriceController extends Controller
{

    /**
     * Create Apple App Store price.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function getPrice($id)
    {

        $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v1/subscriptions/'.$id.'/relationships/prices');

        return $response;
    }

    /**
     * Create Apple App Store Subscription Price.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function storePrice($priceId,$subscriptionId)
    {

        $subscriptionPriceEq =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
            ->get('https://api.appstoreconnect.apple.com/v1/subscriptionPricePoints/'.$priceId.'/equalizations?include=territory&limit=1000');

        $priceEq = json_decode($subscriptionPriceEq->body());

        $priceEqData = $priceEq->data;
        $prices = Arr::pluck($priceEqData,'id');
        $prices[] = $priceId;

        foreach($prices as $price){

            Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.JwtTokenController::getJwtToken()])
            ->post('https://api.appstoreconnect.apple.com/v1/subscriptionPrices',[
                'data' => [
                    'type' => 'subscriptionPrices',
                    'attributes' => [
                        'preserveCurrentPrice' => true,
                        'startDate' => Carbon::now()->format('Y-m-d'),
                    ],
                    'relationships' => [
                        'subscription' => [
                            'data' => [
                                'id' =>  $subscriptionId,
                                'type' => 'subscriptions',
                            ]
                        ],
                        'subscriptionPricePoint' => [
                            'data' => [
                                'id' => $price,
                                'type' => 'subscriptionPricePoints',
                            ]
                        ],
                    ],

                ],
            ]);
        }

    }

    /**
     * Update Apple App Store price.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function updatePrice($request,$id)
    {
        $localeResponse = json_decode($this->getPrice($id)->body());

        if(!empty($localeResponse->data)){
            $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
            ->get('https://api.appstoreconnect.apple.com/v1/subscriptions/6445882186/prices');

            $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
            ->delete('https://api.appstoreconnect.apple.com/v1/subscriptions/eyJhIjoiNjQ0NTg4MjE4NiIsImMiOiJVUyIsImQiOjB9/relationships/pricesy');

            if($response->successful()){
                return $this->storePrice($request, $id);
            }

            if($response->failed()) {
                $localeErrors = json_decode($response->body());
                throw new \ErrorException('Subscription Price Error: '.implode(' | ', Arr::pluck($localeErrors->errors, 'detail')));
            }
        }

        if($this->getLocalization($id)->failed()) {
            $localeErrors = json_decode($this->getLocalization($id)->body());
            throw new \ErrorException('No Price found against this Subscription');
        }
    }

}
