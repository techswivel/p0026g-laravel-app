<?php

namespace App\Http\Controllers\Api\AppleStoreConnect\Subscription;

use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SubscriptionAvailability extends Controller
{


    /**
     * Create Apple App Store Availability For Subscription.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function storeAvailability($subscriptionId)
    {
        try{
            $territory =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                ->get('https://api.appstoreconnect.apple.com/v1/subscriptionAvailabilities/'.$subscriptionId.'/availableTerritories?limit=200');

            $territoryData = json_decode($territory->body());

            foreach($territoryData->data as $territory){

                Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                ->post('https://api.appstoreconnect.apple.com/v1/subscriptionAvailabilities',[
                    'data' => [
                        'type' => 'subscriptionAvailabilities',
                        'attributes' => [
                            'availableInNewTerritories' => true,
                        ],
                        'relationships' => [
                            'availableTerritories' => [
                                'data' => [
                                    'id' => $territory->id,
                                    'type' => 'territories',
                                ]
                            ],
                            'subscription' => [
                                'data' => [
                                    'id' => $subscriptionId,
                                    'type' => 'subscriptions',
                                ]
                            ],
                        ],
                    ],
                ]);
            }
        }catch (\Exception $exception) {
            throw new \ErrorException($exception->getMessage());
        }

    }

}
