<?php

namespace App\Http\Controllers\Api\AppleStoreConnect\Subscription;

use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Controller;
use App\Services\AppJwtTokenService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionLocalizationController extends Controller
{

     /**
     * Create Apple App Store Localization.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function getLocalization($id)
    {

        $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v1/subscriptions/'.$id.'/subscriptionLocalizations');

        return $response;
    }


    /**
     * Create Apple App Store Localization.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function storeLocalization($request,$id)
    {

        Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
        ->post('https://api.appstoreconnect.apple.com/v1/subscriptionLocalizations',[
            'data' => [
                'type' => 'subscriptionLocalizations',
                'attributes' => [
                    'name' => $request->localeName,
                    'locale' => $request->locale,
                    'description' => $request->localeDescription ?? '',
                ],
                'relationships' => [
                    'subscription' => [
                        'data' => [
                            'type' => 'subscriptions',
                            'id' => $id,
                        ]
                    ]
                ],
            ],
        ]);

    }


    /**
     * Update Apple App Store Localization.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function updateLocalization($request,$id)
    {
        $localeResponse = json_decode($this->getLocalization($id)->body());


        if(!empty($localeResponse->data)){
            $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
            ->delete('https://api.appstoreconnect.apple.com/v1/subscriptionLocalizations/'.$localeResponse->data[0]->id);

            if($response->successful()){
                return $this->storeLocalization($request, $id);
            }

            if($response->failed()) {
                $localeErrors = json_decode($response->body());
                throw new \ErrorException('Subscription Localization Error: '.implode(' | ', Arr::pluck($localeErrors->errors, 'detail')));
            }
        }

        if($this->getLocalization($id)->failed()) {
            $localeErrors = json_decode($this->getLocalization($id)->body());
            throw new \ErrorException('No localization found against this Subscription');
        }
    }

}
