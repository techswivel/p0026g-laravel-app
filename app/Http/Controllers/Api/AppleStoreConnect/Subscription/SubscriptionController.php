<?php

namespace App\Http\Controllers\Api\AppleStoreConnect\Subscription;

use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Api\AppleStoreConnect\Subscription\SubscriptionLocalizationController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AppStore\InAppPurchaseStoreRequest;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionController extends Controller
{

    /**
     * Get inApp Purchase App Store Subscription.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */

    public function getSubscription($id)
    {
        try {

            $response =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                ->get('https://api.appstoreconnect.apple.com/v1/subscriptions/' . $id);



            if ($response->successful()) {
                $data = json_decode($response->body());
                return response(['response' => ['status' => true, 'data' => $data->data]], Response::HTTP_OK);
            }


            $errors = json_decode($response->body());
            $message = implode(' | ', Arr::pluck($errors->errors, 'detail'));

            return  response(['response' => ['stats' => 'false', 'message' => $message]], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (Exception $e) {
            return response(['response' => ['status' => 'false', 'message' => $e->getMessage()]], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update Apple App Store Subscription.
     *
     * @param InAppPurchaseStoreRequest $request | $id
     * @return \Illuminate\Http\Response
     */

    public function storeSubscription($request, $id)
    {
        try {

            $response =  Http::withHeaders(['Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
            ->post('https://api.appstoreconnect.apple.com/v1/subscriptions', [
                'data' => [
                    'type' => 'subscriptions',
                    'attributes' => [
                        'name' => $request->name ?? '',
                        'productId' => $request->productId ?? '',
                        'reviewNote' => $request->reviewNote ?? '',
                        'familySharable' => true,
                        'availableInAllTerritories' => true,
                        'subscriptionPeriod' => $request->subscriptionPeriod ?? '',
                        'groupLevel' => (int) $request->groupLevel ?? 0,
                    ],
                    'relationships' => [
                        'group' => [
                            'data' => [
                                'type' => 'subscriptionGroups',
                                'id' => $id,
                            ]
                        ]
                    ],
                ]
            ]);

            if ($response->successful()) {
                $data = json_decode($response->body());

                (new SubscriptionLocalizationController)->storeLocalization($request, $data->data->id);
                (new SubscriptionReviewScreeshotController)->storeReviewScreenshot($request, $data->data->id);


                return $data->data->id;
            }

            if ($response->failed()) {
                $errors = json_decode($response->body());
                $message = implode(' | ', Arr::pluck($errors->errors, 'detail'));

                throw new \ErrorException(' message: ' . $message);
            }
        } catch (Exception $e) {
            throw new \ErrorException('Fail to store Apple Subscription ' . $e->getMessage());
        }
    }


    /**
     * Update Apple App Store Subscription.
     *
     * @param InAppPurchaseStoreRequest $request | $id
     * @return \Illuminate\Http\Response
     */

    public function updateSubscription($request, $id)
    {
        try {

            $response =  Http::withHeaders(['Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                ->patch('https://api.appstoreconnect.apple.com/v1/subscriptions/'. $id, [
                    'data' => [
                        'type' => 'subscriptions',
                        'id' => $id,
                        'attributes' => [
                            'name' => $request->name ?? '',
                            'reviewNote' => $request->reviewNote ?? '',
                            'familySharable' => true,
                            'availableInAllTerritories' => true,
                            'subscriptionPeriod' => $request->subscriptionPeriod ?? '',
                            'groupLevel' =>(int) $request->groupLevel ?? 0,
                        ],
                    ]
                ]);

            if ($response->successful()) {
                $data = json_decode($response->body());

                return true;
            }


            $errors = json_decode($response->body());
            $message = implode(' | ', Arr::pluck($errors->errors, 'detail'));

            return  response(['response' => ['status' => 'false', 'message' => $message]], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (Exception $e) {
            return response(['response' => ['status' => 'false', 'message' => $e->getMessage()]], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete Apple App Store InAppPurchase.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */

    public function deleteSubscription($id)
    {
        try {

            $response = $this->deleteSingleSubscription($id);


            if ($response->failed()) {
                $errors = json_decode($response->body());
                $message = implode(' | ', Arr::pluck($errors->errors, 'detail'));

                throw new \ErrorException(' message: ' . $message);
            }

            return response(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $e) {
            throw new \ErrorException('Fail to update InAppPurchase ' . $e->getMessage());
        }
    }

    /**
     * Delete Single Apple App Store InAppPurchase.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    private function deleteSingleSubscription($id)
    {
        $response =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
            ->delete('https://api.appstoreconnect.apple.com/v1/subscriptions/' . $id);

        return $response;
    }
}
