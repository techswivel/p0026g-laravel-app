<?php

namespace App\Http\Controllers\Api\AppleStoreConnect\Subscription;

use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Controller;
use App\Services\AppJwtTokenService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionReviewScreeshotController extends Controller
{

     /**
     * Get Apple App Store InAppPurchase Review Screenshot.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function getInAppPurchasepScreenshot($id)
    {

        $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$id.'/appStoreReviewScreenshot');

        return $response;
    }

    /**
     * Create Apple App Store InAppPurchase Review Screenshot.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function storeReviewScreenshot($request ,$id)
    {

        $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.JwtTokenController::getJwtToken()])
        ->post('https://api.appstoreconnect.apple.com/v1/subscriptionAppStoreReviewScreenshots',[
            'data' => [
                'type' => 'subscriptionAppStoreReviewScreenshots',
                'attributes' => [
                    'fileName' => $request->reviewScreenshot->getClientOriginalName(),
                    'fileSize' => $request->reviewScreenshot->getSize(),
                ],
                'relationships' => [
                    'subscription' => [
                        'data' => [
                            'id' =>  $id,
                            'type' => 'subscriptions',
                        ]
                    ]
                ],
            ],
        ]);


        if($response->successful()){
            $responseData = json_decode($response->body());


            Http::withBody(file_get_contents($request->reviewScreenshot), 'image/png')
                    ->withHeaders(['Content-Type' => 'image/png'])
                    ->put($responseData->data->attributes->uploadOperations[0]->url);


            Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.JwtTokenController::getJwtToken()])
            ->patch('https://api.appstoreconnect.apple.com/v1/subscriptionAppStoreReviewScreenshots/'.$responseData->data->id,[
                'data' => [
                    'type' => 'subscriptionAppStoreReviewScreenshots',
                    'id' => $responseData->data->id,
                    'attributes' => [
                        'uploaded' => true,
                    ],
                ],
            ]);

        }

    }


     /**
     * Update Apple App Store InAppPurchase Review Screenshot.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function updateInAppPurchaseReviewScreenshot($request, $id)
    {

        $promotedResponse = json_decode($this->getInAppPurchasepScreenshot($id)->body());

        if(!empty($promotedResponse->data)){
            $promoImgResponse =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                        ->get('https://api.appstoreconnect.apple.com/v1/inAppPurchaseAppStoreReviewScreenshots/'.$promotedResponse->data->id);

            $promoImgResponseData = json_decode($promoImgResponse->body());

            Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->delete('https://api.appstoreconnect.apple.com/v1/inAppPurchaseAppStoreReviewScreenshots/'.$promoImgResponseData->data->id);


            $this->storeInAppPurchaseReviewScreenshot($request,$id);
        }

    }
}
