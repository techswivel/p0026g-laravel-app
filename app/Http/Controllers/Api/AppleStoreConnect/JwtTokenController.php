<?php

namespace App\Http\Controllers\Api\AppleStoreConnect;

use App\Http\Controllers\Controller;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JwtTokenController extends Controller
{
    public static function getJwtToken()
    {

        $key = "file://".base_path('AuthKey_6NWRMBYHR9.p8'); //absolute path

        $header = [
            'alg' => 'ES256',
            'kid' => env('APP_STORE_KEY_IDENTIFIER'),
            'typ' => 'JWT',
        ];

        $payload = [
            'iss' => env('APP_STORE_ISSUER_ID'),
            'aud' => 'appstoreconnect-v1',
            'iat' => time(),
            'exp' => (time()+(60*20)),
        ];


        if(Auth::user()->jwtToken == null){

            $jwtToken = JWT::encode($payload, $key, 'ES256', env('APP_STORE_KEY_IDENTIFIER'), $header);
            User::find(Auth::id())->update(['jwtToken' => $jwtToken, 'jwtExpiredTime' => (time()+(60*20))]);
            return User::find(Auth::id())->jwtToken;

        }elseif((int) Auth::user()->jwtExpiredTime > time()){

           return User::find(Auth::id())->jwtToken;

        }else{
            $jwtToken = JWT::encode($payload, $key, 'ES256', env('APP_STORE_KEY_IDENTIFIER'), $header);
            User::find(Auth::id())->update(['jwtToken' => $jwtToken, 'jwtExpiredTime' => (time()+(60*20))]);
            return User::find(Auth::id())->jwtToken;
        }
    }
}
