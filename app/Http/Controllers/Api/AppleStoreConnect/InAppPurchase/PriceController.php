<?php

namespace App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase;

use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class PriceController extends Controller
{



    /**
     * Create Apple App Store InAppPurchase Price.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function storeInAppPurchasePrice($request ,$id)
    {

        $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
            ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$id.'/pricePoints',[
                'filter[priceTier]' => $request->priceTier,
                'limit' => 1
        ]);


        if($getPriceTier->failed()){
            $errors = json_decode($getPriceTier->body());
            return  response(['response' => ['stats' => 'false', 'message' =>  implode(' | ', Arr::pluck($errors->errors, 'detail'))]], Response::HTTP_UNPROCESSABLE_ENTITY);
        }


        if($getPriceTier->successful()){
            $data = json_decode($getPriceTier->body());


            $payload = [
                'data' => [
                    'relationships' => [
                        'inAppPurchase' => [
                            'data' => [
                                'id' =>  $id,
                                'type' => 'inAppPurchases',
                            ]
                        ],
                        'manualPrices' => [

                            'data' => [
                                [
                                    'id' => '${price1}',
                                    'type' => 'inAppPurchasePrices',
                                ]
                            ]
                        ],
                    ],
                    'type' => 'inAppPurchasePriceSchedules',
                ],
                'included' => [
                    [
                        'type' => 'inAppPurchasePrices',
                        'id' => '${price1}',
                        'attributes' => [
                            'startDate' => null
                        ],
                        'relationships' => [
                            'inAppPurchaseV2' => [
                                'data' => [
                                    'id' => $id,
                                    'type' => 'inAppPurchases',
                                ]
                            ],
                            'inAppPurchasePricePoint' => [
                                'data' => [
                                    'id' => $data->data[0]->id,
                                    'type' => 'inAppPurchasePricePoints',
                                ]
                            ],
                        ],
                    ]


                ],
            ];


            $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.JwtTokenController::getJwtToken()])
                                ->post('https://api.appstoreconnect.apple.com/v1/inAppPurchasePriceSchedules',$payload);



        }

    }
}
