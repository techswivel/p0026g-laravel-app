<?php

namespace App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase;

use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\LocalizationController;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\PriceController;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\PromotionController;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\ReviewScreeshotController;
use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AppStore\InAppPurchaseStoreRequest;
use App\Http\Requests\Api\AppStore\InAppPurchaseUpdateRequest;
use App\Services\AppJwtTokenService;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class InAppPurchaseController extends Controller
{


    /**
     * Get inApp Purchase App Store InAppPurchase.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */

    public function getInAppPurchase($id = null)
    {
        try {

            if($id != null){
                $response =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/' . $id);

            }else{

                $response =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                ->get('https://api.appstoreconnect.apple.com/v1/apps/'.env("APP_STORE_APP_ID").'/inAppPurchasesV2');

            }


            if ($response->successful()) {
                $data = json_decode($response->body());
                return response(['response' => ['status' => true, 'data' => $data->data]], Response::HTTP_OK);
            }


            $errors = json_decode($response->body());
            $message = implode(' | ', Arr::pluck($errors->errors, 'detail'));

            return  response(['response' => ['stats' => 'false', 'message' => $message]], Response::HTTP_UNPROCESSABLE_ENTITY);

        } catch (Exception $e) {
            return response(['response' => ['status' => 'false', 'message' => $e->getMessage()]], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create Apple App Store InAppPurchase.
     *
     * @param InAppPurchaseStoreRequest $request
     * @return \Illuminate\Http\Response
     */

    public function storeInAppPurchase($request)
    {
        try {

            $response =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
            ->post('https://api.appstoreconnect.apple.com/v2/inAppPurchases', [
                'data' => [
                    'type' => 'inAppPurchases',
                    'attributes' => [
                        'name' => $request->name,
                        'productId' => $request->productId,
                        'inAppPurchaseType' => 'NON_CONSUMABLE',
                        'reviewNote' => $request->reviewNote ?? '',
                        'familySharable' => true,
                        'availableInAllTerritories' => true,
                    ],
                    'relationships' => [
                        'app' => [
                            'data' => [
                                'type' => 'apps',
                                'id' => env('APP_STORE_APP_ID'),
                            ]
                        ]
                    ],
                ],
            ]);


            if ($response->successful()) {
                $data = json_decode($response->body());
                (new LocalizationController)->storeInAppPurchaseLocalization($request, $data->data->id);
                (new PromotionController)->storeInAppPurchasePromotion($request, $data->data->id);
                (new ReviewScreeshotController)->storeInAppPurchaseReviewScreenshot($request, $data->data->id);

                return $data->data->id;
            }

            if ($response->failed()) {
                $errors = json_decode($response->body());
                $message = implode(' | ', Arr::pluck($errors->errors, 'detail'));

                throw new \ErrorException(' message: ' . $message);
            }

         } catch (Exception $e) {
            throw new \ErrorException('Fail to add Apple InAppPurchase ' . $e->getMessage());
        }
    }


    /**
     * Update Apple App Store InAppPurchase.
     *
     * @param InAppPurchaseStoreRequest $request | $id
     * @return \Illuminate\Http\Response
     */

    public function updateInAppPurchase($request, $id)
    {
        try {

            $response =  Http::withHeaders(['Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
            ->patch('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$id, [
                'data' => [
                    'type' => 'inAppPurchases',
                    'id' => $id,
                    'attributes' => [
                        'name' => $request->name ?? '',
                        'reviewNote' => $request->reviewNote ?? '',
                        'familySharable' => true,
                        'availableInAllTerritories' => true,
                    ],
                ]
            ]);

            if ($response->successful()) {
                $data = json_decode($response->body());
                (new PriceController)->storeInAppPurchasePrice($request, $data->data->id);

                return true;
            }

            if ($response->failed()) {
                $errors = json_decode($response->body());
                $message = implode(' | ', Arr::pluck($errors->errors, 'detail'));

                throw new \ErrorException(' message: ' . $message);
            }

        } catch (Exception $e) {
            throw new \ErrorException('Fail to update InAppPurchase ' . $e->getMessage());
        }
    }

    /**
     * Update Baic Apple App Store InAppPurchase.
     *
     * @param InAppPurchaseStoreRequest $request | $id
     * @return \Illuminate\Http\Response
     */

    public function updateInAppPurchaseBasic($request, $id)
    {
        try {

            $response =  Http::withHeaders(['Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
            ->patch('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$id, [
                'data' => [
                    'type' => 'inAppPurchases',
                    'id' => $id,
                    'attributes' => [
                        'name' => $request->name ?? '',
                        'reviewNote' => $request->reviewNote ?? '',
                        'familySharable' => true,
                        'availableInAllTerritories' => true,
                    ],
                ]
            ]);

            if ($response->successful()) {

                return true;
            }

            if ($response->failed()) {
                $errors = json_decode($response->body());
                $message = implode(' | ', Arr::pluck($errors->errors, 'detail'));

                throw new \ErrorException(' message: ' . $message);
            }

        } catch (Exception $e) {
            throw new \ErrorException('Fail to update Apple InAppPurchase ' . $e->getMessage());
        }
    }


    /**
     * Delete Apple App Store InAppPurchase.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */

    public function deleteInAppPurchase($id)
    {
        try {

            $response = $this->deleteSingleInAppPurchase($id);


            if ($response->failed()) {
                $errors = json_decode($response->body());
                $message = $errors->errors[0]->detail;

                throw new \ErrorException(' message: ' . $message);
             }

            return response(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $e) {
            throw new \ErrorException('Fail to add InAppPurchase ' . $e->getMessage());
        }
    }

    /**
     * Delete Single Apple App Store InAppPurchase.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    private function deleteSingleInAppPurchase($id)
    {
        $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.JwtTokenController::getJwtToken()])
                            ->delete('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$id);

        return $response;
    }



    /**
     * Create Apple App Store InAppPurchase Review Submission.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function storeInAppPurchaseReviewSubmission($id)
    {
        try {

            $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.JwtTokenController::getJwtToken()])
            ->post('https://api.appstoreconnect.apple.com/v1/inAppPurchaseSubmissions',[
                'data' => [
                    'type' => 'inAppPurchaseSubmissions',
                    'relationships' => [
                        'inAppPurchaseV2' => [
                            'data' => [
                                'id' =>  $id,
                                'type' => 'inAppPurchases',
                            ]
                        ]
                    ],
                ],
            ]);

            if($response->successful()){
                $responseData = json_decode($response->body());
                return  response(['response' => ['status' => 'true', 'data' => $responseData]], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            if($response->failed()){
                $inAppPurchaseDel = $this->deleteSingleInAppPurchase($id);
                $inAppPurchaseDelErrors = json_decode($inAppPurchaseDel->body());
                return  response(['response' => ['stats' => 'false', 'message' => implode(' | ', Arr::pluck($inAppPurchaseDelErrors->errors, 'detail'))]], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (Exception $e) {
            throw new \ErrorException('Fail to add InAppPurchase ' . $e->getMessage());
        }

    }
}
