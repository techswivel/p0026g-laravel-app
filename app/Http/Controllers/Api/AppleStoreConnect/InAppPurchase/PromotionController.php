<?php

namespace App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase;

use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Controller;
use App\Services\AppJwtTokenService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class PromotionController extends Controller
{


     /**
     * Get Apple App Store InAppPurchase Promotion.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function getInAppPurchasepPromoted($id)
    {

        $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$id.'/promotedPurchase');

        return $response;
    }

     /**
     * Create Apple App Store InAppPurchase Promotion.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function storeInAppPurchasePromotion($request, $id)
    {

        $response = Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.JwtTokenController::getJwtToken()])
                    ->post('https://api.appstoreconnect.apple.com/v1/promotedPurchases',[
                        'data' => [
                            'type' => 'promotedPurchases',
                            'attributes' => [
                                'visibleForAllUsers' => true,
                                'enabled' => true,
                            ],
                            'relationships' => [
                                'app' => [
                                    'data' => [
                                        'id' =>  env('APP_STORE_APP_ID'),
                                        'type' => 'apps',
                                    ]
                                ],
                                'inAppPurchaseV2' => [
                                    'data' => [
                                        'id' =>  $id,
                                        'type' => 'inAppPurchases',
                                    ]
                                ]
                            ],
                        ],
                    ]);


        if($response->successful()){
            $enabledPromoteImage = json_decode($response->body());


            $promotedImages =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.JwtTokenController::getJwtToken()])
            ->post('https://api.appstoreconnect.apple.com/v1/promotedPurchaseImages',[
                'data' => [
                    'type' => 'promotedPurchaseImages',
                    'attributes' => [
                        'fileName' => $request->promotionIcon->getClientOriginalName(),
                        'fileSize' => $request->promotionIcon->getSize(),
                    ],
                    'relationships' => [
                        'promotedPurchase' => [
                            'data' => [
                                'id' =>  $enabledPromoteImage->data->id,
                                'type' => 'promotedPurchases',
                            ]
                        ]
                    ],
                ],
            ]);



            if($promotedImages->successful()){

                $promotedPurchaseImages = json_decode($promotedImages->body());

                Http::withBody(file_get_contents($request->promotionIcon), 'image/png')
                        ->withHeaders(['Content-Type' => 'image/png'])
                        ->put($promotedPurchaseImages->data->attributes->uploadOperations[0]->url);


                Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.JwtTokenController::getJwtToken()])
                ->patch('https://api.appstoreconnect.apple.com/v1/promotedPurchaseImages/'.$promotedPurchaseImages->data->id,[
                    'data' => [
                        'id' => $promotedPurchaseImages->data->id,
                        'type' => 'promotedPurchaseImages',
                        'attributes' => [
                            'uploaded' => true,
                        ],
                    ],
                ]);

            }


        }

    }

    /**
     * Create Apple App Store InAppPurchase Promotion.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function updateInAppPurchasePromotion($request, $id)
    {

        $promotedResponse = json_decode($this->getInAppPurchasepPromoted($id)->body());

        if(!empty($promotedResponse->data)){
            $promoImgResponse =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                        ->get('https://api.appstoreconnect.apple.com/v1/promotedPurchases/'.$promotedResponse->data->id.'/relationships/promotionImages');

            $promoImgResponseData = json_decode($promoImgResponse->body());


            Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->delete('https://api.appstoreconnect.apple.com/v1/promotedPurchaseImages/'.$promoImgResponseData->data[0]->id);


            $this->storeInAppPurchasePromotion($request,$id);
        }

        return  response(['response' => ['status' => 'false', 'message' => 'No localization found against this inAppPurchase']], Response::HTTP_UNPROCESSABLE_ENTITY);

    }
}
