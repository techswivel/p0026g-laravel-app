<?php

namespace App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase;

use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Http\Controllers\Controller;
use App\Services\AppJwtTokenService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class LocalizationController extends Controller
{


     /**
     * Create Apple App Store InAppPurchase Localization.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function getInAppPurchaseLocalization($id)
    {

        $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$id.'/inAppPurchaseLocalizations');

        return $response;
    }


    /**
     * Create Apple App Store InAppPurchase Localization.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function storeInAppPurchaseLocalization($request,$id)
    {
        try{
            $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
            ->post('https://api.appstoreconnect.apple.com/v1/inAppPurchaseLocalizations',[
                'data' => [
                    'type' => 'inAppPurchaseLocalizations',
                    'attributes' => [
                        'name' => $request->localeName,
                        'locale' => $request->locale,
                        'description' => $request->localeDescription ?? '',
                    ],
                    'relationships' => [
                        'inAppPurchaseV2' => [
                            'data' => [
                                'type' => 'inAppPurchases',
                                'id' => $id,
                            ]
                        ]
                    ],
                ],
            ]);


            if($response->failed()) {
                $localeErrors = json_decode($response->body());
                throw new \ErrorException('Localization Error: '.implode(' | ', Arr::pluck($localeErrors->errors, 'detail')));
            }
        } catch (\Exception $exception) {
            throw new \ErrorException($exception->getMessage());
        }

    }


    /**
     * Update Apple App Store InAppPurchase Localization.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function updateInAppPurchaseLocalization($request,$id)
    {
        $localeResponse = json_decode($this->getInAppPurchaseLocalization($id)->body());

        if(!empty($localeResponse->data)){
            $response =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->delete('https://api.appstoreconnect.apple.com/v1/inAppPurchaseLocalizations/'.$localeResponse->data[0]->id);

            if($response->successful()){
                return $this->storeInAppPurchaseLocalization($request, $id);
            }

            if($response->failed()) {
                $localeErrors = json_decode($response->body());
                throw new \ErrorException('Localization Error: '.implode(' | ', Arr::pluck($localeErrors->errors, 'detail')));
            }
        }

        if($localeResponse->failed()) {
            $localeErrors = json_decode($localeResponse->body());
            throw new \ErrorException('No localization found against this inAppPurchase');
        }

    }

}
