<?php

namespace App\Http\Controllers\Api;

use App\Enums\PurchasedItemTypeEnum;
use App\Enums\PurchaseTypeEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Subscribe\SubscribeRequest;
use App\Http\Resources\PurchasedSubscription\PurchasedSubscriptionResource;
use App\Http\Resources\Song\SongResource;
use App\Http\Resources\User\SubscriptionResource;
use App\Models\Album;
use App\Models\Package;
use App\Models\Purchased;
use App\Models\Song;
use App\Models\User;
use App\Services\FcmTokenService;
use App\Services\FirebaseService;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Google_Client;
use Google_Service_AndroidPublisher;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use \Imdhemy\Purchases\Facades\Product;
use \Imdhemy\GooglePlay\Products\ProductPurchase;
use Imdhemy\GooglePlay\Subscriptions\SubscriptionPurchase;
use Imdhemy\Purchases\Facades\Subscription;
use Symfony\Component\HttpFoundation\Response;

class SubscribeController extends Controller
{
    use loggerExceptionTrait;
    public function subscribe(SubscribeRequest $request)
    {
        try {
            $sku = null;
            $isSong = false;
            $songId  = $albumId  = $packageId = null;
            $cardId = $request->cardId;
            $user = request()->user();

            $token = $request->purchaseToken;

            // check if aleady exists
            if ($request->itemType == PurchasedItemTypeEnum::SONG || $request->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG || $request->itemType == PurchasedItemTypeEnum::ALBUM || $request->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM) {
                $songPreOrderId = null;

                if ($request->itemType == PurchasedItemTypeEnum::SONG) {
                    $songPreOrderId = $request->songId;
                }

                if ($request->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG) {
                    if ($user->address == null) {
                        return response()->json(['response' => ['status' => false, 'message' => 'Add address to proceed!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    $songPreOrderId = $request->preOrderSongId;
                }

                if ($songPreOrderId != null) {
                    $aleadyPurchased = Purchased::where('userId', $user->id)->where('songId', $songPreOrderId)
                        ->where('purchaseType', $request->purchaseType)->where('purchaseStatus', 'Purchased')->first();
                    if ($aleadyPurchased) {
                        return response()->json(['response' => ['status' => false, 'message' => 'You aleady purchased this item!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    }
                }

                $albumPreOrderId = null;
                if ($request->itemType == PurchasedItemTypeEnum::ALBUM) {
                    $albumPreOrderId = $request->albumId;
                }
                if ($request->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM) {
                    if ($user->address == null) {
                        return response()->json(['response' => ['status' => false, 'message' => 'Add address to proceed!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    $albumPreOrderId = $request->preOrderAlbumId;
                }
                if ($albumPreOrderId != null) {
                    $aleadyPurchased = Purchased::where('userId', $user->id)->where('albumId', $albumPreOrderId)
                        ->where('purchaseStatus', 'Purchased')->first();
                    if ($aleadyPurchased) {
                        return response()->json(['response' => ['status' => false, 'message' => 'You aleady purchased this item!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    }
                }
            } elseif ($request->itemType == PurchasedItemTypeEnum::PLAN_SUBSCRIPTION) {
                $aleadyPurchased = Purchased::where('userId', $user->id)->where('transactionId', $token)
                    ->where('purchaseStatus', 'Subscribed')->where('purchaseType', $request->purchaseType)->first();
                if ($aleadyPurchased) {
                    return response()->json(['response' => ['status' => false, 'message' => 'You aleady purchased this item!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
            // end of check


            if ($request->itemType == PurchasedItemTypeEnum::SONG || $request->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG) {
                if ($request->itemType == PurchasedItemTypeEnum::SONG) {
                    $songOrderId = $request->songId;
                    $messageString = "Song";
                }
                if ($request->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG) {
                    $songOrderId = $request->preOrderSongId;
                    $messageString = "Pre order song";
                }

                $checkSong = Song::where('id', $songOrderId)->get();

                if ($checkSong->count() == 0) {
                    return response()->json(['response' => ['status' => false, 'message' => $messageString . ' not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }

                if (Auth()->user()->isSubscribed == null || Auth()->user()->isSubscribed == 0) {
                    if ($checkSong[0]->sku != null) {
                        $sku = $checkSong[0]->sku . "_wod";
                    }
                } else {
                    if ($checkSong[0]->sku != null) {
                        $sku = $checkSong[0]->sku . "_wd";
                    }
                }

                $songId  = $checkSong[0]->id;
                $isSong = true;
            } elseif ($request->itemType == PurchasedItemTypeEnum::ALBUM || $request->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM) {
                if ($request->itemType == PurchasedItemTypeEnum::ALBUM) {
                    $albumPreOrderId = $request->albumId;
                    $messageString = "Album";
                }
                if ($request->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM) {
                    $albumPreOrderId = $request->preOrderAlbumId;
                    $messageString = "Pre order album";
                }
                $checkAlbum = Album::where('id', $albumPreOrderId)->get();
                if ($checkAlbum->count() == 0) {
                    return response()->json(['response' => ['status' => false, 'message' => $messageString . ' not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $albumId  = $checkAlbum[0]->id;
                if (Auth()->user()->isSubscribed == null || Auth()->user()->isSubscribed == 0) {
                    if ($checkAlbum[0]->sku != null) {
                        $sku = $checkAlbum[0]->sku . "_wod";
                    }
                } else {
                    if ($checkAlbum[0]->sku != null) {
                        $sku = $checkAlbum[0]->sku . "_wd";
                    }
                }
            } elseif ($request->itemType == PurchasedItemTypeEnum::PLAN_SUBSCRIPTION) {
                $checkPackage = Package::where('id', $request->planId)->get();
                if ($checkPackage->count() == 0) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Package not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $packageId  = $checkPackage[0]->id;
                $sku = $checkPackage[0]->sku;
            }

            if ($sku == null) {
                return response()->json(['response' => ['status' => false, 'message' => 'Product not exist on playstore.']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            if ($request->purchaseType == PurchaseTypeEnum::GOOGLE_PAY) {

                $client = new Google_Client();
                $client->setAuthConfig(config('inapppurchase.serviceAccountJsonFile.file'));
                $client->addScope('https://www.googleapis.com/auth/androidpublisher');
                $service = new Google_Service_AndroidPublisher($client);
                $packageName = config('inapppurchase.packageName.name');
                $productId = $sku;

                if ($request->itemType == PurchasedItemTypeEnum::PLAN_SUBSCRIPTION) {
                    $inappResponse = $service->purchases_subscriptions->get($packageName, $productId, $token);

                    $this->subscribeToPlan($request, $inappResponse);
                    $acknowledgeStatus = null;
                    $paymentStatus =  $inappResponse['paymentState'];
                } else {
                    // For paid songs
                    try {
                        $inappResponse = $service->purchases_products->get($packageName, $productId, $token);

                        // if payment status 0, then retun same response [0. Purchased]
                        if ($inappResponse['purchaseState'] == 0) {
                            $paymentStatus = "Purchased";
                        }
                        // if payment status 1, same data only [1. Canceled]
                        if ($inappResponse['purchaseState'] == 1) {
                            $paymentStatus = "Canceled";
                        }
                        // if payment status 2, same data only [2. Pending]
                        if ($inappResponse['purchaseState'] == 2) {
                            $paymentStatus = "Pending";
                        }

                        if ($inappResponse['acknowledgementState'] == 0) {   // 0. Yet to be acknowledged
                            $acknowledgeStatus = 'Not Acknowledged';
                        }
                        if ($inappResponse['acknowledgementState'] == 1) {     //1. Acknowledged
                            $acknowledgeStatus = 'Acknowledged';
                        }
                    } catch (Exception $e) {
                        $msg = $e->getMessage();
                        $errorMsg = json_decode($msg);
                        $response = $e->getCode() . '  |  ' . $errorMsg->error->message;
                        return response()->json(['response' => ['status' => false, 'message' => $response, 'fullmessage' => $e->getMessage()]], 400);
                    }
                }

                $pay = Purchased::create([
                    'userId'  => $user->id,
                    'songId'  => $songId,
                    'albumId'  => $albumId,
                    'packageId' => $packageId,
                    'cardId' => $cardId,
                    'purchaseType' => $request->purchaseType,
                    'itemType' => $request->itemType,
                    'amount' => $request->paidAmount,
                    'transactionId' => $token,
                    'paidAmount' => $request->paidAmount,
                    'isSong' => $isSong,
                    'purchaseStatus' => $paymentStatus,
                    'isAcknowledged'  => $acknowledgeStatus,
                    'response'  => json_encode($inappResponse)
                ]);

                if ($request->itemType == PurchasedItemTypeEnum::PLAN_SUBSCRIPTION) {
                    //  0. Payment pending 1. Payment received 2. Free trial
                    // Not present for canceled, expired subscriptions.
                    if ($inappResponse['paymentState'] == 1) {  // Payment received
                        $gplayData = [
                            'datetime_subscribed' => now(),
                            'lastpayment_datetime' => now()->toDateTimeString(),
                            'isSubscribed' => 1,
                        ];
                        $user->update($gplayData);
                        $updatePay = $pay->update([
                            'purchaseStatus' => "Subscribed",
                            'isAcknowledged'  => "Acknowledged"
                        ]);
                        $notificationCheck = User::where('isNotificationEnabled', '!=', 0)->find($user->id);

                        if ($notificationCheck) {
                            $packages = Package::where('id', $request->planId)->first();

                            $fcm['title'] = 'Subscription Purchased';
                            $fcm['body'] = 'Subscribed to Plan Successfully';
                            $fcm['data'] = [
                                'title' => 'Subscription Purchased',
                                'body' => 'Subscribed to Plan Successfully',
                                'type' => 'Subscription',
                                "notificationType" => 'SUBSCRIPTION_PURCHASED',
                                "subscriptionCode" => 4,
                                'notification' => json_encode(new SubscriptionResource($packages))
                            ];
                            $FCMToken = new FcmTokenService();
                            $firebase = new FirebaseService();
                            $tokens = $FCMToken->getUserFCMRegistrationToken($user->id);
                            foreach ($tokens as $key => $token) {
                                $fcm['FCMRegistrationToken'] = $token;
                                $firebase->cloudMessageToSingleDevice($fcm);
                            }
                        }

                        return response()->json(['response' => ['status' => true, 'data' => ['purchasedSubscription' => new PurchasedSubscriptionResource($pay)]]], JsonResponse::HTTP_OK);
                    } elseif ($inappResponse['paymentState'] == 0) {
                        $updatePay = $pay->update([
                            'purchaseStatus' => "SubscriptionPending",
                            'isAcknowledged'  => "Not Acknowledged"
                        ]);
                        return response()->json(['response' => ['status' => false, 'message' => 'Payment Pending!']], 202);
                    } else {
                        $updatePay = $pay->update([
                            'purchaseStatus' => "Unsubscribed",
                            'isAcknowledged'  => "Not Acknowledged"
                        ]);
                        return response()->json(null, 205);
                    }
                } else {
                    if ($inappResponse['purchaseState'] == 0) { //Purchased
                        $updatePay = $pay->update([
                            'purchaseStatus' => "Purchased",
                            'isAcknowledged'  => "Acknowledged"
                        ]);
                        if ($request->itemType == PurchasedItemTypeEnum::SONG) {
                            return response()->json(['response' => ['status' => true, 'data' => ['purchasedSong' => new PurchasedSubscriptionResource($pay)]]], JsonResponse::HTTP_OK);
                        } elseif ($request->itemType == PurchasedItemTypeEnum::ALBUM) {
                            return response()->json(['response' => ['status' => true, 'data' => ['purchasedAlbum' => new PurchasedSubscriptionResource($pay)]]], JsonResponse::HTTP_OK);
                        } elseif ($request->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG || $request->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM) {
                            return response()->json(['response' => ['status' => true, 'data' => ['purchasedPreOrder' => new PurchasedSubscriptionResource($pay)]]], JsonResponse::HTTP_OK);
                        }
                    }
                    if ($inappResponse['purchaseState'] == 1) { //Cancel
                        $updatePay = $pay->update([
                            'purchaseStatus' => "Canceled",
                            'isAcknowledged'  => "Not Acknowledged"
                        ]);
                        return response()->json(null, 205);
                    }
                    if ($inappResponse['purchaseState'] == 2) { //Pending
                        $updatePay = $pay->update([
                            'purchaseStatus' => "Pending",
                            'isAcknowledged'  => "Not Acknowledged"
                        ]);
                        return response()->json(['response' => ['status' => false, 'message' => 'Payment Pending!']], 202);
                    }
                }
            } elseif ($request->purchaseType == PurchaseTypeEnum::APPLE_PAY) {

                // For paid songs
                try {

                    $errors = [
                        '21000' => 'The request to the App Store didn’t use the HTTP POST request method.',
                        '21001' => 'The App Store no longer sends this status code.',
                        '21002' => 'The data in the receipt-data property is malformed or the service experienced a temporary issue. Try again.',
                        '21003' => 'The system couldn’t authenticate the receipt.',
                        '21004' => 'The shared secret you provided doesn’t match the shared secret on file for your account.',
                        '21005' => 'The receipt server was temporarily unable to provide the receipt. Try again.',
                        '21006' => 'This receipt is valid, but the subscription is in an expired state. When your server receives this status code, the system also decodes and returns receipt data as part of the response. This status only returns for iOS 6-style transaction receipts for auto-renewable subscriptions.',
                        '21007' => 'This receipt is from the test environment, but you sent it to the production environment for verification.',
                        '21008' => 'This receipt is from the production environment, but you sent it to the test environment for verification.',
                        '21009' => 'Internal data access error. Try again later.',
                        '21010' => 'The system can’t find the user account or the user account has been deleted.',
                    ];

                    if (env('APP_STORE_IOS_ENVIROMENT') == 'test') {
                        $reciptURL = 'https://sandbox.itunes.apple.com/verifyReceipt';
                    } else {
                        $reciptURL = 'https://buy.itunes.apple.com/verifyReceipt';
                    }

                    $response =  Http::post($reciptURL, ["receipt-data" => $request->purchaseToken, "password" => env('APP_STORE_SHARED_SECRET')]);

                    if ($response->successful()) {
                        $responseData = json_decode($response->body());
                        $latest_receipt_info = collect($responseData->latest_receipt_info)->sortByDesc('purchase_date_ms')->flatten();
                        Log::info('SubscribeToPlan Response:', ['latest_receipt_info' => $latest_receipt_info]);

                        if ($responseData->status == 0) {
                            // For Subscriptions
                            if ($request->itemType == PurchasedItemTypeEnum::PLAN_SUBSCRIPTION) {

                                $subscription = Purchased::create([
                                    'userId'  => $user->id,
                                    'packageId' => $packageId,
                                    'amount' => $request->paidAmount,
                                    'purchaseType' => PurchaseTypeEnum::APPLE_PAY,
                                    'itemType' => PurchasedItemTypeEnum::PLAN_SUBSCRIPTION,
                                    'transactionId' => $latest_receipt_info[0]->transaction_id,
                                    'isSong' => 0,
                                    'purchaseStatus' => 'SUBSCRIBED',
                                    'subType'  => 'INITIAL_BUY',
                                    'response'  => null
                                ]);

                                $notificationCheck = User::where('isNotificationEnabled', '!=', 0)->find($user->id);

                                if ($notificationCheck) {
                                    $packages = Package::where('id', $request->planId)->first();

                                    $fcm['title'] = 'Subscription Purchased';
                                    $fcm['body'] = 'Subscribed to Plan Successfully';
                                    $fcm['data']['title'] = 'Subscription Purchased';
                                    $fcm['data']['body'] = 'Subscription to Plan Successfully';

                                    $user->update([
                                        'datetime_subscribed' => now(),
                                        'lastpayment_datetime' => now()->toDateTimeString(),
                                        'isSubscribed' => 1,
                                    ]);


                                    $fcm['data']['type'] = 'Subscription';
                                    $fcm['data']['notificationType'] = $subscription->purchaseStatus;
                                    $fcm['data']['notificationSubType'] =  $subscription->subType;
                                    $fcm['data']['subscriptionCode'] =  4;
                                    $fcm['data']['notification'] = json_encode(new SubscriptionResource($packages));

                                    $FCMToken = new FcmTokenService();
                                    $firebase = new FirebaseService();
                                    $tokens = $FCMToken->getUserFCMRegistrationToken($user->id);

                                    foreach ($tokens as $key => $token) {
                                        $fcm['FCMRegistrationToken'] = $token;
                                        $firebase->cloudMessageToSingleDevice($fcm);
                                    }
                                }

                                return response()->json(['response' => ['status' => true, 'data' => ['purchasedSubscription' => new PurchasedSubscriptionResource($subscription)]]], JsonResponse::HTTP_OK);
                            } else {

                                // For Single Song | Album | Preorder Song
                                $pay = Purchased::create([
                                    'userId'  => $user->id,
                                    'songId'  => $songId,
                                    'albumId'  => $albumId,
                                    'packageId' => $packageId,
                                    'cardId' => $cardId,
                                    'purchaseType' => $request->purchaseType,
                                    'itemType' => $request->itemType,
                                    'amount' => $request->paidAmount,
                                    'transactionId' => $latest_receipt_info[0]->transaction_id,
                                    'paidAmount' => $request->paidAmount,
                                    'isSong' => $isSong,
                                    'purchaseStatus' => 'PURCHASED',
                                    'isAcknowledged'  => 'Acknowledged',
                                    'response'  => json_encode($responseData)
                                ]);


                                if ($request->itemType == PurchasedItemTypeEnum::SONG) {
                                    return response()->json(['response' => ['status' => true, 'data' => ['purchasedSong' => new PurchasedSubscriptionResource($pay)]]], JsonResponse::HTTP_OK);
                                } elseif ($request->itemType == PurchasedItemTypeEnum::ALBUM) {
                                    return response()->json(['response' => ['status' => true, 'data' => ['purchasedAlbum' => new PurchasedSubscriptionResource($pay)]]], JsonResponse::HTTP_OK);
                                } elseif ($request->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG || $request->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM) {
                                    return response()->json(['response' => ['status' => true, 'data' => ['purchasedPreOrder' => new PurchasedSubscriptionResource($pay)]]], JsonResponse::HTTP_OK);
                                }
                            }
                        } else {
                            return response(['response' => ['status' => false, 'message' => $errors[$responseData->status]]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        }
                    }

                    if ($response->failed()) {
                        return response(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    }
                } catch (Exception $e) {
                    return response(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
                }
            } elseif ($request->purchaseType == PurchaseTypeEnum::PAYPAL) { //will work later on
                return response()->json(['response' => ['status' => false, 'message' => 'Not implimented yet!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            } elseif ($request->purchaseType == PurchaseTypeEnum::CARD) { //will work later on
                return response()->json(['response' => ['status' => false, 'message' => 'Not implimented yet!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Subscribe error exception');
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function subscribeToPlan(Request $request, $subscriptionReceiptResponse)
    {
        $platform = $request->purchaseType;
        $receipt = $request->transactionReceipt;
        $purchase_token = $request->purchaseToken;
        $order_id = $subscriptionReceiptResponse->getOrderId();

        $user = $request->user();
        $this->verifySubscription($user, $platform, $purchase_token, $order_id, $receipt);
        return 'ok';
    }

    private function verifySubscription($user, $platform, $purchase_token, $order_id, $receipt = null)
    {
        $gplay_data = [
            'gplay_order_token' => $purchase_token,
            'gplay_order_id' => $order_id,
        ];

        $user->update($gplay_data);
    }
}
