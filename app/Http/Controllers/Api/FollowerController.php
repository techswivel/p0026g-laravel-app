<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Artist\ArtistDetailRequest;
use App\Http\Requests\Api\Artist\FollowRequest;
use App\Http\Resources\Artist\ArtistDetailResource;
use App\Http\Resources\Follower\FollowerCollection;
use App\Models\Follower;
use App\Models\User;
use App\Traits\loggerExceptionTrait;
use App\Traits\PaginationTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FollowerController extends Controller
{
    use PaginationTrait,loggerExceptionTrait;

    public function artistFollow(FollowRequest $request){
        try {
            $user = request()->user();
            if($request->isFollowed == true){
                $checkUser = User::where('id',$request->artistId)->role('artist')->count();
                if($checkUser == 0){
                    return response()->json(['response' => ['status' => false, 'message' => 'Artist not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                $follower = Follower::where('userId',$user->id)
                    ->where('artistId',$request->artistId)->first();
                if($follower){
                    return response()->json(['response' => ['status' => false, 'message' => 'Already followed this artist!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                DB::beginTransaction();
                $follower = Follower::create([
                    'userId' => $user->id,
                    'artistId' => $request->artistId
                ]);
                DB::commit();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            }

            if($request->isFollowed == false){
                $follower = Follower::where('userId',$user->id)
                    ->where('artistId',$request->artistId)->first();
                if($follower != null){
                    $follower->delete();
                    return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
                }else{
                    return response()->json(['response' => ['status' => false, 'message' => 'Artist not found in follower list!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
            }
            return response()->json(['response' => ['status' => false, 'message' => 'Selected is follower value not correct!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->saveExceptionLog($e, 'Artist follow exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
    public function getFollowingArtist(Request $request){
        try{
            $limit = $request->limit;
            $paginate = $request->page;
            $artistIds = [];
            $artistIds = Follower::where('userId',auth()->user()->id)->pluck('artistId')->toArray();
            $follower = User::whereIn('id',$artistIds)->where('status','!=','Block')->paginate($limit, ['*'], 'page', $paginate );
            return response()->json(['response' => ['status' => true, 'data' => new FollowerCollection($follower)]], JsonResponse::HTTP_OK);
        }catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Get follow artist exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
    public function artistDetail(ArtistDetailRequest $request){
        try{
            $artist = User::where('id',$request->artistId)->where('status','!=','Block')->role('artist')->first();
            if($artist){
                return response()->json(['response' => ['status' => true, 'data' => ['artistDetails' =>new ArtistDetailResource($artist)]]], JsonResponse::HTTP_OK);
            }else{
                return response()->json(['response' => ['status' => false, 'message' => 'Artist not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        }catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Artist detail exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

}
