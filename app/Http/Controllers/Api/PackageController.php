<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Package\PackageCollection;
use App\Models\Package;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    use loggerExceptionTrait;
    public function index(Request $request)
    {
        try {
            $limit = $request->limit;
            $paginate = $request->page;

            $packages = Package::where('status',null)->paginate($limit, ['*'], 'page', $paginate );

            return response()->json(['response' => ['status' => true, 'data' => new PackageCollection($packages), ]], JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e,'package-plans error Exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
