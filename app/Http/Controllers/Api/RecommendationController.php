<?php

namespace App\Http\Controllers\Api;

use App\Enums\AlbumPreOrderEnum;
use App\Enums\SongPreOrderEnum;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RecommandedSongs\RecommandedSong;
use App\Http\Resources\Album\AlbumResource;
use App\Http\Resources\Artist\ArtistResource;
use App\Http\Resources\Song\SongResource;
use App\Models\Album;
use App\Models\Category;
use App\Models\ListeningHistory;
use App\Models\Purchased;
use App\Models\Song;
use App\Models\User;
use App\Models\UserInterest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class RecommendationController extends Controller
{
    use loggerExceptionTrait;
    public function recommended(RecommandedSong $request){
        try {

            //RECOMMENDED_SONGS
            if ($request->requestType == 'RECOMMENDED') {
                $interestCategories = UserInterest::where('userId',Auth::user()->id)->pluck('categoryId')->toArray();
                if ($request->dataType == 'SONGS') {
                    $categoryCount = count($interestCategories);
                    $songIds=Song::whereIn('categoryId',$interestCategories)->pluck('id')->toArray();
                    $songCheck=Song::whereIn('id',$songIds)->pluck('userId')->toArray();
                    $checkArtistBlock=User::whereIn('id',$songCheck)->where('status','!=','Block')->pluck('id')->toArray();
                    $recommendedFreeSongs = Song::with('category','language')->where('type',SongPreOrderEnum::Song)->where('isPaid',0)
                                                    ->whereIn('categoryId',$interestCategories)
                                                    ->whereIn('userId',$checkArtistBlock)
                                                    ->limit($categoryCount*3)->get();
                    $recommendedPaidSongs = Song::with('category','language')->where('type',SongPreOrderEnum::Song)->where('isPaid',1)
                                                    ->whereIn('categoryId',$interestCategories)
                                                    ->whereIn('userId',$checkArtistBlock)
                                                    ->limit($categoryCount)->get();
                    $recommendedSongs = $recommendedFreeSongs->concat($recommendedPaidSongs)->shuffle();
                    return response()->json(['response' => ['status' => true, 'data' => [
                        'songs' => SongResource::collection($recommendedSongs)
                    ]]], JsonResponse::HTTP_OK);
                }
                if ($request->dataType == 'ALBUM') {
                    $categoryAlbums = Album::where('type',AlbumPreOrderEnum::Album)
                                            ->whereHas('getArtist',function($query){
                                                $query->where('status','!=','Block');
                                            })
                                            ->whereHas('albumSongs',function($query)use($interestCategories){
                                                $query->whereHas('getSongs',function($querySong)use($interestCategories){
                                                    $querySong->whereIn('categoryId',$interestCategories);
                                                });
                                            })->limit(10)->get();
                    $countCategoryAlbums = 10 - count($categoryAlbums);
                    $categoryAlbumsIds = $categoryAlbums->pluck('id')->toArray();
                    $nonCaategoryAlbums = Album::where('type',AlbumPreOrderEnum::Album)                    
                                                ->whereHas('getArtist',function($query){
                                                    $query->where('status','!=','Block');
                                                })
                                                ->whereNotIn('id',$categoryAlbumsIds)->limit($countCategoryAlbums)->get();
                    $recommendedAlbums = $categoryAlbums->concat($nonCaategoryAlbums)->shuffle();
                    return response()->json(['response' => ['status' => true, 'data' => [
                        'albums' => AlbumResource::collection($recommendedAlbums)
                    ]]], JsonResponse::HTTP_OK);
                }
                if ($request->dataType == 'ARTIST') {
                    $categoryArtist = User::whereHas('userSongs',function($query)use($interestCategories){
                                            $query->whereIn('categoryId',$interestCategories);
                                        })->role('artist')->where('status','!=','Block')->limit(10)->get();
                    $countCategoryArtists = 10 - count($categoryArtist);
                    $categoryArtistsIds = $categoryArtist->pluck('id')->toArray();
                    $nonCaategoryArtists = User::where('status','!=','Block')->whereNotIn('id',$categoryArtistsIds)->role('artist')->limit($countCategoryArtists)->get();
                    $recommendedArtist = $categoryArtist->concat($nonCaategoryArtists)->shuffle();
                    return response()->json(['response' => ['status' => true, 'data' => [
                        'artist' => ArtistResource::collection($recommendedArtist)
                    ]]], JsonResponse::HTTP_OK);
                }
            }

            // LISTENING_HISTORY
            if ($request->requestType == 'HISTORY') {
                $listeningHistory = ListeningHistory::where('userId',Auth::user()->id);
                if ($request->dataType == 'SONGS') {
                    $songIds = $listeningHistory->pluck('songId')->toArray();
                    $songCheck=Song::whereIn('id',$songIds)->pluck('userId')->toArray();
                    $checkArtistBlock=User::whereIn('id',$songCheck)->where('status','!=','Block')->pluck('id')->toArray();
                    $historySongs = Song::with('category','language')->where('type',SongPreOrderEnum::Song)
                                                ->whereIn('userId',$checkArtistBlock)->orderBy('name','ASC')->whereIn('id',$songIds)->get();
                    return response()->json(['response' => ['status' => true, 'data' => [
                        'songs' => SongResource::collection($historySongs)
                    ]]], JsonResponse::HTTP_OK);
                }
                if ($request->dataType == 'ALBUM') {
                    $albumIds = $listeningHistory->whereNotNull('albumId')->pluck('albumId')->toArray();
                    $albumCheck=Album::whereIn('id',$albumIds)->pluck('userId')->toArray();
                    $artistCheckBlock=User::whereIn('id',$albumCheck)->where('status','!=','Block')->pluck('id')->toArray();
                    $historyAlbums = Album::where('type',AlbumPreOrderEnum::Album)->whereIn('id',$albumIds)->whereIn('userId',$artistCheckBlock)->get();
                    return response()->json(['response' => ['status' => true, 'data' => [
                        'albums' => AlbumResource::collection($historyAlbums)
                    ]]], JsonResponse::HTTP_OK);
                }
                if ($request->dataType == 'ARTIST') {
                    $artistIds = $listeningHistory->whereNotNull('artistId')->pluck('artistId')->toArray();
                    $songIds = ListeningHistory::where('userId',Auth::user()->id)->pluck('songId')->toArray();
                    $songArtistIds = Song::whereIn('id',$songIds)->pluck('userId')->unique()->toArray();
                    $historyArtist = User::whereIn('id', array_merge($artistIds,$songArtistIds))->role('artist')->where('status','!=','Block')->get();
                    return response()->json(['response' => ['status' => true, 'data' => [
                        'artist' => ArtistResource::collection($historyArtist)
                    ]]], JsonResponse::HTTP_OK);
                }
            }

            // PURCHASED_ALBUM
            if($request->requestType == "PURCHASED_ALBUM"){
                $albumIds = Purchased::where('userId',Auth::user()->id)
                ->whereNotNull('albumId')->pluck('albumId')->toArray();
                $albumCheck=Album::whereIn('id',$albumIds)->pluck('userId')->toArray();
                $artistCheckBlock=User::whereIn('id',$albumCheck)->where('status','!=','Block')->withTrashed()->pluck('id')->toArray();
                $purchasedAlbums = Album::where('type',AlbumPreOrderEnum::Album)->whereIn('id',$albumIds)->whereIn('userId',$artistCheckBlock)->withTrashed()->get();
                return response()->json(['response' => ['status' => true, 'data' => [
                                                'albums' => AlbumResource::collection($purchasedAlbums),
                                            ]]], JsonResponse::HTTP_OK);
            }

            // ARTIST_ALBUM
            if($request->requestType == "ARTIST_ALBUM"){
                $artistCheckBlock=User::where('id',$request->artistId)->where('status','!=','Block')->pluck('id')->toArray();
                $artistAlbums = Album::where('type',AlbumPreOrderEnum::Album)->whereIn('userId',$artistCheckBlock)->get();
                return response()->json(['response' => ['status' => true, 'data' => [
                                                'albums' => AlbumResource::collection($artistAlbums),
                                            ]]], JsonResponse::HTTP_OK);

            }

            // CATEGORY
            if ($request->requestType == 'CATEGORY') {
                $getCategory = Category::find($request->categoryId);
                if(!$getCategory){
                    return response()->json(['response' => ['status' => false, 'message' => "Category not found."]], JsonResponse::HTTP_OK);
                }
                $songCheck=Song::where('categoryId',$request->categoryId)->pluck('userId')->toArray();
                $artistCheckBlock=User::whereIn('id',$songCheck)->where('status','!=','Block')->pluck('id')->toArray();
                $categorySongs = Song::with('category','language')->where('type',SongPreOrderEnum::Song)
                                                ->whereIn('userId',$artistCheckBlock)->where('categoryId',$request->categoryId)->orderBy('name','ASC')->get();
                if ($request->dataType == 'SONGS') {

                    return response()->json(['response' => ['status' => true, 'data' => [
                                                    'totalSongs'=> (count($categorySongs)),
                                                    'songs' => SongResource::collection($categorySongs),
                                                ]]], JsonResponse::HTTP_OK);
                }
                if ($request->dataType == 'ALBUM') {
                    $categoryAlbums = Album::where('type',AlbumPreOrderEnum::Album)->whereIn('userId',$artistCheckBlock)
                                            ->whereHas('albumSongs',function($query)use($request){
                                                $query->whereHas('getSongs',function($querySong)use($request){
                                                    $querySong->where('categoryId',$request->categoryId);
                                                });
                                            })->get();

                    return response()->json(['response' => ['status' => true, 'data' => [
                                                    'totalSongs'=> (count($categorySongs)),
                                                    'albums' => AlbumResource::collection($categoryAlbums),
                                                ]]], JsonResponse::HTTP_OK);
                }
                if ($request->dataType == 'ARTIST') {
                    $categoryArtist = User::whereHas('userSongs',function($query)use($request,$artistCheckBlock){
                                                $query->where('categoryId',$request->categoryId);
                                                $query->whereIn('userId',$artistCheckBlock);
                                            })->role('artist')->get();
                    return response()->json(['response' => ['status' => true, 'data' => [
                                                    'totalSongs'=> (count($categorySongs)),
                                                    'artist' => ArtistResource::collection($categoryArtist)
                                                ]]], JsonResponse::HTTP_OK);
                }
            }


            return response()->json(['response' => ['status' => false, 'message' => "Select correct request parameters."]], JsonResponse::HTTP_OK);
        } catch (Exception $e) {
            $this->saveExceptionLog($e, 'Recommended error exception');
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
