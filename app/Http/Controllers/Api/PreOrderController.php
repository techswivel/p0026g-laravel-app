<?php

namespace App\Http\Controllers\Api;

use App\Enums\AlbumPreOrderEnum;
use App\Enums\SongPreOrderEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Artist\PreOrderDetaillRequest;
use App\Http\Resources\PreOrder\PreOrderCollection;
use App\Models\Song;
use App\Models\User;
use App\Traits\loggerExceptionTrait;
use App\Traits\PaginationTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PreOrderController extends Controller
{
    use PaginationTrait,loggerExceptionTrait;

    public function preOrderDetail(PreOrderDetaillRequest $request){
        try{
            $limit = $request->limit?$request->limit:15;
            $paginate = $request->page?$request->page:1;
            $artist  = User::where('id',$request->artistId)->role('artist')->count();
            if($artist == 0){
                return response()->json(['response' => ['status' => false, 'message' => 'Artist not found!']], JsonResponse::HTTP_BAD_REQUEST);
            }
            $artistCheckBlock=User::role('artist')->where('id',$request->artistId)->where('status','!=','BLock')->pluck('id')->first();
            if($artistCheckBlock == null){
                return response()->json(['response' => ['status' => false, 'message' => 'Artist is  Block!']], JsonResponse::HTTP_BAD_REQUEST);
            }
                $albums = DB::table("albums")
                    ->select(DB::raw('albums.id as id
                        ,albums.preOrderReleaseDate as preOrderReleaseDate
                        ,albums.userId as userId
                        ,albums.name as preOrderTitle
                        ,albums.totalPreOrderCount as totalPreOrderCount
                        ,albums.preOrderCount as preOrderCount
                        ,albums.thumbnail as thumbnail
                        ,albums.sku as sku
                        ,"Album" as preOrderType')
                        )->where('type',AlbumPreOrderEnum::PreOrder)->where('userId',$request->artistId)->where('deletedAt',null);
                $songs = DB::table("songs")
                    ->select(DB::raw('songs.id as id
                        ,songs.preOrderReleaseDate as preOrderReleaseDate
                        ,songs.userId as userId
                        ,songs.name as preOrderTitle
                        ,songs.totalPreOrderCount as totalPreOrderCount
                        ,songs.preOrderCount as preOrderCount
                        ,songs.thumbnail as thumbnail
                        ,songs.sku as sku
                        ,"Song" as preOrderType')
                    )
                    ->where('type',SongPreOrderEnum::PreOrder)->where('userId',$request->artistId)->where('deletedAt',null)
                    ->unionAll($albums)
                    ->get();
                $preOrderSong = $this->paginate($songs,$limit,$paginate);
            return response()->json(['response' => ['status' => true, 'data' => new PreOrderCollection($preOrderSong)]], JsonResponse::HTTP_OK);
        }catch (\Exception $e) {
            $this->saveExceptionLog($e, 'PreOrderDetail error exception');
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
