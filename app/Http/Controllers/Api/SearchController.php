<?php

namespace App\Http\Controllers\Api;

use App\Enums\AlbumPreOrderEnum;
use App\Enums\SongPreOrderEnum;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Search\SearchRequest;
use App\Http\Resources\SongAlbumArtist\SongAlbumArtistCollection;
use App\Models\Album;
use App\Models\Song;
use App\Models\User;
use App\Traits\PaginationTrait;
use Exception;
use Illuminate\Http\JsonResponse;

class SearchController extends Controller
{
    use PaginationTrait,loggerExceptionTrait;

    public function search(SearchRequest $request){
        try {
            $limit = $request->limit;
            $paginate = $request->page;

            if ($request->has('languageId')) {
                $songs = Song::where('name','LIKE',"%$request->queryString%")->where('languageId',$request->languageId)->get();
                $searchSongsAlbumsArtists = $this->paginate($songs, $limit, $paginate);
            }else{
                $songs = Song::where('name','LIKE',"%$request->queryString%")->get();
                $albums = Album::where('name','LIKE',"%$request->queryString%")->get();
                $artists = User::where('firstName','LIKE',"%$request->queryString%")->orWhere('lastName','LIKE',"%$request->queryString%")
                                            ->role('artist')->get();
    
                $searchSongsAlbums = $songs->concat($albums);
                $searchResults = $searchSongsAlbums->concat($artists);
                $searchSongsAlbumsArtists = $this->paginate($searchResults, $limit, $paginate);
            }

            return response()->json(['response' => ['status' => true, 'data' =>  new SongAlbumArtistCollection($searchSongsAlbumsArtists)
                                    ]], JsonResponse::HTTP_OK);
        } catch (Exception $e) {
            $this->saveExceptionLog($e, 'Search songs error exception');
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
