<?php

namespace App\Http\Requests\Api\RecommandedSongs;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class RecommandedSong extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * Conditionally Adding Rules
     * https://laravel.com/docs/9.x/validation#rule-boolean
     * exclude_if:has_appointment
     * exclude_unless:has_appointment
     */
    public function rules()
    {
        return [
            'requestType' => 'required|in:RECOMMENDED,HISTORY,PURCHASED_ALBUM,ARTIST_ALBUM,CATEGORY',
            'dataType' => 'required_if:requestType,RECOMMENDED,HISTORY,CATEGORY|in:SONGS,ALBUM,ARTIST',
            'artistId' => 'numeric|required_if:requestType,ARTIST_ALBUM',
            'categoryId' => 'numeric|required_if:requestType,CATEGORY'
        ];

    }
    
    protected function failedValidation(Validator $validator)
    {
        $error = collect($validator->errors())->collapse()->toArray();
        $errors = implode(' | ', $error);
        throw new HttpResponseException(response()->json(
            ['response' => ['status' => false, 'message' => $errors]],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
        ));
    }
}
