<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable',
            'dOb' => 'nullable' ,
            'city' => 'nullable',
            'state' => 'nullable',
            'country' => 'nullable',
            'completeAddress' => 'nullable',
            'zipCode' => 'nullable',
            'profile' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
            'isEnableNotification'=>'nullable|in:true,false',
            'isArtistUpdateEnable'=>'nullable|in:true,false',
            'otp'=>'nullable|required_with:phoneNumber|min:5|integer|exists:otps,otp',
            'phoneNumber'=>'nullable|required_with:otp,required',

        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @return void
     *
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $error = collect($validator->errors())->collapse()->toArray();
        $errors = implode(' | ', $error);
        throw new HttpResponseException(response()->json(
            ['response' => ['status' => false, 'message' => $errors]],
            Response::HTTP_UNPROCESSABLE_ENTITY));
    }
}
