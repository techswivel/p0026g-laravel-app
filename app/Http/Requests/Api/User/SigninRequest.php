<?php

namespace App\Http\Requests\Api\User;

use App\Enums\LoginType;
use App\Enums\SocialSitesEnum;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class SigninRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'loginType'=>'required|in:SOCIAL,SIMPLE',
            'email'=> 'required_if:loginType,SIMPLE|email|exists:users',
            'socialSite'=>'required_if:loginType,SOCIAL|in:'.SocialSitesEnum::FACEBOOK.','.SocialSitesEnum::GMAIL.','.SocialSitesEnum::APPLE.'',
            'accessToken'=> 'required_if:loginType,SOCIAL',
            'password'=>'required_if:loginType,SIMPLE',
            'fcmToken'=>'required',
            'deviceIdentifier'=>'required',
            'deviceName'=>'required'

        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'Email is required!',
            'email.exists' => 'User not registered!',
            'password.required' => 'Password is required',
            "fcmToken.required"=>'Fcm token is required',
            "deviceIdentifier.required"=>'Device identifier is required'

        ];
    }
    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @return void
     *
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $error = collect($validator->errors())->collapse()->toArray();
        $errors = implode(' | ', $error);
        throw new HttpResponseException(response()->json(
            ['response' => ['status' => false, 'message' => $errors]],
            Response::HTTP_UNPROCESSABLE_ENTITY));
    }
}
