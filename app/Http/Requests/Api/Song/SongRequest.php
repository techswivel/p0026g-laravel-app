<?php

namespace App\Http\Requests\Api\Song;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class SongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'=>'required|in:TRENDING,PLAY_LIST,FAVOURITES,PURCHASED,ALBUM,DOWNLOADED,CATEGORY_ARTIST_SONGS', 
            'playListId' => 'numeric|required_if:type,PLAY_LIST',
            'albumId' => 'numeric|required_if:type,ALBUM',
            'categoryId' => 'numeric|required_if:type,CATEGORY_ARTIST_SONGS', 
            'artistId' => 'numeric|required_if:type,CATEGORY_ARTIST_SONGS'          
        ];
    }
    
    protected function failedValidation(Validator $validator)
    {
        $error = collect($validator->errors())->collapse()->toArray();
        $errors = implode(' | ', $error);
        throw new HttpResponseException(response()->json(
            ['response' => ['status' => false, 'message' => $errors]],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
        ));
    }
}
