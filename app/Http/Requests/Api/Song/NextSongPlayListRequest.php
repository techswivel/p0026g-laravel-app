<?php

namespace App\Http\Requests\Api\Song;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class NextSongPlayListRequest extends FormRequest
{
        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "currentSongId" => 'numeric|required',
            "categoryId" => 'numeric|required_if:playedFrom,==,CATEGORY',
            "albumId" => 'numeric|required_if:playedFrom,==,ALBUM,PURCHASED_ALBUM',
            "playListId" => 'numeric|required_if:playedFrom,==,PLAYLIST',
            "artistId" => 'numeric|required_if:playedFrom,==,ARTIST_SONGS',
            "playedFrom" => 'required|in:PLAYLIST,CATEGORY,FAVOURITE,PURCHASED_SONG,PURCHASED_ALBUM,DOWNLOADED,ALBUM,SEARCHED,EMPTY,ARTIST_SONGS',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $error = collect($validator->errors())->collapse()->toArray();
        $errors = implode(' | ', $error);
        throw new HttpResponseException(response()->json(
            ['response' => ['status' => false, 'message' => $errors]],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
        ));
    }
}
