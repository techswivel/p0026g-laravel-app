<?php

namespace App\Http\Requests\Api\Song;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class SyncRecentlyPlayedRequest extends FormRequest
{
        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "isRecentlyPlayed" => 'boolean|required',
            "recentlyPlayed" => 'required_if:isRecentlyPlayed,==,true',
            "isListeningHistory" => 'boolean|required',
            "listeningHistory" => 'required_if:isListeningHistory,==,true',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $error = collect($validator->errors())->collapse()->toArray();
        $errors = implode(' | ', $error);
        throw new HttpResponseException(response()->json(
            ['response' => ['status' => false, 'message' => $errors]],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
        ));
    }
}
