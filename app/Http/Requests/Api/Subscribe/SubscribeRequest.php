<?php

namespace App\Http\Requests\Api\Subscribe;

use Illuminate\Foundation\Http\FormRequest;

class SubscribeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [ 
            'purchaseType'=>'required|in:GOOGLE_PAY,PAYPAL,APPLE_PAY,CARD',
            'purchaseToken'=>'required|string', 
            'itemType' => 'required|in:SONG,ALBUM,PLAN_SUBSCRIPTION,PRE_ORDER_SONG,PRE_ORDER_ALBUM',
            'planId' => 'numeric|required_if:itemType,PLAN_SUBSCRIPTION',
            'songId' => 'numeric|required_if:itemType,SONG',
            'albumId' => 'numeric|required_if:itemType,ALBUM',
            'cardId' => 'numeric|required_if:purchaseType,CARD', 
            'preOrderSongId' => 'numeric|required_if:itemType,PRE_ORDER_SONG',
            'preOrderAlbumId' => 'numeric|required_if:itemType,PRE_ORDER_ALBUM',
            'paidAmount' => 'required'
        ];
    }
}
