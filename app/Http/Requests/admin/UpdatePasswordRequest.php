<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => [
                'required',
                Password::min(8)
                    ->mixedCase() // allows both uppercase and lowercase
                    ->letters() //accepts letter
                    ->numbers() //accepts numbers
                    ->symbols() //accepts special character
                    ->uncompromised(),//check to be sure that there is no data leak

            ],
            'confirmPassword' => 'required_with:password|same:password|min:8',
            'oldPassword'=>'required'
        ];
    }
}
