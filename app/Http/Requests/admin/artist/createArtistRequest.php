<?php

namespace App\Http\Requests\admin\artist;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Rules\IsValidPassword;

class createArtistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->user();
        return [
            'artistFirstName' => 'required|string|max:255',
            'artistLastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|'.Rule::unique('users'),
            'artistPassword' => ['required','string',new IsValidPassword],
            'artistBio' => 'max:1024',
            'artistAvatar' => 'image|mimes:jpg,png,jpeg|max:2048',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'errors' => $validator->errors(),
            'status' => '400'
        ]));
    }
}
