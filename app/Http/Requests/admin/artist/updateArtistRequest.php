<?php

namespace App\Http\Requests\admin\artist;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Rules\IsValidPassword;

class updateArtistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'artistId' => 'required',
            'editArtistFirstName' => 'required|string|max:255',
            'editArtistLastName' => 'required|string|max:255',
            'editArtistPassword' => ['nullable',new IsValidPassword],
            'editArtistBio' => 'max:1024',
            'editArtistAvatar' => 'image|mimes:jpg,png,jpeg|max:2048',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'errors' => $validator->errors(),
            'status' => '400'
        ]));
    }
}
