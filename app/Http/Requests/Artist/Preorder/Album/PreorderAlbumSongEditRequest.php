<?php

namespace App\Http\Requests\Artist\Preorder\Album;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Rules\PriceField;

class PreorderAlbumSongEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $editPreorderSongStatus = 'Paid Song';
        return [
            'edtiAlbumSongPreorderName' => 'required|string|max:30',
            'editAlbumSongPreorderPrice' => new PriceField($editPreorderSongStatus),
            'editAlbumSongPreorderCategory' => 'required',
            'editAlbumSongPreorderLanguage' => 'required',
            'editAlbumSongPreorderThumbnail' => 'image|mimes:jpg,png,jpeg|max:2048|dimensions:ratio=1/1',
            'editAlbumSongPreorderAudio' =>  'max:10240|mimes:audio/mpeg,mp3,wav',
            'editAlbumSongPreorderVideo1' =>'mimetypes:video/mp4',
            'editAlbumSongPreorderVideo' => 'mimetypes:video/mp4',
            'editAlbumSongPreorderLyrics' => 'max:3000',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'editAlbumSongPreorderThumbnail.dimensions' => 'The preorder song thumbnail has invalid image dimensions that must be 1:1'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'errors' => $validator->errors(),
            'status' => '400'
        ]));
    }
}
