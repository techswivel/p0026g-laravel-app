<?php

namespace App\Http\Requests\Artist\Preorder\Album;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PreorderAlbumEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'editPreorderAlbumName'=> 'required|string|max:30',
            'editPreorderAlbumThumbnail' => 'image|mimes:jpg,png,jpeg|max:2048|dimensions:ratio=1/1',
            'editPreorderAlbumDate' => 'required|date_format:m/d/Y',
            'editPreorderAlbumTime' => 'required|date_format:g:i A',
            'editPreorderSelectedSongs' => 'required',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'editPreorderAlbumThumbnail.dimensions' => 'The preorder album thumbnail has invalid image dimensions that must be 1:1'
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'errors' => $validator->errors(),
            'status' => '400'
        ]));
    }
}
