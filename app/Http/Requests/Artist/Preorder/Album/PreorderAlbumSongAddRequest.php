<?php

namespace App\Http\Requests\Artist\Preorder\Album;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Rules\PriceField;

class PreorderAlbumSongAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $preorderSongStatus = 'Paid Song';
        $songStep = $this->request->get('stepNo');

        return [
            'albumSongPreorderName' => $songStep == 1 ? 'required|string|max:30' : 'nullable',
            'albumSongPreorderCategory' => $songStep == 1 ? 'required' : 'nullable',
            'albumSongPreorderLanguage' => $songStep == 1 ? 'required' : 'nullable',
            'albumSongPreorderPrice' => $songStep == 2 ? new PriceField($preorderSongStatus) : 'nullable',
            'albumSongPreorderThumbnail' => $songStep == 2 ? 'required|image|mimes:jpg,png,jpeg|max:2048|dimensions:ratio=1/1' : 'nullable',
            'albumSongPreorderAudio' => $songStep == 2 ?  'required|max:10240|mimes:audio/mpeg,mp3,wav' : 'nullable',
            'albumSongPreorderVideo' => $songStep == 2 ? 'mimetypes:video/mp4' : 'nullable',
            'albumSongPreorderLyrics' => $songStep == 2 ? 'max:3000' : 'nullable',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'albumSongPreorderThumbnail.dimensions' => 'The preorder song thumbnail has invalid image dimensions that must be 1:1'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'errors' => $validator->errors(),
            'status' => '400'
        ]));
    }
}
