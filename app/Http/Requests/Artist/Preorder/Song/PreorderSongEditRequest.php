<?php

namespace App\Http\Requests\Artist\Preorder\Song;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Rules\PriceField;

class PreorderSongEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $preorderSongStatus = 'Paid Song';
        return [
            'editPreorderSongName'=> 'required|string|max:30',
            'editPreorderSongCategory' => 'required',
            'editPreorderSongLanguage' => 'required',
            'editPreorderSongThumbnail' => 'image|mimes:jpg,png,jpeg|max:2048|dimensions:ratio=1/1',
            'editPreorderAudioFile' => 'max:10240|mimes:audio/mpeg,mp3,wav',
            'editPreorderVideoFile' => 'mimetypes:video/mp4',
            'editPreorderSongLyrics' => 'max:3000',
            'editPreorderVideoFile1' => 'mimetypes:video/mp4',
            'editPreorderSongDate' => 'required|date_format:m/d/Y',
            'editPreorderSongTime' => 'required|date_format:g:i A',
        ];
    }

     /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'editPreorderSongThumbnail.dimensions' => 'The preorder song thumbnail has invalid image dimensions that must be 1:1'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'errors' => $validator->errors(),
            'status' => '400'
        ]));
    }
}
