<?php

namespace App\Http\Requests\Artist\Song;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Rules\PriceField;

class SongAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $songStatus = $this->request->get('songStatus');
        $songStep = $this->request->get('stepNo');

        return [
            'songTitle' => $songStep == 1 ? 'required|string|max:30' : 'nullable',
            'songStatus' => $songStep == 1 ? 'required' : 'nullable',
            'songCategory' => $songStep == 1 ? 'required' : 'nullable',
            'songLanguage' => $songStep == 1 ? 'required' : 'nullable',
            'songPrice' => $songStep == 2 ? new PriceField($songStatus) : 'nullable',
            'songThumbnail' => $songStep == 2 ? 'required|image|mimes:jpg,png,jpeg|max:2048|dimensions:ratio=1/1' : 'nullable',
            'songAudio' =>  $songStep == 2 ? 'required|max:10240|mimes:audio/mpeg,mp3,wav' : 'nullable',
            'songVideo' => $songStep == 2 ? 'mimetypes:video/mp4' : 'nullable',
            'songLyrics' => $songStep == 2 ? 'max:3000' : 'nullable',
        ];
    }


    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'songThumbnail.dimensions' => 'The song thumbnail has invalid image dimensions that must be 1:1'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'errors' => $validator->errors(),
            'status' => '400'
        ]));
    }
}
