<?php

namespace App\Http\Requests\Artist\Song;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Rules\PriceField;

class SongEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $songStatus = $this->request->get('editSongStatus');
        return [
            'editSongName' => 'required|string|max:30',
            'editSongStatus' => 'required',
            'editPrice' => new PriceField($songStatus),
            'editSongCategory' => 'required',
            'editSongLanguage' => 'required',
            'editThumbnail' => 'image|mimes:jpg,png,jpeg|max:2048|dimensions:ratio=1/1',
            'editAudioFile' => 'max:10240|mimes:audio/mpeg,mp3,wav',
            'editVideoFile' => 'mimetypes:video/mp4',
            'editVideoFile1' => 'mimetypes:video/mp4',
            'editSongLyrics' => 'max:3000',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'editThumbnail.dimensions' => 'The edit thumbnail has invalid image dimensions that must be 1:1'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'errors' => $validator->errors(),
            'status' => '400'
        ]));
    }
}
