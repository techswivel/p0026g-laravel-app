<?php

namespace App\Http\Resources\PurchasedSubscription;

use App\Models\AlbumSongs;
use Facades\App\Services\SongService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use App\Models\DownloadedSongs;
use App\Models\Favorite;
use App\Models\Playlist;
use App\Models\Purchased;
use Illuminate\Support\Facades\Auth;

class HistoryAlbumSongsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $playListId = null;
        if($this->getSong->getPlaylistId){
            $p = Playlist::where('id',$this->getSong->getPlaylistId->playlistId)->where('userId',Auth::user()->id)->first();
            if($p){
                $playListId = $p->id;
            }
        }

        $favouriteSong = false;
        $favourite = Favorite::where('userId',Auth::user()->id)->where('songId',$this->getSong->id)->get();
        if($favourite){
            $favouriteSong = true;
        }

        $data=SongService::songSkuPrice($this->getSong);
        $songStatus=SongService::songStatus($this->getSong);
        $fileName=SongService::audioVideoFileName($this->getSong);

        return [
            "songId" => $this->getSong->id ,
            "albumId" => $this->getSong->getAlbumId?$this->getSong->getAlbumId->albumId:null,
            "isAddedToPlayList" => $this->getSong->getPlaylistId?true:false,
            "playListId" =>  $playListId,
            "artistId" =>  $this->getSong->userId,
            "categoryId" =>  $this->getSong->categoryId,
            "type" => "SONG",
            "isFavourite" =>  $favouriteSong,
            "songTitle" =>  ($this->getSong)?  $this->getSong->name : null,
            "sku"=>$data['sku'],
            "coverImageUrl" => !is_null($this->getSong->thumbnail) ? Storage::disk('s3')->url($this->getSong->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
            "songDuration" =>  ($this->getSong)?  $this->getSong->songLength : null,
            "songStatus" => $songStatus,
            "price" =>  $this->getSong->isPaid == 1? (double) $data['price'] : null,
            "songAudioUrl" => $fileName['audioFile'],
            "songVideoUrl" => $fileName['videoFile'],
            "artist" =>  ($this->getSong->getArtist)?$this->getSong->getArtist->firstName." ".$this->getSong->getArtist->lastName:null,
            "lyrics" =>  $this->getSong->lyrics,
            "albumName" => ($this->getSong->getAlbumId)?($this->getSong->getAlbumId->getAlbum)?$this->getSong->getAlbumId->getAlbum->name:null:null,
            "audiofileName"=>$fileName['audioFileName'],
            "videofileName"=> $fileName['videoFileName'],
        ];

    }
}
