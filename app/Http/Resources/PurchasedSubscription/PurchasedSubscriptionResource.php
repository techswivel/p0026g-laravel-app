<?php

namespace App\Http\Resources\PurchasedSubscription;

use App\Enums\PurchasedItemTypeEnum;
use App\Http\Resources\PurchasedSubscription\HistoryAlbumSongsCollection as PurchasedSubscriptionHistoryAlbumSongsCollection;
use App\Models\AlbumSongs;
use App\Models\DownloadedSongs;
use App\Models\Favorite;
use App\Models\Playlist;
use App\Models\Purchased;
use App\Models\Song;
use Facades\App\Services\SongService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PurchasedSubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if($this->itemType == PurchasedItemTypeEnum::SONG){         // for Songs
            $playListId = null;
            if($this->getSong->getPlaylistId){
                $p = Playlist::where('id',$this->getSong->getPlaylistId->playlistId)->where('userId',Auth::user()->id)->first();
                if($p){
                    $playListId = $p->id;
                }
            }

            $favouriteSong = false;
            $favourite = Favorite::where('userId',Auth::user()->id)->where('songId',$this->getSong->id)->get();
            if($favourite){
                $favouriteSong = true;
            }
            $data=SongService::songSkuPrice($this->getSong);
            $songStatus=SongService::songStatus($this->getSong);
            $fileName=SongService::audioVideoFileName($this->getSong);

            return [
                "songId" => $this->getSong->id ,
                "albumId" => $this->getSong->getAlbumId?$this->getSong->getAlbumId->albumId:null,
                "isAddedToPlayList" => $this->getSong->getPlaylistId?true:false,
                "playListId" =>  $playListId,
                "artistId" =>  $this->getSong->userId,
                "categoryId" =>  $this->getSong->categoryId,
                "type" => "SONG",
                "isFavourite" =>  $favouriteSong,
                "songTitle" =>  ($this->getSong)?  $this->getSong->name : null,
                "sku"=>$data['sku'],
                "coverImageUrl" => !is_null($this->getSong->thumbnail) ? Storage::disk('s3')->url($this->getSong->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
                "songDuration" =>  ($this->getSong)?  $this->getSong->songLength : null,
                "songStatus" => $songStatus,
                "price" =>  $this->getSong->isPaid == 1? (double) $data['price'] : null,
                "songAudioUrl" => $fileName['audioFile'],
                "songVideoUrl" => $fileName['videoFile'],
                "artist" =>  ($this->getSong->getArtist)?$this->getSong->getArtist->firstName." ".$this->getSong->getArtist->lastName:null,
                "lyrics" =>  $this->getSong->lyrics,
                "albumName" => ($this->getSong->getAlbumId)?($this->getSong->getAlbumId->getAlbum)?$this->getSong->getAlbumId->getAlbum->name:null:null,
                "audiofileName"=>$fileName['audioFileName'],
                "videofileName"=> $fileName['videoFileName'],
            ];
        }
        if($this->itemType == PurchasedItemTypeEnum::ALBUM) {
            $sku = null;
            if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
                if($this->getAlbum){
                    if ($this->getAlbum->sku != null) {
                        $sku = $this->getAlbum->sku."_wod";
                    }
                }

            }else{
                if ($this->getAlbum) {
                    if ($this->getAlbum->sku != null) {
                        $sku = $this->getAlbum->sku."_wd";
                    }
                }
            }// for Albums
            return [
                "itemType" => "ALBUM",
                "albumTitle" => ($this->getAlbum)?  $this->getAlbum->name : null,
                "albumCoverImageURL" => ($this->getAlbum)?  $this->getAlbum->thumbnail : null,
                "totalSongs" => ($this->getAlbum)?  ($this->getAlbum->albumSongs)?   $this->getAlbum->albumSongs->count()  : null : null,
                "price" => (double) $this->amount,
                "sku"=>$sku,
                "timeOfPurchased" => $this->createdAt,
                "typeOfTransection" => $this->purchaseType,
                "cardPrefix" => ($this->purchaseType == "CARD") ? ($this->getCard)? substr($this->getCard->cardId, 0, 4) : null : null,
                "songs" => new PurchasedSubscriptionHistoryAlbumSongsCollection($this->getAlbum->albumSongs)
            ];
        }
        if($this->itemType == PurchasedItemTypeEnum::PLAN_SUBSCRIPTION) {                         // for Package
            return [
                "itemType" => "PLAN_SUBSCRIPTION",
                "planId" => ($this->getPackage)?  $this->getPackage->id : null,
                "planTitle" => ($this->getPackage)?  $this->getPackage->name : null,
                "planTopic" => ($this->getPackage)?  $this->getPackage->firebaseTopic : null,
                "planPrice" => ($this->getPackage)? (double) $this->getPackage->price : null,
                "planDuration" => ($this->getPackage)?  $this->getPackage->duration : null,
                "sku"=>($this->getPackage)?  $this->getPackage->sku : null
            ];
        }

        if($this->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG || $this->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM){
            $isPurchesed = false;
            $purchased = Purchased::where('songId',$this->id)->where('userId',Auth::user()->id)->first();
            if($purchased){
                $isPurchesed = true;
            }
            $purchased = Purchased::where('albumId',$this->id)->where('userId',Auth::user()->id)->first();
            if($purchased){
                $isPurchesed = true;
            }
            $totalnumberOfSongs = null;
            $price = null;
            $preOrderType = null;
            if($this->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG){
                $song = Song::where('id',$this->getSong->id)->first();
                $price = (double) $song->price;
                $data=SongService::songSkuPrice($song);
                $preOrderType = "SONG";
                $relation = 'getSong';
                // dd($relation,$this->$relation); // ->preOrderReleaseDate
            }
            if($this->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM){
                $relation = 'getAlbum';
                $preOrderType = "ALBUM";
                $songIds = AlbumSongs::where('albumId',$this->getAlbum->id)->pluck('songId')->toArray();
                $songs = Song::whereIn('id',$songIds)->get();
                foreach ($songs as $song){
                    $albumPrice = $price+$song->price;
                }
                $totalnumberOfSongs = AlbumSongs::where('albumId',$this->getAlbum->id)->count();
                $price = null;
                $sku = null;
                if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
                    $discount=($albumPrice * 10)/100;
                    $price = $albumPrice + $discount;
                    if($this->getAlbum->sku != null){
                        $sku = $this->getAlbum->sku."_wod";
                    }
                }else{
                    $price = $albumPrice;
                    if($this->getAlbum->sku != null){
                        $sku = $this->getAlbum->sku."_wd";
                    }
                }
                $data['price']=$price;
                $data['sku']=$sku;
            }
            $preOrderedCount = Purchased::where('songId',$this->id)->orWhere('albumId',$this->id)->count();
            return [
                  "preOrderId"=> $this->$relation->id,
                  "releaseDate"=> $this->$relation->preOrderReleaseDate,
                  "preOrderTitle"=> $this->$relation->name,
                  "preOrderCoverImageUrl"=> !is_null($this->$relation->thumbnail) ? Storage::disk('s3')->url($this->$relation->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
                  "preOrderedCount"=> $preOrderedCount?$preOrderedCount:null,
                  "totalPreOrders"=> $this->$relation->totalPreOrderCount,
                  "totalnumberOfSongs"=> $totalnumberOfSongs,
                  "price"=> (double) $data['price'],
                  "sku" =>  $data['sku'],
                  "preOrderType"=> $preOrderType,
                  "isPurchesed"=> $isPurchesed,
                  "totalPurchesed"=> $this->$relation->preOrderCount

            ];
        }

    }
}
