<?php

namespace App\Http\Resources\Song;

use App\Models\Album;
use App\Models\AlbumSongs;
use App\Models\Purchased;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class SongWithMetaCollection extends ResourceCollection
{
    public $collects = SongWithMetaResource::class;
    private $pagination;
    public function __construct($resource)
    {
        $this->pagination = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'perPage' => (int)$resource->perPage(),
            'currentPage' => $resource->currentPage(),
            'totalPages' => $resource->lastPage()
        ];
        $resource = $resource->getCollection();

        parent::__construct($resource);
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($request->type != "ALBUM" ){
            return [
                'songs' => $this->collection,
                'meta' => $this->pagination
            ];
        }else{            
            $album = Album::where('id',$request->albumId)->first();

            $songsCount = $albumPrice = $k = 0; 
            $albumStatus = "FREE";
            $purchasedSongsArray = array();

            $albumSongs = AlbumSongs::where('albumId',$album->id)->get();
            foreach($albumSongs as $albumSong){
                if($albumSong->getSong){
                    if($albumSong->getSong->isPaid == 1){
                        $purchased = Purchased::where('userId',Auth::user()->id)->where('songId',$albumSong->getSong->id)->first();
                        if($purchased){
                            $purchasedSongsArray[$k]  = $albumSong->getSong->id;
                            $k = $k + 1;
                        }
                        $albumPrice = $albumPrice + $albumSong->getSong->price;
                    }
                }
            }
            $albumPaidSongs = AlbumSongs::where('albumId',$album->id)
                                            ->whereHas('getSong', function($q){
                                                $q->where('isPaid',1);
                                            })->count();
            if($albumSongs){
                $songsCount = count($albumSongs);
            }
            
            if($songsCount != 0){
                if($songsCount == $albumPaidSongs){
                    $albumStatus = "PREMIUM";
                }else{
                    $albumPrice = 0;
                }            
            }

            $purchased = Purchased::where('userId',Auth::user()->id)->where('albumId',$album->id)->first();
            if($purchased){
                $albumStatus = "PURCHASED";
            }else{
                if(count($purchasedSongsArray) == $albumPaidSongs){
                    $albumStatus = "PURCHASED";
                }
            }

            if($request->type == "PURCHASED_ALBUM")
            {
                if($album->deletedAt != null){
                    $albumStatus = "DELETED";
                }
            }

            return [
                'albumStatus' => $albumStatus,
                'songs' => $this->collection,
                'meta' => $this->pagination
            ];
        }
    }
}
