<?php

namespace App\Http\Resources\Song;

use App\Models\AlbumSongs;
use App\Models\Purchased;
use Facades\App\Services\SongService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SongForNotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $playListId = null;
        $favouriteSong = false;
        if($request->isPaid == 1){
            $songAudioUrl = !is_null($this->demoAudioFile) ? Storage::disk('s3')->url($this->demoAudioFile):null;
            $songVideoUrl = !is_null($this->demoVideoFile) ? Storage::disk('s3')->url($this->demoVideoFile):null;
    
            $purchased = Purchased::where('userId',Auth::user()->id)->where('songId',$this->id)->first();
            $albumIdOfSong = AlbumSongs::where('songId',$this->id)->first();
            if($albumIdOfSong) {
                $purchased = Purchased::where('userId',Auth::user()->id)->where('albumId',$albumIdOfSong->albumId)->first();
            }
            if($purchased){
                $songAudioUrl = !is_null($this->audioFile) ? Storage::disk('s3')->url($this->audioFile):null;
                $songVideoUrl = !is_null($this->videoFile) ? Storage::disk('s3')->url($this->videoFile):null;
            }
        }else{
            $songAudioUrl = !is_null($this->audioFile) ? Storage::disk('s3')->url($this->audioFile):null;
            $songVideoUrl = !is_null($this->videoFile) ? Storage::disk('s3')->url($this->videoFile):null;
        }

        $explodeFileName=null;
        $explodeVideoFileName=null;
        if ($this->audioFile){
            $fileName= Storage::disk('s3')->url($this->audioFile);
            $audioFIleName=explode('/',$fileName);
            $audio= $audioFIleName;
            $lastArray=count($audio)-1;
            $explodeFileName=$audioFIleName[$lastArray];
        }
        if ($this->videoFile){
            $fileName=Storage::disk('s3')->url($this->videoFile);
            $videoFileName=explode('/',$fileName);
            $video= $videoFileName;
            $lastArray=count($video)-1;
            $explodeVideoFileName=$videoFileName[$lastArray];
        }
        $fileName = array();
        $fileName['audioFile'] = $songAudioUrl;
        $fileName['videoFile'] = $songVideoUrl;
        $fileName['audioFileName'] = $explodeFileName;
        $fileName['videoFileName'] = $explodeVideoFileName;

        return [
            "songId" => $this->id ,
            "albumId" => $this->getAlbumId?$this->getAlbumId->albumId:null,
            "isAddedToPlayList" => $this->getPlaylistId?true:false,
            "playListId" =>  $playListId,
            "artistId" =>  $this->userId,
            "categoryId" =>  $this->categoryId,
            "type" => "SONG",
            "isFavourite" =>  $favouriteSong,
            "songTitle" =>  $this->name,
            "sku" => null,
            "coverImageUrl" => !is_null($this->thumbnail) ? Storage::disk('s3')->url($this->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
            "songDuration" =>  $this->songLength,
            "songStatus" => "PURCHASED",
            "price" => null,
            "songAudioUrl" => $fileName['audioFile'],
            "songVideoUrl" => $fileName['videoFile'],
            "artist" =>  ($this->getArtist)?$this->getArtist->firstName." ".$this->getArtist->lastName:null,
            "lyrics" =>  $this->lyrics,
            "albumName" => ($this->getAlbumId)?($this->getAlbumId->getAlbum)?$this->getAlbumId->getAlbum->name:null:null,
            "audiofileName"=>$fileName['audioFileName'],
            "videofileName"=> $fileName['videoFileName'],
        ];
    }
}
