<?php

namespace App\Http\Resources\Song;

use App\Models\Album;
use App\Models\AlbumSongs;
use App\Models\Purchased;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class NextPlaySongCollection extends ResourceCollection
{
    public $collects = NextPlaySongResource::class;
    private $pagination;
    public function __construct($resource)
    {
        $this->pagination = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'perPage' => (int)$resource->perPage(),
            'currentPage' => $resource->currentPage(),
            'totalPages' => $resource->lastPage()
        ];
        $resource = $resource->getCollection();

        parent::__construct($resource);
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
            return [
                'songs' => $this->collection,
                'meta' => $this->pagination
            ];

    }
}
