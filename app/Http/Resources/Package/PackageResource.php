<?php

namespace App\Http\Resources\Package;

use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "planId" => $this->id,
            "planTitle" => $this->name,
            "planTopic" => $this->firebaseTopic,
            "planPrice" => (double) $this->price,
            "planDuration" => $this->duration,
            "sku"=>$this->sku
        ];
    }
}
