<?php

namespace App\Http\Resources\History;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BuyingHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
            $discount=($this->amount * 10)/100;
            $price= $this->amount + $discount;
        }else{
            $price= $this->amount;
        }
        if($this->isSong == 1){         // for Songs
            $sku = null;
            if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
                if($this->getSong){
                    if($this->getSong->sku != null){
                        $sku = $this->getSong->sku."_wod";
                    }
                }

            }else{
                if ($this->getSong) {
                    if ($this->getSong->sku != null) {
                        $sku = $this->getSong->sku."_wd";
                    }
                }
            }
            return [
                "itemType" => "SONG",
                "title" => ($this->getSong)?  $this->getSong->name : null,
                "coverImageUrl" => ($this->getSong)?  $this->getSong->thumbnail : null,
                "duration" => ($this->getSong)?  $this->getSong->songLength : null,
                "price" => (double) $price,
                "timeOfPurchased" => (int)strtotime($this->createdAt),
                "typeOfTransection" => $this->purchaseType,
                "cardPrefix" => ($this->purchaseType == "CARD") ? ($this->getCard)? substr($this->getCard->cardId, 0, 4) : null : null,
                "sku"=> $sku
            ];
        }else{                          // for Albums
            $sku = null;
            if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
                if($this->getAlbum){
                    if ($this->getAlbum->sku != null) {
                        $sku = $this->getAlbum->sku."_wod";
                    }
                }

            }else{
                if ($this->getAlbum) {
                    if ($this->getAlbum->sku != null) {
                        $sku = $this->getAlbum->sku."_wd";
                    }
                }
            }

            return [
                "itemType" => "ALBUM",
                "title" => ($this->getAlbum)?  $this->getAlbum->name : null,
                "coverImageURL" => ($this->getAlbum)?  $this->getAlbum->thumbnail : null,
                "totalSongs" => ($this->getAlbum)?  ($this->getAlbum->albumSongs)?   $this->getAlbum->albumSongs->count()  : null : null,
                "price" => (double) $price,
                "timeOfPurchased" => (int)strtotime($this->createdAt),
                "typeOfTransection" => $this->purchaseType,
                "cardPrefix" => ($this->purchaseType == "CARD") ? ($this->getCard)? substr($this->getCard->cardId, 0, 4) : null : null,
                "song" => new HistoryAlbumSongsCollection($this->getAlbum->albumSongs),
                "sku"=> $sku
            ];
        }


    }
}
