<?php

namespace App\Http\Resources\History;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Storage;

class BuyingHistoryCollection extends ResourceCollection
{
    public $collects = BuyingHistoryResource::class;
    private $pagination;
    public function __construct($resource)
    {
        $this->pagination = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'perPage' => (int)$resource->perPage(),
            'currentPage' => $resource->currentPage(),
            'totalPages' => $resource->lastPage()
        ];
        $resource = $resource->getCollection();

        parent::__construct($resource);
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $totalAmount = 0;
        if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
            foreach($this as $history){
                $discount=($history->amount * 10)/100;
                $totalAmount = $totalAmount + $history->amount+ $discount;
            }

        }else{
            foreach($this as $history){
                $totalAmount = $totalAmount + $history->amount;
            }
        }


        return [
            'totalAmount' => $totalAmount,
            'buyingHistory' => $this->collection,
            'meta' => $this->pagination
        ];
    }
}
