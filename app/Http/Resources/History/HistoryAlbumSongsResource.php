<?php

namespace App\Http\Resources\History;

use Facades\App\Services\SongService;
use Illuminate\Http\Resources\Json\JsonResource;

class HistoryAlbumSongsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data=SongService::songSkuPrice($this->getSong);
        return [
            "title" => ($this->getSong)?  $this->getSong->name : null,
            "coverImageUrl" => ($this->getSong)?  $this->getSong->thumbnail : null,
            "duration" => ($this->getSong)?  $this->getSong->songLength : null,
            "price" => ($this->getSong)? ($this->getSong->isPaid == 1)? (double) $data['price']  : null : null,
            "sku"=> $data['sku']
        ];

    }
}
