<?php

namespace App\Http\Resources\SongAlbumArtist;

use App\Models\Language;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SongAlbumArtistCollection extends ResourceCollection
{
    public $collects = SongAlbumArtistResource::class;
    private $pagination;
    public function __construct($resource)
    {
        $this->pagination = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'perPage' => (int)$resource->perPage(),
            'currentPage' => $resource->currentPage(),
            'totalPages' => $resource->lastPage()
        ];
        $resource = $resource->getCollection();

        parent::__construct($resource);
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request->languageId == null) {
            $languages = Language::all();
            return [
                'Languages' => LanguageResource::collection($languages),
                'searchResults' => $this->collection,
                'meta' => $this->pagination
            ];
        }
        return [
            'searchResults' => $this->collection,
            'meta' => $this->pagination
        ];
    }
}
