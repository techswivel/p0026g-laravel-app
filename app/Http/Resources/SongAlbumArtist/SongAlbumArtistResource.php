<?php

namespace App\Http\Resources\SongAlbumArtist;

use App\Models\AlbumSongs;
use App\Models\DownloadedSongs;
use App\Models\Favorite;
use App\Models\Playlist;
use App\Models\Purchased;
use Facades\App\Services\SongService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SongAlbumArtistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->getTable() == "songs"){
            $playListId = null;
            if($this->getPlaylistId){
                $p = Playlist::where('id',$this->getPlaylistId->playlistId)->where('userId',Auth::user()->id)->first();
                if($p){
                    $playListId = $p->id;
                }
            }
            $favouriteSong = false;
            $favourite = Favorite::where('userId',Auth::user()->id)->where('songId',$this->id)->get();
            if($favourite){
                $favouriteSong = true;
            }

            $data=SongService::songSkuPrice($this);
            $songStatus=SongService::songStatus($this);
            $fileName=SongService::audioVideoFileName($this);

            return [
                "songId" => $this->id ,
                "albumId" => $this->getAlbumId?$this->getAlbumId->albumId:null,
                "isAddedToPlayList" => $this->getPlaylistId?true:false,
                "playListId" =>  $playListId,
                "artistId" =>  $this->userId,
                "categoryId" =>  $this->categoryId,
                "type" => "SONG",
                "isFavourite" =>  $favouriteSong,
                "songTitle" =>  $this->name,
                "coverImageUrl" => !is_null($this->thumbnail) ? Storage::disk('s3')->url($this->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
                "songDuration" =>  $this->songLength,
                "songStatus" => $songStatus,
                "price" =>  $this->isPaid == 1? (double) $data['price'] : null,
                "songAudioUrl" => $fileName['audioFile'],
                "songVideoUrl" => $fileName['videoFile'],
                "artist" =>  ($this->getArtist)?$this->getArtist->firstName." ".$this->getArtist->lastName:null,
                "lyrics" =>  $this->lyrics,
                "sku" => $data['sku'],
                "albumName" => ($this->getAlbumId)?($this->getAlbumId->getAlbum)?$this->getAlbumId->getAlbum->name:null:null,
                "audiofileName"=>$fileName['audioFileName'],
                "videofileName"=> $fileName['videoFileName'],
            ];
        }
        if($this->getTable() == "albums"){
            $songsCount = $albumPrice = $k = 0;
            $albumStatus = "FREE";
            $purchasedSongsArray = array();

            $albumSongs = AlbumSongs::where('albumId',$this->id)->get();
            foreach($albumSongs as $albumSong){
                if($albumSong->getSong){
                    if($albumSong->getSong->isPaid == 1){
                        $purchased = Purchased::where('userId',Auth::user()->id)->where('songId',$albumSong->getSong->id)->first();
                        if($purchased){
                            $purchasedSongsArray[$k]  = $albumSong->getSong->id;
                            $k = $k + 1;
                        }
                        $albumPrice = $albumPrice + $albumSong->getSong->price;
                    }
                }
            }
            $albumPaidSongs = AlbumSongs::where('albumId',$this->id)
                                            ->whereHas('getSong', function($q){
                                                $q->where('isPaid',1);
                                            })->count();
            if($albumSongs){
                $songsCount = count($albumSongs);
            }

            if($songsCount != 0){
                if($songsCount == $albumPaidSongs){
                    $albumStatus = "PREMIUM";
                }else{
                    $albumPrice = 0;
                }
            }

            $purchased = Purchased::where('userId',Auth::user()->id)->where('albumId',$this->id)->first();
            if($purchased){
                $albumStatus = "PURCHASED";
            }else{
                if(count($purchasedSongsArray) == $albumPaidSongs){
                    $albumStatus = "PURCHASED";
                }
            }

            if($request->type == "PURCHASED_ALBUM")
            {
                if($this->deletedAt != null){
                    $albumStatus = "DELETED";
                }
            }
            $price = null;
            $sku = null;
            if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
                $discount=($albumPrice * 10)/100;
                $price = $albumPrice + $discount;
                if($this->sku != null){
                    $sku = $this->sku."_wod";
                }
            }else{
                $price = $albumPrice;
                if($this->sku != null){
                    $sku = $this->sku."_wd";
                }
            }
            return [
                "albumId"=> $this->id,
                "albumTitle"=> $this->name,
                "albumArtist" => ($this->getArtist)?$this->getArtist->firstName." ".$this->getArtist->lastName:null ,
                "type" => "ALBUM",
                "albumCoverImageUrl"=> !is_null($this->thumbnail) ? Storage::disk('s3')->url($this->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
                "albumStatus"=> $albumStatus,
                "price"=> (double) $price,
                "sku"=>$sku,
                "numberOfSongs"=> $songsCount

            ];
        }
        if($this->getTable() == "users"){
            return [
                "artistId" => $this->id,
                "artistName" => $this->firstName." ".$this->lastName,
                "type" => "ARTIST",
                "artistCoverImageUrl" => !is_null($this->avatar) ? Storage::disk('s3')->url($this->avatar):asset('artist/image/avatar/default-avatar.png')
            ];
        }

    }
}
