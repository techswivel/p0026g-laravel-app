<?php

namespace App\Http\Resources\SongAlbumArtist;

use Illuminate\Http\Resources\Json\JsonResource;

class LanguageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "languageId" =>    $this->id,
            "languageTitle" => $this->name
        ];
    }
}
