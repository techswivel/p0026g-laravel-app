<?php

namespace App\Http\Resources\PreOrder;

use App\Models\Album;
use App\Models\AlbumSongs;
use App\Models\Purchased;
use App\Models\Song;
use Facades\App\Services\SongService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class PreOrderResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $isPurchesed = false;
        $purchasedSong = Purchased::where('songId',$this->id)->where('userId',Auth()->user()->id)->first();
        if($purchasedSong){
            $isPurchesed = true;
        }
        $purchasedAlbum = Purchased::where('albumId',$this->id)->where('userId',Auth()->user()->id)->first();
        if($purchasedAlbum){
            $isPurchesed = true;
        }
        $totalnumberOfSongs = null;
        $price = null;
        if($this->preOrderType == 'Song'){
            $song = Song::where('id',$this->id)->first();
            $price = (int)$song->price;
            $data=SongService::songSkuPrice($song);
        }
        if($this->preOrderType == 'Album'){
            $songIds = AlbumSongs::where('albumId',$this->id)->pluck('songId')->toArray();
            $songs = Song::whereIn('id',$songIds)->get();
            foreach ($songs as $song){
                $albumPrice = $price+$song->price;
            }
            $totalnumberOfSongs = AlbumSongs::where('albumId',$this->id)->count();
            $price = null;
            $sku = null;
            if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
                $discount=($albumPrice * 10)/100;
                $price = $albumPrice + $discount;
                if($this->sku != null){
                    $sku = $this->sku."_wod";
                }
            }else{
                $price = $albumPrice;
                if($this->sku != null){
                    $sku = $this->sku."_wd";
                }
            }
            $data['price']=$price;
            $data['sku']=$sku;
        }
        $preOrderedCount = Purchased::where('songId',$this->id)->where('userId',$this->userId)->count();
        return [
              "preOrderId"=> $this->id,
              "releaseDate"=> $this->preOrderReleaseDate,
              "preOrderTitle"=> $this->preOrderTitle,
              "preOrderCoverImageUrl"=> !is_null($this->thumbnail) ? Storage::disk('s3')->url($this->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
              "preOrderedCount"=> $preOrderedCount?$preOrderedCount:null,
              "totalPreOrders"=> $this->totalPreOrderCount,
              "totalnumberOfSongs"=> $totalnumberOfSongs,
              "price"=> (double) $data['price'],
              "sku" =>  $data['sku'],
              "preOrderType"=> $this->preOrderType,
              "isPurchesed"=> $isPurchesed,
              "totalPurchesed"=> $this->preOrderCount

        ];
    }
}
