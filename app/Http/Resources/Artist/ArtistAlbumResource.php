<?php

namespace App\Http\Resources\Artist;

use App\Models\AlbumSongs;
use App\Models\Purchased;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArtistAlbumResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $songsCount = $albumPrice = $k = 0;
        $albumStatus = "FREE";
        $purchasedSongsArray = array();

        $albumSongs = AlbumSongs::where('albumId',$this->id)->get();
        foreach($albumSongs as $albumSong){
            if($albumSong->getSong){
                if($albumSong->getSong->isPaid == 1){
                    $purchased = Purchased::where('userId',Auth::user()->id)->where('songId',$albumSong->getSong->id)->first();
                    if($purchased){
                        $purchasedSongsArray[$k]  = $albumSong->getSong->id;
                        $k = $k + 1;
                    }
                    $albumPrice = $albumPrice + $albumSong->getSong->price;
                }
            }
        }
        $albumPaidSongs = AlbumSongs::where('albumId',$this->id)
                                        ->whereHas('getSong', function($q){
                                            $q->where('isPaid',1);
                                        })->count();
        if($albumSongs){
            $songsCount = count($albumSongs);
        }

        if($songsCount != 0){
            if($songsCount == $albumPaidSongs){
                $albumStatus = "PREMIUM";
            }else{
                $albumPrice = 0;
            }
        }

        $purchased = Purchased::where('userId',Auth::user()->id)->where('albumId',$this->id)->first();
        if($purchased){
            $albumStatus = "PURCHASED";
        }else{
            if(count($purchasedSongsArray) == $albumPaidSongs){
                $albumStatus = "PURCHASED";
            }
        }

        if($request->type == "PURCHASED_ALBUM")
        {
            if($this->deletedAt != null){
                $albumStatus = "DELETED";
            }
        }

        $sku = null;
        if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
            if($this->sku != null){
                $sku = $this->sku."_wod";
            }
        }else{
            if($this->sku != null){
                $sku = $this->sku."_wd";
            }
        }

        return [
            "albumId" => $this->id,
            "albumTitle" => $this->name,
            "albumCoverImageUrl" => !is_null($this->thumbnail) ? Storage::disk('s3')->url($this->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
            "albumStatus" => $albumStatus,
            "numberOfSongs" => $songsCount,
            "price" => (double) $albumPrice,
            "sku" => $sku,
        ];
    }
}
