<?php

namespace App\Http\Resources\Artist;

use App\Enums\AlbumPreOrderEnum;
use App\Enums\SongPreOrderEnum;
use App\Http\Resources\Album\AlbumResource;
use App\Models\Album;
use App\Models\Follower;
use App\Models\Song;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArtistDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $countSongs = Song::where('userId',$this->id)->where('type',SongPreOrderEnum::Song)->where('status','!=','PENDING')->count();
        $albumSongs = Album::where('userId',$this->id)->where('type',AlbumPreOrderEnum::Album)->where('status','!=','PENDING')->count();
        $isPreOrderAvailble = false;
        $preOrderAvailble = Song::where('type',SongPreOrderEnum::PreOrder)->where('userId',$this->id)->where('status','!=','PENDING')->first();
        if(!empty($preOrderAvailble)){
            $isPreOrderAvailble = true;
        }
        $preOrderAvailble = Album::where('type',AlbumPreOrderEnum::PreOrder)->where('userId',$this->id)->where('status','!=','PENDING')->first();
        if(!empty($preOrderAvailble)){
            $isPreOrderAvailble = true;
        }
        $isFollower = false;
        $follower = Follower::where('userId',Auth::user()->id)->where('artistId',$this->id)->first();
        if($follower){
            $isFollower = true;
        }
        $songs = Song::where('userId',$this->id)->where('type',SongPreOrderEnum::Song)->where('status','!=','PENDING')->get();
        $albnum = Album::where('userId',$this->id)->where('type',AlbumPreOrderEnum::Album)->where('status','!=','PENDING')->get();
        return [
            'totalSongs' => $countSongs?$countSongs:0,
            'totalAlbums' => $albumSongs?$albumSongs:0,
            "artistName" => $this->firstName." ".$this->lastName,
            "artistCoverImageUrl" => !is_null($this->avatar) ? Storage::disk('s3')->url($this->avatar):asset('artist/image/avatar/default-avatar.png'),
            "aboutArtist" => $this->aboutArtist,
            'isPreOrderAvailble' => $isPreOrderAvailble,
            'isUserFollowed' => $isFollower,
            'songs'=> new ArtistSongCollection($songs),
            'albums'=> new ArtistAlbumCollection($albnum),
            "sku"=>$this->sku
        ];
    }
}
