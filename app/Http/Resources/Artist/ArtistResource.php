<?php

namespace App\Http\Resources\Artist;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ArtistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "artistId" => $this->id,
            "artistName" => $this->firstName." ".$this->lastName,
            "type" => "ARTIST",
            "artistCoverImageUrl" => !is_null($this->avatar) ? Storage::disk('s3')->url($this->avatar):asset('artist/image/avatar/default-avatar.png'),
        ];
    }
}
