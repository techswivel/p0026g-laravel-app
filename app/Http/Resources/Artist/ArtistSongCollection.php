<?php

namespace App\Http\Resources\Artist;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ArtistSongCollection extends ResourceCollection
{
    public $collects = ArtistSongResource::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
            return $this->collection;
    }
}
