<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SocialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return ["auth" =>[
            'socialId' => $this['id'],
            'firstName' => $this['firstName']?? null,
            'lastName' => $this['lastName']?? null,
            'email' => $this['email'] ?? null,
            'phoneNumber' => $this['phoneNumber'] ?? null,
        ]];
    }
}
