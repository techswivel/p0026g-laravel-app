<?php

namespace App\Http\Resources\Category;

use App\Models\Album;
use App\Models\UserInterest;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CategoryResource extends JsonResource
{
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $categoryId = $this->id;
        $songIds = $this->categorySongs->pluck('id')->toArray();
        $albumCount = Album::wherehas('albumSongs',function($q) use($songIds){
            $q->whereIn('songId',$songIds);
        })->count();
        $isSelected = false;
        if($request->categoryType == "INTERESTS"){
            $intrust = UserInterest::where('categoryId',$categoryId)->where('userId', Auth()->user()->id)->first();
            if($intrust){
                $isSelected = true;
            }else{
                $isSelected = false;
            }
        }
        return [
            "categoryId"=> $this->id,
            "totalSongs"=> ($this->categorySongs)? $this->categorySongs->count() : 0,
            "totalAlbums"=> $albumCount,
            "categoryTitle"=> $this->name,            
            "isSelected"=> $isSelected,
            "categoryImageUrl"=> !is_null($this->imageUrl) ? Storage::disk('s3')->url($this->imageUrl):asset('artist/image/avatar/default-avatar.png')
        ];
    }
}
