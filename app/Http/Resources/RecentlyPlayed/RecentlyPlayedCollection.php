<?php

namespace App\Http\Resources\RecentlyPlayed;

use App\Http\Resources\Album\AlbumCollection;
use App\Http\Resources\Artist\ArtistAlbumCollection;
use App\Http\Resources\Artist\ArtistCollection;
use App\Http\Resources\Song\SongCollection;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Models\Purchased;
use App\Models\User;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class RecentlyPlayedCollection extends ResourceCollection
{
    public $collects = RecentlyPlayedResource::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
          return [
              'recentlyPlayed'=>[
                  'songs' => RecentlyPlayedResource::collection($this['songs']),
                  'albums' => ArtistAlbumCollection::make($this['albums']->resource),
                  'artists' => ArtistCollection::make($this['artists']->resource),
              ],
              'listeningHistory'=>[
                  'songs' => SongCollection::make($this['isListeningHistorySongs']->resource),
                  'albums' => ArtistAlbumCollection::make($this['isListeningHistoryAlbums']->resource),
                  'artists' => ArtistCollection::make($this['isListeningHistoryArtists']->resource),
              ],
          ];

    }
}
