<?php

namespace App\Http\Resources\RecentlyPlayed;

use App\Models\AlbumSongs;
use App\Models\DownloadedSongs;
use App\Models\Favorite;
use App\Models\Playlist;
use App\Models\Purchased;
//use App\Services\SkuPriceService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Facades\App\Services\SongService;


class RecentlyPlayedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $playListId = null;
        if($this->getPlaylistId){
            $p = Playlist::where('id',$this->getPlaylistId->playlistId)->where('userId',Auth::user()->id)->first();
            if($p){
                $playListId = $p->id;
            }
        }
        $favouriteSong = false;
        $favourite = Favorite::where('userId',Auth::user()->id)->where('songId',$this->id)->get();
        if($favourite){
            $favouriteSong = true;
        }
        $songStatus=SongService::songStatus($this);
        $fileName=SongService::audioVideoFileName($this);
        $data=SongService::songSkuPrice($this);
        return [
            "songId" => $this->id ,
            "albumId" => $this->getAlbumId?$this->getAlbumId->albumId:null,
            "isAddedToPlayList" => $this->getPlaylistId?true:false,
            "playListId" =>  $playListId,
            "artistId" =>  $this->userId,
            "categoryId" =>  $this->categoryId,
            "isFavourite" =>  $favouriteSong,
            "songTitle" =>  $this->name,
            "coverImageUrl" => !is_null($this->thumbnail) ? Storage::disk('s3')->url($this->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
            "songDuration" =>  $this->songLength,
            "songStatus" => $songStatus,
            "price" =>  $this->isPaid == 1? (double) $data['price'] : null,
            "songAudioUrl" => $fileName['audioFile'],
            "songVideoUrl" => $fileName['videoFile'],
            "artist" =>  ($this->getArtist)?$this->getArtist->firstName." ".$this->getArtist->lastName:null,
            "lyrics" =>  $this->lyrics,
            "albumName" => ($this->getAlbumId)?($this->getAlbumId->getAlbum)?$this->getAlbumId->getAlbum->name:null:null,
            "audiofileName"=>$fileName['audioFileName'],
            "videofileName"=> $fileName['videoFileName'],
            "sku"=> $data['sku'],
        ];
    }
}
