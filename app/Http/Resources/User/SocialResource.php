<?php

namespace App\Http\Resources\User;

use App\Enums\SellerDocumentEnum;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use App\Services\FirebaseService;

class SocialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'jwt' => $this['token']?$this['token']:null,
            'firebaseCustomToken'=>$this->firebaseCustomToken?$this->firebaseCustomToken:null,
            'firstName' => $this->firstName?$this->firstName:null,
            'lastName' => $this->lastName?$this->lastName:null,
            "sellerStatus"=>$this->sellers?$this->sellers->status:SellerDocumentEnum::NO_SELLER,
            "sellerStatusMessage"=>$this->sellers?$this->sellers->rejectedReason:null,
            "totalEarnings"=>$this->clearedPayment?(float)$this->clearedPayment:null,
            'email' => $this->email?$this->email:null,
            'phoneNumber' => $this->phoneNumber?$this->phoneNumber:null,
            "avatar"=>!is_null($this->avatar) ? Storage::disk('s3')->url($this->avatar):asset('img/default-avatar.png')
        ];
    }
}
