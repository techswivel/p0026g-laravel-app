<?php

namespace App\Http\Resources\User;

use App\Enums\SellerDocumentEnum;
use App\Enums\WithdrawAmountTypeEnum;
use App\Models\Payment;
use App\Models\SellerDocument;
use App\Services\FirebaseService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'name' =>$this->lastName? $this->firstName." ".$this->lastName:$this->firstName,
            'email' => $this->email,
            'phoneNumber' => $this->phoneNumber,
            'dOb' => $this->birthDate,
            'gender' => $this->gender,
            'isInterestsSet' => ($this->userInterests->count() > 0)?true:false,
            'address'=> new AddressResource($this),
            'notification'=> new NotificationResource($this),
            'subscription'=> new SubscriptionResource($this->userActivePackages),
            "avatar"=>!is_null($this->avatar) ? Storage::disk('s3')->url($this->avatar):asset('artist/image/avatar/default-avatar.png')
        ];
    }
}
