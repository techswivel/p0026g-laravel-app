<?php

namespace App\Http\Resources\User;

use App\Models\AlbumSongs;
use App\Models\DownloadedSongs;
use App\Models\Favorite;
use App\Models\Playlist;
use App\Models\Purchased;
use Facades\App\Services\SongService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SongResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $playListId = null;
        if($this->getPlaylistId){
            $p = Playlist::where('id',$this->getPlaylistId->playlistId)->where('userId',$request->authId)->first();
            if($p){
                $playListId = $p->id;
            }
        }
        $favouriteSong = false;
        $favourite = Favorite::where('userId',$request->authId)->where('songId',$this->id)->get();
        if($favourite){
            $favouriteSong = true;
        }

        //check if user paid or free
        // $data=SongService::songSkuPrice($this);
        $sku = null;
        if ($request->isSubscribed==null || $request->isSubscribed==0){
            $discount=($this->price * 10)/100;
            $price=$this->price + $discount;
            if ($this->sku != null) {
                 $sku = $this->sku."_wod";
             }
        }else{
            $price=$this->price;
            if ($this->sku != null) {
                 $sku = $this->sku."_wd";
             }
        }
        $songStatus = $this->isPaid == 1? "PREMIUM" : "FREE";
        $downloaded = DownloadedSongs::where('userId',$request->authId)->where('songId',$this->id)->where('isDownloaded',1)->first();
        if($downloaded){
            $songStatus = "DOWNLOADED";
        }else{
            if($this->isPaid == 1){
                $purchased = Purchased::where('userId',$request->authId)->where('songId',$this->id)->first();
                if($purchased){
                    $songStatus = "PURCHASED";
                }
                $albumIdOfSong = AlbumSongs::where('songId',$this->id)->first();
                if($albumIdOfSong)
                {
                    $purchased = Purchased::where('userId',$request->authId)->where('albumId',$albumIdOfSong->albumId)->first();
                    if($purchased){
                        $songStatus = "PURCHASED";
                    }
                }
            }
        }
        $explodeFileName=null;
        $explodeVideoFileName=null;
        if ($this->audioFile){
            $fileName= Storage::disk('s3')->url($this->audioFile);
            $audioFIleName=explode('/',$fileName);
            $audio= $audioFIleName;
            $lastArray=count($audio)-1;
            $explodeFileName=$audioFIleName[$lastArray];
        }
        if ($this->videoFile){
            $fileName=Storage::disk('s3')->url($this->videoFile);
            $videoFileName=explode('/',$fileName);
            $video= $videoFileName;
            $lastArray=count($video)-1;
            $explodeVideoFileName=$videoFileName[$lastArray];
        }

        return [
            "songId" => $this->id ,
            "albumId" => $this->getAlbumId?$this->getAlbumId->albumId:null,
            "isAddedToPlayList" => $this->getPlaylistId?true:false,
            "playListId" =>  $playListId,
            "artistId" =>  $this->userId,
            "categoryId" =>  $this->categoryId,
            "type" => "SONG",
            "isFavourite" =>  $favouriteSong,
            "songTitle" =>  $this->name,
            "sku" => $sku,
            "coverImageUrl" => !is_null($this->thumbnail) ? Storage::disk('s3')->url($this->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
            "songDuration" =>  $this->songLength,
            "songStatus" => $songStatus,
            "price" =>  $this->isPaid == 1? (double) $price : null,
            "songAudioUrl" => $fileName['audioFile'],
            "songVideoUrl" => $fileName['videoFile'],
            "artist" =>  ($this->getArtist)?$this->getArtist->firstName." ".$this->getArtist->lastName:null,
            "lyrics" =>  $this->lyrics,
            "albumName" => ($this->getAlbumId)?($this->getAlbumId->getAlbum)?$this->getAlbumId->getAlbum->name:null:null,
            "audiofileName"=>$explodeFileName,
            "videofileName"=> $explodeVideoFileName,
        ];
    }
}
