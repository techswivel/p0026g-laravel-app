<?php

namespace App\Http\Resources\User;

use App\Http\Resources\User\AlbumResource;
use App\Http\Resources\User\NotificationResource;
use App\Http\Resources\User\SongResource;
use App\Http\Resources\User\SubscriptionResource;
use App\Models\Album;
use App\Models\Package;
use App\Models\Purchased;
use App\Models\Song;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $subscription = null;
        if($this->gplay_order_token != null && $this->isSubscribed == 1){
            $subscriptionId = Purchased::where('userId',$this->id)->where('transactionId',$this->gplay_order_token)->first();
            $subscription = Package::where('id',$subscriptionId->packageId)->first();
        }
        $purchasedAlbums = null;
        $purchasedSongs = null;
        $purchasedSongIds = Purchased::whereNotNull('songId')->where('userId',$this->id)
                                        ->where('purchaseStatus','Purchased')
                                        ->where('isAcknowledged','Not Acknowledged')
                                        ->pluck('songId')->toArray();
        $purchasedSongs = Song::whereIn('id',$purchasedSongIds)->get();
        $purchasedAlbumIds = Purchased::whereNotNull('albumId')->where('userId',$this->id)
                                            ->where('purchaseStatus','Purchased')
                                            ->where('isAcknowledged','Not Acknowledged')
                                            ->pluck('albumId')->toArray();
        $purchasedAlbums = Album::whereIn('id',$purchasedAlbumIds)->get();
        return [
            'jwt' => $this->jwt,
            'name' => $this->lastName? $this->firstName." ".$this->lastName:$this->firstName,
            'email' => $this->email,
            'phoneNumber' => $this->phoneNumber,
            'dOb' => (int) $this->birthDate,
            'gender' => $this->gender,
            'isInterestsSet' => ($this->userInterests->count() > 0)?true:false,
            'address'=> new AddressResource($this),
            'notification'=> new NotificationResource($this),
            'subscription'=> new SubscriptionResource($subscription),
            'purchasedsongs'=> SongResource::collection($purchasedSongs),
            'purchasedAlbums' => AlbumResource::collection($purchasedAlbums),
            "avatar"=>!is_null($this->avatar) ? Storage::disk('s3')->url($this->avatar):asset('artist/image/avatar/default-avatar.png')
        ];
    }
}
