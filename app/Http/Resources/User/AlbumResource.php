<?php

namespace App\Http\Resources\User;

use App\Models\AlbumSongs;
use App\Models\Purchased;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AlbumResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $songsCount = $albumPrice = $k = 0;
        $albumStatus = "FREE";
        $purchasedSongsArray = array();

        $albumSongs = AlbumSongs::where('albumId',$this->id)->get();
        foreach($albumSongs as $albumSong){
            if($albumSong->getSong){
                if($albumSong->getSong->isPaid == 1){
                    $purchased = Purchased::where('userId',Auth::user()->id)->where('songId',$albumSong->getSong->id)->first();
                    if($purchased){
                        $purchasedSongsArray[$k]  = $albumSong->getSong->id;
                        $k = $k + 1;
                    }
                    $albumPrice = $albumPrice + $albumSong->getSong->price;
                }
            }
        }
        $albumPaidSongs = AlbumSongs::where('albumId',$this->id)
                                        ->whereHas('getSong', function($q){
                                            $q->where('isPaid',1);
                                        })->count();
        if($albumSongs){
            $songsCount = count($albumSongs);
        }

        if($songsCount != 0){
            if($songsCount == $albumPaidSongs){
                $albumStatus = "PREMIUM";
            }else{
                $albumPrice = 0;
            }
        }

        $purchased = Purchased::where('userId',$request->authId)->where('albumId',$this->id)->first();
        if($purchased){
            $albumStatus = "PURCHASED";
        }else{
            if(count($purchasedSongsArray) == $albumPaidSongs){
                $albumStatus = "PURCHASED";
            }
        }

        if($request->type == "PURCHASED_ALBUM")
        {
            if($this->deletedAt != null){
                $albumStatus = "DELETED";
            }
        }
        //check if user paid or free
        $price = null;
        $sku = null;
        if ($request->isSubscribed==null || $request->isSubscribed==0){
            $discount=($albumPrice * 10)/100;
            $price = $albumPrice + $discount;
            if($this->sku != null){
                $sku = $this->sku."_wod";
            }
        }else{
            $price = $albumPrice;
            if($this->sku != null){
                $sku = $this->sku."_wd";
            }
        }
        return [
            "albumId"=> $this->id,
            "albumTitle"=> $this->name,
            "albumArtist" => ($this->getArtist)?$this->getArtist->firstName." ".$this->getArtist->lastName:null ,
            "type" => "ALBUM",
            "albumCoverImageUrl"=> !is_null($this->thumbnail) ? Storage::disk('s3')->url($this->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
            "albumStatus"=> $albumStatus,
            "price"=> (double) $price,
            "sku"=>$sku,
            "numberOfSongs"=> $songsCount

        ];
    }
}
