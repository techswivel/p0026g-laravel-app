<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "completeAddress"=> $this->address,
            "city"=> $this->city,
            "state"=> $this->state,
            "country"=> $this->country,
            "zipCode"=> $this->zipCode
        ];
    }
}
