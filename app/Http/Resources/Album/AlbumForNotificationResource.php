<?php

namespace App\Http\Resources\Album;

use App\Http\Resources\Song\SongForNotificationCollection;
use App\Models\AlbumSongs;
use App\Models\Purchased;
use App\Models\Song;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class AlbumForNotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $songsCount = 0;

        $albumSongs = AlbumSongs::where('albumId',$this->id)->get();
        if($albumSongs){
            $songsCount = count($albumSongs);
        }



        $getSongs = AlbumSongs::where('albumId', $this->id)
                    ->pluck('songId')->toArray();
        $songs = Song::whereIn('id',$getSongs)->get();

        $albumStatus = "PURCHASED";

        //check if user paid or free
        $price = null;
        $sku = null;

        return [
            "albumId"=> $this->id,
            "albumTitle"=> $this->name,
            "albumArtist" => ($this->getArtist)?$this->getArtist->firstName." ".$this->getArtist->lastName:null ,
            "type" => "ALBUM",
            "albumCoverImageURL"=> !is_null($this->thumbnail) ? Storage::disk('s3')->url($this->thumbnail):asset('defaultImages/thumbnailDefaultImage.png'),
            "albumStatus"=> $albumStatus,
            "price"=> (double) $price,
            "sku"=>$sku,
            "numberOfSongs"=> $songsCount,
            "songs" => new SongForNotificationCollection($songs)

        ];
    }
}
