<?php


namespace App\Http\Middleware;
use App\Models\User;
use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = User::where('email', Auth::user()->email)->where('status','Block')->first();
        if($user)
        {
            $request->session()->flush();
            $request->session()->regenerateToken();
            return redirect()->guest('/artist/login')->with('message', 'Your account is blocked from the artist dashboard');
        }
        else
        { 
            return $next($request);
            
        }
        

    }
}
