<?php

namespace App\Repositories\Interfaces;

use App\Models\User;

interface UserRepositoryInterface extends BasicRepositoryInterface
{
    public function getById(int $userId): ?User;

    public function getByPhoneNumber(string $phoneNumber): ?User;

    public function add(array $user): User;

    public function getByFbSocialId(string $id): ?User;

    public function getByGmailSocialId(string $id): ?User;
    public function getByAppleSocialId(string $id): ?User;
}
