<?php


namespace App\Repositories\Interfaces;


use App\Models\FcmToken;

interface FCMTokenRepositoryInterface extends BasicRepositoryInterface
{
    public function updateOrCreate(array $condition, array $data): FcmToken;

    public function first(): FcmToken;
}
