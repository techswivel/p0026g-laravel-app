<?php


namespace App\Repositories\Interfaces;


use Illuminate\Support\Collection;

interface BasicRepositoryInterface
{
    public function destroy(int $id);

    public function all(): Collection;

    public function update(int $id, array $data);
}
