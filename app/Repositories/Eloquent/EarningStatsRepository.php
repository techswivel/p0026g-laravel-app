<?php

namespace App\Repositories\Eloquent;

use App\Models\Purchased;
use App\Models\Song;
use App\Models\Album;
use App\Models\User;
use App\Models\AlbumSongs;
use Illuminate\Support\Facades\DB;

class EarningStatsRepository
{
    public function purchaseItemDetail($id)
    {
        try {
            $purchase = Purchased::with('song','album','user')->find($id);
            $albumSongId = AlbumSongs::withTrashed()->where('albumId',$purchase->albumId)->get('songId');
            $albumSongs = Song::withTrashed()->find($albumSongId);
            $price = $albumSongs->sum('price');
            return array($purchase,$albumSongs,number_format($price, 2, '.', ''));
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to show  purchase detail '.$exception->getMessage());
        }
    }
}