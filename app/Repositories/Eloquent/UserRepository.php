<?php


namespace App\Repositories\Eloquent;

use App\Models\PersonalAccessToken;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserToken;
use Illuminate\Support\Str;
use App\Enums\UserStatusEnum;
use Illuminate\Support\Collection;
use App\Repositories\Interfaces\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }


    public function all(): Collection
    {
        return User::all();
    }

    public function getById(int $userId): ?User
    {
        return User::find($userId);
    }

    public function findOrFail(int $id)
    {
        return User::findOrFail($id);
    }

    public function add(array $user): User
    {
        return User::create($user);
    }

    public function destroy(int $id): void
    {
        // TODO: Implement destroy() method.
    }

    public function update(int $id, array $data)
    {
        return User::whereId($id)->update($data);
    }

    public function getByFbSocialId(string $id): ?User
    {
        return User::where('fbId', $id)->first();
    }

    public function getByGmailSocialId(string $id): ?User
    {
        return User::where('gmailId', $id)->first();
    }
    public function getByAppleSocialId(string $id): ?User
    {
        return User::where('appleId', $id)->first();
    }
    public function getByTiktokSocialId(string $id): ?User
    {
        return User::where('tiktokId', $id)->first();
    }

    public function getByPhoneNumber(string $phoneNumber): ?User
    {
        return User::where('phoneNumber', $phoneNumber)->first();
    }

    public function isStripeCustomerId(string $stripeCustomerId)
    {
        return User::where('stripeCustomerId', $stripeCustomerId)->first();
    }

    public function getByPhoneEmail(string $email): ?User
    {
        return User::where('email', $email)->first();
    }

    public function getIndividualInfoByUserId(int $userId): ?User
    {
        return User::where('id', $userId)->first();

    }

    public function updateAvailability(User $user, bool $isAvailable)
    {
        $user->isAvailable = $isAvailable;
        return $user->update();
    }

    public function delete(int $userId)
    {
        return User::where('id', $userId)->update([
            'deletedAt' => Carbon::now()->timestamp,
            'status' => UserStatusEnum::Removed
        ]);
    }
    public function userAuthTokenInactive($userId){
        $userInfo=User::find($userId);
        if($userInfo){
            $userInfo->tokens()->delete();
        }
        return true;
    }
}
