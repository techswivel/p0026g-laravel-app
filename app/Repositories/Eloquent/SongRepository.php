<?php

namespace App\Repositories\Eloquent;

use App\Models\Song;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Traits\SongTrait;
use App\Traits\FileUploadTrait;
use App\Traits\AlbumTrait;
use App\Traits\InAppPurchaseTrait;
use App\Enums\SongPreOrderEnum;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\InAppPurchaseController;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\PriceController;
use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SongRepository
{
    use SongTrait, FileUploadTrait, AlbumTrait, InAppPurchaseTrait;

    public function store($request)
    {
        try {


            $price = 0;
            $isPaid = FALSE;
            $sku = null;
            $timestamp = Carbon::now()->timestamp;

            if ($request['songStatus'] == 'Paid Song') {
                $isPaid = TRUE;
            }

            if ($request->songId == null) {

                // Store Song Information
                $song = Song::create([
                    'userId' => $request->artistId,
                    'name' => $request['songTitle'],
                    'isPaid' => $isPaid,
                    'categoryId' => $request['songCategory'],
                    'languageId' => $request['songLanguage'],
                    'lyrics' => $request['songLyrics'] ?? '',
                    'type' => SongPreOrderEnum::Song,
                    'createdType' => SongPreOrderEnum::Song,
                    'createdBy' => auth()->user()->id,
                ]);


                //Apple Store Connect
                if ($request['songStatus'] == 'Paid Song') {
                    $sku = str_replace(' ', '_', strtolower($request->songTitle)) . "_" . $request->artistId . "_" . $song->id . "_" . $timestamp;

                    $request['reviewNote'] = 'reviewNote';
                    $request['locale'] = 'en-US';
                    $request['localeName'] = 'English';
                    $request['localeDescription'] = 'English language';
                    $request['promotionIcon'] = new UploadedFile(public_path('appleAppStore/1024.png'), '1024.png', 'image/png', filesize(public_path('appleAppStore/1024.png')), true, TRUE);
                    $request['reviewScreenshot'] = new UploadedFile(public_path('appleAppStore/Simulator-Screenshot.png'), 'Simulator-Screenshot.png', 'image/png', filesize(public_path('appleAppStore/Simulator-Screenshot.png')), true, TRUE);

                    $request['name'] = $request->songTitle . ' wd';
                    $request['productId'] = $sku . '_wd';

                    $iAppPurchaseIdWd = (new InAppPurchaseController)->storeInAppPurchase($request);

                    $request['name'] = $request->songTitle . ' wod';
                    $request['productId'] = $sku . '_wod';

                    $iAppPurchaseIdWod = (new InAppPurchaseController)->storeInAppPurchase($request);


                    Song::where("id", $song->id)->update([
                        'sku' => $sku,
                        'iAppPurchaseIdWd' => $iAppPurchaseIdWd,
                        'iAppPurchaseIdWod' => $iAppPurchaseIdWod,
                        'status' => 'PENDING',
                    ]);

                    $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                        ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/' . $iAppPurchaseIdWd . '/pricePoints', [
                            'filter[territory]' => 'USA',
                            'limit' => 1000
                        ]);

                    if ($getPriceTier->failed()) {
                        $errors = json_decode($getPriceTier->body());
                        throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                    }

                    if ($getPriceTier->successful()) {
                        $data = json_decode($getPriceTier->body());
                        $prices = [];

                        foreach ($data->data as $i => $price) {
                            if ($i > 0) {
                                $prices[$i]['priceTier'] = $price->attributes->priceTier;
                                $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                                $prices[$i]['songId'] = $song->id;
                            }
                        }

                        return $prices;
                    }
                }
            } else {
                $song = Song::find($request->songId);
            }



            if ($request->stepNo == 2) {
                // Song thumbnail
                $thumbnail = NULL;
                if (request()->hasFile('songThumbnail')) {
                    $file = request()->file('songThumbnail');
                    $thumbnail = $this->songThumbnail($file, $song->id);
                }
                // Song Audio
                $songAudio = NULL;
                $songAudioDemo = NULL;
                $songLength = 0;
                if (request()->hasFile('songAudio')) {
                    $file = request()->file('songAudio');
                    $songLength  = $this->songLength($file);
                    $songs = $this->songAudio($file, $song->id, $request->songTitle, $isPaid);
                    $songAudio = $songs[0];
                    $songAudioDemo = $songs[1];
                }

                // Song Video
                $songVideo = NULL;
                $songVideoDemo = NULL;
                if (request()->hasFile('songVideo')) {
                    $file = request()->file('songVideo');
                    $videos = $this->songVideo($file, $song->id, $request->songTitle, $isPaid);
                    $songVideo = $videos[0];
                    $songVideoDemo = $videos[1];
                }

                if ($request['songStatus'] == 'Paid Song') {

                    $name = $request['songTitle'];
                    $price = $request['songPrice'];

                    $discountedPriceTier = 0;
                    $discountedPrice = 0;

                    $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                        ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/' . $song->iAppPurchaseIdWd . '/pricePoints', [
                            'filter[territory]' => 'USA',
                            'limit' => 1000
                        ]);

                    if ($getPriceTier->failed()) {
                        $errors = json_decode($getPriceTier->body());
                        throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                    }

                    if ($getPriceTier->successful()) {
                        $data = json_decode($getPriceTier->body());
                        $discount = $request->songPrice * 0.9;
                        foreach ($data->data as $i => $inAppPrice) {
                            if ($i > 0) {
                                if ($discount < (float) $inAppPrice->attributes->customerPrice) {
                                    $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                    $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                    break;
                                }
                            }
                        }
                    }


                    if ($song->name != $request->songTitle) {
                         // Create product in withDiscount inapppurchase
                        $request['name'] = $name.' wd';
                        (new InAppPurchaseController)->updateInAppPurchaseBasic($request,$song->iAppPurchaseIdWd);

                        $request['name'] = $name.' wod';
                        (new InAppPurchaseController)->updateInAppPurchaseBasic($request,$song->iAppPurchaseIdWod);

                        //this method check if android inApp Exists already then update if not then in
                        //CATCH Excepetion: we create new inApp with already saved SKU from Model
                        try{

                            $this->getInAppPurchasedProduct($song->sku);
                        }catch(Exception $e){

                                // Create product in withOutDiscount inapppurchase
                                $skuWithOutDiscount = $song->sku . "_wod";
                                $costWithOutDiscount = round($price, 2);
                                $this->createProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);
                                (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWod);

                                // Create product in withDiscount inapppurchase
                                $request['priceTier'] = $discountedPriceTier;
                                $skuWithDiscount = $song->sku . "_wd";
                                $costWithDiscount = round($discountedPrice, 2);
                                $this->createProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);
                                (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWd);

                        }
                    }else{

                        // Create product in withOutDiscount inapppurchase
                        $request['name'] = $name.' wod';
                        $skuWithOutDiscount = $song->sku.'_wod';
                        $costWithOutDiscount = round($price, 2);
                        $this->createProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);
                        (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWod);

                         // Create product in withDiscount inapppurchase
                        $request['name'] = $name.' wd';
                        $request['priceTier'] = $discountedPriceTier;
                        $skuWithDiscount = $song->sku.'_wd';
                        $costWithDiscount = round($discountedPrice, 2);
                        $this->createProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);
                        (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWd);

                    }

                    Song::where("id", $song->id)->update([
                        'discountedPrice' => $discountedPrice,
                        'status' => 'COMPLETED',
                    ]);
                }

                Song::where("id", $song->id)->update([
                    'name' => $request['songTitle'],
                    'isPaid' => $isPaid,
                    'categoryId' => $request['songCategory'],
                    'languageId' => $request['songLanguage'],
                    'lyrics' => $request['songLyrics'] ?? '',
                    'type' => SongPreOrderEnum::Song,
                    'price' => round($price, 2),
                    'thumbnail' => $thumbnail,
                    'audioFile' => $songAudio,
                    'songLength' => $songLength,
                    'demoAudioFile' => $songAudioDemo,
                    'videoFile' => $songVideo,
                    'demoVideoFile' => $songVideoDemo,
                ]);

                if($request['songStatus'] == 'Free Song') {
                    Song::where("id", $song->id)->update([
                        'status' => 'COMPLETED',
                    ]);
                }
            }

            return Song::find($song->id);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to stored Song' . $exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $song = Song::find($id);

            Song::where('id', $id)->update([
                'deletedBy' => auth()->user()->id,
            ]);
            if ($song == null) {
                throw new \ErrorException('This data is not present in table');
            }

            $albumSong = AlbumSongs::where('songId', $id)->first();
            if ($albumSong) {
                $albumSong->delete();
                $albumStatus = $this->AlbumStatusCheck($albumSong->albumId);

                if ($albumStatus) {
                    Album::where("id", $albumSong->albumId)->update([
                        'albumStatus' => $albumStatus,
                    ]);
                }

                $album = Album::find($albumSong->albumId);
                if ($album->sku != null) {
                    try {
                        $this->deleteInAppPurchasedProduct($album->sku . "_wod");
                        $this->deleteInAppPurchasedProduct($album->sku . "_wd");
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to delete sku ' . $exception->getMessage());
                    }
                }

            }

            // sku
            if ($song->sku != null) {
                try {
                    $this->deleteInAppPurchasedProduct($song->sku . "_wod");
                    $this->deleteInAppPurchasedProduct($song->sku . "_wd");
                } catch (\Exception $exception) {
                    throw new \ErrorException('Fail to delete sku ' . $exception->getMessage());
                }
            }

            //Apple App Store Connect
            if ($song->iAppPurchaseIdWd != null) {
                try {
                    (new InAppPurchaseController)->deleteInAppPurchase($song->iAppPurchaseIdWd);
                } catch (\Exception $exception) {
                    throw new \ErrorException('Fail to delete sku ' . $exception->getMessage());
                }
            }

            if ($song->iAppPurchaseIdWod != null) {
                try {
                    (new InAppPurchaseController)->deleteInAppPurchase($song->iAppPurchaseIdWod);
                } catch (\Exception $exception) {
                    throw new \ErrorException('Fail to delete sku ' . $exception->getMessage());
                }
            }

            $song->delete();

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to delete Song' . $exception->getMessage());
        }
    }

    public function show($id)
    {
        try {
            $prices = [];
            $song = Song::find($id);
            if ($song == null) {
                throw new \ErrorException('This data is not present in table');
            }

            if ($song->preOrderReleaseDate) {
                $song->date = Carbon::parse($song->preOrderReleaseDate)->format('m/d/Y');
                $song->time = Carbon::parse($song->preOrderReleaseDate)->format('g:i A');
            }

            if ($song->iAppPurchaseIdWd != null) {
                $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/' . $song->iAppPurchaseIdWd . '/pricePoints', [
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                    ]);

                if ($getPriceTier->failed()) {
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if ($getPriceTier->successful()) {
                    $data = json_decode($getPriceTier->body());

                    foreach ($data->data as $i => $price) {
                        if ($i > 0) {
                            $prices[$i]['priceTier'] = $price->attributes->priceTier;
                            $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                        }
                    }
                }

                $song->prices = $prices;
            }



            return $song;
        } catch (\Exception $exception) {
            throw new \ErrorException('This data is not present in table');
        }
    }

    public function update($request)
    {
        try {

            DB::beginTransaction();
            $song = Song::find($request->id);
            //Song price
            $price = 0;
            $isPaid = FALSE;
            $albumSku = null;
            $timestamp = Carbon::now()->timestamp;
            if ($request['editSongStatus'] == 'Paid Song') {
                $isPaid = TRUE;
            }
            // Song Thumbnail
            $thumbnail = $song->thumbnail;
            if (request()->hasFile('editThumbnail')) {
                $this->deleteFile($song->thumbnail);
                $file = request()->file('editThumbnail');
                $thumbnail = $this->songThumbnail($file, $song->id);
            }


            // Song Audio
            $songAudio = $song->audioFile;
            $songAudioDemo =  $song->demoAudioFile;
            if ($song->audioFile != null) {
                if ($song->demoAudioFile == null && $request['editSongStatus'] == 'Paid Song') {
                    $songs = $this->songPaidAudio($songAudio, $song->id, $request->editSongName, $isPaid);
                    $songAudioDemo = $songs;
                }

                if ($request['editSongStatus'] == 'Free Song') {
                    $songAudioDemo = null;
                }
            }

            $songLength = $song->songLength;
            if (request()->hasFile('editAudioFile')) {
                $this->deleteFile($song->audioFile);
                $this->deleteFile($song->demoAudioFile);
                $file = request()->file('editAudioFile');
                $songLength  = $this->songLength($file);
                $songs = $this->songAudio($file, $song->id, $request->editSongName, $isPaid);
                $songAudio = $songs[0];
                $songAudioDemo = $songs[1];
            }

            // Song Video
            $songVideo = $song->videoFile;
            $songVideoDemo = $song->demoVideoFile;
            if ($song->videoFile != null) {
                if ($song->demovideoFile == null && $request['editSongStatus'] == 'Paid Song') {
                    $songs = $this->songPaidVideo($songVideo, $song->id, $request->editSongName, $isPaid);
                    $songVideoDemo = $songs;
                }
                if ($request['editSongStatus'] == 'Free Song') {
                    $songVideoDemo = null;
                }
            }

            if (request()->hasFile('editVideoFile')) {
                $file = request()->file('editVideoFile');
                $videos = $this->songVideo($file, $song->id, $request->editSongName, $isPaid);
                $songVideo =  $videos[0];
                $songVideoDemo = $videos[1];
            }

            if (request()->hasFile('editVideoFile1')) {
                $this->deleteFile($song->videoFile);
                $this->deleteFile($song->demoVideoFile);
                $file = request()->file('editVideoFile1');
                $videos = $this->songVideo($file, $song->id, $request->editSongName, $isPaid);
                $songVideo =  $videos[0];
                $songVideoDemo = $videos[1];
            }


            $albumSong = AlbumSongs::where('songId', $song->id)->first();
            if ($albumSong) {
                $albumStatus = $this->AlbumStatusCheck($albumSong->albumId);
                if ($albumStatus) {
                    Album::where("id", $albumSong->albumId)->update([
                        'albumStatus' => $albumStatus,
                    ]);
                }

                $album = Album::find($albumSong->albumId);

                if ($album->sku != null) {
                    try {
                        $this->deleteInAppPurchasedProduct($album->sku . "_wod");
                        $this->deleteInAppPurchasedProduct($album->sku . "_wd");
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to delete sku ' . $exception->getMessage());
                    }
                }

                if ($albumStatus == 'Paid') {
                    // song title _ artist Id_song Id__timestamp_type
                    $albumSku = str_replace(' ', '_', strtolower($album->name)) . "_" . $album->userId . "_" . $album->id . "_" . $timestamp;
                    // Create product in inapppurchase
                    $name = $album->name;
                    $cost = Song::whereHas('songs', function ($query) use ($album) {
                        $query->where('albumId', $album->id);
                    })->sum('price');
                    // Create product in withDiscount inapppurchase
                    $skuWithDiscount = $albumSku . "_wd";
                    $costWithDiscount = round($cost, 2);
                    $this->createProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);
                    // Create product in withOutDiscount inapppurchase
                    $skuWithOutDiscount = $albumSku . "_wod";
                    $discount = ($cost * 10) / 100;
                    $costWithOutDiscount = round($cost + $discount, 2);
                    $this->createProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);
                }
                Album::where('id', $album->id)->update([
                    'sku' => $albumSku
                ]);
            }


            if ($request['editSongStatus'] == 'Paid Song') {
                $price = $request['editPrice'];
                $name = $request['editSongName'];

                $discountedPriceTier = 0;
                $discountedPrice = 0;

                $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/' . $song->iAppPurchaseIdWd . '/pricePoints', [
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                    ]);

                if ($getPriceTier->failed()) {
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if ($getPriceTier->successful()) {
                    $data = json_decode($getPriceTier->body());
                    $discount = $request->editPrice * 0.9;
                    foreach ($data->data as $i => $inAppPrice) {
                        if ($i > 0) {
                            if ($discount < (float) $inAppPrice->attributes->customerPrice) {
                                $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                break;
                            }
                        }
                    }
                }




                if($song->status != 'PENDING'){
                    // Update product in withDiscount inapppurchase
                    $skuWithDiscount = $song->sku . "_wd";
                    $costWithDiscount = round($discountedPrice, 2);
                    $this->updateProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);

                    // Update product in withOutDiscount inapppurchase
                    $skuWithOutDiscount = $song->sku . "_wod";
                    $costWithOutDiscount = round($price, 2);
                    $this->updateProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);

                }else{
                    // Create product in withDiscount inapppurchase
                    $skuWithDiscount = $song->sku . "_wd";
                    $costWithDiscount = round($discountedPrice, 2);
                    $this->createProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);

                    // Create product in withOutDiscount inapppurchase
                    $skuWithOutDiscount = $song->sku . "_wod";
                    $costWithOutDiscount = round($price, 2);
                    $this->createProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);
                }

                if ($song->iAppPurchaseIdWod != null) {
                    try {
                        $request['name'] = $request->editSongName . ' wod';
                        $request['priceTier'] = $request->priceTier;

                        (new InAppPurchaseController)->updateInAppPurchase($request, $song->iAppPurchaseIdWod);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update InAppPurchase ' . $exception->getMessage());
                    }
                }


                if ($song->iAppPurchaseIdWd != null) {
                    try {
                        $request['name'] = $request->editSongName . ' wd';
                        $request['priceTier'] = $discountedPriceTier;

                        (new InAppPurchaseController)->updateInAppPurchase($request, $song->iAppPurchaseIdWd);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update InAppPurchase ' . $exception->getMessage());
                    }
                }

            }

            Song::where("id", $song->id)->update([
                'name' => $request->editSongName,
                'isPaid' => $isPaid,
                'price' => round($price, 2),
                'categoryId' => $request->editSongCategory,
                'languageId' => $request->editSongLanguage,
                'thumbnail' => $thumbnail,
                'audioFile' =>  $songAudio,
                'demoAudioFile' => $songAudioDemo,
                'songLength' => $songLength,
                'videoFile' => $songVideo,
                'demoVideoFile' => $songVideoDemo,
                'lyrics' => $request->editSongLyrics,
                'discountedPrice' => $discountedPrice ?? 0,
                'status' => 'COMPLETED',
            ]);

            DB::commit();
            return $song;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to update Song' . $exception->getMessage());
        }
    }
}
