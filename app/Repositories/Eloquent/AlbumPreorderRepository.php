<?php

namespace App\Repositories\Eloquent;

use App\Models\Song;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Models\Purchased;
use App\Traits\SongTrait;
use App\Traits\AlbumTrait;
use App\Traits\FileUploadTrait;
use App\Enums\SongPreOrderEnum;
use App\Enums\AlbumPreOrderEnum;
use App\Enums\AlbumStatusEnum;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\InAppPurchaseController;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\PriceController;
use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Jobs\CheckPreorderAlbumJob;
use App\Traits\InAppPurchaseTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AlbumPreorderRepository
{
    use SongTrait,AlbumTrait,FileUploadTrait,InAppPurchaseTrait;

    public function store($request)
    {
        try {
            $timestamp = Carbon::now()->timestamp;
            $date = Carbon::createFromDate($request['preorderAlbumDate'].' .'.$request['preorderAlbumTime'])->timestamp;


            if($request->stepNo == 1){

                $album = Album::create([
                    'userId' => $request->artistId,
                    'name' => $request->preorderAlbumName,
                    'type' => AlbumPreOrderEnum::PreOrder,
                    'createdType' => AlbumPreOrderEnum::PreOrder,
                    'createdBy' => auth()->user()->id,
                    'preOrderReleaseDate' => $date,
                    'preOrderCount' => 0,
                    'totalPreOrderCount' => 100,
                ]);

                // Album thumbnail
                $thumbnail = NULL;
                if(request()->hasFile('preorderAlbumThumbnail'))
                {
                    $file = request()->file('preorderAlbumThumbnail');
                    $thumbnail = $this->albumThumbnail($file,$album->id);
                    Album::where("id",$album->id)->update([
                        'thumbnail' => $thumbnail,
                    ]);
                }

                $totalPreorderAlbumPrice = 0;
                foreach ($request->preorderSelectedSongs as $songId) {
                    if(AlbumSongs::where('songId',$songId)->exists()){
                        throw new \ErrorException('This Song is already relate with another album');
                    }else{
                        AlbumSongs::create([
                            'albumId' => $album->id,
                            'songId' => $songId,
                        ]);
                        $song = Song::find($songId);
                        $totalPreorderAlbumPrice = $totalPreorderAlbumPrice + $song->price;
                    }
                }

                $albumStatus = $this->AlbumStatusCheck($album->id);
                if($albumStatus == 'Paid'){
                    $sku = str_replace(' ', '_', strtolower($request->preorderAlbumName))."_".$request->artistId."_".$album->id."_".$timestamp;

                    //Apple Store Connect
                    $request['reviewNote'] = 'reviewNote';
                    $request['locale'] = 'en-US';
                    $request['localeName'] = 'English';
                    $request['localeDescription'] = 'English language';
                    $request['promotionIcon'] = new UploadedFile(public_path('appleAppStore/1024.png'),'1024.png','image/png',filesize(public_path('appleAppStore/1024.png')),true,TRUE);
                    $request['reviewScreenshot'] = new UploadedFile(public_path('appleAppStore/Simulator-Screenshot.png'),'Simulator-Screenshot.png','image/png',filesize(public_path('appleAppStore/Simulator-Screenshot.png')),true,TRUE);

                    $request['name'] = $request->preorderAlbumName.' wd';
                    $request['productId'] = $sku.'_wd';
                    $iAppPurchaseIdWd = (new InAppPurchaseController)->storeInAppPurchase($request);

                    $request['name'] = $request->preorderAlbumName.' wod';
                    $request['productId'] = $sku.'_wod';
                    $iAppPurchaseIdWod = (new InAppPurchaseController)->storeInAppPurchase($request);


                    Album::where("id",$album->id)->update([
                        'albumStatus' => $albumStatus,
                        'sku' => $sku,
                        'iAppPurchaseIdWd' => $iAppPurchaseIdWd ?? '',
                        'iAppPurchaseIdWod' => $iAppPurchaseIdWod ?? '',
                        'iAppPurchasePrice' => $request->iAppPurchasePrice ?? '',
                        'status' => 'PENDING',
                    ]);

                    $getPriceTier = Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                        ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$iAppPurchaseIdWd.'/pricePoints',[
                            'filter[territory]' => 'USA',
                            'limit' => 1000
                    ]);

                    if($getPriceTier->failed()){
                        $errors = json_decode($getPriceTier->body());
                        throw new \ErrorException('Fail to get Apple InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                    }

                    if($getPriceTier->successful()){
                        $data = json_decode($getPriceTier->body());
                        $prices = [];

                        foreach($data->data as $i => $price){
                            if($i > 0){
                                $prices[$i]['priceTier'] = $price->attributes->priceTier;
                                $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                                $prices[$i]['albumId'] = $album->id;
                                $prices[$i]['songsCost'] = $totalPreorderAlbumPrice;
                            }
                        }

                        return $prices;
                    }
                }elseif($albumStatus){
                    Album::where("id",$album->id)->update([
                        'albumStatus' => $albumStatus,
                    ]);
                }


            }elseif($request->stepNo == 2){

                $album = Album::find($request->albumId);

                $discountedPriceTier = 0;
                $discountedPrice = 0;


                $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$album->iAppPurchaseIdWd.'/pricePoints',[
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                ]);

                if($getPriceTier->failed()){
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());
                    $discount = $request->iAppPurchasePrice * 0.9;
                    foreach($data->data as $i => $inAppPrice){
                        if($i > 0){
                            if($discount < (float) $inAppPrice->attributes->customerPrice){
                                $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                break;
                            }
                        }
                    }
                }

                // Create product in withDiscount inapppurchase
                $name = $request['preorderAlbumName'];

                if($album->name != $request->preorderAlbumName){

                    $request['name'] = $request->preorderAlbumName.' wod';
                    (new InAppPurchaseController)->updateInAppPurchaseBasic($request,$album->iAppPurchaseIdWod);

                    $request['name'] = $request->preorderAlbumName.' wd';
                    (new InAppPurchaseController)->updateInAppPurchaseBasic($request,$album->iAppPurchaseIdWd);

                    try{
                        $this->getInAppPurchasedProduct($album->sku);
                    }catch(Exception $e){

                         // Create product in withOutDiscount inapppurchase
                        $skuWithOutDiscount = $album->sku."_wod";
                        $costWithOutDiscount = round($request->iAppPurchasePrice ,2);
                        $this->createProductInAppPurchase($costWithOutDiscount,$name,$skuWithOutDiscount);
                        (new PriceController)->storeInAppPurchasePrice($request, $album->iAppPurchaseIdWod);

                        // Create product in withDiscount inapppurchase
                        $skuWithDiscount = $album->sku."_wd";
                        $request['priceTier'] = $discountedPriceTier;
                        $costWithDiscount = round($discountedPrice,2);
                        $this->createProductInAppPurchase($costWithDiscount,$name,$skuWithDiscount);
                        (new PriceController)->storeInAppPurchasePrice($request, $album->iAppPurchaseIdWd);

                    }


                }else{

                    // Create product in withOutDiscount inapppurchase
                    $skuWithOutDiscount = $album->sku."_wod";
                    $costWithOutDiscount = round($request->iAppPurchasePrice ,2);
                    $this->createProductInAppPurchase($costWithOutDiscount,$name,$skuWithOutDiscount);
                    (new PriceController)->storeInAppPurchasePrice($request, $album->iAppPurchaseIdWod);

                    $skuWithDiscount = $album->sku."_wd";
                    $request['priceTier'] = $discountedPriceTier;
                    $costWithDiscount = round($discountedPrice,2);
                    $this->createProductInAppPurchase($costWithDiscount,$name,$skuWithDiscount);
                    (new PriceController)->storeInAppPurchasePrice($request, $album->iAppPurchaseIdWd);

                }



                Album::where("id",$album->id)->update([
                    'name' => $request->preorderAlbumName,
                    'preOrderReleaseDate' => $date,
                    'discountedPrice' => $discountedPrice,
                    'iAppPurchasePrice' => $request->iAppPurchasePrice,
                    'status' => 'COMPLETED',
                ]);

                dispatch(new CheckPreorderAlbumJob())->delay(Carbon::parse($date));

                return $album;
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to stored preorder Album'.$exception->getMessage());
        }

    }

    public function storeAlbumSong($request)
    {
        try {

            $price = 0;
            $sku = null;
            $timestamp = Carbon::now()->timestamp;
            $isPaid = TRUE;
            $price = $request['songPrice'] ?? 0;

            if($request->stepNo == 1){
                 // Store Song Information
                $song = Song::create([
                    'userId' => $request->artistId,
                    'name' => $request['albumSongPreorderName'],
                    'isPaid' => $isPaid,
                    'categoryId' => $request['albumSongPreorderCategory'],
                    'languageId' => $request['albumSongPreorderLanguage'],
                    'lyrics' => $request['albumSongPreorderLyrics'],
                    'createdBy' => auth()->user()->id,
                    'type' => SongPreOrderEnum::PreOrderAlbum,
                    'createdType' => SongPreOrderEnum::PreOrderAlbum,
                ]);

                $sku = str_replace(' ', '_', strtolower($request->albumSongPreorderName)) . "_" . $request->artistId . "_" . $song->id . "_" . $timestamp;
                $request['reviewNote'] = 'reviewNote';
                $request['locale'] = 'en-US';
                $request['localeName'] = 'English';
                $request['localeDescription'] = 'English language';
                $request['promotionIcon'] = new UploadedFile(public_path('appleAppStore/1024.png'),'1024.png','image/png',filesize(public_path('appleAppStore/1024.png')),true,TRUE);
                $request['reviewScreenshot'] = new UploadedFile(public_path('appleAppStore/Simulator-Screenshot.png'),'Simulator-Screenshot.png','image/png',filesize(public_path('appleAppStore/Simulator-Screenshot.png')),true,TRUE);

                $request['name'] = $request->albumSongPreorderName.' wd';
                $request['productId'] = $sku.'_wd';
                $iAppPurchaseIdWd = (new InAppPurchaseController)->storeInAppPurchase($request);

                $request['name'] = $request->albumSongPreorderName.' wod';
                $request['productId'] = $sku.'_wod';
                $iAppPurchaseIdWod = (new InAppPurchaseController)->storeInAppPurchase($request);


                Song::where("id", $song->id)->update([
                    'sku' => $sku,
                    'iAppPurchaseIdWd' => $iAppPurchaseIdWd,
                    'iAppPurchaseIdWod' => $iAppPurchaseIdWod,
                    'status' => 'PENDING',
                ]);

                $getPriceTier = Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$iAppPurchaseIdWd.'/pricePoints',[
                        'filter[territory]' => 'USA',
                        'include' => 'territory',
                        'limit' => 1000
                ]);

                if($getPriceTier->failed()){
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get Apple InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());
                    $prices = [];

                    foreach($data->data as $i => $price){
                        if($i > 0){
                            $prices[$i]['priceTier'] = $price->attributes->priceTier;
                            $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                            $prices[$i]['songId'] = $song->id;
                        }
                    }

                    return $prices;
                }


            }elseif($request->stepNo == 2){
                $song = Song::find($request->songId);
                // Song thumbnail
                $thumbnail = NULL;
                if(request()->hasFile('albumSongPreorderThumbnail'))
                {
                    $file = request()->file('albumSongPreorderThumbnail');
                    $thumbnail = $this->songThumbnail($file,$song->id);
                }
                // Song Audio
                $songAudio = NULL;
                $songAudioDemo = NULL;
                $songLength = 0;
                if(request()->hasFile('albumSongPreorderAudio'))
                {
                    $file = request()->file('albumSongPreorderAudio');
                    $songLength  = $this->songLength($file);
                    $songs = $this->songAudio($file,$song->id,$request->albumSongPreorderName,TRUE);
                    $songAudio = $songs[0];
                    $songAudioDemo = $songs[1];
                }

                // Song Video
                $songVideo = NULL;
                $songVideoDemo = NULL;
                if(request()->hasFile('albumSongPreorderVideo'))
                {
                    $file = request()->file('albumSongPreorderVideo');
                    $videos = $this->songVideo($file,$song->id,$request->albumSongPreorderName,TRUE);
                    $songVideo = $videos[0];
                    $songVideoDemo = $videos[1];
                }


                $sku = $song->sku;
                $price = $price;


                $discountedPriceTier = 0;
                $discountedPrice = 0;

                $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$song->iAppPurchaseIdWd.'/pricePoints',[
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                ]);

                if($getPriceTier->failed()){
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());
                    $discount = $request->songPrice * 0.9;
                    foreach($data->data as $i => $inAppPrice){
                        if($i > 0){
                            if($discount < (float) $inAppPrice->attributes->customerPrice){
                                $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                break;
                            }
                        }
                    }
                }


                if($song->name != $request->albumSongPreorderName){

                    $request['name'] = $request->albumSongPreorderName.' wod';
                    (new InAppPurchaseController)->updateInAppPurchaseBasic($request,$song->iAppPurchaseIdWod);

                    $request['name'] = $request->albumSongPreorderName.' wd';
                    (new InAppPurchaseController)->updateInAppPurchaseBasic($request,$song->iAppPurchaseIdWd);

                    try{
                        $this->getInAppPurchasedProduct($song->sku);
                    }catch(Exception $e){

                        // Create product in withDiscount inapppurchase
                        $name = $request['albumSongPreorderName'];

                        $skuWithOutDiscount = $song->sku."_wod";
                        $costWithOutDiscount = round($request->songPrice ,2);
                        $this->createProductInAppPurchase($costWithOutDiscount,$name,$skuWithOutDiscount);
                        (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWod);

                        $request['priceTier'] = $discountedPriceTier;
                        $costWithDiscount = round($discountedPrice,2);
                        $skuWithDiscount = $song->sku."_wd";
                        $this->createProductInAppPurchase($costWithDiscount,$name,$skuWithDiscount);
                        (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWd);

                    }
                }else{

                    $name = $request['albumSongPreorderName'];

                    // Create product in withOutDiscount inapppurchase
                    $skuWithOutDiscount = $sku."_wod";
                    $costWithOutDiscount = round($request->songPrice,2);
                    $this->createProductInAppPurchase($costWithOutDiscount,$name,$skuWithOutDiscount);
                    (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWod);


                    // Create product in withDiscount inapppurchase
                    $request['priceTier'] = $discountedPriceTier;
                    $skuWithDiscount = $sku."_wd";
                    $costWithDiscount = round($discountedPrice,2);
                    $this->createProductInAppPurchase($costWithDiscount,$name,$skuWithDiscount);
                    (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWd);

                }

                Song::where("id",$song->id)->update([
                    'price' => $request->songPrice,
                    'thumbnail' => $thumbnail,
                    'audioFile' => $songAudio,
                    'songLength' => $songLength,
                    'demoAudioFile' => $songAudioDemo,
                    'videoFile' => $songVideo,
                    'demoVideoFile' => $songVideoDemo,
                    'discountedPrice' => $discountedPrice,
                    'status' => 'COMPLETED',
                ]);

                return Song::find($song->id);
            }

        } catch (\Exception $exception) {

            throw new \ErrorException('Fail to stored song preorder Album'.$exception->getMessage());
        }

    }

    public function updateAlbumSong($request)
    {
        try {
            DB::beginTransaction();

            $timestamp = Carbon::now()->timestamp;
            $song = Song::find($request->editPreorderAlbumSongid);
            if($song){
                $sku = $song->sku;

                //Song price
                $price = 0;
                $isPaid = TRUE;
                $price = $request['editAlbumSongPreorderPrice'];
                // Song Thumbnail
                $thumbnail = $song->thumbnail;
                if(request()->hasFile('editAlbumSongPreorderThumbnail'))
                {
                    $this->deleteFile($song->thumbnail);
                    $file = request()->file('editAlbumSongPreorderThumbnail');
                    $thumbnail = $this->songThumbnail($file,$song->id);
                }

                // Song Audio
                $songAudio = $song->audioFile ;
                $songAudioDemo =  $song->demoAudioFile ;
                $songLength = $song->songLength;
                if(request()->hasFile('editAlbumSongPreorderAudio'))
                {
                    $this->deleteFile($song->audioFile);
                    $this->deleteFile($song->demoAudioFile);
                    $file = request()->file('editAlbumSongPreorderAudio');
                    $songLength  = $this->songLength($file);
                    $songs = $this->songAudio($file,$song->id,$request->editSongName,TRUE);
                    $songAudio = $songs[0];
                    $songAudioDemo = $songs[1];
                }

                // Song Video
                $songVideo = $song->videoFile;
                $songVideoDemo = $song->demoVideoFile;
                if(request()->hasFile('editAlbumSongPreorderVideo1'))
                {
                    $file = request()->file('editAlbumSongPreorderVideo1');
                    $videos = $this->songVideo($file,$song->id,$request->editSongName,TRUE);
                    $songVideo =  $videos[0];
                    $songVideoDemo = $videos[1];
                }

                if(request()->hasFile('editAlbumSongPreorderVideo'))
                {
                    $this->deleteFile($song->videoFile);
                    $this->deleteFile($song->demoVideoFile);
                    $file = request()->file('editAlbumSongPreorderVideo');
                    $videos = $this->songVideo($file,$song->id,$request->editSongName,TRUE);
                    $songVideo =  $videos[0];
                    $songVideoDemo = $videos[1];
                }

                $price = $price;

                $discountedPriceTier = 0;
                $discountedPrice = 0;

                $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$song->iAppPurchaseIdWd.'/pricePoints',[
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                ]);

                if($getPriceTier->failed()){
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());
                    $discount = $request->editAlbumSongPreorderPrice * 0.9;
                    foreach($data->data as $i => $inAppPrice){
                        if($i > 0){
                            if($discount < (float) $inAppPrice->attributes->customerPrice){
                                $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                break;
                            }
                        }
                    }
                }


                // Create product in withOutDiscount inapppurchase
                $name = $request['edtiAlbumSongPreorderName'];

                $skuWithOutDiscount = $sku."_wod";
                $costWithOutDiscount = round($request->editAlbumSongPreorderPrice,2);

                if($song->status != 'PENDING'){
                    $this->updateProductInAppPurchase($costWithOutDiscount,$name,$skuWithOutDiscount);
                }else{
                    $this->createProductInAppPurchase($costWithOutDiscount,$name,$skuWithOutDiscount);
                }

                if ($song->iAppPurchaseIdWod != null) {
                    try {

                        $request['name'] = $request->edtiAlbumSongPreorderName.' wod';
                        $request['priceTier'] = $request->priceTier;

                        (new InAppPurchaseController)->updateInAppPurchase($request,$song->iAppPurchaseIdWod);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update Apple InAppPurchase ' . $exception->getMessage());
                    }
                }

                // Create product in withDiscount inapppurchase
                $skuWithDiscount = $sku."_wd";
                $costWithDiscount = round($discountedPrice,2);

                if($song->status != 'PENDING'){
                    $this->updateProductInAppPurchase($costWithDiscount,$name,$skuWithDiscount);
                }else{
                    $this->createProductInAppPurchase($costWithDiscount,$name,$skuWithDiscount);
                }

                if ($song->iAppPurchaseIdWd != null) {
                    try {
                        $request['name'] = $request->edtiAlbumSongPreorderName.' wd';
                        $request['priceTier'] = $discountedPriceTier;
                        (new InAppPurchaseController)->updateInAppPurchase($request,$song->iAppPurchaseIdWd);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update Apple InAppPurchase ' . $exception->getMessage());
                    }
                }



                $albumSongs = AlbumSongs::where('songId',$song->id)->first();

                if($albumSongs){
                    $album = Album::find($albumSongs->albumId);
                    if($album){
                        if ( $album->sku != null ){
                            try {
                                $this->deleteInAppPurchasedProduct($album->sku."_wod");
                                $this->deleteInAppPurchasedProduct($album->sku."_wd");
                            } catch (\Exception $exception) {
                                throw new \ErrorException('Fail to delete sku '.$exception->getMessage());
                            }
                        }

                        // song title _ artist Id_song Id__timestamp_type
                        $albumSku = str_replace(' ', '_', strtolower($album->name))."_".$album->userId."_".$album->id."_".$timestamp;
                        // Create product in inapppurchase
                        $name = $album->name;
                        $cost = Song::whereHas('songs',function($query) use ($album){
                            $query->where('albumId',$album->id);
                        })->sum('price');
                        // Create product in withDiscount inapppurchase
                        $skuWithDiscount = $albumSku."_wd";
                        $costWithDiscount = round($cost,2);
                        $this->createProductInAppPurchase($costWithDiscount,$name,$skuWithDiscount);
                        // Create product in withOutDiscount inapppurchase
                        $skuWithOutDiscount = $albumSku."_wod";
                        $discount=($cost * 10)/100;
                        $costWithOutDiscount = round($cost + $discount,2);
                        $this->createProductInAppPurchase($costWithOutDiscount,$name,$skuWithOutDiscount);

                        Album::where('id',$album->id)->update([
                            'sku' => $albumSku,
                        ]);

                    }
                }

                Song::where("id",$song->id)->update([
                    'name' => $request->edtiAlbumSongPreorderName,
                    'isPaid' => $isPaid,
                    'price' => $price,
                    'categoryId' => $request->editAlbumSongPreorderCategory,
                    'languageId' => $request->editAlbumSongPreorderLanguage,
                    'thumbnail' => $thumbnail,
                    'audioFile' =>  $songAudio,
                    'demoAudioFile' => $songAudioDemo,
                    'songLength' => $songLength,
                    'videoFile' => $songVideo,
                    'demoVideoFile' => $songVideoDemo ,
                    'lyrics' => $request->editAlbumSongPreorderLyrics,
                    'discountedPrice' => $discountedPrice,
                    'status' => 'COMPLETED',
                ]);
            }
            DB::commit();
            return Song::find($request->editPreorderAlbumSongid);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to update song preorder Album'.$exception->getMessage());
        }

    }

    public function removeAlbumSong($id)
    {
        try {
            DB::beginTransaction();
            $song = Song::where('id',$id)->first();
            if($song){
                if ( $song->sku != null ){
                    try {
                        $this->deleteInAppPurchasedProduct($song->sku."_wod");
                        $this->deleteInAppPurchasedProduct($song->sku."_wd");
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to delete Google sku '.$exception->getMessage());
                    }
                }


                //Apple App Store Connect
                if ($song->iAppPurchaseIdWd != null) {
                    try {
                        (new InAppPurchaseController)->deleteInAppPurchase($song->iAppPurchaseIdWd);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to delete Apple InAppPurchase ' . $exception->getMessage());
                    }
                }

                if ($song->iAppPurchaseIdWod != null) {
                    try {
                        (new InAppPurchaseController)->deleteInAppPurchase($song->iAppPurchaseIdWod);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to delete Apple InAppPurchase ' . $exception->getMessage());
                    }
                }

            }
            AlbumSongs::where('songId',$id)->delete();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to remove song for preorder album'.$exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $album = Album::find($id);
            if($album == null){
                throw new \ErrorException('This data is not present in table');
            }
            $albumSongs = AlbumSongs::where('albumId',$id)->get();
            // First delete all the song relation and then delete
            foreach ($albumSongs as $song) {
                Song::where('id',$song->songId)->delete();
                $song->delete();
                if ($song->sku){
                    try {
                        $this->deleteInAppPurchasedProduct($song->sku."_wod");
                        $this->deleteInAppPurchasedProduct($song->sku."_wd");

                        //Apple App Store Connect
                        if ($song->iAppPurchaseIdWd != null) {
                            try {
                                (new InAppPurchaseController)->deleteInAppPurchase($song->iAppPurchaseIdWd);
                            } catch (\Exception $exception) {
                                throw new \ErrorException('Fail to delete Apple InAppPurchase ' . $exception->getMessage());
                            }
                        }

                        if ($song->iAppPurchaseIdWod != null) {
                            try {
                                (new InAppPurchaseController)->deleteInAppPurchase($song->iAppPurchaseIdWod);
                            } catch (\Exception $exception) {
                                throw new \ErrorException('Fail to delete Apple InAppPurchase ' . $exception->getMessage());
                            }
                        }

                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to delete sku '.$exception->getMessage());
                    }
                }
            }

            $album->delete();
            if ($album->sku){
                try {
                    $this->deleteInAppPurchasedProduct($album->sku."_wod");
                    $this->deleteInAppPurchasedProduct($album->sku."_wd");
                } catch (\Exception $exception) {
                    throw new \ErrorException('Fail to delete sku '.$exception->getMessage());
                }
            }

            //Apple App Store Connect
            if ($album->iAppPurchaseIdWd != null) {
                try {
                    (new InAppPurchaseController)->deleteInAppPurchase($album->iAppPurchaseIdWd);
                } catch (\Exception $exception) {
                    throw new \ErrorException('Fail to delete Apple InAppPurchase ' . $exception->getMessage());
                }
            }

            if ($album->iAppPurchaseIdWod != null) {
                try {
                    (new InAppPurchaseController)->deleteInAppPurchase($album->iAppPurchaseIdWod);
                } catch (\Exception $exception) {
                    throw new \ErrorException('Fail to delete Apple InAppPurchase ' . $exception->getMessage());
                }
            }


            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to delete preorder album'.$exception->getMessage());
        }

    }

    public function show($id)
    {
        try {
            $album = Album::find($id);
            if($album->preOrderReleaseDate){
                $album->date = Carbon::parse($album->preOrderReleaseDate)->format('m/d/Y');
                $album->time = Carbon::parse($album->preOrderReleaseDate)->format('g:i A');
            }
            $albumSongs = AlbumSongs::with('song')->where('albumId',$id)->get();

            $getPriceTier = Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'. $album->iAppPurchaseIdWd.'/pricePoints',[
                    'filter[territory]' => 'USA',
                    'limit' => 1000
            ]);

            if($getPriceTier->failed()){
                $errors = json_decode($getPriceTier->body());
                throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
            }

            if($getPriceTier->successful()){
                $data = json_decode($getPriceTier->body());
                $prices = [];

                foreach($data->data as $i => $price){
                    if($i > 0){
                        $prices[$i]['priceTier'] = $price->attributes->priceTier;
                        $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                        $prices[$i]['albumId'] = $album->id;
                        $prices[$i]['songsCost'] = $albumSongs->pluck('song.price')->sum();
                    }
                }

                $album->prices = $prices;
            }

            return array($album,$albumSongs);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to Show preorder album'.$exception->getMessage());
        }
    }

    public function getPreorderSong($id)
    {
        try {
            return Song::where('userId',$id)->where('type',SongPreOrderEnum::PreOrderAlbum)->doesntHave("songs")->get();
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to get preorder album song '.$exception->getMessage());
        }
    }

    public function update($request)
    {
        try {
            DB::beginTransaction();
            $timestamp = Carbon::now()->timestamp;
            $date = Carbon::createFromDate($request['editPreorderAlbumDate'].' .'.$request['editPreorderAlbumTime'])->timestamp;
            $album = Album::find($request->editPreorderAlbumId);

            $albumSongs = AlbumSongs::where('albumId',$album->id)->delete();
            // Album thumbnail
            $thumbnail = $album->thumbnail;
            if(request()->hasFile('editPreorderAlbumThumbnail'))
            {
                $file = request()->file('editPreorderAlbumThumbnail');
                $thumbnail = $this->albumThumbnail($file,$album->id);
            }
            $totalPreorderAlbumPrice=0;
            foreach ($request->editPreorderSelectedSongs as $songId) {
                AlbumSongs::create([
                    'albumId' => $album->id,
                    'songId' => $songId,
                ]);
                $song = Song::find($songId);
                $totalPreorderAlbumPrice = $totalPreorderAlbumPrice + $song->price;
            }

            if($request->deleteAlbumPreorderSelectedSongs){
                foreach($request->deleteAlbumPreorderSelectedSongs as $songId){
                    $song = Song::where('id',$songId)->get();
                    if($song){
                        if ($song->sku){
                            try {
                                $this->deleteInAppPurchasedProduct($song->sku."_wod");
                                $this->deleteInAppPurchasedProduct($song->sku."_wd");
                            } catch (\Exception $exception) {
                                throw new \ErrorException('Fail to delete sku '.$exception->getMessage());
                            }
                        }
                        $albumSongs = AlbumSongs::where('songId',$song->id)->delete();
                        $song->delete();
                    }
                }
            }



            $albumStatus = $this->AlbumStatusCheck($album->id);
            if($albumStatus == 'Paid'){

                $discountedPriceTier = 0;
                $discountedPrice = 0;


                $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$album->iAppPurchaseIdWd.'/pricePoints',[
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                ]);

                if($getPriceTier->failed()){
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());
                    $discount = $request->iAppPurchasePrice * 0.9;
                    foreach($data->data as $i => $inAppPrice){
                        if($i > 0){
                            if($discount < (float) $inAppPrice->attributes->customerPrice){
                                $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                break;
                            }
                        }
                    }
                }

                // Create product in withDiscount inapppurchase
                $name = $request['editPreorderAlbumName'];
                $skuWithDiscount = $album->sku."_wd";
                $costWithDiscount = round($discountedPrice,2);
                $this->updateProductInAppPurchase($costWithDiscount,$name,$skuWithDiscount);

                // Create product in withOutDiscount inapppurchase
                $skuWithOutDiscount = $album->sku."_wod";
                $costWithOutDiscount = round($request->iAppPurchasePrice,2);
                $this->updateProductInAppPurchase($costWithOutDiscount,$name,$skuWithOutDiscount);


                if ($song->iAppPurchaseIdWod != null) {
                    try {
                        $request['name'] = $request->editPreorderAlbumName.' wod';
                        $request['priceTier'] = $request->priceTier;

                        (new InAppPurchaseController)->updateInAppPurchase($request,$album->iAppPurchaseIdWod);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update Apple InAppPurchase ' . $exception->getMessage());
                    }
                }

                //Apple Preorder Album Song Update
                if ($song->iAppPurchaseIdWd != null) {
                    try {
                        $request['name'] = $request->editPreorderAlbumName.' wd';
                        $request['priceTier'] = $discountedPriceTier;

                        (new InAppPurchaseController)->updateInAppPurchase($request,$album->iAppPurchaseIdWd);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update Apple InAppPurchase ' . $exception->getMessage());
                    }
                }

            }


            if($albumStatus){
                Album::where("id",$album->id)->update([
                    'name' => $request->editPreorderAlbumName,
                    'preOrderReleaseDate' => $date,
                    'albumStatus' => $albumStatus,
                    'thumbnail' => $thumbnail,
                    'iAppPurchasePrice' => $request->iAppPurchasePrice ?? '',
                    'discountedPrice' => $discountedPrice,
                ]);
            }
            DB::commit();
            return Album::find($album->id);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to update preorder album'.$exception->getMessage());
        }

    }

    public function preorderAlbumPurchaseList($id)
    {
        try {
            $album = Album::find($id);
            if($album == null){
                throw new \ErrorException('This data is not present in table');
            }
            $album->preOrderReleaseDate = Carbon::parse($album->preOrderReleaseDate)->format('d M Y');
            $albumSongs = AlbumSongs::with('song')->where('albumId',$id)->get();
            $sum = 0;
            foreach ($albumSongs as $key => $albumSong) {
                $song = Song::find($albumSong->songId);
                $sum = $sum + $song->price;
            }
            $purchase = Purchased::with('user')->where('albumId',$id)->get();
            return array($album,$sum,$purchase,$albumSongs);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to show preorder album purchase list '.$exception->getMessage());
        }

    }
}
