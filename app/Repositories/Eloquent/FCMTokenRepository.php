<?php


namespace App\Repositories\Eloquent;


use App\Models\FcmToken;
use App\Repositories\Interfaces\FCMTokenRepositoryInterface;
use Illuminate\Support\Collection;

class FCMTokenRepository extends BaseRepository implements FCMTokenRepositoryInterface
{
    public function __construct(FcmToken $model)
    {
        parent::__construct($model);
    }

    public function destroy(int $id): void
    {
        // TODO: Implement destroy() method.
    }

    public function all(): Collection
    {
        // TODO: Implement all() method.
    }

    public function update(int $id, array $data)
    {
        // TODO: Implement update() method.
    }

    public function updateOrCreate(array $condition, array $data): FcmToken
    {
        return FcmToken::updateOrCreate($condition, $data);
    }

    public function first(): FcmToken
    {
        return FcmToken::first();
    }

    public function inactiveSession(int $userId)
    {
        return FcmToken::where('userId', $userId)->update(['isActive' => 0]);
    }

    public function getUsersFCMRegistrationTokens(array $userIds)
    {
        $FCMRegistrationTokens = FcmToken::select('fcmToken')->where('isActive', 1)->whereIn('userId', $userIds)->get()->toArray();
        return array_column($FCMRegistrationTokens, 'fcmToken');
    }

    public function getUserFCMRegistrationToken(int $userId)
    {
        $FCMRegistrationTokens = FcmToken::select('FcmToken')->where('isActive', 1)->where('userId', $userId)->get()->toArray();
        return array_column($FCMRegistrationTokens, 'FcmToken');
    }
}
