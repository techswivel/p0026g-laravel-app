<?php

namespace App\Repositories\Eloquent;

use App\Models\Song;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Enums\SongPreOrderEnum;
use App\Enums\AlbumPreOrderEnum;
use App\Enums\AlbumStatusEnum;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\InAppPurchaseController;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\PriceController;
use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Traits\FileUploadTrait;
use App\Traits\AlbumTrait;
use App\Traits\InAppPurchaseTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AlbumRepository
{
    use FileUploadTrait, AlbumTrait, InAppPurchaseTrait;

    public function store($request)
    {
        try {

            $cost = 0;
            $sku = null;
            $timestamp = Carbon::now()->timestamp;

            if ($request->stepNo == 1) {
                // Album thumbnail
                $thumbnail = NULL;

                $album = Album::create([
                    'userId' => $request->artistId,
                    'name' => $request->albumName,
                    'thumbnail' => $thumbnail,
                    'type' => AlbumPreOrderEnum::Album,
                    'createdType' => AlbumPreOrderEnum::Album,
                    'createdBy' => auth()->user()->id,
                ]);

                // song title _ artist Id_song Id__timestamp_type
                $sku = str_replace(' ', '_', strtolower($request->albumName)) . "_" . $request->artistId . "_" . $album->id . "_" . $timestamp;


                if (request()->hasFile('albumThumbnail')) {
                    $file = request()->file('albumThumbnail');
                    $thumbnail = $this->albumThumbnail($file, $album->id);
                }


                // List Song of Album
                foreach ($request->selectedSongsName as $songId) {
                    if (AlbumSongs::where('songId', $songId)->exists()) {
                        throw new \ErrorException('This Song is already relate with another album');
                    } else {
                        AlbumSongs::create([
                            'albumId' => $album->id,
                            'songId' => $songId,
                        ]);
                    }
                }
                // Check album is ['Paid','Free','Partially']
                $albumStatus = $this->AlbumStatusCheck($album->id);
                if ($albumStatus) {
                    Album::where("id", $album->id)->update([
                        'albumStatus' => $albumStatus,
                    ]);
                }


                if ($albumStatus == 'Paid') {

                    $cost = Song::whereHas('songs', function ($query) use ($album) {
                        $query->where('albumId', $album->id);
                    })->sum('price');

                    //Apple Store Connect
                    $request['reviewNote'] = 'reviewNote';
                    $request['locale'] = 'en-US';
                    $request['localeName'] = 'English';
                    $request['localeDescription'] = 'English language';
                    $request['promotionIcon'] = new UploadedFile(public_path('appleAppStore/1024.png'), '1024.png', 'image/png', filesize(public_path('appleAppStore/1024.png')), true, TRUE);
                    $request['reviewScreenshot'] = new UploadedFile(public_path('appleAppStore/Simulator-Screenshot.png'), 'Simulator-Screenshot.png', 'image/png', filesize(public_path('appleAppStore/Simulator-Screenshot.png')), true, TRUE);

                    $request['name'] = $request->albumName . ' wd';
                    $request['productId'] = $sku . '_wd';
                    $iAppPurchaseIdWd = (new InAppPurchaseController)->storeInAppPurchase($request);

                    $request['name'] = $request->albumName . ' wod';
                    $request['productId'] = $sku . '_wod';
                    $iAppPurchaseIdWod = (new InAppPurchaseController)->storeInAppPurchase($request);

                    Album::where("id", $album->id)->update([
                        'thumbnail' => $thumbnail,
                        'sku' => $sku,
                        'iAppPurchaseIdWd' => $iAppPurchaseIdWd ?? '',
                        'iAppPurchaseIdWod' => $iAppPurchaseIdWod ?? '',
                        'status' => 'PENDING',
                    ]);

                    $getPriceTier = Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                        ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/' . $iAppPurchaseIdWd . '/pricePoints', [
                            'filter[territory]' => 'USA',
                            'limit' => 1000
                        ]);

                    if ($getPriceTier->failed()) {
                        $errors = json_decode($getPriceTier->body());
                        throw new \ErrorException('Fail to get Apple InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                    }

                    if ($getPriceTier->successful()) {
                        $data = json_decode($getPriceTier->body());
                        $prices = [];

                        foreach ($data->data as $i => $price) {
                            if ($i > 0) {
                                $prices[$i]['priceTier'] = $price->attributes->priceTier;
                                $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                                $prices[$i]['albumId'] = $album->id;
                                $prices[$i]['songsCost'] = $cost;
                            }
                        }

                        return $prices;
                    }
                } else{

                    Album::where("id", $album->id)->update([
                        'thumbnail' => $thumbnail ?? '',
                        'albumStatus' => $albumStatus,
                        'sku' => $sku,
                        'status' => 'COMPLETED',
                    ]);

                    return $album;
                }

            } elseif ($request->stepNo == 2) {

                $album = Album::find($request->albumId);

                if ($album->albumStatus == 'Paid') {

                    $discountedPriceTier = 0;
                    $discountedPrice = 0;

                    $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                        ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/' . $album->iAppPurchaseIdWd . '/pricePoints', [
                            'filter[territory]' => 'USA',
                            'limit' => 1000
                        ]);

                    if ($getPriceTier->failed()) {
                        $errors = json_decode($getPriceTier->body());
                        throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                    }

                    if ($getPriceTier->successful()) {
                        $data = json_decode($getPriceTier->body());
                        $discount = $request->iAppPurchasePrice * 0.9;
                        foreach ($data->data as $i => $inAppPrice) {
                            if ($i > 0) {
                                if ($discount < (float) $inAppPrice->attributes->customerPrice) {
                                    $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                    $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                    break;
                                }
                            }
                        }
                    }


                    if ($album->name != $request->albumName) {

                        $request['name'] = $request->albumName . ' wod';
                        (new InAppPurchaseController)->updateInAppPurchaseBasic($request, $album->iAppPurchaseIdWod);

                        $request['name'] = $request->albumName . ' wd';
                        (new InAppPurchaseController)->updateInAppPurchaseBasic($request, $album->iAppPurchaseIdWd);
                         //this method check if android inApp Exists already then update if not then in
                        //CATCH Excepetion: we create new inApp with already saved SKU from Model
                        try{

                            $this->getInAppPurchasedProduct($album->sku);
                        }catch(Exception $e){

                            // Create product in withOutDiscount inapppurchase
                            $skuWithOutDiscount = $album->sku . "_wod";
                            $costWithOutDiscount = round($request->iAppPurchasePrice, 2);
                            $this->createProductInAppPurchase($costWithOutDiscount, $album->name, $skuWithOutDiscount);
                            (new PriceController)->storeInAppPurchasePrice($request, $album->iAppPurchaseIdWod);

                            // Create product in withDiscount inapppurchase
                            $request['priceTier'] = $discountedPriceTier;
                            $skuWithDiscount = $album->sku . "_wd";
                            $costWithDiscount = round($discountedPrice, 2);
                            $this->createProductInAppPurchase($costWithDiscount, $album->name, $skuWithDiscount);
                            (new PriceController)->storeInAppPurchasePrice($request, $album->iAppPurchaseIdWd);
                        }
                    }else{
                            // Create product in withOutDiscount inapppurchase
                            $skuWithOutDiscount = $album->sku . "_wod";
                            $costWithOutDiscount = round($request->iAppPurchasePrice, 2);
                            $this->createProductInAppPurchase($costWithOutDiscount, $album->name, $skuWithOutDiscount);
                            (new PriceController)->storeInAppPurchasePrice($request, $album->iAppPurchaseIdWod);

                            // Create product in withDiscount inapppurchase
                            $request['priceTier'] = $discountedPriceTier;
                            $skuWithDiscount = $album->sku . "_wd";
                            $costWithDiscount = round($discountedPrice, 2);
                            $this->createProductInAppPurchase($costWithDiscount, $album->name, $skuWithDiscount);
                            (new PriceController)->storeInAppPurchasePrice($request, $album->iAppPurchaseIdWd);

                    }

                    Album::where("id", $album->id)->update([
                        'iAppPurchasePrice' => $request->iAppPurchasePrice,
                        'discountedPrice' => $discountedPrice,
                        'status' => 'COMPLETED',
                    ]);

                }

                Album::where("id", $album->id)->update([
                    'name' => $request->albumName,
                ]);

            }


            return $album;
        } catch (\Exception $exception) {
            throw new \ErrorException($exception->getMessage());
        }
    }

    public function selectSong($request)
    {
        try {
            return Song::find($request['checkbox']);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to show select song ' . $exception->getMessage());
        }
    }

    public function albumDetail($id)
    {
        try {
            $album = Album::find($id);
            if ($album == null) {
                throw new \ErrorException('This data is not present in table');
            }
            $albumSongs = AlbumSongs::where('albumId', $id)->get('songId');
            $count = count($albumSongs);
            //list the all song of the album in albumDetail
            $songs = Song::find($albumSongs);
            return array($album, $songs, $count);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to show album detail ' . $exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $album = Album::find($id);
            if ($album == null) {
                throw new \ErrorException('This data is not present in table');
            }
            Album::where('id', $id)->update([
                'deletedBy' => auth()->user()->id,
            ]);
            $albumSongs = AlbumSongs::where('albumId', $id)->get();
            // First delete all the song relation and then delete
            foreach ($albumSongs as $song) {
                $song->delete();
            }

            $album->delete();
            if ($album->sku != null) {
                try {
                    $this->deleteInAppPurchasedProduct($album->sku . "_wod");
                    $this->deleteInAppPurchasedProduct($album->sku . "_wd");
                } catch (\Exception $exception) {
                    throw new \ErrorException('Fail to delete InAppPurchaseId ' . $exception->getMessage());
                }
            }

            //Apple App Store Connect
            if ($album->iAppPurchaseIdWd != null) {
                try {
                    (new InAppPurchaseController)->deleteInAppPurchase($album->iAppPurchaseIdWd);
                } catch (\Exception $exception) {
                    throw new \ErrorException('Fail to delete InAppPurchaseId ' . $exception->getMessage());
                }
            }

            if ($album->iAppPurchaseIdWod != null) {
                try {
                    (new InAppPurchaseController)->deleteInAppPurchase($album->iAppPurchaseIdWod);
                } catch (\Exception $exception) {
                    throw new \ErrorException('Fail to delete InAppPurchaseId ' . $exception->getMessage());
                }
            }
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to delete album ' . $exception->getMessage());
        }
    }

    public function listTheSong($id)
    {
        try {
            // List all song in the song List to select song
            return Song::where('userId', $id)->where('type', SongPreOrderEnum::Song)->doesntHave("songs")->get();
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to show list the song ' . $exception->getMessage());
        }
    }

    public function removeSong($id)
    {
        try {
            DB::beginTransaction();
            $albumSong = AlbumSongs::where('songId', $id)->first();
            $albumSong->delete();
            $timestamp = Carbon::now()->timestamp;
            $albumStatus = $this->AlbumStatusCheck($albumSong->albumId);

            $album = Album::find($albumSong->albumId);
            $currentSku = null;
            if ($album) {
                if ($album->sku != null) {
                    try {
                        $this->deleteInAppPurchasedProduct($album->sku . "_wod");
                        $this->deleteInAppPurchasedProduct($album->sku . "_wd");
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to delete sku ' . $exception->getMessage());
                    }
                }
                if ($albumStatus == 'Paid') {
                    // Create product in inapppurchase
                    $cost = Song::whereHas('songs', function ($query) use ($album) {
                        $query->where('albumId', $album->id);
                    })->sum('price');
                    $currentSku = str_replace(' ', '_', strtolower($album->name)) . "_" . $album->userId . "_" . $album->id . "_" . $timestamp;
                    // Create product in withDiscount inapppurchase
                    $skuWithDiscount = $currentSku . "_wd";
                    $costWithDiscount = round($cost, 2);
                    $this->createProductInAppPurchase($costWithDiscount, $album->name, $skuWithDiscount);
                    // Create product in withOutDiscount inapppurchase
                    $skuWithOutDiscount = $currentSku . "_wod";
                    $discount = ($cost * 10) / 100;
                    $costWithOutDiscount = round($cost + $discount, 2);
                    $this->createProductInAppPurchase($costWithOutDiscount, $album->name, $skuWithOutDiscount);
                }
                if ($albumStatus) {
                    Album::where("id", $albumSong->albumId)->update([
                        'albumStatus' => $albumStatus,
                        'sku' => $currentSku
                    ]);
                }
            }
            DB::commit();
            return $album;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to Remove song ' . $exception->getMessage());
        }
    }

    public function show($id)
    {
        try {
            $album = Album::find($id);
            if ($album == null) {
                throw new \ErrorException('This data is not present in table');
            }
            $albumSongs = AlbumSongs::where('albumId', $id)->get('songId');
            $songs = Song::find($albumSongs);

            $cost = Song::whereHas('songs', function ($query) use ($album) {
                $query->where('albumId', $album->id);
            })->sum('price');

            if($album->iAppPurchaseIdWd != null){
                $getPriceTier = Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/' . $album->iAppPurchaseIdWd . '/pricePoints', [
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                    ]);

                if ($getPriceTier->failed()) {
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if ($getPriceTier->successful()) {
                    $data = json_decode($getPriceTier->body());
                    $prices = [];

                    foreach ($data->data as $i => $price) {
                        if ($i > 0) {
                            $prices[$i]['priceTier'] = $price->attributes->priceTier;
                            $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                            $prices[$i]['albumId'] = $album->id;
                            $prices[$i]['songsCost'] = $cost;
                        }
                    }

                    $album->prices = $prices;
                }
            }

            return array($album, $songs);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to show Album ' . $exception->getMessage());
        }
    }

    public function update($request)
    {
        try {
            DB::beginTransaction();
            $album = Album::find($request->albumId);
            $thumbnail = $album->thumbnail;
            $timestamp = Carbon::now()->timestamp;

            if (request()->hasFile('editAlbumThumbnail')) {
                $this->deleteFile($thumbnail);
                $file = request()->file('editAlbumThumbnail');
                $thumbnail = $this->albumThumbnail($file, $album->id);
            }

            // update the album
            $AlbumSong = AlbumSongs::where('albumId', $request->albumId)->delete();
            foreach ($request->editSelectedSongsName as $songId) {
                if (AlbumSongs::where('songId', $songId)->exists()) {
                    throw new \ErrorException('This Song is already relate with another album');
                } else {
                    AlbumSongs::create([
                        'albumId' => $request->albumId,
                        'songId' => $songId,
                    ]);
                }
            }

            $albumStatus = $this->AlbumStatusCheck($request->albumId);
            if ($albumStatus) {
                Album::where("id", $request->albumId)->update([
                    'albumStatus' => $albumStatus,
                ]);
            }

            if ($albumStatus == 'Paid') {

                // Create product in inapppurchase
                $name = $request->editAlbumName;
                $cost = Song::whereHas('songs', function ($query) use ($album) {
                    $query->where('albumId', $album->id);
                })->sum('price');


                $discountedPriceTier = 0;
                $discountedPrice = 0;


                $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json', 'Accept' => 'application/a-gzip, application/json', 'Authorization' => 'Bearer ' . JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/' . $album->iAppPurchaseIdWd . '/pricePoints', [
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                    ]);


                if ($getPriceTier->failed()) {
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if ($getPriceTier->successful()) {
                    $data = json_decode($getPriceTier->body());
                    $discount = $request->iAppPurchasePrice * 0.9;
                    foreach ($data->data as $i => $inAppPrice) {
                        if ($i > 0) {
                            if ($discount < (float) $inAppPrice->attributes->customerPrice) {
                                $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                break;
                            }
                        }
                    }
                }


                if($album->status != 'PENDING'){

                    // Create product in withOutDiscount inapppurchase
                    $skuWithOutDiscount = $album->sku . "_wod";
                    $costWithOutDiscount = round($request->iAppPurchasePrice, 2);
                    $this->updateProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);

                    // Create product in withDiscount inapppurchase
                    $skuWithDiscount = $album->sku . "_wd";
                    $costWithDiscount = round($discountedPrice, 2);
                    $this->updateProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);
                }else{

                    // Create product in withOutDiscount inapppurchase
                    $skuWithOutDiscount = $album->sku . "_wod";
                    $costWithOutDiscount = round($request->iAppPurchasePrice, 2);
                    $this->createProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);

                    // Create product in withDiscount inapppurchase
                    $skuWithDiscount = $album->sku . "_wd";
                    $costWithDiscount = round($discountedPrice, 2);
                    $this->createProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);
                }

                if ($album->iAppPurchaseIdWod != null) {
                    try {
                        $request['name'] = $request->editAlbumName . ' wod';
                        $request['priceTier'] = $request->priceTier;

                        (new InAppPurchaseController)->updateInAppPurchase($request, $album->iAppPurchaseIdWod);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update Apple InAppPurchase ' . $exception->getMessage());
                    }
                }

                if ($album->iAppPurchaseIdWd != null) {
                    try {
                        $request['name'] = $request->editAlbumName . ' wd';
                        $request['priceTier'] = $discountedPriceTier;

                        (new InAppPurchaseController)->updateInAppPurchase($request, $album->iAppPurchaseIdWd);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update Apple InAppPurchase ' . $exception->getMessage());
                    }
                }


                Album::where("id", $request->albumId)->update([
                    'iAppPurchasePrice' => $request->iAppPurchasePrice ?? '',
                    'discountedPrice' => $discountedPrice ?? 0,
                ]);
            }

            Album::where("id", $request->albumId)->update([
                'name' => $request->editAlbumName,
                'thumbnail' => $thumbnail,
                'status' => 'COMPLETED',
            ]);

            DB::commit();
            return Album::find($request->albumId);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to update Albun ' . $exception->getMessage());
        }
    }

    public function AlbumStatus($id)
    {
        try {
            $albumSongs = AlbumSongs::where('songId', $id)->first();
            $albumStatus = $this->AlbumStatusCheck($albumSongs->albumId);
            if ($albumStatus) {
                Album::where("id", $albumSongs->albumId)->update([
                    'albumStatus' => $albumStatus,
                ]);
            }
            return Album::find($albumSongs->albumId);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to set Albun status ' . $exception->getMessage());
        }
    }
}
