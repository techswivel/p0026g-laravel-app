<?php

namespace App\Repositories\Eloquent;

use App\Http\Requests\Artist\Notification\NotificationAddRequest;
use App\Models\Notification;
use App\Models\MonthlyNotificationCount;
use App\Enums\NotificationStatusEnum;
use App\Enums\NotificationTypeEnum;
use App\Enums\UserTypeNotificationEnum;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\User;

class NotificationRepository
{

    public function store($request)
    {
        try {
            DB::beginTransaction();
            $date = Carbon::now()->startofMonth();
            $user = User::find($request->artistId);
            $notification = Notification::where('userId',$request->artistId)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->count();
            $monthlyNotificationCount = MonthlyNotificationCount::first();
            if(is_null($monthlyNotificationCount)){
                $notification = Notification::create([
                    'userId' => $user->id,
                    'title' => $request->notificationTitle,
                    'message' => $request->notificationMessage,
                    'notificationType' => NotificationTypeEnum::Artist,
                   'status' => NotificationStatusEnum::Pending,
                ]);
                DB::commit();
                return $notification;
            }else{
                if(($notification < $monthlyNotificationCount->totalNotificationCount) and ($user->isNotificationEnabled == 1)){
                $notification = Notification::create([
                    'userId' => $user->id,
                    'title' => $request->notificationTitle,
                    'message' => $request->notificationMessage,
                    'notificationType' => NotificationTypeEnum::Artist,
                   'status' => NotificationStatusEnum::Pending,
                ]);
                DB::commit();
                return $notification;
                }
                else{
                    throw new \ErrorException('Notification limit reached');
                }
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to store notification '.$exception->getMessage());   
        }
        
    }

    public function show($id)
    {
        try {
            return Notification::find($id);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to show notification '.$exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $notification = Notification::where("id",$id);
            if($notification){
                Notification::where("id",$id)->update([
                    'status' => NotificationStatusEnum::DeletedByArtist,
                ]);
            $notification->delete();
            DB::commit();
        }
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to delete notification '.$exception->getMessage());   
        }
        
    }
}
