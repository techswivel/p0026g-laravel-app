<?php

namespace App\Repositories\Eloquent;

use App\Models\Song;
use App\Models\Album;
use App\Models\Purchased;
use App\Traits\SongTrait;
use App\Traits\FileUploadTrait;
use App\Enums\SongPreOrderEnum;
use App\Enums\AlbumPreOrderEnum;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\InAppPurchaseController;
use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\PriceController;
use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use App\Jobs\CheckPreorderSongJob;
use App\Traits\InAppPurchaseTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SongPreorderRepository
{
    use SongTrait, FileUploadTrait, InAppPurchaseTrait;

    public function store($request)
    {
        try {

            $timestamp = Carbon::now()->timestamp;
            $date = Carbon::createFromDate($request['preorderSongDate'] . " " . $request['preorderSongTime'])->timestamp;

            // Store preorder Song Information
            if($request->songId == null){
                $song = Song::create([
                    'userId' => $request->artistId,
                    'name' => $request['preorderSongName'],
                    'isPaid' => TRUE,
                    'categoryId' => $request['preorderSongCategory'],
                    'languageId' => $request['preorderSongLanguage'],
                    'type' => SongPreOrderEnum::PreOrder,
                    'createdType' => SongPreOrderEnum::PreOrder,
                    'createdBy' => auth()->user()->id,
                ]);

                $sku = str_replace(' ', '_', strtolower($request->preorderSongName)) . "_" . $request->artistId . "_" . $song->id . "_" . $timestamp;

                $request['reviewNote'] = 'reviewNote';
                $request['locale'] = 'en-US';
                $request['localeName'] = 'English';
                $request['localeDescription'] = 'English language';
                $request['promotionIcon'] = new UploadedFile(public_path('appleAppStore/1024.png'),'1024.png','image/png',filesize(public_path('appleAppStore/1024.png')),true,TRUE);
                $request['reviewScreenshot'] = new UploadedFile(public_path('appleAppStore/Simulator-Screenshot.png'),'Simulator-Screenshot.png','image/png',filesize(public_path('appleAppStore/Simulator-Screenshot.png')),true,TRUE);

                $request['name'] = $request->preorderSongName.' wd';
                $request['productId'] = $sku.'_wd';
                $iAppPurchaseIdWd = (new InAppPurchaseController)->storeInAppPurchase($request);

                $request['name'] = $request->preorderSongName.' wod';
                $request['productId'] = $sku.'_wod';
                $iAppPurchaseIdWod = (new InAppPurchaseController)->storeInAppPurchase($request);


                Song::where("id", $song->id)->update([
                    'sku' => $sku,
                    'iAppPurchaseIdWd' => $iAppPurchaseIdWd,
                    'iAppPurchaseIdWod' => $iAppPurchaseIdWod,
                    'status' => 'PENDING',
                ]);

                $getPriceTier = Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$iAppPurchaseIdWd.'/pricePoints',[
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                ]);

                if($getPriceTier->failed()){
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());
                    $prices = [];

                    foreach($data->data as $i => $price){
                        if($i > 0){
                            $prices[$i]['priceTier'] = $price->attributes->priceTier;
                            $prices[$i]['customerPrice'] = $price->attributes->customerPrice;
                            $prices[$i]['songId'] = $song->id;
                        }
                    }

                    return $prices;
                }
            }else{
                $song = Song::find($request->songId);
            }

            if($request->stepNo == 2){
                // Song thumbnail
                $thumbnail = NULL;
                if (request()->hasFile('preorderSongThumbnail')) {
                    $file = request()->file('preorderSongThumbnail');
                    $thumbnail = $this->songThumbnail($file, $song->id);
                }

                // Song Audio
                $songAudio = NULL;
                $songAudioDemo = NULL;
                $songLength = 0;
                if (request()->hasFile('preorderAudioFile')) {
                    $file = request()->file('preorderAudioFile');
                    $songLength  = $this->songLength($file);
                    $songs = $this->songAudio($file, $song->id, $request->preorderSongName, TRUE);
                    $songAudio = $songs[0];
                    $songAudioDemo = $songs[1];
                }

                // Song Video
                $songVideo = NULL;
                $songVideoDemo = NULL;
                if (request()->hasFile('preorderVideoFile')) {
                    $file = request()->file('preorderVideoFile');
                    $videos = $this->songVideo($file, $song->id, $request->preorderSongName, TRUE);
                    $songVideo = $videos[0];
                    $songVideoDemo = $videos[1];
                }


                $price = $request['preorderSongPrice'];

                $discountedPriceTier = 0;
                $discountedPrice = 0;

                $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$song->iAppPurchaseIdWd.'/pricePoints',[
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                ]);

                if($getPriceTier->failed()){
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());
                    $discount = $request->preorderSongPrice * 0.9;
                    foreach($data->data as $i => $inAppPrice){
                        if($i > 0){
                            if($discount < (float) $inAppPrice->attributes->customerPrice){
                                $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                break;
                            }
                        }
                    }
                }


                Song::where("id", $song->id)->update([
                    'name' => $request['preorderSongName'],
                    'isPaid' => TRUE,
                    'categoryId' => $request['preorderSongCategory'],
                    'languageId' => $request['preorderSongLanguage'],
                    'type' => SongPreOrderEnum::PreOrder,
                    'thumbnail' => $thumbnail,
                    'audioFile' => $songAudio,
                    'songLength' => $songLength,
                    'demoAudioFile' => $songAudioDemo,
                    'videoFile' => $songVideo,
                    'demoVideoFile' => $songVideoDemo,
                    'price' => $request['preorderSongPrice'],
                    'lyrics' => $request['preorderSongLyrics'],
                    'preOrderReleaseDate' => $date,
                    'preOrderCount' => 0,
                    'totalPreOrderCount' => 100,
                    'discountedPrice' => $discountedPrice,
                    'status' => 'COMPLETED',
                ]);

                $name = $request['preorderSongName'];

                if($song->name != $request->preorderSongName){

                    // Create product in withDiscount inapppurchase
                    $request['name'] = $name.' wod';
                    (new InAppPurchaseController)->updateInAppPurchaseBasic($request,$song->iAppPurchaseIdWod);

                    $request['name'] = $name.' wd';
                    (new InAppPurchaseController)->updateInAppPurchaseBasic($request,$song->iAppPurchaseIdWd);

                    //this method check if android inApp Exists already then update if not then in
                    //CATCH Excepetion: we create new inApp with already saved SKU from Model
                    try{

                        $this->getInAppPurchasedProduct($song->sku);
                    }catch(Exception $e){
                        // Create product in withOutDiscount inapppurchase
                        $skuWithOutDiscount = $song->sku.'_wod';
                        $costWithOutDiscount = round($request->preorderSongPrice, 2);
                        $this->createProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);
                        (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWod);

                        // Create product in withDiscount inapppurchase
                        $request['priceTier'] = $discountedPriceTier;
                        $skuWithDiscount = $song->sku.'_wd';
                        $costWithDiscount = round($discountedPrice, 2);
                        $this->createProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);
                        (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWd);


                    }

                }else{

                    // Create product in withOutDiscount inapppurchase
                    $request['name'] = $name.' wod';
                    $skuWithOutDiscount = $song->sku.'_wod';
                    $costWithOutDiscount = round($request->preorderSongPrice, 2);
                    $this->createProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);
                    (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWod);

                    // Create product in withDiscount inapppurchase
                    $request['name'] = $name.' wd';
                    $request['priceTier'] = $discountedPriceTier;
                    $skuWithDiscount = $song->sku.'_wd';
                    $costWithDiscount = round($discountedPrice, 2);
                    $this->createProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);
                    (new PriceController)->storeInAppPurchasePrice($request, $song->iAppPurchaseIdWd);


                }


                dispatch(new CheckPreorderSongJob())->delay(Carbon::parse($date));

                return Song::find($song->id);
            }
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to stored preorder song ' . $exception->getMessage());
        }
    }

    public function update($request)
    {
        try {

            DB::beginTransaction();

            $timestamp = Carbon::now()->timestamp;
            $song = Song::find($request->preorderSongId);

            if ($song) {
                // Song Thumbnail
                $thumbnail = $song->thumbnail;
                if (request()->hasFile('editPreorderSongThumbnail')) {
                    $this->deleteFile($song->thumbnail);
                    $file = request()->file('editPreorderSongThumbnail');
                    $thumbnail = $this->songThumbnail($file, $song->id);
                }

                // Song Audio
                $songAudio = $song->audioFile;
                $songAudioDemo =  $song->demoAudioFile;
                $songLength = $song->songLength;
                if (request()->hasFile('editPreorderAudioFile')) {
                    $this->deleteFile($song->audioFile);
                    $this->deleteFile($song->demoAudioFile);
                    $file = request()->file('editPreorderAudioFile');
                    $songLength  = $this->songLength($file);
                    $songs = $this->songAudio($file, $song->id, $request->editSongName, TRUE);
                    $songAudio = $songs[0];
                    $songAudioDemo = $songs[1];
                }

                // Song Video
                $songVideo = $song->videoFile;
                $songVideoDemo = $song->demoVideoFile;
                if (request()->hasFile('editPreorderVideoFile')) {
                    $file = request()->file('editPreorderVideoFile');
                    $videos = $this->songVideo($file, $song->id, $request->editSongName, TRUE);
                    $songVideo =  $videos[0];
                    $songVideoDemo = $videos[1];
                }

                if (request()->hasFile('editPreorderVideoFile1')) {
                    $this->deleteFile($song->videoFile);
                    $this->deleteFile($song->demoVideoFile);
                    $file = request()->file('editPreorderVideoFile1');
                    $videos = $this->songVideo($file, $song->id, $request->editSongName, TRUE);
                    $songVideo =  $videos[0];
                    $songVideoDemo = $videos[1];
                }

                $date = Carbon::createFromDate($request['editPreorderSongDate'] . ' .' . $request['editPreorderSongTime'])->timestamp;

                $discountedPriceTier = 0;
                $discountedPrice = 0;


                $getPriceTier =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. JwtTokenController::getJwtToken()])
                    ->get('https://api.appstoreconnect.apple.com/v2/inAppPurchases/'.$song->iAppPurchaseIdWd.'/pricePoints',[
                        'filter[territory]' => 'USA',
                        'limit' => 1000
                ]);

                if($getPriceTier->failed()){
                    $errors = json_decode($getPriceTier->body());
                    throw new \ErrorException('Fail to get InAppPurchase Price List ' . implode(' | ', Arr::pluck($errors->errors, 'detail')));
                }

                if($getPriceTier->successful()){
                    $data = json_decode($getPriceTier->body());
                    $discount = $request->editPreorderSongPrice * 0.9;
                    foreach($data->data as $i => $inAppPrice){
                        if($i > 0){
                            if($discount < (float) $inAppPrice->attributes->customerPrice){
                                $discountedPriceTier = (int) $data->data[$i]->attributes->priceTier;
                                $discountedPrice = (float) $data->data[$i]->attributes->customerPrice;
                                break;
                            }
                        }
                    }
                }


                $name = $request['editPreorderSongName'];

                $skuWithDiscount = $song->sku.'_wd';
                $costWithDiscount = round($discountedPrice, 2);

                $skuWithOutDiscount = $song->sku.'_wod';
                $costWithOutDiscount = round($request->editPreorderSongPrice, 2);

                if($song->status != 'PENDING'){
                     // Create product in withDiscount inapppurchase
                    $this->updateProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);

                    // Create product in withOutDiscount inapppurchase
                    $this->updateProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);
                }else{
                    // Create product in withDiscount inapppurchase
                    $this->createProductInAppPurchase($costWithDiscount, $name, $skuWithDiscount);

                    // Create product in withOutDiscount inapppurchase
                    $this->createProductInAppPurchase($costWithOutDiscount, $name, $skuWithOutDiscount);
                }


                if ($song->iAppPurchaseIdWod != null) {
                    try {
                        $request['name'] = $request->editPreorderSongName.' wod';
                        $request['priceTier'] = $request->priceTier;

                        (new InAppPurchaseController)->updateInAppPurchase($request,$song->iAppPurchaseIdWod);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update Apple InAppPurchase ' . $exception->getMessage());
                    }
                }

                if ($song->iAppPurchaseIdWd != null) {
                    try {
                        $request['name'] = $request->editPreorderSongName.' wd';
                        $request['priceTier'] = $discountedPriceTier;

                        (new InAppPurchaseController)->updateInAppPurchase($request, $song->iAppPurchaseIdWd);
                    } catch (\Exception $exception) {
                        throw new \ErrorException('Fail to Update Apple InAppPurchase ' . $exception->getMessage());
                    }
                }


                Song::where("id", $song->id)->update([
                    'name' => $request->editPreorderSongName,
                    'isPaid' => TRUE,
                    'price' => $request->editPreorderSongPrice,
                    'categoryId' => $request->editPreorderSongCategory,
                    'languageId' => $request->editPreorderSongLanguage,
                    'thumbnail' => $thumbnail,
                    'audioFile' =>  $songAudio,
                    'demoAudioFile' => $songAudioDemo,
                    'songLength' => $songLength,
                    'videoFile' => $songVideo,
                    'demoVideoFile' => $songVideoDemo,
                    'lyrics' => $request->editPreorderSongLyrics,
                    'preOrderReleaseDate' => $date,
                    'discountedPrice' => $discountedPrice ?? 0,
                    'status' => 'COMPLETED',
                ]);
            }


            DB::commit();
            return Song::find($request->id);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \ErrorException('Fail to update preorder song ' . $exception->getMessage());
        }
    }

    public function preorderSongPurchaseList($id)
    {
        try {
            $song = Song::find($id);
            if ($song == null) {
                throw new \ErrorException('This data is not present in table');
            }
            $song->preOrderReleaseDate = Carbon::parse($song->preOrderReleaseDate)->format('d M Y');
            $purchase = Purchased::with('user')->where('songId', $id)->get();
            return array($song, $purchase);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to show purchase list ' . $exception->getMessage());
        }
    }

    public function carbonDate($request, $id)
    {
        try {
            $preorderSongs = Song::where('userId', $id)->where('type', SongPreOrderEnum::PreOrder)->get();
            $preorderAlbums = Album::where('userId', $id)->where('type', AlbumPreOrderEnum::PreOrder)->whereNotNull('preOrderReleaseDate')->get();
            foreach ($preorderSongs as $preorderSong) {
                $preorderSong->preOrderReleaseDate = Carbon::parse($preorderSong->preOrderReleaseDate)->format('M/d/Y g:i A');
            }
            foreach ($preorderAlbums as $preorderAlbum) {
                $preorderAlbum->preOrderReleaseDate = Carbon::parse($preorderAlbum->preOrderReleaseDate)->format('M/d/Y g:i A');
            }
            return array($preorderSongs, $preorderAlbums);
        } catch (\Exception $exception) {
            throw new \ErrorException('Fail to show preorder date ' . $exception->getMessage());
        }
    }
}
