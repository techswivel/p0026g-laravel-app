<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use App\Models\Song;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Enums\SongPreOrderEnum;
use App\Enums\AlbumPreOrderEnum;
use Kreait\Firebase\Factory;
use App\Http\Resources\Album\AlbumForNotificationResource;
use App\Http\Resources\Song\SongForNotificationResource;
use App\Models\FcmToken;
use App\Models\Purchased;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CheckPreorderSongJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $factory = (new Factory())->withServiceAccount(base_path(env('FIREBASE_CREDENTIALS')));

            $songs = Song::with('purchase')->where('type', SongPreOrderEnum::PreOrder)->get();

            foreach ($songs as $song) {

                Song::where('id', $song->id)->update([
                    'type' => SongPreOrderEnum::Song,
                ]);

                $title = 'Song Pre-Order';
                $data = [
                    'title' => 'Song Pre-Order',
                    'body' => 'Song Pre-Order Released.',
                    'type' => 'PRE_ORDER_SONG',
                    'notificationType' => 'RELEASED',
                    'notification' => json_encode(new SongForNotificationResource($song))
                ];
                $topic = $song->sku;
                $body = 'Song Pre-Order Released.';

                if ($topic != null) {

                    $userIds = Purchased::where('songId',$song->id)->pluck('userId')->toArray();
                    $fcmTokens = FcmToken::whereIn('userId', $userIds)->get();
                    Log::info('Firebase FCM Tokens', [$fcmTokens]);
                    foreach ($fcmTokens as $token) {
                        $messaging = $factory->createMessaging();
                        $notification = Notification::create($title, $body, '/');
                        $message = CloudMessage::withTarget('token', $token->token)
                            ->withNotification($notification)->withData($data)
                            ->withDefaultSounds();
                        $message = $messaging->send($message);
                    }
                }
            }
        } catch (\Exception $e) {
            Log::alert($e);
        }
    }
}
