<?php

namespace App\Jobs;

use App\Http\Controllers\Api\AppleStoreConnect\Subscription\SubscriptionController;
use App\Http\Controllers\Api\AppleStoreConnect\Subscription\SubscriptionPriceController;
use App\Models\Package;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\Api\AppleStoreConnect\JwtTokenController;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class AppleInAppSubscription implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $request, $package, $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request,$package,$token)
    {
         $this->request = $request;
         $this->package = $package;
         $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try{
            $subscriptionPriceEq =  Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. $this->token])
                ->get('https://api.appstoreconnect.apple.com/v1/subscriptionPricePoints/'.$this->request['priceTier'].'/equalizations?include=territory&limit=1000');

            if($subscriptionPriceEq->successful()){

                $priceEq = json_decode($subscriptionPriceEq->body());

                $priceEqData = $priceEq->data;
                $prices = Arr::pluck($priceEqData,'id');
                $prices[] = $this->request['priceTier'];

                foreach($prices as $price){
                    Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '.$this->token])
                    ->post('https://api.appstoreconnect.apple.com/v1/subscriptionPrices',[
                        'data' => [
                            'type' => 'subscriptionPrices',
                            'attributes' => [
                                'preserveCurrentPrice' => true,
                                'startDate' => Carbon::now()->format('Y-m-d'),
                            ],
                            'relationships' => [
                                'subscription' => [
                                    'data' => [
                                        'id' =>  $this->package->subscriptionId,
                                        'type' => 'subscriptions',
                                    ]
                                ],
                                'subscriptionPricePoint' => [
                                    'data' => [
                                        'id' => $price,
                                        'type' => 'subscriptionPricePoints',
                                    ]
                                ],
                            ],

                        ],
                    ]);

                    sleep(1.5);
                }

                dispatch(new AppleInAppSubscriptionUpdate($this->request, $this->package, $this->token));
            }

            if($subscriptionPriceEq->failed()){
                Package::find($this->package->id)->update([
                    "status" => "Failed",
                ]);
            }
        }catch(Exception $e){
            Package::find($this->package->id)->update([
                "status" => "Failed",
            ]);

            Log::error("AppleInApp Subscription ",['Exception' => $e->getMessage()]);
        }
    }

    public function failed($exception)
    {
        Package::find($this->package->id)->update([
            "status" => "Failed",
        ]);

        Log::error("AppleInApp Subscription ",['Exception' => $exception->getMessage()]);

    }
}
