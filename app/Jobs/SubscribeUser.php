<?php


namespace App\Jobs;

use App\Enums\PurchasedItemTypeEnum;
use App\Http\Resources\User\SubscriptionResource;
use App\Models\Package;
use App\Models\Purchased;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Services\FcmTokenService;
use App\Services\FirebaseService;
use Illuminate\Support\Facades\Log;

class SubscribeUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $notification_type;
    private $purchase_token;
    private $type;

    public function __construct($notification_type, $purchase_token, $type)
    {
        $this->notification_type = $notification_type;
        $this->purchase_token = $purchase_token;
        $this->type = $type;
    }

    public function handle()
    {
        if($this->type == 'ONETIMEPRODUCT'){
            $purchasedOneTimeProduct = Purchased::where('transactionId',$this->purchase_token)->first();
            if(!$purchasedOneTimeProduct){
                return false;
            }
            $userObject = $user = User::where('id',$purchasedOneTimeProduct->userId)->first();
    
            if ($user) {
                if ($this->notification_type == 1) { // ONE_TIME_PRODUCT_PURCHASED
                    if($purchasedOneTimeProduct){
                        $purchasedOneTimeProduct->update([
                            'purchaseStatus' => "Purchased",
                            'isAcknowledged'  => "Acknowledged"
                        ]);
                    }
                    $messageBody = "purchased";
                    $subscriptionStatus = "ONE_TIME_PRODUCT_PURCHASED";
                }
                if ($this->notification_type == 2) { // ONE_TIME_PRODUCT_CANCELED
                    if ($purchasedOneTimeProduct) {
                        $purchasedOneTimeProduct->update([
                            'purchaseStatus' => "Canceled",
                            'isAcknowledged'  => "Not Acknowledged"
                        ]);
                    }
                    $messageBody = "canceled";
                    $subscriptionStatus = "ONE_TIME_PRODUCT_CANCELED";
                }
            }
            $songAblum = Purchased::where('transactionId',$this->purchase_token)->first();
            $itemType = null;
            if($songAblum){
                if($songAblum->itemType == PurchasedItemTypeEnum::SONG){
                    $itemType = 'Song';
                }
                if($songAblum->itemType == PurchasedItemTypeEnum::ALBUM){
                    $itemType = 'Album';
                }
                if($songAblum->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG){
                    $itemType = 'Preorder song';
                }
                if($songAblum->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM){
                    $itemType = 'Preorder album';
                }
            }
            $fcm['title'] = $itemType;
            $fcm['body'] = $itemType ." ". $messageBody;
            $fcm['data'] = [
                'title' => $itemType,
                'body' => $itemType ." ". $messageBody,
                'type' => $songAblum->itemType,
                "notificationType" => $subscriptionStatus,
                "subscriptionCode" => $this->notification_type,
                'notification' => json_encode(new SubscriptionResource($songAblum))
            ];
        }
        if($this->type == 'SUBSCRIPTION'){
            $subscribe_codes = [1, 2, 4, 7];
            $unsubscribe_codes = [3, 5, 10, 12, 13];

            $userObject = $user = User::where('gplay_order_token', $this->purchase_token)
                ->first();
            if(!$user){
                return false;
            }
            $purchasedSubscription = Purchased::where('userId',$user->id)->where('transactionId',$this->purchase_token)->first();
            if(!$purchasedSubscription){
                return false;
            }
            $message = null;

            if ($user) {
                if (in_array($this->notification_type, $subscribe_codes)) { // Subscribed

                    $gplayData = [
                        'datetime_subscribed' => now(),
                        'lastpayment_datetime' => now()->toDateTimeString(),
                        'isSubscribed'=>1,

                    ];
                    $user->update($gplayData);
                    if($purchasedSubscription){
                        $purchasedSubscription->update([
                            'purchaseStatus' => "Subscribed",
                            'isAcknowledged'  => "Acknowledged"
                        ]);
                    }

                    $messageBody = "Subscribed";
                }

                if (in_array($this->notification_type, $unsubscribe_codes)) { // Unsubscribed

                    $gplayData = [
                        'datetime_subscribed' => null,
                        'lastpayment_datetime' => null,
                        'isSubscribed'=>0,

                    ];
                    $user->update($gplayData);
                    if ($purchasedSubscription) {
                        $purchasedSubscription->update([
                            'purchaseStatus' => "Unsubscription",
                            'isAcknowledged'  => "Not Acknowledged"
                        ]);
                    }
                    $messageBody = "Unsubscribed";
                }
            }

            $subscriptionStatus = null;
            if($this->notification_type == 1){
                $subscriptionStatus = 'SUBSCRIPTION_RECOVERED';
            }
            if($this->notification_type == 2){
                $subscriptionStatus = 'SUBSCRIPTION_RENEWED';
            }
            if($this->notification_type == 3){
                $subscriptionStatus = 'SUBSCRIPTION_CANCELED';
            }
            if($this->notification_type == 4){
                $subscriptionStatus = 'SUBSCRIPTION_PURCHASED';
            }
            if($this->notification_type == 5){
                $subscriptionStatus = 'SUBSCRIPTIONON_HOLD';
            }
            if($this->notification_type == 7){
                $subscriptionStatus = 'SUBSCRIPTION_RESTARTED';
            }
            if($this->notification_type == 10){
                $subscriptionStatus = 'SUBSCRIPTION_PAUSED';
            }
            if($this->notification_type == 12){
                $subscriptionStatus = 'SUBSCRIPTION_REVOKED';
            }
            if($this->notification_type == 13){

                $subscriptionStatus = 'SUBSCRIPTION_EXPIRED';
            }

            $subscriptionString = strtolower(str_replace("_"," ",$subscriptionStatus));
            $subscriptionString = ucwords($subscriptionString);

            $subscriptionId = Purchased::where('userId', $userObject->id)->where('transactionId', $userObject->gplay_order_token)->first();
            $subscription = Package::where('id', $subscriptionId->packageId)->first();
            $fcm['title'] = $subscriptionString;
            $fcm['body'] = $subscriptionString . ", Subscription " . $messageBody;
            $fcm['data'] = [
                'title' => $subscriptionString,
                'body' => $subscriptionString . ", Subscription " . $messageBody,
                'type' => 'Subscription',
                "notificationType" => $subscriptionStatus,
                "subscriptionCode" => $this->notification_type,
                'notification' => json_encode(new SubscriptionResource($subscription))
            ];
        }

        
        $FCMToken = new FcmTokenService();
        $firebase = new FirebaseService();
 
        $tokens = $FCMToken->getUserFCMRegistrationToken($userObject->id);
        foreach ($tokens as $key => $token) {
            $fcm['FCMRegistrationToken'] = $token;
            $firebase->cloudMessageToSingleDevice($fcm);
        }
        

        $infologArray['markerStart'] = "===================================================================Start Info From Here===============================================================================";
        $infologArray['user'] = $userObject;
        $infologArray['markerEnd'] = "===================================================================End Info From Here===============================================================================";
        Log::info("Subscription Job Info",$infologArray);
    }
}
