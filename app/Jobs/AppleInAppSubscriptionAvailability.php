<?php

namespace App\Jobs;

use App\Models\Package;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class AppleInAppSubscriptionAvailability implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $request, $package, $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request,$package,$token)
    {
         $this->request = $request;
         $this->package = $package;
         $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $territory = Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. $this->token])
                ->get('https://api.appstoreconnect.apple.com/v1/subscriptionAvailabilities/'.$this->package->subscriptionId.'/availableTerritories?limit=200');

            $territoryData = json_decode($territory->body());

            foreach($territoryData->data as $territory){

                Http::withHeaders(['Content-Type' => 'application/json','Accept' => 'application/a-gzip, application/json','Authorization' => 'Bearer '. $this->token])
                ->post('https://api.appstoreconnect.apple.com/v1/subscriptionAvailabilities',[
                    'data' => [
                        'type' => 'subscriptionAvailabilities',
                        'attributes' => [
                            'availableInNewTerritories' => true,
                        ],
                        'relationships' => [
                            'availableTerritories' => [
                                'data' => [
                                    'id' => $territory->id,
                                    'type' => 'territories',
                                ]
                            ],
                            'subscription' => [
                                'data' => [
                                    'id' => $this->package->subscriptionId,
                                    'type' => 'subscriptions',
                                ]
                            ],
                        ],
                    ],
                ]);
            }
        }catch (\Exception $exception) {
            Package::find($this->package->id)->update([
                "status" => "Failed",
            ]);

            Log::error("InApp Subscription:",['ErrorException' => $exception->getMessage()]);
        }
    }


    public function failed($exception)
    {
        Package::find($this->package->id)->update([
            "status" => "Failed",
        ]);

        Log::error("AppleInApp Subscription ",['Exception' => $exception->getMessage()]);

    }
}
