<?php

namespace App\Jobs;

use App\Models\Package;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class AppleInAppSubscriptionUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $request, $package, $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request,$package,$token)
    {
         $this->request = $request;
         $this->package = $package;
         $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $response = Http::withHeaders(['Authorization' => 'Bearer ' . $this->token])
            ->patch('https://api.appstoreconnect.apple.com/v1/subscriptions/'. $this->package->subscriptionId, [
                'data' => [
                    'type' => 'subscriptions',
                    'id' => $this->package->subscriptionId,
                    'attributes' => [
                        'name' => $this->request['planTitle'] ?? '',
                        'reviewNote' => 'reviewNote',
                        'familySharable' => true,
                        'availableInAllTerritories' => true,
                        'subscriptionPeriod' => $this->request['planDuration'] ?? '',
                        'groupLevel' => (int) Package::all()->count() ?? 0,
                    ],
                ]
            ]);

            Package::find($this->package->id)->update([
                "status" => null,
            ]);

            if($response->failed()){
                Log::info("INApp Subscription update:",['response' => $response->body()]);
            }

        }catch (Exception $exception) {
            Package::find($this->package->id)->update([
                "status" => "Failed",
            ]);

            Log::error("InApp Subscription:",['ErrorException' => $exception->getMessage()]);
        }
    }

    public function failed($exception)
    {
        Package::find($this->package->id)->update([
            "status" => "Failed",
        ]);

        Log::error("AppleInApp Subscription ",['Exception' => $exception->getMessage()]);
    }
}
