<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use App\Models\Song;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Enums\SongPreOrderEnum;
use App\Enums\AlbumPreOrderEnum;
use Kreait\Firebase\Factory;
use App\Http\Resources\Album\AlbumForNotificationResource;
use App\Http\Resources\Song\SongForNotificationResource;
use App\Models\FcmToken;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CheckPreorderAlbumJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $factory = (new Factory())->withServiceAccount(base_path(env('FIREBASE_CREDENTIALS')));
            $albums = Album::where('type', AlbumPreOrderEnum::PreOrder)->get();

            foreach ($albums as $album) {

                Album::where("id", $album->id)->update([
                    'type' => AlbumPreOrderEnum::Album,
                ]);
                $albumSongs = AlbumSongs::where('albumId', $album->id)->get();
                foreach ($albumSongs as $albumSong) {
                    Song::where("id", $albumSong->songId)->update([
                        'type' => SongPreOrderEnum::Song,
                    ]);
                }
                $title = "Album Pre-Order";
                $data = [
                    'title' => 'Album Pre-Order',
                    'body' => 'Album Pre-Order Released.',
                    'type' => 'PRE_ORDER_ALBUM',
                    'notificationType' => 'RELEASED',
                    'purchasedAlbum' => new AlbumForNotificationResource($album)
                ];
                $topic = $album->sku;
                $body = 'Album Pre-Order Released.';
                if ($topic != null) {

                    $fcmTokens = FcmToken::whereIn('userId', $album->purchase->pluck('userId'))->get();
                    Log::info('Firebase FCM Tokens', [$fcmTokens]);
                    foreach ($fcmTokens as $token) {
                        $messaging = $factory->createMessaging();
                        $notification = Notification::create($title, $body, '/');
                        $message = CloudMessage::withTarget('token', $token->token)
                            ->withNotification($notification)->withData($data)
                            ->withDefaultSounds();
                        $message = $messaging->send($message);
                    }
                }
            }
        } catch (\Exception $e) {
            Log::alert($e);
        }
    }
}
