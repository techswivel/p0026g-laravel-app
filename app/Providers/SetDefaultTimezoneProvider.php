<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SetDefaultTimezoneProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $ip = request()->ip();
        $ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
        $ipInfo = json_decode($ipInfo);
        $timezone = $ipInfo->timezone;
        date_default_timezone_set($timezone);
        setcookie("timezone", date_default_timezone_get(), time()+3600*24*365*10, '/');
    }
}
