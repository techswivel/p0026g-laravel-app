<?php

namespace App\Providers;

use App\Repositories\Eloquent\AddressRepository;
use App\Repositories\Eloquent\BaseRepository;
use App\Repositories\Eloquent\BuyerRatingRepository;
use App\Repositories\Eloquent\CardRepository;
use App\Repositories\Eloquent\CartRepository;
use App\Repositories\Eloquent\CategoryRepository;
use App\Repositories\Eloquent\CommissionRepository;
use App\Repositories\Eloquent\FakedProductRepository;
use App\Repositories\Eloquent\FCMTokenRepository;
use App\Repositories\Eloquent\OrderItemRepository;
use App\Repositories\Eloquent\OrderRepository;
use App\Repositories\Eloquent\OrderSupportRepository;
use App\Repositories\Eloquent\PaymentRepository;
use App\Repositories\Eloquent\PlanRepository;
use App\Repositories\Eloquent\ProductRepository;
use App\Repositories\Eloquent\RecyclingRepository;
use App\Repositories\Eloquent\SavedProductRepository;
use App\Repositories\Eloquent\ScheduleOrderRepository;
use App\Repositories\Eloquent\SellerRatingRepository;
use App\Repositories\Eloquent\StoreRepository;
use App\Repositories\Eloquent\SubCategoryRepository;
use App\Repositories\Eloquent\SubscriptionRepository;
use App\Repositories\Eloquent\TradeUpRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Repositories\Interfaces\AddressRepositoryInterface;
use App\Repositories\Interfaces\BuyerRatingRepositoryInterface;
use App\Repositories\Interfaces\CardRepositoryInterface;
use App\Repositories\Interfaces\CartRepositoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\CommissionRepositoryInterface;
use App\Repositories\Interfaces\EloquentRepositoryInterface;
use App\Repositories\Interfaces\FakedRepositoryInterface;
use App\Repositories\Interfaces\FCMTokenRepositoryInterface;
use App\Repositories\Interfaces\OrderItemRepositoryInterface;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Repositories\Interfaces\OrderSupportRepositoryInterface;
use App\Repositories\Interfaces\PaymentRepositoryInterface;
use App\Repositories\Interfaces\PlanRepositoryInterface;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Repositories\Interfaces\RecyclingRepositoryInterface;
use App\Repositories\Interfaces\SavedRepositoryInterface;
use App\Repositories\Interfaces\ScheduleOrderRepositoryInterface;
use App\Repositories\Interfaces\SellerRatingRepositoryInterface;
use App\Repositories\Interfaces\StoreRepositoryInterface;
use App\Repositories\Interfaces\SubCategoryRepositoryInterface;
use App\Repositories\Interfaces\SubscriptionRepositoryInterface;
use App\Repositories\Interfaces\TradeUpRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 * @package App\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(FCMTokenRepositoryInterface::class, FCMTokenRepository::class);
    }
}
