<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Laravel\Fortify\Contracts\LoginResponse;
use Laravel\Fortify\Contracts\LogoutResponse;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Hash;
use App\Actions\Fortify\CreateNewUser;
use Illuminate\Support\ServiceProvider;
use Illuminate\Cache\RateLimiting\Limit;
use App\Actions\Fortify\ResetUserPassword;
use Illuminate\Validation\ValidationException;
use App\Actions\Fortify\UpdateUserPassword;
use Illuminate\Support\Facades\RateLimiter;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Enums\UserStatusEnum;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->instance(LoginResponse ::class, new class implements LoginResponse  {
            public function toResponse($request)
            {
                $user = Auth::user();
                if ($user->hasRole('artist')){
                    return redirect()->intended(config('fortify.artisthome'));
                }elseif ($user->hasRole('admin')){
                    return redirect()->intended(config('fortify.adminhome'));
                }else{
                    Auth::logout();
                    $loginPath = Route::current();
                    if ($loginPath->uri()=='artist/login'){
                        toastr()->error('You don t have access panel');
                        return redirect('/artist/login');
                    }else{
                        toastr()->error('You don t have access panel');
                        return redirect('/admin/login');
                    }
                }

            }
        });
        $this->app->instance(LogoutResponse ::class, new class implements LogoutResponse  {
            public function toResponse($request){
                $loginPath = URL::previous();
                if (str_contains($loginPath, '/artist/')){
                    return redirect('/artist/login');
                }else if (str_contains($loginPath, '/admin/')){
                    return redirect('/admin/login');
                }
            }
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Fortify::createUsersUsing(CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);
        /* login auth rate limit/attempts */
        RateLimiter::for('login', function (Request $request) {
            return Limit::perMinute(5)->by($request->email.$request->ip());
        });
        /* 2 factor auth rate limit/attempts */
        RateLimiter::for('two-factor', function (Request $request) {
            return Limit::perMinute(5)->by($request->session()->get('login.id'));
        });
         /* return your custom view for signin */
        Fortify::loginView(function () {
            return view('artist.auth.login');
        });
         /* Login Attemp with email and password and it can be changed for phone number */
        Fortify::authenticateUsing(function (Request $request) {
            Session::forget('timeZone');
            Session::put('timeZone', $request->timeZone);
            $user = User::where('email', $request->email)->first();
            if ($user && Hash::check($request->password, $user->password)) {
                $userRole = $user->getRoleNames()->toArray()[0];

                if($request->role == $userRole){
                    if($user->status == UserStatusEnum::Active){
                        return $user;
                    }else{
                        throw ValidationException::withMessages([
                            Fortify::username() => "Your account is block from artist dashboard.",]);
                    }
                }
            };
        });
         /* return your custom view for signup */
        Fortify::registerView(function () {
            return view('artist.auth.register');
        });
       /* custom password reset login */

        Fortify::requestPasswordResetLinkView(function () {
            return view('artist.auth.password.forgetPassword');
        });
        Fortify::resetPasswordView(function($request){
        return view('artist.auth.password.resetPassword',['request' => $request]);
        });
        Fortify::verifyEmailView(function () {
            return view('artist.auth.emailVerify');
        });
    }
}


// email
