<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PriceField implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($songStatus)
    {
        $this->songStatus = $songStatus;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($this->songStatus != 'Paid Song'){
            return true;
        }else {
            if(is_numeric($value)){
                $numlength = strlen($value);
                if (str_contains($value, '.')){
                    $numlength = $numlength -1;
                    if($numlength <= 12){
                        return true;    
                    }
                }
                else {
                    if($numlength <= 10){
                        return true;    
                    }
                }
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Price must be in number.';
    }
}
