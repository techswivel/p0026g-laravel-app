<?php

namespace App\Services;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;


class AppJwtTokenService
{

    public static function getJwtToken()
    {

        $key = "file://".base_path('AuthKey_6NWRMBYHR9.p8'); //absolute path

        $header = [
            'alg' => 'ES256',
            'kid' => env('APP_STORE_KEY_IDENTIFIER'),
            'typ' => 'JWT',
        ];

        $payload = [
            'iss' => env('APP_STORE_ISSUER_ID'),
            'aud' => 'appstoreconnect-v1',
            'iat' => time(),
            'exp' => (time()+(60*20)),
        ];


        return JWT::encode($payload, $key, 'ES256', env('APP_STORE_KEY_IDENTIFIER'), $header);
    }

}
