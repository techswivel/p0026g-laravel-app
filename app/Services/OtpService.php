<?php


namespace App\Services;

use App\Enums\OtpTypes;
use App\Enums\UserStatus;
use App\Http\Resources\User\UserResource;
use App\Mail\SendOtp;
use App\Models\FcmToken;
use App\Models\Otp;
use App\Models\User;
use App\Traits\loggerExceptionTrait;
use App\Traits\PhoneNumberFormattingTraits;
use Facades\App\Services\FcmTokenService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use PHPUnit\Exception;

class OtpService
{
    use PhoneNumberFormattingTraits,loggerExceptionTrait;

    // protected $firebase;

    public function __construct()
    {
        // $this->firebase = new FirebaseService();
    }

    public function validate($request)
    {
        $authType = $request->otpType;
        if ($authType == OtpTypes::PHONE_NUMBER) {
            return $this->signUpNumberOtp($request);
        }
        if ($authType == OtpTypes::EMAIL) {
            return $this->SignUpEmailOtp($request);
        }
        if ($authType == OtpTypes::FORGET_PASSWORD) {
            return $this->resetPasswordEmailOtp($request);
        }
    }

    public function signUpNumberOtp($request)
    {
        try {
            $request->phoneNumber =$this->formattingPhone($request->phoneNumber);
            $otpCode = Otp::where('email_phoneNumber', $request->phoneNumber)->where('otp', $request->otp)->first();
            if (!$otpCode) {
                return response()->json(['response' => ['status' => false, 'message' => 'Otp code is invalid!']], JsonResponse::HTTP_BAD_REQUEST);
            }
            if ($otpCode->otpType == OtpTypes::PHONE_NUMBER) {
                $otpCode->isVerified = 1;
                $otpCode->update();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            } else {
                return response()->json(['response' => ['status' => false, 'message' => 'Otp Type not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Validate otp SignUp number exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function SignUpEmailOtp($request)
    {
        try {
            $otpCode = Otp::where('email_phoneNumber', $request->email)->where('otp', $request->otp)->first();
            if (!$otpCode) {
                return response()->json(['response' => ['status' => false, 'message' => 'Otp code is invalid!']], JsonResponse::HTTP_BAD_REQUEST);
            }
            if ($otpCode->otpType == OtpTypes::EMAIL) {
                $otpCode->isVerified = 1;
                $otpCode->update();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            } else {
                return response()->json(['response' => ['status' => false, 'message' => 'Otp Type not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Validate otp SignUp email exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function resetPasswordEmailOtp($request)
    {
        try {
            $otpCode = Otp::where('email_phoneNumber', $request->email)->where('otp', $request->otp)->first();
            if (!$otpCode) {
                return response()->json(['response' => ['status' => false, 'message' => 'Otp code is invalid!']], JsonResponse::HTTP_BAD_REQUEST);
            }
            if ($otpCode->otpType == OtpTypes::FORGET_PASSWORD) {
                $otpCode->isVerified = 1;
                $otpCode->update();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            } else {
                return response()->json(['response' => ['status' => false, 'message' => 'Otp Type not found!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Validate otp resetPassword email exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}
