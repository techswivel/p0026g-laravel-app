<?php

namespace App\Services;

use Facebook\Exceptions\FacebookAuthenticationException;
use Facebook\Exceptions\FacebookClientException;
use Facebook\Exceptions\FacebookOtherException;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

class FacebookService
{


    public static function validateAccessToken($token)
    {
        $appsecret_proof = hash_hmac('sha256', $token, env('Facebook_APP_SECRET'));
        $fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID'),
            'app_secret' => env('FACEBOOK_APP_SECRET'),
            'default_graph_version' => env('DEFAULT_GRAPH_VERSION'),
            'appsecret_proof' => $appsecret_proof
        ]);
        try {
            $response = $fb->get(
                '/me?fields=id,first_name,last_name,email,picture',
                $token
            );
        } catch (FacebookResponseException $e) {
            self::logErrors($e);
            return false;
        } catch (FacebookAuthenticationException $e) {
            self::logErrors($e);
            return false;
        } catch (FacebookClientException $e) {
            self::logErrors($e);
            return false;
        } catch (FacebookOtherException $e) {
            self::logErrors($e);
            return false;
        } catch (FacebookSDKException $e) {
            self::logErrors($e);
            return false;
        }
        try {
            $response = $response->getGraphUser();
        } catch (FacebookSDKException $e) {
            self::logErrors($e);
            return false;
        }
        return [
            'id' => $response->getId(),
            'email' => $response->getEmail(),
            'firstName' => $response->getFirstName(),
            'lastName' => $response->getLastName(),
            'picture' => $response->getPicture()->getUrl(),
        ];
    }

    private static function logErrors($e)
    {
        if (env('FACEBOOK_LOGGER') == false) {
            return;
        }
        LoggingService::generate('FaceBook', $e);
        return false;
    }
}
