<?php


namespace App\Services;


use Illuminate\Support\Facades\Log;

class LoggingService
{
    public static function generate($message, $e)
    {
        $data = [
            'Time' => gmdate("F j, Y, g:i a"),
            'Status Code' => $e->getCode(),
            'Message' => $e->getMessage(),
            'Error Type' => method_exists($e, 'getErrorType') ?? $e->getErrorType()
        ];
        Log::error($message, $data);
    }
}
