<?php


namespace App\Services;

use App\Models\AlbumSongs;
use App\Models\DownloadedSongs;
use App\Models\Purchased;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;


class SongService
{
   public function songSkuPrice($request){
       $sku = null;
       if (Auth()->user()->isSubscribed==null || Auth()->user()->isSubscribed==0){
           $discount=($request->price * 10)/100;
           $price=$request->price + $discount;
           if ($request->sku != null) {
                $sku = $request->sku."_wod";
            }
       }else{
           $price=$request->price;
           if ($request->sku != null) {
                $sku = $request->sku."_wd";
            }
       }
        $data=[
            'price'=>$price,
            'sku'=>$sku
        ];
       return $data;
   }
   public function songStatus($request){
       $songStatus = $request->isPaid == 1? "PREMIUM" : "FREE";
       $downloaded = DownloadedSongs::where('userId',Auth::user()->id)->where('songId',$request->id)->where('isDownloaded',1)->first();
       if($downloaded){
           $songStatus = "DOWNLOADED";
       }else{
           if($request->isPaid == 1){
               $purchased = Purchased::where('userId',Auth::user()->id)->where('songId',$request->id)
                                        ->where('purchaseStatus','Purchased')->first();
               if($purchased){
                   $songStatus = "PURCHASED";
               }
               $albumIdOfSong = AlbumSongs::where('songId',$request->id)->first();
               if($albumIdOfSong)
               {
                   $purchased = Purchased::where('userId',Auth::user()->id)->where('albumId',$albumIdOfSong->albumId)->first();
                   if($purchased){
                       $songStatus = "PURCHASED";
                   }
               }
           }
       }
       if($request->deletedAt != null){
            $songStatus = "DELETED";
       }
       return $songStatus;
   }
    public function audioVideoFileName($request){
        if($request->isPaid == 1){
            $songAudioUrl = !is_null($request->demoAudioFile) ? Storage::disk('s3')->url($request->demoAudioFile):null;
            $songVideoUrl = !is_null($request->demoVideoFile) ? Storage::disk('s3')->url($request->demoVideoFile):null;
    
            $purchased = Purchased::where('userId',Auth::user()->id)->where('songId',$request->id)->first();
            if($purchased){
                $songAudioUrl = !is_null($request->audioFile) ? Storage::disk('s3')->url($request->audioFile):null;
                $songVideoUrl = !is_null($request->videoFile) ? Storage::disk('s3')->url($request->videoFile):null;
            }
            $albumIdOfSong = AlbumSongs::where('songId',$request->id)->first();
            if($albumIdOfSong) {
                $purchased = Purchased::where('userId',Auth::user()->id)->where('albumId',$albumIdOfSong->albumId)->first();
                if($purchased){
                    $songAudioUrl = !is_null($request->audioFile) ? Storage::disk('s3')->url($request->audioFile):null;
                    $songVideoUrl = !is_null($request->videoFile) ? Storage::disk('s3')->url($request->videoFile):null;
                }
            }

        }else{
            $songAudioUrl = !is_null($request->audioFile) ? Storage::disk('s3')->url($request->audioFile):null;
            $songVideoUrl = !is_null($request->videoFile) ? Storage::disk('s3')->url($request->videoFile):null;
        }

        $explodeFileName=null;
        $explodeVideoFileName=null;
        if ($request->audioFile){
            $fileName= Storage::disk('s3')->url($request->audioFile);
            $audioFIleName=explode('/',$fileName);
            $audio= $audioFIleName;
            $lastArray=count($audio)-1;
            $explodeFileName=$audioFIleName[$lastArray];
        }
        if ($request->videoFile){
            $fileName=Storage::disk('s3')->url($request->videoFile);
            $videoFileName=explode('/',$fileName);
            $video= $videoFileName;
            $lastArray=count($video)-1;
            $explodeVideoFileName=$videoFileName[$lastArray];
        }
        $fileName=[
            'audioFileName'=>$explodeFileName,
            'videoFileName'=>$explodeVideoFileName,
            'audioFile' => $songAudioUrl,
            'videoFile' => $songVideoUrl,
        ];
        return $fileName;
    }
    public function audioVideoFileNameWithOutCheck($request){

        $songAudioUrl = !is_null($request->audioFile) ? Storage::disk('s3')->url($request->audioFile):null;
        $songVideoUrl = !is_null($request->videoFile) ? Storage::disk('s3')->url($request->videoFile):null;
        
        $explodeFileName=null;
        $explodeVideoFileName=null;
        if ($request->audioFile){
            $fileName= Storage::disk('s3')->url($request->audioFile);
            $audioFIleName=explode('/',$fileName);
            $audio= $audioFIleName;
            $lastArray=count($audio)-1;
            $explodeFileName=$audioFIleName[$lastArray];
        }
        if ($request->videoFile){
            $fileName=Storage::disk('s3')->url($request->videoFile);
            $videoFileName=explode('/',$fileName);
            $video= $videoFileName;
            $lastArray=count($video)-1;
            $explodeVideoFileName=$videoFileName[$lastArray];
        }
        $fileName=[
            'audioFileName'=>$explodeFileName,
            'videoFileName'=>$explodeVideoFileName,
            'audioFile' => $songAudioUrl,
            'videoFile' => $songVideoUrl,
        ];
        return $fileName;
    }
}
