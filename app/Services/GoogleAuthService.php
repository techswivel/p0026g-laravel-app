<?php

namespace App\Services;

use Google_Client;
use Google_Exception;
use Google_Service_Oauth2;
use Session;

class GoogleAuthService
{
    protected $client;
    protected $googleOAuth;

    function __construct()
    {
        $this->client = new Google_Client();
        $this->client->setApplicationName('Demo App');
        $this->client->setClientId(env('GOOGLE_CLIENT_ID'));
        $this->client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $this->client->setRedirectUri(env('GOOGLE_REDIRECT_URL'));
        $this->client->addScope("https://www.googleapis.com/auth/userinfo.email");
        $this->googleOAuth = new Google_Service_Oauth2($this->client);

    }

    function getInfo($token)
    {
        $httpService = new HTTPRequestService();
        $url = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' . $token;
        $response = $httpService->get($url);
        if ($response) {
            $response = (json_decode($response));
            try {
                $this->client->setAccessToken($token);
                $me = $this->googleOAuth->userinfo->get();
            } catch (Google_Exception $e) {
                $this->logErrors($e);
                return false;
            }
            if (!$me) {
                return false;
            }
            return [
                'id' => $me->id,
                'email' => $me->email,
                'firstName' => $me->getGivenName(),
                'lastName' => $me->getFamilyName(),
                'picture' => $me->picture,
            ];
        } else {
            return false;
        }
    }

    private static function logErrors($e)
    {
        if (env('GMAIL_LOGGER') == false) {
            return;
        }
        LoggingService::generate('Gmail', $e);
        return false;
    }
}
