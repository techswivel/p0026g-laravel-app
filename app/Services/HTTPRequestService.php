<?php

namespace App\Services;

use App\Http\Resources\HTTPRequestResource;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class HTTPRequestService
{
    function __construct()
    {
        $this->client = new Client();
    }

    public function get($uri)
    {
        try {
            $request = $this->client->get($uri);
            return $request->getBody()->getContents();
        } catch (BadResponseException $e) {
            return $this->logErrors($e);
        }
    }

    private function logErrors($e)
    {
        if (env('GuzzleHttp_LOGGER') == false) {
            return;
        }
        LoggingService::generate('GuzzleHttp', $e);
        return false;
    }

    public function post(string $uri, array $data)
    {
        try {
            $request = $this->client->post($uri, $data);
            return $request->send();
        } catch (BadResponseException $e) {
            return false;
        }
    }

    public function put(string $uri, array $data)
    {
        try {
            $request = $this->client->put($uri, $data);
            return $request->send();
        } catch (BadResponseException $e) {
            return false;
        }
    }

    public function delete(string $uri)
    {
        try {
            $request = $this->client->delete($uri);
            return $request->send();
        } catch (BadResponseException $e) {
            return false;
        }
    }
}
