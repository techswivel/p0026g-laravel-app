<?php


namespace App\Services;

use App\Enums\OtpTypes;
use App\Mail\ResetPassword;
use App\Mail\SendOtp;
use App\Models\Otp;
use App\Models\User;
use App\Traits\loggerExceptionTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Services\TwilioService;
use App\Traits\PhoneNumberFormattingTraits;

class SendOtpService
{
    use PhoneNumberFormattingTraits,loggerExceptionTrait;

    protected $twilio;

    public function __construct()
    {
        $this->twilio = new TwilioService();
    }

    public function sendOtp($request)
    {
        $authType = $request->otpType;
        if ($authType == OtpTypes::EMAIL) {
            return $this->sendSignUpEmailOtp($request);
        }
        if ($authType == OtpTypes::PHONE_NUMBER) {
            return $this->sendSignUpNumberOtp($request);
        }
        if ($authType == OtpTypes::FORGET_PASSWORD) {
            return $this->sendResetPasswordEmailOtp($request);
        }
    }

    public function sendSignUpEmailOtp($request)
    {
        try {
            $otp = $this->generateOtp($request);
            $email = $request->email;
            $user = User::where('email', $request->email)->first();
            if (!is_null($user)) {
                return response()->json(['response' => ['status' => false, 'message' => 'User already exist!']], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            $otpVerfiy = Otp::where('email_phoneNumber', $email)->first();
            if (!is_null($otpVerfiy)) {
                try {
                    DB::beginTransaction();
                    Mail::to($email)->send(new SendOtp($otp));
                    $otpVerfiy->update([
                        'otp' => $otp,
                        'otpType' => OtpTypes::EMAIL,
                    ]);
                    DB::commit();
                    return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
                } catch (\Exception $e) {
                    return response()->json(['response' => ['status' => false, 'message' => 'Mail could not be sended']], JsonResponse::HTTP_BAD_REQUEST);
                }

            }
            Otp::create([
                'email_phoneNumber' => $request->email,
                'otp' => $otp,
                'otpType' => OtpTypes::EMAIL,
            ]);
            try {
                Mail::to($email)->send(new SendOtp($otp));
            } catch (\Exception $e) {
                return response()->json(['response' => ['status' => false, 'message' => 'Mail could not be sended']], JsonResponse::HTTP_BAD_REQUEST);
            }
            return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Send otp Email exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);

        }
    }

    public function sendResetPasswordEmailOtp($request)
    {
        try {
            $otp = $this->generateOtp($request);
            $user = User::where('email', $request->email)->first();
            if (is_null($user)) {
                // JsonResponse::HTTP_UNPROCESSABLE_ENTITY there was 422 i think its good
                // Unable to find user with given email
                return response()->json(['response' => ['status' => false, 'message' => 'No account registered for this email!']], JsonResponse::HTTP_NOT_FOUND);
            }
            $email = $request->email;
            $otpVerfiy = Otp::where('email_phoneNumber', $email)->first();
            if (!is_null($otpVerfiy)) {
                DB::beginTransaction();
                Mail::to($email)->send(new ResetPassword($otp, $user));
                $otpVerfiy->update([
                    'otp' => $otp,
                    'otpType' => OtpTypes::FORGET_PASSWORD,
                ]);
                DB::commit();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            }
            Otp::create([
                'email_phoneNumber' => $request->email,
                'otp' => $otp,
                'otpType' => OtpTypes::FORGET_PASSWORD,
            ]);
            Mail::to($email)->send(new ResetPassword($otp, $user));
            return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Send otp Reset password Email exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }

    }

    public function sendSignUpNumberOtp($request)
    {
        try {
            $otp = $this->generateOtp($request);
            $phoneNumber = $this->formattingPhone($request->phoneNumber);

            // removed this check after discussion with Nasar, he wants to add same contact number with multiple users. 
            $otpVerfiy = Otp::where('email_phoneNumber', $phoneNumber)->first();
            if (!is_null($otpVerfiy)) {
                DB::beginTransaction();
                $message = $otp . " is your Q the Music verification code.";
                if(env('IS_TWILLIO_ENABLED')){
                    $msg = $this->twilio->sendMessage($message, $phoneNumber);
                }else{
                    $msg['status']=true;
                }                
                if ($msg['status']==true) {
                    $otpVerfiy->update([
                        'otp' => $otp,
                        'otpType' => OtpTypes::PHONE_NUMBER,
                    ]);
                } else {
                    return response()->json(['response' => ['status' => false, 'message' =>$msg['Message']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
                DB::commit();
                return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
            }
            $message = $otp . " is your Q the Music verification code.";
            if(env('IS_TWILLIO_ENABLED')){
                $msg = $this->twilio->sendMessage($message, $phoneNumber);
            }else{
                $msg['status']=true;
            } 
            if ($msg['status']==true) {
                Otp::create([
                    'email_phoneNumber' => $phoneNumber,
                    'otp' => $otp,
                    'otpType' => OtpTypes::PHONE_NUMBER,
                ]);
            } else {
                return response()->json(['response' => ['status' => false, 'message' =>$msg['Message']]], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            $this->saveExceptionLog($e, 'Send SingUp otp  number exception');
            return response()->json(['response' => ['status' => false, 'message' => 'Something went wrong!']], JsonResponse::HTTP_BAD_REQUEST);
        }

    }

    public function generateOtp($request)
    {
        $otp = mt_rand(10000, 99999);
        if (env('APP_ENV') == 'staging' || env('APP_ENV') == 'local' || env('APP_ENV') == 'acceptance'){
            $otp = 11111;
        }
        return $otp;
    }
}
