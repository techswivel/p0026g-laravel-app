<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Illuminate\Support\Facades\File;
use App\Traits\FileUploadTrait;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules,FileUploadTrait;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'firstName' => ['required', 'string', 'max:255'],
            'lastName' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'aboutArtist' => [ 'max:1024'],
            'password' => $this->passwordRules(),
            'avatar' => ['image','mimes:jpg,png,jpeg','max:2048'],
        ])->validate();
        $avatar = NULL;
        if(request()->hasFile('avatar'))
            {
            $file = request()->file('avatar');
            $path = 'artist/image/avatar';
            $avatar = $this->uploadFile($file,$path);
        }
        $user = User::create([
            'firstName' => $input['firstName'],
            'lastName' => $input['lastName'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'aboutArtist' => $input['aboutArtist'],
            'avatar' => $avatar,
            'isNotificationEnabled' => 1,
        ]);
        $user->assignRole('artist');
        return $user;
    }
}
