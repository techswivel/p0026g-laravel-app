<?php

namespace App\Console\Commands;

use Facades\App\Helpers\FcmHelper;
use Illuminate\Console\Command;
use App\Models\Song;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Enums\SongPreOrderEnum;
use App\Enums\AlbumPreOrderEnum;
use App\Http\Resources\Album\AlbumForNotificationResource;
use App\Http\Resources\Album\AlbumResource;
use App\Http\Resources\Song\SongForNotificationResource;
use App\Http\Resources\Song\SongResource;
use App\Models\FcmToken;
use App\Models\Purchased;
use App\Services\FcmTokenService;
use App\Services\FirebaseService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class CheckPreorder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:preorder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To check if the preorder release';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $factory = (new Factory())->withServiceAccount(base_path(env('FIREBASE_CREDENTIALS')));

            $scheduledOrderNotificationTime = 5;
            $songs = Song::with('purchase')->where('type', SongPreOrderEnum::PreOrder)->get();

            foreach ($songs as $song) {
                $date = Carbon::parse($song->preOrderReleaseDate)->unix();
                $now = Carbon::now()->addHours($scheduledOrderNotificationTime)->unix();

                if ($date <= $now) {
                    Song::where('id', $song->id)->update([
                        'type' => SongPreOrderEnum::Song,
                    ]);

                    $title = 'Song Pre-Order';
                    $data = [
                        'title' => 'Song Pre-Order',
                        'body' => 'Song Pre-Order Released.',
                        'type' => 'PRE_ORDER_SONG',
                        'notificationType' => 'RELEASED',
                        'notification' => json_encode(new SongForNotificationResource($song))
                    ];
                    $topic = $song->sku;
                    $body = 'Song Pre-Order Released.';

                    if ($topic != null) {
                        $messaging = $factory->createMessaging();
                        $notification = Notification::create($title, $body , '/');
                        $fcmTokens = FcmToken::whereIn('userId',$song->purchase->pluck('userId'))->get();

                        foreach($fcmTokens as $token){
                            $message = CloudMessage::withTarget('token', $token->token)
                                ->withNotification($notification)->withData($data)
                                ->withDefaultSounds();

                            $message = $messaging->send($message);
                        }

                        // if (FcmHelper::topicWithData(json_encode($data), $title, $topic, $body)) {
                        //     $this->info('song the released');
                        // }
                    }
                }
            }
            $albums = Album::where('type', AlbumPreOrderEnum::PreOrder)->get();
            foreach ($albums as $album) {
                $date = Carbon::parse($album->preOrderReleaseDate)->format('Y-m-d H:i');
                $now = Carbon::now()->addHours($scheduledOrderNotificationTime)->format('Y-m-d H:i');
                if ($date <= $now) {
                    Album::where("id", $album->id)->update([
                        'type' => AlbumPreOrderEnum::Album,
                    ]);
                    $albumSongs = AlbumSongs::where('albumId', $album->id)->get();
                    foreach ($albumSongs as $albumSong) {
                        Song::where("id", $albumSong->songId)->update([
                            'type' => SongPreOrderEnum::Song,
                        ]);
                    }
                    $title = "Album Pre-Order";
                    $data = [
                        'title' => 'Album Pre-Order',
                        'body' => 'Album Pre-Order Released.',
                        'type' => 'PRE_ORDER_ALBUM',
                        'notificationType' => 'RELEASED',
                        'purchasedAlbum' => new AlbumForNotificationResource($album)
                    ];
                    $topic = $album->sku;
                    $body = 'Song Pre-Order Released.';
                    if ($topic != null) {
                        if (FcmHelper::topicWithData(json_encode($data), $title, $topic, $body)) {
                        }
                        $this->info('Album the released');
                    }
                }
            }
            $this->info('Check the preorder');
        } catch (\Exception $e) {
            Log::alert($e);
        }
    }
}
