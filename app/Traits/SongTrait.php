<?php
namespace App\Traits;
use App\Helpers\PHPMP3;
use Illuminate\Support\Facades\File;
use App\Traits\FileUploadTrait;
use Illuminate\Support\Facades\Storage;
use App\Helpers\MP3Length;
use Illuminate\Support\Facades\Log;

trait SongTrait {

    use FileUploadTrait;
    public function songThumbnail($file,$id) {
        // store thumbnail image
        $path = 'artist/songs/'.auth()->user()->id.'/'.$id.'/thumbnail';
        $thumbnail = $this->uploadFile($file, $path);
        return $thumbnail;
    }


    public function songAudio($file,$id,$songTitle,$isPaid)
    {
        //store Audio file
        $path = 'artist/songs/'.auth()->user()->id.'/'.$id.'/audio';
        $songAudio = $this->uploadFile($file, $path);
        if($isPaid){
            $extension = $file->getClientOriginalExtension();
            $songName = str_replace(' ', '',$songTitle);
            $songAudioDemo = $path.'/demo/demo'.$songName.'.'.$extension;
            $audioPath = $this->getfileUrl($songAudio);

            // Making Audio file demo and saving it
            $folderPath = public_path().'/artist/demo';
            if(!File::exists($folderPath)) {
                File::makeDirectory($folderPath, $mode = 0777, true, true);
                chmod($folderPath, 0777);
            }

            $audioDemoPath = 'artist/demo/audio'.$id.'.mp3';

            $cmd = 'ffmpeg -i '.$audioPath.' -ss 00:00:00 -t  00:00:10 '.$audioDemoPath;
            $a = shell_exec($cmd);
            Storage::disk('s3')->put($songAudioDemo, fopen($audioDemoPath, 'r+'));
            if(File::exists($audioDemoPath))
            {
                File::delete($audioDemoPath);
            }
        }else{
            $songAudioDemo = NULL;
        }
        return array($songAudio,$songAudioDemo);
    }
    public function songPaidAudio($songAudio,$id,$songTitle,$isPaid)
    {
        //store Audio file
        $path = 'artist/songs/'.auth()->user()->id.'/'.$id.'/audio';
        if($isPaid){
            $audioFileName=explode('/',$songAudio);
            $lastArray=count($audioFileName)-1;
            $explodeFileName=$audioFileName[$lastArray];

            $audioFileExtension=explode('.',$explodeFileName);
            $lastArray=count($audioFileExtension)-1;
            $explodeFileExtension=$audioFileExtension[$lastArray];

            $extension = $explodeFileExtension;
            $songName = str_replace(' ', '',$songTitle);
            $songAudioDemo = $path.'/demo/demo'.$songName.'.'.$extension;
            $audioPath = $this->getfileUrl($songAudio);

            // Making Audio file demo and saving it
            $folderPath = public_path().'/artist/demo';
            if(!File::exists($folderPath)) {
                File::makeDirectory($folderPath, $mode = 0777, true, true);
                chmod($folderPath, 0777);
            }

            $audioDemoPath = 'artist/demo/audio'.$id.'.mp3';

            $cmd = 'ffmpeg -i '.$audioPath.' -ss 00:00:00 -t  00:00:10 '.$audioDemoPath;
            $a = shell_exec($cmd);
            $a123 = Storage::disk('s3')->put($songAudioDemo, fopen($audioDemoPath, 'r+'));
            if(File::exists($audioDemoPath))
            {
                File::delete($audioDemoPath);
            }
        }else{
            $songAudioDemo = NULL;
        }
        return $songAudioDemo;
    }

    public function songVideo($file,$id,$songTitle,$isPaid)
    {
         // store Video file
        $path = 'artist/songs/'.auth()->user()->id.'/'.$id.'/video';
        $songVideo = $this->uploadFile($file, $path);
        if($isPaid){
            // Making Video file demo and saving it
            $extension = $file->getClientOriginalExtension();
            $songName = str_replace(' ', '',$songTitle);
            $songVideoDemo = $path.'/demo/demo'.$songName.'.'.$extension;
            $videoPath = $this->getfileUrl($songVideo);

            $folderPath = public_path().'/artist/demo';
            if(!File::exists($folderPath)) {
                File::makeDirectory($folderPath, $mode = 0777, true, true);
                chmod($folderPath, 0777);
            }

            $videoDemoPath ='artist/demo/video'.$id.'.mp4';

            $cmd = 'ffmpeg -i '.$videoPath.' -ss 00:00:00 -t  00:00:10 '.$videoDemoPath;
            $a = shell_exec($cmd);
            Storage::disk('s3')->put($songVideoDemo, fopen($videoDemoPath, 'r+'));
            if(File::exists($videoDemoPath))
            {
                File::delete($videoDemoPath);
            }
        }else{
            $songVideoDemo = NUll;
        }

        return array($songVideo,$songVideoDemo);
    }
    public function songPaidVideo($songVideo,$id,$songTitle,$isPaid)
    {
         // store Video file
        $path = 'artist/songs/'.auth()->user()->id.'/'.$id.'/video';
        if($isPaid){
            // Making Video file demo and saving it
            $audioFileName=explode('/',$songVideo);+
            $lastArray=count($audioFileName)-1;
            $explodeFileName=$audioFileName[$lastArray];

            $audioFileExtension=explode('.',$explodeFileName);
            $lastArray=count($audioFileExtension)-1;
            $explodeFileExtension=$audioFileExtension[$lastArray];

            $extension = $explodeFileExtension;
            $songName = str_replace(' ', '',$songTitle);
            $songVideoDemo = $path.'/demo/demo'.$songName.'.'.$extension;
            $videoPath = $this->getfileUrl($songVideo);

            $folderPath = public_path().'/artist/demo';
            if(!File::exists($folderPath)) {
                File::makeDirectory($folderPath, $mode = 0777, true, true);
                chmod($folderPath, 0777);
            }
            $videoDemoPath ='artist/demo/video'.$id.'.mp4';

            $cmd = 'ffmpeg -i '.$videoPath.' -ss 00:00:00 -t  00:00:10 '.$videoDemoPath;
            $a = shell_exec($cmd);
            Storage::disk('s3')->put($songVideoDemo, fopen($videoDemoPath, 'r+'));
            if(File::exists($videoDemoPath))
            {
                File::delete($videoDemoPath);
            }
        }else{
            $songVideoDemo = NUll;
        }
        return $songVideoDemo;
    }
    public function songLength($file)
    {
        $mp3file = new MP3Length($file);//http://www.npr.org/rss/podcast.php?id=510282
        $duration1 = $mp3file->getDurationEstimate();//(faster) for CBR only
        $duration2 = $mp3file->getDuration();//(slower) for VBR (or CBR)
        return MP3Length::formatTime($duration2);
    }

}
