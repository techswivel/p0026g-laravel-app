<?php
namespace App\Traits;
use App\Traits\FileUploadTrait;
use App\Enums\AlbumStatusEnum;
use App\Models\AlbumSongs;
use App\Models\Song;

trait AlbumTrait {
    
    use FileUploadTrait;
    public function albumThumbnail($file,$id) {
        // store thumbnail image
        $path = 'artist/albums/'.auth()->user()->id.'/'.$id.'/thumbnail';
        $thumbnail = $this->uploadFile($file, $path);
        return $thumbnail;
    }

    public function AlbumStatusCheck($id)
    {
        $albumStatus = AlbumStatusEnum::Free;
        $albumSongs = AlbumSongs::where('albumId',$id)->get('songId');
        if(count($albumSongs)){
            $songs = Song::whereIn('id',$albumSongs)->where('isPaid',1)->get();
            if(count($albumSongs) == count($songs)){
                $albumStatus = AlbumStatusEnum::Paid;  // if All song is Paid albumStatus equal to 2
            }else if(count($songs) == 0){
                $albumStatus = AlbumStatusEnum::Free; // if All song is Free albumStatus equal to 0
            }else if (count($albumSongs) > count($songs)){
                $albumStatus = AlbumStatusEnum::Partially; // if Some song is Paid and some Song is free albumStatus equal to 1
            }
        }
        return $albumStatus;
    }
}