<?php
namespace App\Traits;
use App\DataTables\NotificationDataTable;
use App\Enums\UserStatusEnum;
use App\Http\Controllers\Controller;
use App\Models\MonthlyNotificationCount;
use App\Models\NotificationTopic;
use Illuminate\Http\Request;
use App\Http\Requests\Artist\Notification\NotificationAddRequest;
use Illuminate\Support\Facades\DB;
use App\Repositories\Eloquent\NotificationRepository;
use App\Enums\NotificationStatusEnum;
use App\Enums\NotificationTypeEnum;
use App\Enums\UserTypeNotificationEnum;
use App\Http\Requests\Notifications\NotificationRequest;
use App\Http\Resources\User\SubscriptionResource;
use App\Models\FcmToken;
use App\Models\Follower;
use App\Models\Notification;
use App\Models\Package;
use App\Models\User;
use App\Services\FcmTokenService;
use App\Services\FirebaseService;
use Carbon\Carbon;
use Facades\App\Helpers\FcmHelper;
use Illuminate\Support\Facades\Auth;


trait NotificationTrait{


    public function approveNotificationSend($artistId,$notificationId){
        try {
            //artist  the find and get to followers sent the notification
            $user=User::role('artist')->where('id',$artistId)->first();
            //notification get id base data return
           if ($user!=null){
                   $notificationData=Notification::where('id',$notificationId)->where('status',NotificationStatusEnum::Pending)->first();
                   $date = Carbon::now()->startofMonth();
                   $notification = Notification::where('userId',$user->id)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->count();
                   $monthlyNotificationCount = MonthlyNotificationCount::first();
                   if(is_null($monthlyNotificationCount)){
                       if ($notificationData!=null){
                            $followers=Follower::whereHas('user', function ($q) {
                                $q->where('isNotificationEnabled','!=',0);
                                $q->where('isArtistUpdateEnabled','!=',0);
                            })->where('artistId',$user->id)->get();
                           if (count($followers)!=0){
                               foreach ($followers as $follower){

                                   $fcm['title'] = $notificationData->title;
                                   $fcm['body'] = $notificationData->message;
                                   $fcm['data'] = [
                                       'title' => $notificationData->title,
                                       'body' => $notificationData->message,
                                       "notificationType"=>$notificationData->notificationType,
                                   ];
                                   $FCMToken = new FcmTokenService();
                                   $firebase = new FirebaseService();
                                   $tokens =$FCMToken->getUserFCMRegistrationToken($follower->userId);
                                   foreach ($tokens as $key => $token) {
                                       $fcm['FCMRegistrationToken'] = $token;
                                       $firebase->cloudMessageToSingleDevice($fcm);

                                   }
                               }
                               Notification::where('id',$notificationId)->update([
                                'status' => NotificationStatusEnum::Approved,
                                ]);
                                toastr()->success('Notification sent to Multiples devices successfully!');
                               return redirect('/admin/artist/notifications');
                           }else{
                               toastr()->error('Followers not found!');
                               return back();
                           }
                       }else{
                           toastr()->error('Notification not found!');
                           return redirect('/admin/artist/notifications');
                       }
                   }else if(($notification < $monthlyNotificationCount->totalNotificationCount) and ($user->isNotificationEnabled == 1)){
                       if ($notificationData!=null){
                           $followers=Follower::whereHas('user', function ($q) {
                               $q->where('isNotificationEnabled','!=',0);
                               $q->where('isArtistUpdateEnabled','!=',0);
                           })->where('artistId',$user->id)->get();
                           if (count($followers)!=0){
                               foreach ($followers as $follower){

                                   $fcm['title'] = $notificationData->title;
                                   $fcm['body'] = $notificationData->message;
                                   $fcm['data'] = [
                                       'title' => $notificationData->title,
                                       'body' => $notificationData->message,
                                       "notificationType"=>$notificationData->notificationType,
                                   ];
                                   $FCMToken = new FcmTokenService();
                                   $firebase = new FirebaseService();
                                   $tokens =$FCMToken->getUserFCMRegistrationToken($follower->userId);
                                   foreach ($tokens as $key => $token) {
                                       $fcm['FCMRegistrationToken'] = $token;
                                       $firebase->cloudMessageToSingleDevice($fcm);
                                   }
                                    Notification::where('id',$notificationId)->update([
                                        'status' => NotificationStatusEnum::Approved,
                                    ]);
                                   toastr()->success('Notification sent to Multiples devices successfully!');
                                   return true;

                               }
                           }else{
                               toastr()->error('Followers not found!');
                               return redirect('/admin/artist/notifications');
                           }
                       }else{
                           toastr()->error('Notification not found!');
                           return redirect('/admin/artist/notifications');
                       }

                   }
                   else{
                       toastr()->error('Your notification limit is reached');
                       return redirect('/admin/artist/notifications');

                   }
           }
        }catch (\Exception $e){
            toastr()->error($e->getMessage());
            return redirect('/admin/artist/notifications');
        }
    }
}