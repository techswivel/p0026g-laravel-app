<?php

namespace App\Traits;

use Exception;
use Google_Client;
use Google_Service_AndroidPublisher;
use Google_Service_AndroidPublisher_InAppProduct;
use Google_Service_AndroidPublisher_Price;

/**
 * This trait was built to easily upload files based on a path set in your model
 */
trait InAppPurchaseTrait
{
    public function getInAppPurchasedProduct($productSku)
    {
        $client = new Google_Client();
        $client->setAuthConfig(config('inapppurchase.serviceAccountJsonFile.file'));
        $client->addScope('https://www.googleapis.com/auth/androidpublisher');

        $service = new Google_Service_AndroidPublisher($client);
        $inApp = $service->inappproducts->get(
            config('inapppurchase.packageName.name'),
            $productSku
        );

        return $inApp;
    }

    public function createProductInAppPurchase($cost, $name, $productSKU)
    {
        $productPrice = $cost;
        $productTitle = $name . " title";
        $productDescription = $name . " description.";

        $client = new Google_Client();
        $client->setAuthConfig(config('inapppurchase.serviceAccountJsonFile.file'));
        $client->addScope('https://www.googleapis.com/auth/androidpublisher');

        $service = new Google_Service_AndroidPublisher($client);

        $body = new \Google_Service_AndroidPublisher_InAppProduct();
        $body->setSku($productSKU);
        $body->setPackageName(config('inapppurchase.packageName.name'));

        // Create a new price object
        $price = new \Google_Service_AndroidPublisher_Price();
        $price->setCurrency('USD');
        $new_price = $productPrice;
        $price->priceMicros = round($new_price * 1000000, 2);
        $body->setDefaultPrice($price);
        $body->defaultLanguage = "en_US";
        $body->setListings([
            'en_US' => [
                'title' => $productTitle,
                'description' => $productDescription
            ]
        ]);
        $body->setPurchaseType('managedUser');
        $body->setStatus('active');
        $purchase = $service->inappproducts->insert(
            config('inapppurchase.packageName.name'),
            $body,
            ['autoConvertMissingPrices' => true]
        );
        $purchase->setStatus(true);
    }

    public function updateProductInAppPurchase($cost, $name, $productSKU)
    {
        $productPrice = $cost;
        $productTitle = $name . " title";
        $productDescription = $name . " description.";

        $client = new Google_Client();
        $client->setAuthConfig(config('inapppurchase.serviceAccountJsonFile.file'));
        $client->addScope('https://www.googleapis.com/auth/androidpublisher');

        $service = new Google_Service_AndroidPublisher($client);

        $body = new \Google_Service_AndroidPublisher_InAppProduct();
        $body->setSku($productSKU);
        $body->setPackageName(config('inapppurchase.packageName.name'));

        // Create a new price object
        $price = new \Google_Service_AndroidPublisher_Price();
        $price->setCurrency('USD');
        $new_price = $productPrice;
        $price->priceMicros = round($new_price * 1000000, 2);
        $body->setDefaultPrice($price);
        $body->defaultLanguage = "en_US";
        $body->setListings([
            'en_US' => [
                'title' => $productTitle,
                'description' => $productDescription
            ]
        ]);
        $body->setPurchaseType('managedUser');
        $body->setStatus('active');
        $purchase = $service->inappproducts->update(
            config('inapppurchase.packageName.name'),
            $productSKU,
            $body,
            ['autoConvertMissingPrices' => true]
        );
        $purchase->setStatus(true);
    }

    public function deleteInAppPurchasedProduct($productSku)
    {
        $client = new Google_Client();
        $client->setAuthConfig(config('inapppurchase.serviceAccountJsonFile.file'));
        $client->addScope('https://www.googleapis.com/auth/androidpublisher');

        $service = new Google_Service_AndroidPublisher($client);
        $deleteProduct = $service->inappproducts->delete(
            config('inapppurchase.packageName.name'),
            $productSku
        );
    }
}
