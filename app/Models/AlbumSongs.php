<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AlbumSongs extends Model
{
    use HasFactory,SoftDeletes;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT='deletedAt';

    protected $fillable = [
        'albumId', 'songId'
    ];

    public function getDateFormat()
    {
        return 'U';
    }
    
    public function getSongs()
    {
        return $this->hasMany(Song::class, 'id', 'songId');
    }  
    public function getAlbum()
    {
        return $this->hasOne(Album::class, 'id', 'albumId');
    }
      
    public function getSong()
    {
        return $this->hasOne(Song::class, 'id', 'songId');
    }

    public function song()
    {
        return $this->belongsTo(Song::class,'songId','id');
    }

    public function album()
    {
        return $this->belongsTo(Album::class,'albumId','id');
    }
}
