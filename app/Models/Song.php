<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Song extends Model
{
    use HasFactory,SoftDeletes;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT='deletedAt';

    protected $fillable = [
        'userId', 'name', 'isPaid', 'price','discountedPrice', 'categoryId', 'languageId', 'thumbnail','preOrderCount','totalPreOrderCount'
        , 'audioFile', 'videoFile', 'lyrics', 'demoAudioFile', 'demoVideoFile', 'preOrderReleaseDate', 'type','createdType', 'deletedBy','createdBy'
        , 'songLength','sku','iAppPurchaseIdWd','iAppPurchaseIdWod','status'
    ];
    protected $appends = ['avatarFullUrl','audioFileUrl'];

    public function getAvatarFullUrlAttribute()
    {
        return is_null($this->attributes['thumbnail']) ? asset('defaultImages/default-avatar.png')
            : Storage::disk('s3')->url($this->attributes['thumbnail']);
    }
    public function getAudioFileUrlAttribute()
    {
        return is_null($this->attributes['audioFile']) ? ''
            : Storage::disk('s3')->url($this->attributes['audioFile']);
    }
    public function songs(){
        return $this->belongsTo('App\Models\AlbumSongs','id','songId');
    }

    public function getSongRelation()
    {
        return $this->belongsToMany('App\Models\Album','album_songs','songId','albumId');
    }


    public function getDateFormat()
    {
        return 'U';
    }


    public function category()
    {
        return $this->belongsTo(Category::class, 'categoryId', 'id');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'languageId', 'id');
    }

    public function getAlbumId()
    {
        return $this->hasOne(AlbumSongs::class, 'songId', 'id');
    }

    public function getPlaylistId()
    {
        return $this->hasOne(PlaylistSongs::class, 'songId', 'id');
    }
    public function favoriteSong()
    {
        return $this->hasMany(Favorite::class, 'songId', 'id');
    }

    public function getArtist()
    {
        return $this->hasOne(User::class, 'id', 'userId');
    }

    public function purchase()
    {
        return $this->hasMany(Purchased::class, 'songId', 'id');
    }

}
