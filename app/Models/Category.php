<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Category extends Model
{
    use HasFactory,SoftDeletes;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT='deletedAt';

    protected $fillable = [
        'name', 'imageUrl'
    ];
    protected $appends = ['avatarFullUrl'];

    public function getDateFormat()
    {
        return 'U';
    }

    public function categorySongs()
    {
        return $this->hasMany(Song::class, 'categoryId', 'id')->with('getArtist');
    }
    public function getAvatarFullUrlAttribute()
    {
        return is_null($this->attributes['imageUrl']) ? asset('defaultImages/default-avatar.png')
            : Storage::disk('s3')->url($this->attributes['imageUrl']);
    }
}
