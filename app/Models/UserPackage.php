<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model
{
    use HasFactory;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $fillable = [
        'userId', 'packageId', 'startDate', 'endDate', 'status'
    ];

    public function getDateFormat()
    {
        return 'U';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userId', 'id');
    }
    public function package()
    {
        return $this->belongsTo(Package::class, 'packageId', 'id');
    }

}
