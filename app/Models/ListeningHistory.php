<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListeningHistory extends Model
{
    use HasFactory;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $fillable = [
        'userId','songId', 'albumId', 'artistId'
        , 'isRecentlyPlayed', 'isListeningHistory', 'type'
    ];

    public function getDateFormat()
    {
        return 'U';
    }
}
