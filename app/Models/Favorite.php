<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
      
    use HasFactory,SoftDeletes;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT = 'deletedAt';

    protected $fillable = [
        'userId', 'songId'
    ];

    public function getDateFormat()
    {
        return 'U';
    }

    public function songs()
    {
        return $this->hasMany(Song::class, 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'userId', 'id');
    }

    public function song()
    {
        return $this->belongsTo(Song::class, 'songId', 'id');
    }
}
