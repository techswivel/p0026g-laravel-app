<?php

namespace App\Models;

use App\Enums\PackageStatusEnum;
use App\Enums\PurchasedItemTypeEnum;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable,HasApiTokens, HasRoles,SoftDeletes;
    protected $dateFormat = 'U';
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT = 'deletedAt';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName', 'lastName',
        'email',
        'password',
         'phoneNumber','gender' ,'birthDate','two_factor_secret',
        'two_factor_recovery_codes', 'avatar','uid',
        'paypalCustomerId', 'status', 'fbId', 'gmailId', 'appleId',
        'aboutArtist','address','city','state','country','zipCode',
        'isNotificationEnabled','isArtistUpdateEnabled',

           'jwtToken',
           'jwtExpiredTime',
           'gplay_order_token',
           'gplay_order_id',
           'apple_order_id',
           'datetime_subscribed',
           'lastpayment_datetime','isSubscribed'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    public function isSubscribed()
    {
        $one_month = now()->subDays(32);
        $is_subscribed = (!is_null($this->datetime_subscribed)) ? $this->datetime_subscribed->greaterThanOrEqualTo($one_month) : false;
        return $is_subscribed;
    }


    public function setSubscribed($stripe = [])
    {
        $this->datetime_subscribed = now();
        $this->lastpayment_datetime = now()->toDateTimeString();

        return $this;
    }


    public function setUnsubscribe()
    {
        $this->datetime_subscribed = null;
        $this->last_payment_date = null;
        return $this;
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $appends = ['avatarFullUrl','name','formattedDate'];

    public function getNameAttribute()
    {
        return $this->attributes['firstName'] . ' ' . $this->attributes['lastName'];
    }
    public function getFormattedDateAttribute()
    {
        $date=Carbon::parse($this->birthDate)->format('d-m-Y');
        return $date;
    }
    public function getAvatarFullUrlAttribute()
    {
        return is_null($this->attributes['avatar']) ? asset('defaultImages/default-avatar.png')
            : Storage::disk('s3')->url($this->attributes['avatar']);
    }
    public function userInterests()
    {
        return $this->hasMany(UserInterest::class, 'userId', 'id');
    }
    public function userActivePackages()
    {
        return $this->hasOne(Purchased::class, 'userId', 'id')->where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION);
    }

    public function userSongs()
    {
        return $this->hasMany(Song::class, 'userId', 'id');
    }

    public function userBuyingHistories(){
        return $this->hasMany(Purchased::class,'userId','id');
    }
    public function userFavouritSongs(){
        return $this->hasMany(Favorite::class,'userId','id');
    }
    public function purchasedALbum(){
        return $this->hasMany(Purchased::class,'userId','id');
    }
    public function downloadSongs(){
        return $this->hasMany(DownloadedSongs::class,'userId','id');
    }
    public function userArtist()
    {
        return $this->hasMany(Follower::class, 'userId', 'id');
    }
    public function favoriteSong()
    {
        return $this->hasMany(Favorite::class, 'userId', 'id');
    }
    public function playlists(){
        return $this->hasMany(Playlist::class,'userId','id');
    }

    public function card()
    {
        return $this->hasMany(Card::class,'userId','id');
    }
}
