<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInterest extends Model
{
    use HasFactory,SoftDeletes;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT = 'deletedAt';


    protected $fillable = [
        'userId', 'categoryId'
    ];

    public function getDateFormat()
    {
        return 'U';
    }
    public function categroy(){
        return $this->hasMany(Category::class,'id');
    }
}
