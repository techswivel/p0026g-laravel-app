<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    use HasFactory,SoftDeletes;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT='deletedAt';

    protected $fillable = [
        'title', 'userId'
    ];

    public function getDateFormat()
    {
        return 'U';
    }
    public function playlistSongs()
    {
        return $this->hasMany(PlaylistSongs::class, 'playlistId', 'id');
    }
}
