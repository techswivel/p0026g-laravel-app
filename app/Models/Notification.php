<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory,SoftDeletes;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT='deletedAt';

    protected $fillable = [
        'userId', 'title', 'message', 'userType',
         'status', 'notificationType', 'deletedBy'
    ];

    public function getDateFormat()
    {
        return 'U';
    }

    public function getArtist()
    {
        return $this->hasOne(User::class, 'id', 'userId');
    }
}
