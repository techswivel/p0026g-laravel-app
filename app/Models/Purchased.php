<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Purchased extends Model
{
    use HasFactory,SoftDeletes;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT='deletedAt';

    protected $fillable = [
        'userId', 'songId', 'albumId', 'purchaseType'
        , 'amount', 'transactionId', 'paidAmount', 'isSong'
        , 'packageId', 'itemType', 'purchaseStatus', 'isAcknowledged', 'subType', 'response'
    ];
    public function getSongs()
    {
        return $this->hasMany(Song::class, 'id', 'songId')->withTrashed();
    }
    public function getAlbums()
    {
        return $this->hasMany(Album::class, 'id', 'albumId')->withTrashed();
    }
    public function getDateFormat()
    {
        return 'U';
    }
    public function getSong()
    {
        return $this->hasOne(Song::class, 'id', 'songId');
    }
    public function getAlbum()
    {
        return $this->hasOne(Album::class, 'id', 'albumId');
    }
    public function getPackage()
    {
        return $this->hasOne(Package::class, 'id', 'packageId');
    }
    public function getCard()
    {
        return $this->hasOne(Card::class, 'id', 'cardId');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'userId','id');
    }
    public function song()
    {
        return $this->belongsTo(Song::class,'songId','id')->withTrashed();
    }
    public function album()
    {
        return $this->belongsTo(Album::class,'albumId','id')->withTrashed();
    }
    public function package()
    {
        return $this->hasOne(Package::class, 'id', 'packageId');
    }
}
