<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PlaylistSongs extends Model
{
    use HasFactory,SoftDeletes;
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT = 'deletedAt';

    public function getDateFormat()
    {
        return 'U';
    }

    protected $fillable = [
        'playlistId', 'songId'
    ];
    public function getPlaylist()
    {
        return $this->belongsTo(Playlist::class, 'playlistId', 'id');
    }
    public function getSongs()
    {
        return $this->hasMany(Song::class, 'id', 'songId');
    }
}
