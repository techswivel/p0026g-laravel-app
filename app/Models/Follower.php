<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    use HasFactory;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $fillable = [
        'userId', 'artistId'
    ];
    public function user()
    {
        return $this->belongsTo(User::class,'userId','id');
    }

    public function getDateFormat()
    {
        return 'U';
    }
}
