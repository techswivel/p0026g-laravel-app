<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class Album extends Model
{
    use HasFactory,SoftDeletes;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT='deletedAt';

    protected $fillable = [
        'userId', 'name', 'thumbnail', 'preOrderCount'
        , 'preOrderReleaseDate', 'type' ,'createdType','createdBy','deletedBy','totalPreOrderCount','sku','iAppPurchaseIdWd','iAppPurchaseIdWod','iAppPurchasePrice','discountedPrice'
    ];
    protected $appends = ['avatarFullUrl'];

    public function getAvatarFullUrlAttribute()
    {
        return is_null($this->attributes['thumbnail']) ? asset('defaultImages/default-avatar.png')
            : Storage::disk('s3')->url($this->attributes['thumbnail']);
    }
    public function getSongRelation()
    {
        return $this->belongsToMany('App\Models\Song','album_songs','albumId','songId')->with('getArtist');
    }

    public function getDateFormat()
    {
        return 'U';
    }


    public function albumSongs()
    {
        return $this->hasMany(AlbumSongs::class, 'albumId', 'id');
    }

    public function getArtist()
    {
        return $this->hasOne(User::class, 'id', 'userId');
    }

    public function purchase()
    {
        return $this->hasMany(Purchased::class, 'albumId', 'id');
    }

}
