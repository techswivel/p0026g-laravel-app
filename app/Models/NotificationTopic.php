<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationTopic extends Model
{
    use HasFactory,SoftDeletes;
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT='deletedAt';
  protected $table='notification_topics';
    protected $fillable=[
      'notificationId','topicName'
    ];
    public function getDateFormat()
    {
        return 'U';
    }
}
