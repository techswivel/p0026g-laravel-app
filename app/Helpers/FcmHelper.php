<?php

namespace App\Helpers;

use stdClass;
use Exception;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Illuminate\Support\Facades\Log;
use App\Traits\loggerExceptionTrait;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging;



class FcmHelper
{

    use loggerExceptionTrait;


    public $factory;
    public $messaging;
    public function __construct(Messaging $messaging)
    {

        /*Sending service account information to google firebase */
        $this->factory = (new Factory())
            ->withServiceAccount(config('firebase.projects.app.credentials.file'));
        /* Authenticate application using service account */
        $this->auth = $this->factory->createAuth();
        $this->messaging = $messaging;
}

    /**
     * topic
     * send notification to a topic or all topics subscribed by user
     * @param  mixed $data
     * @param  mixed $title
     * @param  mixed $topic
     * @return bool
     */
    public function topic($data, $title, $topic):bool
    {
        try {
                $message = CloudMessage::withTarget('topic', $topic)
                    ->withNotification(['title' => ucfirst($title), 'body' => $data])
                    ->withDefaultSounds();
                $this->messaging->send($message);
                return true;
        } catch (Exception $e) {
            $this->saveExceptionLog($e,'Firebase send notification to a topic');
            return false;
        }
    }
    public function topicWithData($data, $title, $topic, $body):bool
    {
        try {
                $message = CloudMessage::withTarget('topic', $topic)
                    ->withNotification(['title' => ucfirst($title), 'body' => $body])
                    ->withData(['data' => $data])
                    ->withDefaultSounds();
                $this->messaging->send($message);
                return true;
        } catch (Exception $e) {
            $this->saveExceptionLog($e,'Firebase send notification to a topic');
            return false;
        }
    }





}
