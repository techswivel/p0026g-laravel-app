<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use App\Traits\FileUploadTrait;
use App\Models\User;
use App\Models\Album;
use App\Models\AlbumSongs;
use App\Models\Purchased;
use App\Models\Song;
use App\Models\Notification;
use App\Models\Earning;
use App\Models\MonthlyNotificationCount;
use Carbon\Carbon;
use App\Enums\SongPreOrderEnum;
use App\Enums\AlbumPreOrderEnum;
use App\Enums\PurchasedItemTypeEnum;
use App\Enums\WithdrawalStatusEnum;

class Helper
{
    public static function urlPath($path)
    {
        return FileUploadTrait::getfileUrl($path);
    }

    public static function carbonDate($date)
    {
        return Carbon::parse($date)->format('d/m/Y.g:i A');
    }

    public static function totalSong($id)
    {
        return count(AlbumSongs::where('albumId',$id)->get());
    }


    public static function checkItem($id)
    {
        $item = Purchased::withTrashed()->with('song','album')->find($id);
        if($item->itemType == PurchasedItemTypeEnum::SONG){
            return 'Song';
        }else if($item->itemType == PurchasedItemTypeEnum::ALBUM){
            return 'Album';
        }else if($item->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG){
            return '[Preorder] Song';
        }else if($item->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM){
            return '[Preorder] Album';
        }else{
            return 'Plan';
        }
    }


    public static function totalEarning($id)
    {
        $totalEarning =  Purchased::whereHas('song', function($buyer) use ($id){
                $buyer->where('userId', $id);
        })->orWhereHas('album', function($buyer) use ($id){
                $buyer->where('userId', $id);
        })->get()->sum('amount');
        return Helper::number_format_short($totalEarning);
    }

    public static function totalSongEarning()
    {
        $totalSongEarning =  Purchased::whereHas('song', function($buyer) {
                $buyer->where('userId', auth()->user()->id);
        })->get()->sum('amount');
        return Helper::number_format_short($totalSongEarning);
    }

    public static function preorderBookEarning($id)
    {
        $preorderEarning =  Purchased::whereHas('song', function($buyer) use ($id){
                $buyer->where('userId', $id)->where('type',SongPreOrderEnum::PreOrder);
        })->orWhereHas('album', function($buyer) use ($id){
                $buyer->where('userId', $id)->where('type',AlbumPreOrderEnum::PreOrder);
        })->get()->sum('amount');
        return Helper::number_format_short($preorderEarning);
    }

    public static function totalNotification($id)
    {
        $date = Carbon::now()->startofMonth();
        $notification = Notification::withTrashed()->where('userId',$id)->whereBetween('createdAt', array(Carbon::createFromDate($date->format('Y-m-d')." 00:00")->timestamp,Carbon::createFromDate($date->format('Y-m-d')." 23:59")->endOfMonth()->timestamp))->count();
        $notificaitionCount = MonthlyNotificationCount::first();
        if($notificaitionCount){
            $monthlyNotificationCount = $notificaitionCount->totalNotificationCount;
            $notification = $monthlyNotificationCount-$notification;
        }
        return array($notification,$monthlyNotificationCount);
    }

    public static function totalAlbumEarning()
    {
        $totalAlbumEarning =  Purchased::whereHas('album', function($buyer){
                $buyer->where('userId', auth()->user()->id);
        })->get()->sum('amount');
        return Helper::number_format_short($totalAlbumEarning);
    }

    public static function artistTotalSong($id)
    {
        $song = Song::where('userId',$id)->where('type',SongPreOrderEnum::Song)->get();
        return count($song);
    }

    public static function artistTotalAlbum($id)
    {
        $album = Album::where('userId',$id)->where('type',AlbumPreOrderEnum::Album)->get();
        return count($album);
    }

    public static function artistPaidSong($id)
    {
        $song = Song::where([
            'userId' => $id,
            'isPaid' =>1,
            'type' =>SongPreOrderEnum::Song,
            ])->get();
        return count($song);
    }

    public static function artistPaidAlbum($id)
    {
        $album = Album::where([
            'userId' => $id,
            'albumStatus' => 'Paid',
            'type' =>AlbumPreOrderEnum::Album,
            ])->get();
        return count($album);
    }

    public static function number_format_short( $n, $precision = 2 ) {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }
      // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
      // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }
        return $n_format . $suffix;
    }

    public static function countArtist()
    {
        $artists =  User::role('artist')->count();
        return Helper::number_format_short($artists);
    }

    public static function countUser()
    {
        $users =  User::role('user')->count();
        return Helper::number_format_short($users);
    }

    public static function totatBuyingSales()
    {
        $sales = Purchased::where('itemType','!=',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->sum('amount');
        return Helper::number_format_short($sales);
    }

    public static function totatSubscriptionsSales()
    {
        $sales = Purchased::where('itemType',PurchasedItemTypeEnum::PLAN_SUBSCRIPTION)->sum('amount');
        return Helper::number_format_short($sales);
    }

    public static function purchaseItem($item)
    {
        if($item->itemType == PurchasedItemTypeEnum::SONG){
            return 'Song';
        }else if($item->itemType == PurchasedItemTypeEnum::ALBUM){
            return 'Album';
        }else if($item->itemType == PurchasedItemTypeEnum::PRE_ORDER_SONG){
            return '[Preorder] Song';
        }else if($item->itemType == PurchasedItemTypeEnum::PRE_ORDER_ALBUM){
            return '[Preorder] Album';
        }else{
            return 'Plan';
        }
    }

    public static function totalWithdrawalAmount($id)
    {
        $earning = Earning::where('userId',$id)->where('withdrawalStatus',WithdrawalStatusEnum::Paid)->sum('amount');
        return Helper::number_format_short($earning);
    }

    public static function totalAvailableAmount($id)
    {
        $totalEarning =  Purchased::whereHas('song', function($buyer) use ($id){
                $buyer->where('userId', $id);
            })->orWhereHas('album', function($buyer) use ($id){
                $buyer->where('userId', $id);
            })->get()->sum('amount');

        $withdrawalEarning = Earning::where('userId',$id)->where('withdrawalStatus',WithdrawalStatusEnum::Paid)->sum('amount');
        return Helper::number_format_short($totalEarning - $withdrawalEarning);
        # code...
    }

    public static function artistRequest($id)
    {
        if(Earning::where('userId',$id)->where('withdrawalStatus',WithdrawalStatusEnum::Requested)->exists()){
            return 'Yes';
        }else{
            return 'No';
        }
    }
}
