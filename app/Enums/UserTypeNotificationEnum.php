<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static AndroidUser()
 * @method static static IOSUser()
 * @method static static PackageUser()
 */
final class UserTypeNotificationEnum extends Enum
{
    const AndroidUser =   'AndroidUser';
    const IOSUser =   'IOSUser';
    const PackageUser = 'PackageUser';
}
