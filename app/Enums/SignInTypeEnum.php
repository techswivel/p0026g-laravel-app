<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SIMPLE()
 * @method static static SOCIAL()
 */
final class SignInTypeEnum extends Enum
{
    const SIMPLE =   "SIMPLE";
    const SOCIAL =   "SOCIAL";
}
