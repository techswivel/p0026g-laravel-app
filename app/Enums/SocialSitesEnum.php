<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static FACEBOOK()
 * @method static static GMAIL()
 * @method static static APPLE()
 */
final class SocialSitesEnum extends Enum
{
    const FACEBOOK = 'FACEBOOK';
    const GMAIL = 'GMAIL';
    const APPLE = 'APPLE';
}
