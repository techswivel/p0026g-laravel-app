<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Active()
 * @method static static Expired()
 */
final class PackageStatusEnum extends Enum
{
    const Active =   'Active';
    const Expired =   'Expired';
}
