<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Pending()
 * @method static static Approved()
 * @method static static Rejected()
 * @method static static DeletedByAdmin()
 * @method static static DeletedByArtist()
 */
final class NotificationStatusEnum extends Enum
{
    const Pending =   'Pending';
    const Approved =   'Approved';
    const Rejected = 'Rejected';
    const DeletedByAdmin =   'DeletedByAdmin';
    const DeletedByArtist = 'DeletedByArtist';
}
