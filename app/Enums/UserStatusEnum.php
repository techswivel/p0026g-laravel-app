<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Active()
 * @method static static Block()
 */
final class UserStatusEnum extends Enum
{
    const Active =   'Active';
    const Block =   'Block';
}
