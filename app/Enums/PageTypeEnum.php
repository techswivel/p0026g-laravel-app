<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static TermAndCondition()
 * @method static static PrivacyPolicy()
 */
final class PageTypeEnum extends Enum
{
    const TermAndCondition =   'TermAndCondition';
    const PrivacyPolicy =   'PrivacyPolicy';
}
