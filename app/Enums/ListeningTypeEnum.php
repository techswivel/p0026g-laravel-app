<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Song()
 * @method static static Album()
 * @method static static Artist()
 */
final class ListeningTypeEnum extends Enum
{
    const Song =   'Song';
    const Album =   'Album';
    const Artist = 'Artist';
}
