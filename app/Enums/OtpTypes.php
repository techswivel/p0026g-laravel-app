<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static EMAIL()
 * @method static static PHONE_NUMBER()
 * @method static static FORGET_PASSWORD()
 */
final class OtpTypes extends Enum
{
    const EMAIL  = 'EMAIL';
    const PHONE_NUMBER  = 'PHONE_NUMBER';
    const FORGET_PASSWORD ='FORGET_PASSWORD';
}
