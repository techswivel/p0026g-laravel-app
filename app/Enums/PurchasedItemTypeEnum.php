<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SONG()
 * @method static static ALBUM()
 * @method static static PLAN_SUBSCRIPTION()
 */
final class PurchasedItemTypeEnum extends Enum
{
    const SONG =   'SONG';
    const ALBUM =   'ALBUM';
    const PLAN_SUBSCRIPTION =   'PLAN_SUBSCRIPTION';
    const PRE_ORDER_SONG =   'PRE_ORDER_SONG';
    const PRE_ORDER_ALBUM =   'PRE_ORDER_ALBUM';
}
