<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Song()
 * @method static static PreOrder()
 * @method static static PreOrderAlbum()
 */
final class SongPreOrderEnum extends Enum
{
    const Song =   'Song';
    const PreOrder =   'PreOrder';
    const PreOrderAlbum = 'PreOrderAlbum';
}
