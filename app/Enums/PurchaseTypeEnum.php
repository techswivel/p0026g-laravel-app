<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static GOOGLE_PAY()
 * @method static static APPLE_PAY()
 * @method static static PAYPAL()
 * @method static static CARD()
 */
final class PurchaseTypeEnum extends Enum
{
    const GOOGLE_PAY =   'GOOGLE_PAY';
    const APPLE_PAY =   'APPLE_PAY';
    const PAYPAL =   'PAYPAL';
    const CARD =   'CARD';
}
