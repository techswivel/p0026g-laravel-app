<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Album()
 * @method static static PreOrder()
 */
final class AlbumPreOrderEnum extends Enum
{
    const Album =   'Album';
    const PreOrder =   'PreOrder';
}
