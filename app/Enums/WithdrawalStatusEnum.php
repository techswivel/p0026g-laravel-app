<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Requested()
 * @method static static Paid()
*/
final class WithdrawalStatusEnum extends Enum
{
    const Requested =   'Requested';
    const Paid =   'Paid';
}
