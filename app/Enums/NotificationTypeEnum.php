<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Admin()
 * @method static static Artist()
 */
final class NotificationTypeEnum extends Enum
{
    const Admin =   'Admin';
    const Artist =   'Artist';
}
