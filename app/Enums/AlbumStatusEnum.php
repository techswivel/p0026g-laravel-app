<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Free()
 * @method static static Paid()
 * @method static static Partially()
 */
final class AlbumStatusEnum extends Enum
{
    const Free =   'Free';
    const Paid =   'Paid';
    const Partially = 'Partially';
}
