<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::redirect('/', '/artist/login');
Route::redirect('/artist', '/artist/login');
Route::post('/pubsub', [\App\Http\Controllers\PubsubController::class, 'subscribe']);
route::get('/artist/login', [\App\Http\Controllers\HomeController::class, 'artistLogin'])->middleware(['checkAuthenticated']);

Route::group(['prefix' => '/artist', 'middleware' => ['auth', 'verified', 'checkUserStatus', 'role:artist']], function () {

    // Dashboard
    Route::group(['prefix' => '/dashboard'], function () {
        Route::get('/', [App\Http\Controllers\Artist\DashBoardController::class, 'index'])->name('artist.dashboard');
        Route::get('/purchase/detail/{id}', [App\Http\Controllers\Artist\DashBoardController::class, 'purchaseDetail'])->name('artist.dashboard.buyerDetail');
        Route::get('/salesReport/{report}', [App\Http\Controllers\Artist\DashBoardController::class, 'salesReport'])->name('artist.dashboard.salesReport');
    });

    // Profile
    Route::group(['prefix' => '/profile'], function () {
        Route::get('/', [App\Http\Controllers\Artist\ArtistProfileController::class, 'index'])->name('artist.profile');
        Route::view('/update/password', 'artist.auth.password.updatePassword');
        Route::post('/update', [App\Http\Controllers\Artist\ArtistProfileController::class, 'update'])->name('artist.profile.update');
    });

    // Song
    Route::group(['prefix' => '/song'], function () {
        Route::post('/', [App\Http\Controllers\Artist\SongController::class, 'store'])->name('artist.song');
        Route::delete('/{id}', [App\Http\Controllers\Artist\SongController::class, 'delete'])->name('artist.song.delete');
        Route::get('/{id}', [App\Http\Controllers\Artist\SongController::class, 'show'])->name('artist.song.show');
        Route::post('/update', [App\Http\Controllers\Artist\SongController::class, 'update'])->name('artist.song.update');
    });

    // Album
    Route::group(['prefix' => '/album'], function () {
        Route::post('/', [App\Http\Controllers\Artist\AlbumController::class, 'store'])->name('artist.album');
        Route::get('/listTheSong/{artistId}', [App\Http\Controllers\Artist\AlbumController::class, 'listTheSong'])->name('artist.listTheSong');
        Route::get('/detail/{id}', [App\Http\Controllers\Artist\AlbumController::class, 'albumDetail'])->name('artist.detail');
        Route::post('/select/song', [App\Http\Controllers\Artist\AlbumController::class, 'selectSong'])->name('artist.select.song');
        Route::delete('/{id}', [App\Http\Controllers\Artist\AlbumController::class, 'delete'])->name('artist.album.delete');
        Route::delete('/song/{id}', [App\Http\Controllers\Artist\AlbumController::class, 'removeSong'])->name('artist.album.song.delete');
        Route::get('/{id}', [App\Http\Controllers\Artist\AlbumController::class, 'show'])->name('artist.show');
        Route::get('/albumStatus/{id}', [App\Http\Controllers\Artist\AlbumController::class, 'AlbumStatus'])->name('artist.paid.status');
        Route::post('/update', [App\Http\Controllers\Artist\AlbumController::class, 'update'])->name('artist.album.update');
    });

    // Notification
    Route::group(['prefix' => '/notification'], function () {
        Route::get('/', [App\Http\Controllers\Artist\NotificationController::class, 'index'])->name('artist.notification');
        Route::post('/', [App\Http\Controllers\Artist\NotificationController::class, 'store'])->name('artist.notification.store');
        Route::get('/{id}', [App\Http\Controllers\Artist\NotificationController::class, 'show'])->name('artist.notification.show');
        Route::delete('/{id}', [App\Http\Controllers\Artist\NotificationController::class, 'delete'])->name('artist.notification.delete');
    });
    // Withdrawal Earning
    Route::group(['prefix' => '/withdrawal/earning'], function () {
        Route::get('/', [App\Http\Controllers\Artist\WithdrawalEarnnigController::class, 'index'])->name('artist.WithdrawalEarnnig');
        Route::post('/store/account', [App\Http\Controllers\Artist\CardController::class, 'store'])->name('artist.WithdrawalEarnnig.card.store');
        Route::get('/account/detail/{id}', [App\Http\Controllers\Artist\CardController::class, 'show'])->name('artist.WithdrawalEarnnig.card.show');
        Route::post('/update/account', [App\Http\Controllers\Artist\CardController::class, 'update'])->name('artist.WithdrawalEarnnig.card.update');
        Route::post('/', [App\Http\Controllers\Artist\WithdrawalEarnnigController::class, 'withdrawalRequest'])->name('artist.Withdrawal');
        Route::post('/again', [App\Http\Controllers\Artist\WithdrawalEarnnigController::class, 'withdrawalRequestAgain'])->name('artist.Withdrawal.again');
    });
    //Preorder
    Route::group(['prefix' => '/preorder'], function () {
        //PreOrder Song
        Route::group(['prefix' => '/song'], function () {
            Route::post('/', [App\Http\Controllers\Artist\SongPreorderController::class, 'store'])->name('artist.preorder.song');
            Route::delete('/{id}', [App\Http\Controllers\Artist\SongPreorderController::class, 'delete'])->name('artist.preorder.song.delete');
            Route::get('/{id}', [App\Http\Controllers\Artist\SongPreorderController::class, 'show'])->name('artist.preorder.song.show');
            Route::post('/update', [App\Http\Controllers\Artist\SongPreorderController::class, 'update'])->name('artist.preorder.song.update');
            Route::get('/buyerList/{id}', [App\Http\Controllers\Artist\SongPreorderController::class, 'preorderSongPurchaseList'])->name('artist.preorder.song.buyerList');
        });
        //PreOrder Album
        Route::group(['prefix' => '/album'], function () {
            Route::post('/', [App\Http\Controllers\Artist\AlbumPreorderController::class, 'store'])->name('artist.preorder.album');
            Route::post('/song', [App\Http\Controllers\Artist\AlbumPreorderController::class, 'storeAlbumSong'])->name('artist.preorder.album.song');
            Route::post('/song/update', [App\Http\Controllers\Artist\AlbumPreorderController::class, 'updateAlbumSong'])->name('artist.preorder.album.song.update');
            Route::delete('/song/{id}', [App\Http\Controllers\Artist\AlbumPreorderController::class, 'removeAlbumSong'])->name('artist.preorder.album.song.delete');
            Route::delete('/{id}', [App\Http\Controllers\Artist\AlbumPreorderController::class, 'delete'])->name('artist.preorder.album.delete');
            Route::get('/{id}', [App\Http\Controllers\Artist\AlbumPreorderController::class, 'show'])->name('artist.preorder.album.show');
            Route::post('/update', [App\Http\Controllers\Artist\AlbumPreorderController::class, 'update'])->name('artist.preorder.album.update');
            Route::get('/buyerList/{id}', [App\Http\Controllers\Artist\AlbumPreorderController::class, 'preorderAlbumPurchaseList'])->name('artist.preorder.Album.buyerList');
        });
        Route::get('/albumSong/{id}', [App\Http\Controllers\Artist\AlbumPreorderController::class, 'getPreorderSong'])->name('artist.preorder.albumSong');
    });
    Route::get('/earningStats/item/{id}', [App\Http\Controllers\Artist\EarningStatsController::class, 'purchaseItemDetail'])->name('artist.purchase.itemDetail');
    Route::get('/terms-conditions', [App\Http\Controllers\Artist\TermsAndConditionsController::class, 'index'])->name('artist.terms');
    Route::get('/privacy-policies', [App\Http\Controllers\Artist\PrivacyAndPolicyController::class, 'index'])->name('artist.policy');
    Route::post('/carbon/date/{id}', [App\Http\Controllers\Artist\SongPreorderController::class, 'carbonDate'])->name('artist.preorder.carbonDate');
});


Route::get('/fetchData', [App\Http\Controllers\HomeController::class, 'anyData'])->name('datatables.data');
Route::get('/getUsers', [App\Http\Controllers\HomeController::class, 'getUsers'])->name('listing');

Route::redirect('/admin', '/admin/login');
Route::get('/admin/login', [\App\Http\Controllers\HomeController::class, 'loginPage'])->name('admin.login')->middleware('checkAuthenticated');
Route::get('/admin/forgot-password', [\App\Http\Controllers\HomeController::class, 'forgetPassword']);
Route::group(['prefix' => '/admin', 'middleware' => ['role:admin', 'auth']], function () {
    /*============================Admin auth routes goes here========================== */

    Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('/dashboard/salesReport/{report}', [App\Http\Controllers\Admin\DashboardController::class, 'salesReport'])->name('admin.dashboard.salesReport');

    Route::get('/changePassword', [\App\Http\Controllers\Admin\AuthController::class, 'changePassword']);
    Route::post('change-password', [\App\Http\Controllers\Admin\AuthController::class, 'updatePassword'])->name('change.password');
    /*==============================================App users Routes============================================= */
    Route::resource('users', \App\Http\Controllers\Admin\UserController::class);
    Route::get('/userBlock/{id}', [App\Http\Controllers\Admin\UserController::class, 'userBlock'])->name('userBlock');
    Route::get('users/detail/{id}/details', [\App\Http\Controllers\Admin\UserController::class, 'listing']);
    Route::get('usersDetail/{id}', [\App\Http\Controllers\Admin\UserController::class, 'userDetail']);
    Route::post('users/update/{id}', [\App\Http\Controllers\Admin\UserController::class, 'update']);
    Route::get('user/delete/{id}', [\App\Http\Controllers\Admin\UserController::class, 'destroy']);
    Route::get('favouriteDelete/{id}', [\App\Http\Controllers\Admin\UserController::class, 'favouriteDelete'])->name('favouriteDelete');
    Route::post('playlistUpdate', [\App\Http\Controllers\Admin\PlaylistController::class, 'playlistUpdate'])->name('playlistUpdate');
    Route::get('playlistDeleteSong/{id}', [\App\Http\Controllers\Admin\PlaylistController::class, 'playlistDelete']);
    //    Route::get('/listTheSong',[App\Http\Controllers\Admin\PlaylistController::class, 'listTheSong'])->name('artist.listTheSong');
    Route::get('/detail/{id}', [App\Http\Controllers\Admin\PlaylistController::class, 'albumDetail'])->name('album.detail');
    Route::get('playlist/detail/{id}', [App\Http\Controllers\Admin\PlaylistController::class, 'playlistDetail'])->name('playlist.detail');
    Route::delete('/playlistdeletesong/{id}', [App\Http\Controllers\Admin\PlaylistController::class, 'removeSong'])->name('playlistdeletesong');

    // Active Artists
    Route::group(['prefix' => '/active/artists'], function () {
        Route::get('/', [\App\Http\Controllers\Admin\ActiveArtistController::class, 'index'])->name('activeArtists');
        Route::post('/create', [\App\Http\Controllers\Admin\ActiveArtistController::class, 'store'])->name('activeArtists.create');
        Route::get('/show/{id}', [\App\Http\Controllers\Admin\ActiveArtistController::class, 'show'])->name('activeArtists.show');
        Route::post('/update', [\App\Http\Controllers\Admin\ActiveArtistController::class, 'update'])->name('activeArtists.update');
        Route::delete('/delete/{id}', [\App\Http\Controllers\Admin\ActiveArtistController::class, 'delete'])->name('activeArtists.delete');
        Route::delete('/block/{id}', [\App\Http\Controllers\Admin\ActiveArtistController::class, 'block'])->name('activeArtists.block');
    });
    Route::get('/artists/detail/{id}', [\App\Http\Controllers\Admin\ActiveArtistController::class, 'detail'])->name('activeArtists.detail');
    Route::get('/artists/complete-detail/{id}', [\App\Http\Controllers\Admin\ActiveArtistController::class, 'artistCompleteDetail']);
    // Block Artists
    Route::group(['prefix' => '/block/artists'], function () {
        Route::get('/', [\App\Http\Controllers\Admin\BlockArtistController::class, 'index'])->name('blockArtists');
        Route::delete('/unblock/{id}', [\App\Http\Controllers\Admin\BlockArtistController::class, 'unblock'])->name('blockArtists.unblock');
    });
    // Song
    Route::group(['prefix' => '/song'], function () {
        Route::post('/', [App\Http\Controllers\Admin\SongController::class, 'store'])->name('admin.artist.song');
        Route::delete('/{id}', [App\Http\Controllers\Admin\SongController::class, 'delete'])->name('admin.artist.song.delete');
        Route::get('/{id}', [App\Http\Controllers\Admin\SongController::class, 'show'])->name('admin.artist.song.show');
        Route::post('/update', [App\Http\Controllers\Admin\SongController::class, 'update'])->name('admin.artist.song.update');
    });
    // Album
    Route::group(['prefix' => '/album'], function () {
        Route::post('/', [App\Http\Controllers\Admin\AlbumController::class, 'store'])->name('admin.artist.album');
        Route::get('/listTheSong/{artistId}', [App\Http\Controllers\Admin\AlbumController::class, 'listTheSong'])->name('admin.artist.listTheSong');
        Route::get('/detail/{id}', [App\Http\Controllers\Admin\AlbumController::class, 'albumDetail'])->name('admin.artist.detail');
        Route::post('/select/song', [App\Http\Controllers\Admin\AlbumController::class, 'selectSong'])->name('admin.artist.select.song');
        Route::delete('/{id}', [App\Http\Controllers\Admin\AlbumController::class, 'delete'])->name('admin.artist.album.delete');
        Route::delete('/song/{id}', [App\Http\Controllers\Admin\AlbumController::class, 'removeSong'])->name('admin.artist.album.song.delete');
        Route::get('/{id}', [App\Http\Controllers\Admin\AlbumController::class, 'show'])->name('admin.artist.show');
        Route::get('/albumStatus/{id}', [App\Http\Controllers\Admin\AlbumController::class, 'AlbumStatus'])->name('admin.artist.paid.status');
        Route::post('/update', [App\Http\Controllers\Admin\AlbumController::class, 'update'])->name('admin.artist.album.update');
    });
    //Preorder
    Route::group(['prefix' => '/preorder'], function () {
        //PreOrder Song
        Route::group(['prefix' => '/song'], function () {
            Route::post('/', [App\Http\Controllers\Admin\SongPreorderController::class, 'store'])->name('admin.artist.preorder.song');
            Route::delete('/{id}', [App\Http\Controllers\Admin\SongPreorderController::class, 'delete'])->name('admin.artist.preorder.song.delete');
            Route::get('/{id}', [App\Http\Controllers\Admin\SongPreorderController::class, 'show'])->name('admin.artist.preorder.song.show');
            Route::post('/update', [App\Http\Controllers\Admin\SongPreorderController::class, 'update'])->name('admin.artist.preorder.song.update');
            Route::get('/buyerList/{id}', [App\Http\Controllers\Admin\SongPreorderController::class, 'preorderSongPurchaseList'])->name('admin.artist.preorder.song.buyerList');
        });

        //PreOrder Album
        Route::group(['prefix' => '/album'], function () {
            Route::post('/', [App\Http\Controllers\Admin\AlbumPreorderController::class, 'store'])->name('admin.artist.preorder.album');
            Route::post('/song', [App\Http\Controllers\Admin\AlbumPreorderController::class, 'storeAlbumSong'])->name('admin.artist.preorder.album.song');
            Route::post('/song/update', [App\Http\Controllers\Admin\AlbumPreorderController::class, 'updateAlbumSong'])->name('admin.artist.preorder.album.song.update');
            Route::delete('/song/{id}', [App\Http\Controllers\Admin\AlbumPreorderController::class, 'removeAlbumSong'])->name('admin.artist.preorder.album.song.delete');
            Route::delete('/{id}', [App\Http\Controllers\Admin\AlbumPreorderController::class, 'delete'])->name('admin.artist.preorder.album.delete');
            Route::get('/{id}', [App\Http\Controllers\Admin\AlbumPreorderController::class, 'show'])->name('admin.artist.preorder.album.show');
            Route::post('/update', [App\Http\Controllers\Admin\AlbumPreorderController::class, 'update'])->name('admin.artist.preorder.album.update');
            Route::get('/buyerList/{id}', [App\Http\Controllers\Admin\AlbumPreorderController::class, 'preorderAlbumPurchaseList'])->name('admin.artist.preorder.Album.buyerList');
        });

        Route::get('/albumSong/{id}', [App\Http\Controllers\Admin\AlbumPreorderController::class, 'getPreorderSong'])->name('admin.artist.preorder.albumSong');
    });
    Route::post('/carbon/date/{id}', [App\Http\Controllers\Admin\SongPreorderController::class, 'carbonDate'])->name('admin.artist.preorder.carbonDate');
    Route::get('/earningStats/item/{id}', [App\Http\Controllers\Admin\EarningStatsController::class, 'purchaseItemDetail'])->name('admin.artist.purchase.itemDetail');
    // Package
    Route::group(['prefix' => '/package'], function () {
        Route::get('/', [App\Http\Controllers\Admin\PackageController::class, 'index'])->name('admin.packages');
        Route::post('/', [App\Http\Controllers\Admin\PackageController::class, 'store'])->name('admin.packages.store');
        Route::get('/name', [App\Http\Controllers\Admin\PackageController::class, 'package'])->name('admin.packages.name');
        Route::delete('/{id}', [App\Http\Controllers\Admin\PackageController::class, 'delete'])->name('admin.packages.delete');
        Route::get('/show/{id}', [App\Http\Controllers\Admin\PackageController::class, 'show'])->name('admin.packages.show');
        Route::post('/update', [App\Http\Controllers\Admin\PackageController::class, 'update'])->name('admin.packages.update');
    });
    // Artist Notification in admin panel
    Route::group(['prefix' => '/artist/notifications'], function () {
        Route::get('/', [App\Http\Controllers\Admin\ArtistNotificationController::class, 'index'])->name('admin.artist.notification');
        Route::get('/detail/{id}', [App\Http\Controllers\Admin\ArtistNotificationController::class, 'notificationDetail'])->name('admin.artist.notification.detail');
        Route::delete('/reject/{id}', [App\Http\Controllers\Admin\ArtistNotificationController::class, 'rejectNotification'])->name('admin.artist.notification.reject');
        Route::delete('/approve/{id}', [App\Http\Controllers\Admin\ArtistNotificationController::class, 'approveNotification'])->name('admin.artist.notification.approve');
        Route::delete('/delete/{id}', [App\Http\Controllers\Admin\ArtistNotificationController::class, 'deleteNotification'])->name('admin.artist.notification.delete');
    });
    // Artist Notification in artist detail
    Route::group(['prefix' => '/notification'], function () {
        Route::post('/', [App\Http\Controllers\Admin\NotificationController::class, 'store'])->name('admin.artist.notification.store');
        Route::get('/{id}', [App\Http\Controllers\Admin\NotificationController::class, 'show'])->name('admin.artist.notification.show');
        Route::delete('/{id}', [App\Http\Controllers\Admin\NotificationController::class, 'delete'])->name('admin.artistProfile.notification.delete');
    });
    // Purchase History
    Route::group(['prefix' => '/purchased/history'], function () {
        Route::get('/', [App\Http\Controllers\Admin\PurchasedHistoryController::class, 'index'])->name('admin.purchased.history');
        Route::get('/detail/{id}', [App\Http\Controllers\Admin\PurchasedHistoryController::class, 'purchaseDetail'])->name('admin.purchased.history.detail');
        Route::get('/{name}', [App\Http\Controllers\Admin\PurchasedHistoryController::class, 'purchaseStatus'])->name('admin.purchased.history.status');
    });
    // Artist Earning in admin panel
    Route::get('/artist/earning', [App\Http\Controllers\Admin\ArtistEarningController::class, 'index'])->name('admin.artist.earning');
    Route::post('/artist/earning/amount', [App\Http\Controllers\Admin\ArtistEarningController::class, 'sendAmount'])->name('admin.artist.earning.sendAmount');
    Route::get('/artist/earning/card/{id}', [App\Http\Controllers\Admin\ArtistEarningController::class, 'artistCard'])->name('admin.artist.earning.card');


    Route::get('/categories', [App\Http\Controllers\Admin\CategoryController::class, 'index'])->name('categories');
    Route::post('categories', [\App\Http\Controllers\Admin\CategoryController::class, 'store'])->name('categories.store');
    Route::get('categories/detail/{id}', [\App\Http\Controllers\Admin\CategoryController::class, 'categoryDetail'])->name('categories.detail');
    Route::get('categories-delete/{id}', [\App\Http\Controllers\Admin\CategoryController::class, 'destroy']);
    Route::get('categories/{id}/edit', [\App\Http\Controllers\Admin\CategoryController::class, 'edit']);
    Route::post('categories/{id}/update', [\App\Http\Controllers\Admin\CategoryController::class, 'update']);
    /* Send Notifications*/
    Route::get('notifications', [\App\Http\Controllers\Admin\NotificationController::class, 'index'])->name('create.notifications');
    Route::post('notificationsSend', [\App\Http\Controllers\Admin\NotificationController::class, 'send'])->name('notificationsSend');
    Route::get('approveNotificationSend/{artisId}/{notificationId}', [\App\Http\Controllers\Admin\NotificationController::class, 'approveNotificationSend'])->name('approveNotificationSend');
    Route::get('notificationViewDetail/{notificationId}', [\App\Http\Controllers\Admin\NotificationController::class, 'notificationDetail'])->name('notificationViewDetail');
    Route::get('notificationTopicDelete/{notificationId}', [\App\Http\Controllers\Admin\NotificationController::class, 'notificationDeleteTopic'])->name('notificationTopicDelete');
    Route::get('serverTimeZone', [\App\Http\Controllers\Admin\UserController::class, 'serverTimeZone'])->name('serverTimeZone');
    Route::resources(['content-type' => \App\Http\Controllers\Admin\ContentTypeController::class]);
    Route::get('termsConditions', [App\Http\Controllers\Admin\ContentTypeController::class, 'termConditions'])->name('admin.terms-conditions');
    Route::get('privacyPolicy', [App\Http\Controllers\Admin\ContentTypeController::class, 'privacyPolicy'])->name('admin.privacy-policy');
    Route::get('notification-setting', [\App\Http\Controllers\Admin\NotificationController::class, 'notificationSetting'])->name('notification-setting');
    Route::post('notificationLimit', [\App\Http\Controllers\Admin\NotificationController::class, 'artistLimitPerDayNotification'])->name('notificationLimit');
    Route::get('notificationArtistStatus', [\App\Http\Controllers\Admin\NotificationController::class, 'notificationStatusCheck'])->name('notificationArtistStatus');
});
Route::get('terms-conditions', [App\Http\Controllers\Admin\ContentTypeController::class, 'webViewTermConditions'])->name('terms-conditions');
Route::get('privacy-policy', [App\Http\Controllers\Admin\ContentTypeController::class, 'webViewPrivacyPolicy'])->name('privacy-policy');


Route::post('serverNotification/callback',[App\Http\Controllers\Api\InAppPurchase\AppPurchaseController::class,'InAppPurchaseServerNotification']);

/*==============================================Artisan routes here============================================= */
Route::get('artisan-login', [App\Http\Controllers\ArtisanCommandController::class, 'configurationPassword'])->middleware('artisan_view');
Route::post('artisanlogin', [App\Http\Controllers\ArtisanCommandController::class, 'checkConfigurationPassword'])->name('artisanlogin');
Route::get('php_artisan_cmd', [App\Http\Controllers\ArtisanCommandController::class, 'runCommand'])->middleware('artisan_view');
Route::get('artisan', [App\Http\Controllers\ArtisanCommandController::class, 'indexArtisan'])->middleware(['artisan_view', 'artisan']);
Route::get('artisan-logout', [App\Http\Controllers\ArtisanCommandController::class, 'configurationLogout'])->name('artisanLogout')->middleware('artisan_view');

