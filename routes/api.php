<?php

use App\Http\Controllers\Api\AppleStoreConnect\InAppPurchase\InAppPurchaseController;
use App\Http\Controllers\Api\AppleStoreConnect\Subscription\SubscriptionController;
use App\Http\Controllers\Api\InAppPurchase\AppPurchaseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'v1/','middleware'=>'auth:sanctum'], function () {
    /** Song and artist routes */
    Route::post('songs', [App\Http\Controllers\Api\SongController::class, 'songs']);
    Route::post('songs/recommended-songs', [App\Http\Controllers\Api\RecommendationController::class, 'recommended']);
    Route::get('songs/categories', [App\Http\Controllers\Api\CategoryController::class, 'categories']);
    Route::post('songs/search', [App\Http\Controllers\Api\SearchController::class, 'search']);
    Route::post('songs/update-playlist', [App\Http\Controllers\Api\PlaylistController::class, 'updateplaylist']);
    Route::post('songs/download', [App\Http\Controllers\Api\SongController::class, 'downloadedSongs']); // update
    Route::post('songs/set-favourite-song', [App\Http\Controllers\Api\SongController::class, 'setFavouriteSong']);
    Route::post('songs/next-play-song-list', [App\Http\Controllers\Api\SongController::class, 'nextPlaySongList']);
    Route::post('songs/sync-recently-played', [App\Http\Controllers\Api\SongController::class, 'syncRecentlyPlayed']);
    Route::post('artist/artist-follow', [App\Http\Controllers\Api\FollowerController::class, 'artistFollow']);
    Route::get('artist/get-following-artist', [App\Http\Controllers\Api\FollowerController::class, 'getFollowingArtist']);
    Route::get('artist/artist-detail', [App\Http\Controllers\Api\FollowerController::class, 'artistDetail']);

    /** User profile routes */
    Route::get('user/profile', [App\Http\Controllers\Api\AuthController::class, 'profile']);
    Route::post('user/update-profile', [App\Http\Controllers\Api\AuthController::class, 'updateProfile']);
    Route::post('user/save-interest', [App\Http\Controllers\Api\InterestController::class, 'saveInterest']);
    Route::get('user/playlist', [App\Http\Controllers\Api\PlaylistController::class, 'playlist']);
    Route::post('user/save-playlist', [App\Http\Controllers\Api\PlaylistController::class, 'saveplaylist']);
    Route::delete('user/delete-playlist/{playlistId}', [App\Http\Controllers\Api\PlaylistController::class, 'deleteplaylist']);
    Route::get('user/buying-history', [App\Http\Controllers\Api\BuyingHistoryController::class, 'buyingHistory']);


    /** Cards and package routes */
    Route::post('card/subscribe-to-plan', [App\Http\Controllers\Api\SubscribeController::class, 'subscribe']);
    Route::post('card/package-plans', [App\Http\Controllers\Api\PackageController::class, 'index']);

    /** InApp Purchase @includibg VerifyReceipt | Server-Server Notification*/
    Route::post('in-app-purchase/verifyReceipt',[AppPurchaseController::class,'InAppPurchaseVerifyReceipt']);
    Route::post('in-app-purchase/serverNotification',[AppPurchaseController::class,'InAppPurchaseServerNotification']);

    /** Apple App Store Connects API's InAppPurchae*/
    Route::get('app-store/in-app-purchase/{id?}',[InAppPurchaseController::class,'getInAppPurchase']);
    Route::post('app-store/in-app-purchase/create',[InAppPurchaseController::class,'storeInAppPurchase']);
    Route::post('app-store/in-app-purchase/update/{id}',[InAppPurchaseController::class,'updateInAppPurchase']);
    Route::delete('app-store/in-app-purchase/delete/{id}',[InAppPurchaseController::class,'deleteInAppPurchase']);

     /** Apple App Store Connects API's Auto-Renewal Subscription*/
    Route::get('app-store/subscription/{id}',[SubscriptionController::class,'getSubscription']);
    Route::post('app-store/subscription/create/{id}',[SubscriptionController::class,'storeSubscription']);
    Route::post('app-store/subscription/update/{id}',[SubscriptionController::class,'updateSubscription']);
    Route::delete('app-store/subscription/delete/{id}',[SubscriptionController::class,'deleteSubscription']);


});


Route::group(['prefix' => 'v1/user/'], function () {
    /*                   Auth routes               */
    Route::post('signup', [App\Http\Controllers\Api\AuthController::class, 'signUp']);
    Route::post('login', [App\Http\Controllers\Api\AuthController::class, 'signIn']);
    Route::post('send-otp', [App\Http\Controllers\Api\OtpController::class, 'sendOtpCode']);
    Route::post('verify-otp', [App\Http\Controllers\Api\OtpController::class, 'validateOTP']);
    Route::put('reset', [App\Http\Controllers\Api\ResetPasswordController::class, 'resetPassword']);
    Route::delete('logout', [App\Http\Controllers\Api\AuthController::class, 'logout'])->middleware('auth:sanctum');
});

Route::group(['prefix' => 'v1/card/','middleware'=>'auth:sanctum'], function () {
    Route::get('pre-order-detail', [App\Http\Controllers\Api\PreOrderController::class, 'preOrderDetail']);
});
